//
//  NotificationService.swift
//  EoutletPushService
//
//  Created by Sudhir Rohilla on 31/12/19.
//  Copyright © 2019 Unyscape. All rights reserved.
//


/*
import UserNotifications

class NotificationService: UNNotificationServiceExtension {

    var contentHandler: ((UNNotificationContent) -> Void)?
    var bestAttemptContent: UNMutableNotificationContent?

    override func didReceive(_ request: UNNotificationRequest, withContentHandler contentHandler: @escaping (UNNotificationContent) -> Void) {
        
        
        /*
        self.contentHandler = contentHandler
        bestAttemptContent = (request.content.mutableCopy() as? UNMutableNotificationContent)
        
        if let bestAttemptContent = bestAttemptContent {
            // Modify the notification content here...
            bestAttemptContent.title = "\(bestAttemptContent.title)"
            contentHandler(bestAttemptContent)
        }
        
        
        */
        
        
        
        self.contentHandler = contentHandler    // Skipping redundant initializing to itself
        bestAttemptContent = (request.content.mutableCopy() as? UNMutableNotificationContent)
        let aps : NSDictionary = request.content.userInfo["aps"] as? NSDictionary ?? NSDictionary()
        print(aps)
        //let attachmentUrlString = aps["attachment"] as? String
        let attachmentUrlString = "https://www.eoutlet.com/pub/media/catalog/category/img2.jpg"

        if !(attachmentUrlString != nil) {
            return
        }

        let url = URL(string: attachmentUrlString ?? "")
        if url == nil {
            return
        }

        if let url = url {
            (URLSession.shared.downloadTask(with: url, completionHandler: { location, response, error in
                if error == nil {
                    let tempDict = NSTemporaryDirectory()
                    var attachmentID = UUID().uuidString + (response?.url?.lastPathComponent ?? "")

                    if response?.suggestedFilename != nil {
                        attachmentID = UUID().uuidString + (response?.suggestedFilename ?? "")
                    }

                    let tempFilePath = URL(fileURLWithPath: tempDict).appendingPathComponent(attachmentID).absoluteString

                    do {
                        if try FileManager.default.moveItem(atPath: location?.path ?? "", toPath: tempFilePath) != nil {
                            let attachment = try UNNotificationAttachment(identifier: attachmentID, url: URL(fileURLWithPath: tempFilePath), options: nil)

                            if attachment == nil {
                                if let error = error {
                                    print("Create attachment error: \(error)")
                                }
                            } else {
                                self.bestAttemptContent?.attachments = (((self.bestAttemptContent?.attachments ?? nil) ?? nil)!) + [attachment]
                            }
                        } else {
                            if let error = error {
                                print("Move file error: \(error)")
                            }
                        }
                    } catch {
                    }
                } else {
                    if let error = error {
                        print("Download file error: \(error)")
                    }
                }

                OperationQueue.main.addOperation({
                    ((self.contentHandler ?? nil)!)(((self.bestAttemptContent ?? nil) ?? nil)!)
                })
            })).resume()
        }

        
    }
    
    override func serviceExtensionTimeWillExpire() {
        // Called just before the extension will be terminated by the system.
        // Use this as an opportunity to deliver your "best attempt" at modified content, otherwise the original push payload will be used.
        if let contentHandler = contentHandler, let bestAttemptContent =  bestAttemptContent {
            contentHandler(bestAttemptContent)
        }
    }

}


*/
import UserNotifications

class NotificationService: UNNotificationServiceExtension {

    var contentHandler: ((UNNotificationContent) -> Void)?
    var bestAttemptContent: UNMutableNotificationContent?

    override func didReceive(_ request: UNNotificationRequest, withContentHandler contentHandler: @escaping (UNNotificationContent) -> Void) {
        
        
        //MARK: Note: Default payload structure from
        
        /*
         
         [AnyHashable("aps"): {
             alert =     {
                 body = rthrth
                 title = rthrt
             }
             "mutable-content" = 1
         }, AnyHashable("google.c.a.ts"): 1577955798, AnyHashable("google.c.a.c_id"): 5953932423704215515, AnyHashable("gcm.message_id"): 1577955798249912, AnyHashable("fcm_options"): {
             image = "https://www.eoutlet.com/pub/media/catalog/category/img2.jpg"
         }, AnyHashable("google.c.a.e"): 1, AnyHashable("google.c.a.udt"): 0, AnyHashable("gcm.n.e"): 1]
         */
        
        let imageKey = "firebaseimageUrl"
        self.contentHandler = contentHandler
        bestAttemptContent = (request.content.mutableCopy() as? UNMutableNotificationContent)
        
        // MARK: - FireBase Rich Push
        if let bestAttemptContent = bestAttemptContent {
            
            //Need to customize payload coming from Firbase because image url key has it's own dictionary("fcm_options-->image") key...
            let notifPayload = request.content.userInfo
            let fcm_Options = notifPayload["fcm_options"] as! NSDictionary
            let imageUrl = fcm_Options["image"] as! String
            let userInfo : NSMutableDictionary = NSMutableDictionary(dictionary: notifPayload as NSDictionary)
            userInfo.setValue(imageUrl, forKey: "firebaseimageUrl")// Modify payload for imageUrl...
           
            // If there is no image_URL in the payload then
            // the code will still show the push notification.
            if userInfo[imageKey] == nil {
                contentHandler(bestAttemptContent)
                return
            }
            
            // If there is an image in the payload,
            // download and display the image.
            if let attachmentMedia = userInfo[imageKey] as? String {
                let mediaUrl = URL(string: attachmentMedia)
                let LPSession = URLSession(configuration: .default)
                LPSession.downloadTask(with: mediaUrl!, completionHandler: { temporaryLocation, response, error in
                    if let err = error {
                        print("FireBasem: Error with downloading rich push: \(String(describing: err.localizedDescription))")
                        contentHandler(bestAttemptContent)
                        return
                    }
                    
                    let fileType = self.determineType(fileType: (response?.mimeType)!)
                    let fileName = temporaryLocation?.lastPathComponent.appending(fileType)
                    
                    let temporaryDirectory = URL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent(fileName!)
                    
                    do {
                        try FileManager.default.moveItem(at: temporaryLocation!, to: temporaryDirectory)
                        let attachment = try UNNotificationAttachment(identifier: "", url: temporaryDirectory, options: nil)
                        
                        bestAttemptContent.attachments = [attachment]
                        contentHandler(bestAttemptContent)
                        // The file should be removed automatically from temp
                        // Delete it manually if it is not
                        if FileManager.default.fileExists(atPath: temporaryDirectory.path) {
                            try FileManager.default.removeItem(at: temporaryDirectory)
                        }
                    } catch {
                        print("FireBase: Error with the rich push attachment: \(error)")
                        contentHandler(bestAttemptContent)
                        return
                    }
                }).resume()
                
            }
        }
    }
    
    override func serviceExtensionTimeWillExpire() {
        // Called just before the extension will be terminated by the system.
        // Use this as an opportunity to deliver your "best attempt" at modified content, otherwise the original push payload will be used.
        if let contentHandler = contentHandler, let bestAttemptContent =  bestAttemptContent {
            contentHandler(bestAttemptContent)
        }
    }
    
    func determineType(fileType: String) -> String {
        // Determines the file type of the attachment to append to URL.
        if fileType == "image/jpeg" {
            return ".jpg"
        }
        if fileType == "image/gif" {
            return ".gif"
        }
        if fileType == "image/png" {
            return ".png"
        } else {
            return ".tmp"
        }
    }

}
