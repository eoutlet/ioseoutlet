//
//  MyOrderVC.swift
//  Eoutlet
//
//  Created by Unyscape Infocom on 28/01/20.
//  Copyright © 2020 Unyscape. All rights reserved.
//

import UIKit
import Alamofire

let iPhone_Cell_Height = 120
let iPad_Cell_Height = 128

class MyOrderTableViewCell : UITableViewCell
{
    @IBOutlet weak var imageview_Product: UIImageView!
    @IBOutlet weak var cellUIView: UIView!
    @IBOutlet weak var label_Name: UILabel!
    @IBOutlet weak var label_Amount: UILabel!
    @IBOutlet weak var label_details: UILabel!
    @IBOutlet weak var cell_bottomView: UIView!
}

//#pragma
class MyOrderVC: UIViewController, UIGestureRecognizerDelegate {
    @IBOutlet weak var myOrderTableView: UITableView!
    var arrayOfBool:[Bool] = NSMutableArray() as! [Bool]
    var array_OrderList : NSMutableArray = NSMutableArray()
    var refreshControl = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let headerNib = UINib.init(nibName: "MyOrderHeaderUIView", bundle: Bundle.main)
        myOrderTableView.register(headerNib, forHeaderFooterViewReuseIdentifier: "MyOrderHeaderUIView")
        
        refreshControl.addTarget(self, action: #selector(pullToRefreshTableView), for: UIControl.Event.valueChanged)
        myOrderTableView.addSubview(refreshControl)
        
        self.getMyOrderAPICalled()
    }
    
    @objc func pullToRefreshTableView(sender:AnyObject) {
        self.getMyOrderAPICalled()
    }
    
    override func viewWillAppear(_ _animated: Bool) {
        super.viewWillAppear(_animated)
        self.navigationController?.navigationBar.isHidden = true
    }
    
    //MARK: - UIButton Clicks
    @IBAction func backButtonClicked(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func buttonSearchClicked(_ sender: UIButton) {
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "searchVC") as! SearchVC
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    //MARK: API Calling...
    func getMyOrderAPICalled(){
        
        let parameters : Parameters = [
            "customer_id" : loggedInUser.shared.string_userID,
            "cacheignore" : "\(Int(NSDate().timeIntervalSince1970))" //To ignore cache from api...
        ]
        
        AppDelegate.showHUD(inView: self.view, message:"")
        Alamofire.request(Constants.apiURL+"webservice/getcustomerorders.php", method: .post, parameters: parameters)
            .responseJSON { response in
                AppDelegate.hideHUD(inView: self.view)
                switch response.result {
                case .success:
                    if let json = response.result.value{
                        //print("JSON Response : \(json)")
                        guard let _ : NSDictionary = json as? NSDictionary else {
                            return
                        }
                        do {
                            // make sure this JSON is in the format we expect
                            let jsonData =  try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
                            let decoded = try JSONSerialization.jsonObject(with: jsonData, options: []) as! NSDictionary
                            // here "decoded" is of type `Any`, decoded from JSON data
                            guard let dataArray : NSArray = decoded["data"] as? NSArray else {
                                return
                            }
                            
                            if dataArray.count > 0 {
                                //print(dataArray)
                                self.array_OrderList.removeAllObjects()
                                self.arrayOfBool.removeAll()
                                dataArray.forEach({ (value) in
                                    self.arrayOfBool.append(true)
                                })
                                self.array_OrderList.addObjects(from: (dataArray ) as! [Any])
                                self.myOrderTableView.reloadData {
                                    self.refreshControl.endRefreshing()
                                }
                            }
                            else{
                                //iToast.show("لايوجد نتائج للبحث")
                            }
                        }
                        catch let error as NSError {
                            print("Failed to load: \(error.localizedDescription)")
                        }
                    }
                case .failure( _):
                    //Utility.showAlert(message: error.localizedDescription, controller: self)
                    Utility.showAlert(message: "حدث خطأ - يرجي اعادة المحاولة", controller: self) //Something went wrong, please try again.
                }
        }
    }
    
    func reorderApiCalled(orderId : String){
        let parameters : Parameters = [
            "customer_id" : loggedInUser.shared.string_userID,
            "order_id" : orderId,
            "cacheignore" : "\(Int(NSDate().timeIntervalSince1970))" //To ignore cache from api...
        ]
        
        AppDelegate.showHUD(inView: self.view, message:"Loading....")
        Alamofire.request(Constants.apiURL+"webservice/createreorder.php", method: .post, parameters: parameters)
            .responseJSON { response in
                AppDelegate.hideHUD(inView: self.view)
                switch response.result {
                case .success:
                    if let json = response.result.value{
                        //print("JSON Response : \(json)")
                        guard let _ : NSDictionary = json as? NSDictionary else {
                            return
                        }
                        do {
                            // make sure this JSON is in the format we expect
                            let jsonData =  try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
                            let decoded = try JSONSerialization.jsonObject(with: jsonData, options: []) as! NSDictionary
                            if decoded["msg"] as? String == "success"{
                                iToast.show(decoded["data"] as? String ?? "")
                                self.getAllCartItemAPICalled()
                            }else {
                                iToast.show(decoded["data"] as? String ?? "")
                            }
                        }
                        catch let error as NSError {
                            print("Failed to load: \(error.localizedDescription)")
                        }
                    }
                case .failure( _):
                    //Utility.showAlert(message: error.localizedDescription, controller: self)
                    Utility.showAlert(message: "حدث خطأ - يرجي اعادة المحاولة", controller: self) //Something went wrong, please try again.
                }
        }
    }
    
    
    func cancelOrderAPICalled(orderId : String){
        
        let parameters : Parameters = [
            "order_id" : orderId,
            "cacheignore" : "\(Int(NSDate().timeIntervalSince1970))" //To ignore cache from api...
        ]
        
        AppDelegate.showHUD(inView: self.view, message:"Loading....")
        Alamofire.request(Constants.apiURL+"webservice/ordercancel.php", method: .post, parameters: parameters)
            .responseJSON { response in
                AppDelegate.hideHUD(inView: self.view)
                switch response.result {
                case .success:
                    if let json = response.result.value{
                        //print("JSON Response : \(json)")
                        guard let _ : NSDictionary = json as? NSDictionary else {
                            return
                        }
                        do {
                            // make sure this JSON is in the format we expect
                            let jsonData =  try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
                            let decoded = try JSONSerialization.jsonObject(with: jsonData, options: []) as! NSDictionary
                            if decoded["msg"] as? String == "success"{
                                self.getMyOrderAPICalled()
                            }
                        }
                        catch let error as NSError {
                            print("Failed to load: \(error.localizedDescription)")
                        }
                    }
                case .failure( _):
                    //Utility.showAlert(message: error.localizedDescription, controller: self)
                    Utility.showAlert(message: "حدث خطأ - يرجي اعادة المحاولة", controller: self) //Something went wrong, please try again.
                }
        }
    }
    
    func getAllCartItemAPICalled(){
        
        let userDefaults = UserDefaults.standard
        let decoded : Data = userDefaults.data(forKey: "isguestMaskKeyAndCardIdFetched") ?? Data()
        
        var str_cartID = ""
        do {
            if let decodedData : Dictionary<String, Any> = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? Dictionary<String, Any> {
                if let crtId =  decodedData["cart_id"]{
                    str_cartID = String(format:"\(crtId)")
                }
            }
        }
        catch {
            print("Couldn't unarchive data")
        }
        
        var str_maskKey = ""
        do {
            if let decodedData : Dictionary<String, Any> = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? Dictionary<String, Any> {
                if let mskKey =  decodedData["mask_key"]{
                    str_maskKey = String(format:"\(mskKey)")
                }
            }
        }
        catch {
            print("Couldn't unarchive data")
        }
        
        
        var str_userID = ""
        
        if loggedInUser.shared.string_userID.isEmpty || loggedInUser.shared.string_userID.count == 0 {
        }
        else {
            str_userID = String(format:"\(loggedInUser.shared.string_userID)")
            str_cartID = ""
            str_maskKey = ""
        }
        
        let parameters : Parameters = [
            "customer_id" : str_userID,
            "cart_id" : str_cartID,
            "mask_key" : str_maskKey,
            "cacheignore" : "\(Int(NSDate().timeIntervalSince1970))" //To ignore cache from api...
        ]
        
        AppDelegate.showHUD(inView: self.view, message:"Loading....")
        Alamofire.request(Constants.apiURL+"webservice/viewcart.php", method: .post, parameters: parameters)
            .responseJSON{ response in
                AppDelegate.hideHUD(inView: self.view)
                switch response.result {
                case .success:
                    if let json = response.result.value{
                        print("JSON Response : \(json)")
                        guard let _ : NSDictionary = json as? NSDictionary else {
                            return
                        }
                        do {
                            // make sure this JSON is in the format we expect
                            let jsonData =  try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
                            let decoded = try JSONSerialization.jsonObject(with: jsonData, options: []) as! NSDictionary
                            if decoded["msg"] as? String == "success"{
                                guard let dataArray : NSArray = decoded["data"] as? NSArray else {
                                    return
                                }
                                if dataArray.count > 0{
                                    if let tabItems = self.tabBarController?.tabBar.items {
                                        var total_ItemInCart = 0
                                        for indx in 0 ..< dataArray.count{
                                            let dict_Product = dataArray[indx] as! NSDictionary
                                            if let intValue = (dict_Product["qty"] as? Int) {
                                                total_ItemInCart = total_ItemInCart + intValue
                                            }
                                        }
                                        let tabItem = tabItems[0]
                                        tabItem.badgeValue = "\(total_ItemInCart)"
                                    }
                                }
                            }
                            else{
                                iToast.show("حدث خطأ - يرجي اعادة المحاولة")//An error has occurred - please try again
                            }
                        }
                        catch let error as NSError {
                            print("Failed to load: \(error.localizedDescription)")
                        }
                    }
                case .failure( _):
                    //Utility.showAlert(message: error.localizedDescription, controller: self)
                    Utility.showAlert(message: "حدث خطأ - يرجي اعادة المحاولة", controller: self) //Something went wrong, please try again.
                }
        }
    }
}

//all tableview datasource and delegates methods...
extension MyOrderVC : UITableViewDataSource, UITableViewDelegate {
    //set the number of section of tableview
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.array_OrderList.count
    }
    
    //set the number of row as per section
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if arrayOfBool[section] == true {
            return 0
        }
        else {
            let dict : NSDictionary = array_OrderList[section] as! NSDictionary
            let productArray : NSArray = dict["items"] as? NSArray ?? []
            return productArray.count
        }
    }
    
    //configure the tableview row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "myOrderTableViewCell", for: indexPath) as! MyOrderTableViewCell
        let orderDict : NSDictionary = array_OrderList[indexPath.section] as! NSDictionary
        let productArray : NSArray = orderDict["items"] as? NSArray ?? []
        let productDict : NSDictionary = productArray[indexPath.row] as! NSDictionary
        let url = NSURL(string: productDict["image_url"] as? String ?? "")
        //cell.imageview_Product.backgroundColor = UIColor.init(red: 225/255, green: 225/255, blue: 225/255, alpha: 1.0)
        cell.imageview_Product.sd_setImage(with: url as URL?) { (image, error, cache, urls) in
            if (error != nil) {// Failed to load image
            } else {// Successful in loading image
                cell.imageview_Product.sd_imageIndicator?.stopAnimatingIndicator()
            }
        }
        let product_name = productDict["name"] as? String ?? ""
        let product_amount = productDict["price"] as? Int ?? 0
        let product_size = productDict["size"] as? String ?? ""
        let product_color = productDict["color"] as? String ?? ""
        let product_quantity = productDict["ordered_qty"] as? Int ?? 0
        
        cell.label_Name.text = product_name
        cell.label_Amount.text = "قيمة الطلب: \(product_amount) SAR" //Amount
        
        //configure the detail label on tableview header like size, color, quantity...
        var detailsString = ""
        if product_size == "" && product_color == "" {
            detailsString = ""
        }
        else if product_size != "" && product_color == "" {
            detailsString =  "المقاس: \(product_size)  |" //Size
        }
        else if product_size == "" && product_color != "" {
            detailsString = "اللون: \(product_color)  |" //Color
        }
        else {
            detailsString = "المقاس: \(product_size)  |  اللون: \(product_color)  |"
        }
        cell.label_details.text = "\(detailsString)  الكمية: \(product_quantity)"  // Size | Color | Qty
        
        if indexPath.row == productArray.count - 1 {
            //cell.cell_bottomView.isHidden = true
            cell.separatorInset = .zero
        }
        else {
            // cell.cell_bottomView.isHidden = false
        }
        
        return cell
    }
    
    //set tableview row height as per device like iphone or ipad
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.phone {
            return CGFloat(iPhone_Cell_Height)
        }
        else {
            return CGFloat(iPad_Cell_Height)
        }
    }
    
    //did select action when user click on tableview cell
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    
    //table header view
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "MyOrderHeaderUIView") as! MyOrderHeaderUIView
        headerView.clipsToBounds = true
        
        let orderDict : NSDictionary = array_OrderList[section] as! NSDictionary
        headerView.orderNumberLbl.text = "طلب رقم: \(orderDict["increment_order_id"] as? String ?? "")" //Order NO
        headerView.totalAmountLbl.text = "SAR \(orderDict["total_amount"] as? Int ?? 0)" //Total Amount
        headerView.statusLbl.text = "\(orderDict["display_status"] as? String ?? "")"
        headerView.dateLbl.text = "\(orderDict["created_at"] as? String ?? "")"
        
        headerView.reorderBtn.addTarget(self, action: #selector(reorderBtnTap), for: .touchUpInside)
        headerView.reorderBtn.accessibilityLabel = "\(section)"
                //
        //hide cancel button if staus is cancelled already
         
        if orderDict["status"] as? String == "complete" {
        headerView.cancelBtn.isHidden = true
            headerView.statusLbl.textColor = UIColor.init(red: 52/255, green: 196/255, blue: 124/255, alpha: 1.0)  //green color
        }
        else if orderDict["status"] as? String == "processing" || orderDict["status"] as? String == "pending" || orderDict["status"] as? String == "payfort_fort_new"   {
            headerView.cancelBtn.isHidden = false
            headerView.cancelBtn.addTarget(self, action: #selector(cancelBtnTap), for: .touchUpInside)
            headerView.cancelBtn.accessibilityLabel = "\(section)"
            headerView.statusLbl.textColor = Constants.default_ThemeColor
        }
        else { // cancel
            headerView.cancelBtn.isHidden = true
            headerView.statusLbl.textColor = Constants.default_TextColor
        }
        
        headerView.accessibilityLabel = "\(section)"
        let tap = UITapGestureRecognizer(target: self, action:#selector(self.handleTap(_:)))
        tap.delegate = self
        headerView.addGestureRecognizer(tap)
        
        return headerView
    }
    
    //set tableview header height as per device like iphone or ipad
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 125
    }
    
    //table view header view action
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        guard let accessibilityLabel = sender.view?.accessibilityLabel else { return }
        if arrayOfBool[Int(accessibilityLabel)!] == true {
            for i in 0..<arrayOfBool.count {  // set the bool value false (only one section will expand at a time)
                if Int(accessibilityLabel)! == i {
                    arrayOfBool[Int(accessibilityLabel)!] = false    
                }
                else {
                    arrayOfBool[i] = true
                }
            }
        }
        else{
            arrayOfBool[Int(accessibilityLabel)!] = true // this condition called when user click on expanded tableview section preview button
        }
        
        // reload tableview
        myOrderTableView.reloadData {
            //reload completed
            DispatchQueue.main.async {
                if self.arrayOfBool[Int(accessibilityLabel)!] == false {
                    //scroll to top the sected section after reloading the tableview
                    self.myOrderTableView.scrollToRow(at: IndexPath(row: 0, section: Int(accessibilityLabel)!), at: .top, animated: true)
                }
            }
        }
    }
    
    //headerview reorder button click action
    @objc func reorderBtnTap(sender: UIButton) {
        let accessibilityLabel : String = sender.accessibilityLabel!
        let orderDict : [String : Any] = self.array_OrderList[Int(accessibilityLabel)!] as! Dictionary
        let orderIdStr : String = (orderDict["order_id"] as! String) as String
        self.reorderApiCalled(orderId:orderIdStr)
    }
    
    //header view cance button click action
    @objc func cancelBtnTap(sender: UIButton) {
        let alertController = UIAlertController(title: Constants.msg_Alert, message: "هل أنت متأكد من الغاء الطلب", preferredStyle: .alert)//Are you sure to cancel this order
        
        alertController.addAction(UIAlertAction(title: "لا", style: .default, handler: { action in
            switch action.style{
            case .default:
                print("default")
            case .cancel:
                print("cancel")
            case .destructive:
                print("destructive")
            @unknown default: break
            }}))
        
        alertController.addAction(UIAlertAction(title: "نعم", style: .default, handler: { action in
            switch action.style{
            case .default:
                print("default")
                let accessibilityLabel : String = sender.accessibilityLabel!
                let orderDict : [String : Any] = self.array_OrderList[Int(accessibilityLabel)!] as! Dictionary
                let orderIdStr : String = (orderDict["order_id"] as! String) as String
                //call the cancel api if user want to cancel the order
                self.cancelOrderAPICalled(orderId: orderIdStr)
            case .cancel:
                print("cancel")
            case .destructive:
                print("destructive")
            @unknown default: break
            }}))
        self.present(alertController, animated: true, completion: nil)
    }
}   
