
//
//  WalletPaymentOptionsVC.swift
//  Eoutlet
//
//  Created by admin on 18/05/20.
//  Copyright © 2020 Unyscape. All rights reserved.
//

import UIKit
import Alamofire
import WebKit
import MonthYearPicker
import Frames

class WalletRememberMeCardTableViewCell: UITableViewCell{
    
    @IBOutlet weak var base_View : UIView!
    @IBOutlet weak var circle_Image_RememberMeCard : UIImageView!       //Circle Image icons
    @IBOutlet weak var card_NameLbl: UILabel!
    @IBOutlet weak var card_NumberLbl: UILabel!
    @IBOutlet weak var card_ImageView: UIImageView!
    @IBOutlet weak var card_CVVTextField: UITextField!
    
}

class WalletPaymentOptionsVC: UIViewController, UITextFieldDelegate, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var label_totalAmount : UILabel!
    
    @IBOutlet weak var view_Base : UIView!
    @IBOutlet weak var view_creditDebitCard : UIView!
    @IBOutlet weak var view_creditDebitCardForm : UIView!
    @IBOutlet weak var view_cardFormHeightConstraints : NSLayoutConstraint!
    @IBOutlet weak var card_NameTextField: UITextField!
    @IBOutlet weak var card_NumberTextField: UITextField!
    @IBOutlet weak var card_SecurityCodeTextField: UITextField!
    @IBOutlet weak var card_ExpiryTextField: UITextField!
    @IBOutlet weak var image_autoSavedCheckBox: UIImageView!
    @IBOutlet weak var rememberMe_Card_TableView : UITableView!
    @IBOutlet weak var rememberMe_Card_TableViewHeightConstraints: NSLayoutConstraint!
    
    //Circle Image icons
    @IBOutlet weak var image_creditDebitCard : UIImageView!
    @IBOutlet weak var view_creditCardAndCODButton: UIView!
    var expirationDatePicker = MonthYearPickerView()
    var maxlength_CardNumberfield : Int = 16
    var maxlength_CartCVVNumberfield : Int = 4
    
    var paymentMethod = "" //i.e. credit/debit card.
    var amount_Final = 0
    var str_rememberMe = "NO"
    var array_rememberMeCards = NSMutableArray()
    var selectedRememberMeCardIndex : Int = -1
    
    var checkoutAPIClient: CheckoutAPIClient {
           return CheckoutAPIClient(publicKey: Constants.KCheckOutApiKey, environment: Constants.KCheckOutEnviroment)
       }
       
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        label_totalAmount.text = "الاجمالي \(amount_Final) SAR"
        
        
        paymentMethod = "payfort_fort_cc"
        image_creditDebitCard.image = UIImage.init(named: "yellow-tick")
        view_cardFormHeightConstraints.constant = 280
        
        self.rememberMe_Card_TableViewHeightConstraints.constant = CGFloat(80 * array_rememberMeCards.count)
        self.rememberMe_Card_TableView.tableFooterView = UIView()
       
        //Fetch Already Saved Cards For Transactions...
        self.getRememberMeCardsAPICalled()
        
    }
    
    override func viewWillAppear(_ _animated: Bool) {
        super.viewWillAppear(_animated)
        self.navigationController?.navigationBar.isHidden = true
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        self.view_Base.setShadow(shadowSize: 10.0, radius: 10.0)
        self.label_totalAmount.setShadow(shadowSize: 10.0, radius: 10.0)
        self.view_creditDebitCard.setShadow(shadowSize: 10.0, radius: 10.0)
    }
    
    //MARK: API Calling...
    func getRememberMeCardsAPICalled() {
        
        var str_userID = ""
        if !loggedInUser.shared.string_userID.isEmpty && loggedInUser.shared.string_userID.count != 0{
            str_userID = String(format:"\(loggedInUser.shared.string_userID)")
        }
        
        let parameters : Parameters = [
            
            "customer_id" : str_userID,
            "cacheignore" : "\(Int(NSDate().timeIntervalSince1970))" //To ignore cache from api...
        ]
        
        AppDelegate.showHUD(inView: self.view, message:"Loading....")
        Alamofire.request(Constants.apiURL+"webservice/getremembermecards.php", method: .post, parameters: parameters)
            .responseJSON { response in
                AppDelegate.hideHUD(inView: self.view)
                switch response.result {
                case .success:
                    if let json = response.result.value{
                        print("JSON Response : \(json)")
                        guard let _ : NSDictionary = json as? NSDictionary else {
                            return
                        }
                        do {
                            // make sure this JSON is in the format we expect
                            let jsonData =  try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
                            let decoded = try JSONSerialization.jsonObject(with: jsonData, options: []) as! NSDictionary
                            if decoded["status"] as? String == "success"{
                                let arrayofGetData = decoded["data"] as? NSArray ?? NSArray()
                                if arrayofGetData.count > 0{
                                    self.array_rememberMeCards.addObjects(from: arrayofGetData as! [Any])
                                    self.rememberMe_Card_TableViewHeightConstraints.constant = CGFloat(80*self.array_rememberMeCards.count)
                                    self.rememberMe_Card_TableView.reloadData()
                                }
                            }
                        }
                        catch let error as NSError {
                            print("Failed to load: \(error.localizedDescription)")
                        }
                    }
                case .failure( _):
                    Utility.showAlert(message: "حدث خطأ - يرجي اعادة المحاولة", controller: self) //Something went wrong, please try again.
                }
        }
        
    }
    
    func addMoneyApiCalled(paymentMethod: String, cardDetail: NSMutableDictionary, token_name: NSString) {
        
        var str_userID = ""
        if !loggedInUser.shared.string_userID.isEmpty && loggedInUser.shared.string_userID.count != 0{
            str_userID = String(format:"\(loggedInUser.shared.string_userID)")
        }
        
        let parameters : Parameters = [
            
            "customer_id" : str_userID,
            "amount" : "\(amount_Final)",
            "card_detail" : cardDetail,
            "remember_me" : str_rememberMe,
            "token_name" : "\(token_name)",
            "paymentmethod" : paymentMethod,
            "cacheignore" : "\(Int(NSDate().timeIntervalSince1970))"//To ignore cache from api...
        ]
        
        AppDelegate.showHUD(inView: self.view, message:"Loading....")
        Alamofire.request(Constants.apiURL+"webservice/addmoneywithcheckout.php", method: .post, parameters: parameters)
            .responseJSON { response in
                AppDelegate.hideHUD(inView: self.view)
                switch response.result {
                case .success:
                    if let json = response.result.value{
                        guard let _ : NSDictionary = json as? NSDictionary else {
                            return
                        }
                        do {
                            // make sure this JSON is in the format we expect
                            let jsonData =  try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
                            let decoded = try JSONSerialization.jsonObject(with: jsonData, options: []) as! NSDictionary
                            // here "decoded" is of type `Any`, decoded from JSON data
                            
                            if let _ = decoded["redirect_link"], let _ =  decoded["order_id"] {
                                let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "wallet3DSecureURLWebViewVC") as! Wallet3DSecureURLWebViewVC
                                vc.str_threeDSecureURL = decoded["redirect_link"] as? String ?? ""
                                vc.str_orderID = decoded["order_id"] as? String ?? ""
                                vc.amount_Final = self.amount_Final
                                if self.str_rememberMe == "YES" {
                                    vc.tokenNameStr = token_name as String
                                }
                                self.navigationController?.pushViewController(vc, animated: false)
                            }
                            else if let _ = decoded["response_message"]{
                                iToast.show(decoded["response_message"] as? String)
                            }
                            else{
                                iToast.show("حدث خطأ - يرجي اعادة المحاولة") //Something went wrong, please try again.
                            }
                        }
                        catch let error as NSError {
                            print("Failed to load: \(error.localizedDescription)")
                        }
                    }
                case .failure( _):
                    Utility.showAlert(message: "حدث خطأ - يرجي اعادة المحاولة", controller: self) //Something went wrong, please try again.
                }
        }
    }
    
    //MARK: - UITableView DataSource & Delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return array_rememberMeCards.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "walletRememberMeCardTableViewCell", for: indexPath) as! WalletRememberMeCardTableViewCell
        if selectedRememberMeCardIndex == indexPath.row { cell.circle_Image_RememberMeCard.image = UIImage.init(named: "yellow-tick")
        }else{ cell.circle_Image_RememberMeCard.image = UIImage.init(named: "gray-tick")}
        cell.layoutIfNeeded()
        cell.base_View.setShadow(shadowSize: 10.0, radius: 10.0)
        let dict_Card : NSDictionary = array_rememberMeCards[indexPath.row] as! NSDictionary
        cell.card_NameLbl.text = "\(dict_Card["name"] ?? "")"
        cell.card_NumberLbl.text = "\(dict_Card["card_number"] ?? "")"
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return indexPath.row == selectedRememberMeCardIndex ? 135 : 80
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        paymentMethod = "remember_me_cards"
        image_creditDebitCard.image = UIImage.init(named: "gray-tick")
        str_rememberMe = "NO"
        DispatchQueue.main.async {
            self.view_cardFormHeightConstraints.constant = 0
            self.view_creditDebitCardForm.layoutIfNeeded()
        }
        selectedRememberMeCardIndex = indexPath.row
        self.rememberMe_Card_TableViewHeightConstraints.constant = CGFloat(80*array_rememberMeCards.count + 55)
        self.rememberMe_Card_TableView.reloadData()
        
    }
    
    //MARK: - UIButton Clicks
    @IBAction func backButtonClicked(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func button_creditCardMadaCardClicked(_ sender: UIButton) {
        
        if paymentMethod ==  "payfort_fort_cc"{return}
        paymentMethod = "payfort_fort_cc"
        image_creditDebitCard.image = UIImage.init(named: "yellow-tick")
        DispatchQueue.main.async {
            self.view_cardFormHeightConstraints.constant = 280
            self.view_creditDebitCardForm.layoutIfNeeded()
        }
        self.view_creditDebitCardForm.layoutIfNeeded()
        self.selectedRememberMeCardIndex = -1
        self.rememberMe_Card_TableViewHeightConstraints.constant = CGFloat(80*array_rememberMeCards.count)
        self.rememberMe_Card_TableView.reloadData()
        
    }
    
    @IBAction func button_rememberMeCheckBoxClicked(_ sender: UIButton) {
        
        if image_autoSavedCheckBox.image == UIImage.init(named: "checkBoxSelected") {
            
            image_autoSavedCheckBox.image = UIImage.init(named: "checkBoxUnselected")
            str_rememberMe = "NO"
            
        }else {
            
            image_autoSavedCheckBox.image = UIImage.init(named: "checkBoxSelected")
            str_rememberMe = "YES"
        }
    }
    
    @IBAction func button_SaveAndContinueClicked(_ sender: UIButton) {
        
        if (paymentMethod == "remember_me_cards"){
            
            let indxPath : IndexPath = IndexPath.init(row: selectedRememberMeCardIndex, section: 0)
            let cell: WalletRememberMeCardTableViewCell = self.rememberMe_Card_TableView.cellForRow(at: indxPath) as! WalletRememberMeCardTableViewCell
            let trimmedSecurityCode = cell.card_CVVTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            if trimmedSecurityCode == ""{
                Utility.showAlert(message: "برجاء فحص بيانات البطاقة", controller: self)//Please check card detail.
                return
            }
            
            let dict_Card : NSDictionary = array_rememberMeCards[selectedRememberMeCardIndex] as! NSDictionary
            //Card Detail
            let dict_CardDetail : NSMutableDictionary = NSMutableDictionary()
            dict_CardDetail.setValue("", forKey: "name")//card_Name
            dict_CardDetail.setValue("", forKey: "number")//card_Number
            dict_CardDetail.setValue("\(trimmedSecurityCode)", forKey: "securitycode")//card_SecurityCode
            dict_CardDetail.setValue("", forKey: "expiry")//card_Expiry(i.e. 2207 format)
          
//            self.addMoneyApiCalled(cardDetail: dict_CardDetail, token_name: "\(dict_Card["token_name"] ?? "")" as NSString)

            self.addMoneyApiCalled(paymentMethod: "checkoutcom_card_payment", cardDetail: dict_CardDetail, token_name: "\(dict_Card["token_name"] ?? "")" as NSString)
        }else{
            
            let trimmedCardName = card_NameTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            let trimmedCardNumber = card_NumberTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            let trimmedSecurityCode = card_SecurityCodeTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            let trimmedExpiry = card_ExpiryTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            
            if trimmedCardName == "" || trimmedCardNumber == "" || trimmedSecurityCode == "" || trimmedExpiry == ""{
                Utility.showAlert(message: "برجاء فحص بيانات البطاقة", controller: self)//Please check card detail.
                return
            }
            
            //Card Detail
            let dict_CardDetail : NSMutableDictionary = NSMutableDictionary()
            dict_CardDetail.setValue(trimmedCardName, forKey: "name")//card_Name
            dict_CardDetail.setValue(trimmedCardNumber, forKey: "number")//card_Number
            dict_CardDetail.setValue(trimmedSecurityCode, forKey: "securitycode")//card_SecurityCode
            dict_CardDetail.setValue(trimmedExpiry, forKey: "expiry")//card_Expiry(i.e. 2207 format)
//            let ary_Date =  trimmedExpiry.components(separatedBy: " / ")
//            var str_finalExpiry = String()
//            str_finalExpiry.append(ary_Date[1])
//            str_finalExpiry.append(ary_Date[0])
//            dict_CardDetail.setValue("\(str_finalExpiry)", forKey: "expiry")//card_Expiry(i.e. 2207 format)
            
//            self.createOrderAPICalled_Payfort(paymentMethod:"payfort_fort_cc", isPartial: is_Partial, cardDetail: dict_CardDetail, token_name: "")
            
            let card = getCardTokenRequest(cardName : trimmedCardName, cardNumber : trimmedCardNumber, expiry : trimmedExpiry, cvv: trimmedSecurityCode)
            print(card)
            AppDelegate.showHUD(inView: self.view, message:"Loading....")
            checkoutAPIClient.createCardToken(card: card, successHandler: { cardToken in
                AppDelegate.hideHUD(inView: self.view)
                self.addMoneyApiCalled(paymentMethod:"checkoutcom_card_payment", cardDetail: dict_CardDetail, token_name: cardToken.token as NSString)
                
                // self.createOrderAPICalled_CheckOut(paymentMethod:"checkoutcom_card_payment", isPartial: is_Partial, cardDetail: dict_CardDetail, token_name: cardToken.token as NSString)
            }, errorHandler: { error in
                iToast.show(error.errorType)
                AppDelegate.hideHUD(inView: self.view)
            })
            return
        }
    }
    
     private func getCardTokenRequest(cardName : String, cardNumber : String, expiry : String, cvv: String) -> CkoCardTokenRequest {
            let cardUtils = CardUtils()
            let cardNumber = cardUtils.standardize(cardNumber: cardNumber)
            let cvv = cvv
            let (expiryMonth, expiryYear) = cardUtils.standardize(expirationDate: expiry)
    //        // create the phone number
    //        let phoneNumber = CkoPhoneNumber(countryCode: "44", number: "7777777777")
    //        // create the address
    //        let address = CkoAddress(addressLine1: "test1", addressLine2: "test2", city: "London", state: "London", zip: "N12345", country: "GB")
            return CkoCardTokenRequest(number: cardNumber, expiryMonth: expiryMonth, expiryYear: expiryYear, cvv: cvv, name: cardName, billingAddress: CkoAddress(), phone: CkoPhoneNumber())
        }
    
    func showOpenSettingAppAlert() {
        let alertController = UIAlertController(title: Constants.msg_Alert, message: "برجاء إضافة بطاقتك الائتمانية الى محفظة الايفون", preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "حسنا", style: .default, handler: { action in
            switch action.style{
            case .default:
                print("default")
                //let url = URL(string: "App-Prefs:root=PASSBOOK")
                //let url = URL(string: "prefs:root=MUSIC")
                //let url = URL(string: UIApplication.openSettingsURLString)
                let url = URL(string: "shoebox://url-scheme")
                if UIApplication.shared.canOpenURL(url!) {
                    UIApplication.shared.open(url!, options: [:], completionHandler: nil)
                }
            case .cancel:
                print("cancel")
            case .destructive:
                print("destructive")
            @unknown default: break
            }}))
        self.present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func expirationDateButtonClicked(_ sender: UIButton) {
        
        card_ExpiryTextField.resignFirstResponder()
        expirationDatePicker = MonthYearPickerView(frame: CGRect(origin: CGPoint(x: 0, y: (view.bounds.height - 216) / 2), size: CGSize(width: view.bounds.width, height: 216)))
        expirationDatePicker.minimumDate = Date()
        expirationDatePicker.maximumDate = Calendar.current.date(byAdding: .year, value: 10, to: Date())
        expirationDatePicker.addTarget(self, action: #selector(datePickerValueChanged(_:)), for: .valueChanged)
        view.addSubview(expirationDatePicker)
        card_ExpiryTextField?.inputView = expirationDatePicker
        card_ExpiryTextField.tintColor = UIColor.clear
        card_ExpiryTextField?.becomeFirstResponder()
    }
    
    //MARK: - UIPickerViewDelegate Delegates
    @objc func datePickerValueChanged(_ picker: MonthYearPickerView) {
     
        print("date changed: \(picker.date)")
        let dateFormatter: DateFormatter = DateFormatter()//Create date formatter
        dateFormatter.dateFormat = "MM / YY"// Set date format
        
        // Apply date format
        let selectedDate = dateFormatter.string(from: picker.date)
        print("Selected value \(selectedDate)")
        card_ExpiryTextField?.text = selectedDate
        
    }
    //MARK: - UITextField Delegates
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == card_NumberTextField {
            if CharacterSet(charactersIn: "0123456789").isSuperset(of: CharacterSet(charactersIn: string)) {
                let currentString: NSString = textField.text! as NSString
                let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
                return newString.length <= maxlength_CardNumberfield
            }else{return false}
        }
        else if textField == card_SecurityCodeTextField {
            if CharacterSet(charactersIn: "0123456789").isSuperset(of: CharacterSet(charactersIn: string)) {
                let currentString: NSString = textField.text! as NSString
                let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
                return newString.length <= maxlength_CartCVVNumberfield
            }else{return false}
        }else if textField.tag == 99{//TextField Within RememberMe TableView's cell
            
            if CharacterSet(charactersIn: "0123456789").isSuperset(of: CharacterSet(charactersIn: string)) {
                let currentString: NSString = textField.text! as NSString
                let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
                return newString.length <= maxlength_CartCVVNumberfield
            }else{return false}
        }
        return true
    }
    
  
    
}


////for appsflyer analytics
//extension WalletPaymentOptionsVC {
//    func initiateCheckOutAppsFlyer(coupanCode:String) {
//        let productInfo : NSMutableDictionary =  NSMutableDictionary()
//        let total_Price_Str = self.final_amount
//        productInfo.setValue("\(total_Price_Str)", forKey: "price")
//        productInfo.setValue("SAR", forKey: "currency")
//        productInfo.setValue(loggedInUser.shared.string_userID, forKey: "user_id")
//        productInfo.setValue(coupanCode, forKey: "coupon")
//        analyticsAppsFlyer.shared.AnalyticsEventInitiateCheckout(paramas: productInfo)
//    }
//
//    func addPaymentInfoAppsFlyper() {
//        let productInfo : NSMutableDictionary =  NSMutableDictionary()
//        let total_Price_Str = self.final_amount
//        productInfo.setValue("\(total_Price_Str)", forKey: "price")
//        productInfo.setValue("SAR", forKey: "currency")
//        productInfo.setValue(loggedInUser.shared.string_userID, forKey: "user_id")
//        analyticsAppsFlyer.shared.AnalyticsEventAddPaymentInfo(paramas: productInfo)
//    }
//}

////For Firebase Analytics
//extension WalletPaymentOptionsVC {
//    func beginCheckoutFirebaseAnylatics(){
//        let productInfo : NSMutableDictionary =  NSMutableDictionary()
//        let total_Price_Str = self.final_amount
//        productInfo.setValue("SAR", forKey: "currency")
//        productInfo.setValue("\(total_Price_Str)", forKey: "value")
//        productInfo.setValue(self.str_Coupon_Code, forKey: "coupon")
//        analyticsFirebase.shared.AnalyticsEventBeginCheckout(paramas: productInfo as NSDictionary)
//    }
//
//    func addPaymentInfoFirebaseAnylatics() {
//        analyticsFirebase.shared.AnalyticsEventAddPaymentInfo(paramas: [:])
//    }
//}

