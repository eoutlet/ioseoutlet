
import UIKit
import Alamofire
import WebKit

class WebViewController: UIViewController {
    @IBOutlet weak var webView: WKWebView!
    var titleStr : String!
    var selectedIndex : Int = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        
        webView.navigationDelegate = self
        webView.scrollView.bounces = false
        self.loadUrlOnWebView()
    }
    
     override func viewWillAppear(_ _animated: Bool) {
             super.viewWillAppear(_animated)
             self.navigationController?.navigationBar.isHidden = true
         }        
        
         //MARK: - UIButton Clicks
         @IBAction func backButtonClicked(_ sender: UIButton) {
             self.navigationController?.popViewController(animated: false)
         }
      
    func loadUrlOnWebView() {
        self.getHelpAndSupportAPICalled()
    }
    
    //MARK: API Calling...
    func getHelpAndSupportAPICalled(){
     
         let parameters : Parameters = [
                        "cacheignore" : "\(Int(NSDate().timeIntervalSince1970))" //To ignore cache from api...
                    ]
        
        AppDelegate.showHUD(inView: self.view, message:"Loading....")
        Alamofire.request(Constants.apiURL + "webservice/help.php", method: .get, parameters: parameters)
            .responseJSON { response in
                AppDelegate.hideHUD(inView: self.view)
                switch response.result {
                case .success:
                    if let json = response.result.value{
                        // print("JSON Response : \(json)")
                        guard let _ : NSDictionary = json as? NSDictionary else {
                            return
                        }
                        do {
                            // make sure this JSON is in the format we expect
                            let jsonData =  try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
                            let decoded = try JSONSerialization.jsonObject(with: jsonData, options: []) as! NSDictionary
                            if decoded["msg"] as? String == "success" {
                                guard let arrayList : NSArray = decoded["data"] as? NSArray else {
                                    return
                                }

                                if self.selectedIndex == 0 {//Shipping and delivery
                                    let dict : NSDictionary = arrayList[2] as! NSDictionary
                                    self.webView.loadHTMLString(dict["content"] as! String, baseURL: nil)
                                }
                                else if self.selectedIndex == 1 {//common questions
                                    let dict : NSDictionary = arrayList[0] as! NSDictionary
                                    self.webView.loadHTMLString(dict["content"] as! String, baseURL: nil)
                                }
                                else if self.selectedIndex == 2 {//Replace and Return policy
                                    let dict : NSDictionary = arrayList[1] as! NSDictionary
                                    self.webView.loadHTMLString(dict["content"] as! String, baseURL: nil)
                                }
                            }
                            else {
                                print("Failed to load")
                            }
                        }
                        catch let error as NSError {
                            print("Failed to load: \(error.localizedDescription)")
                        }
                    }
                case .failure( _):
                    //Utility.showAlert(message: error.localizedDescription, controller: self)
                    Utility.showAlert(message: "حدث خطأ - يرجي اعادة المحاولة", controller: self) //Something went wrong, please try again.
                }
        }
    }
}

extension WebViewController : WKNavigationDelegate {
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        DispatchQueue.main.async {
            AppDelegate.hideHUD(inView: self.view)
        }
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        DispatchQueue.main.async {
            AppDelegate.hideHUD(inView: self.view)
        }
        let myalert = UIAlertController(title: "", message: "حدث خطأ - يرجي اعادة المحاولة", preferredStyle: UIAlertController.Style.alert)
        myalert.addAction(UIAlertAction(title: "Ok", style: .default) { (action:UIAlertAction!) in
            self.loadUrlOnWebView()
        })
        self.present(myalert, animated: true)
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        if navigationAction.navigationType == WKNavigationType.linkActivated {
            if let url = navigationAction.request.url {
                decisionHandler(.cancel)
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        }
        else {
            decisionHandler(.allow)
        }
    }
}
