import Alamofire

protocol addAddressDelegate {
    
    func addressAddeddSuccessfully()
}

class AddressBookVC: UIViewController,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate {
    
    var array_CountryList : NSMutableArray = NSMutableArray()
    var buttonasfullScreenForCountrySelection : UIButton = UIButton()
    var tableViewForCountrySelection  : UITableView = UITableView()
    let TAGPPICKERFORSELECTION = 4
    var maxlength_MobileNumberfield : Int = 0
    var buttonasfullScreenForOTP = UIButton()
    var textFieldEnterOTP = UITextField()
    var label_ForTimerCountDown = UILabel()
    var  buttonVerifyOTP = UIButton()
    var  buttonResendOTP = UIButton()
    var otp_Timer: Timer?
    var value_Timer : Int = 30
    
    @IBOutlet weak var textField_firstName: UITextField!
    @IBOutlet weak var textField_lastName: UITextField!
    @IBOutlet weak var textField_Street: UITextField!
    @IBOutlet weak var textField_City: UITextField!
    @IBOutlet weak var textField_Country: UITextField!
    @IBOutlet weak var textField_mobileNumber: UITextField!
    var delegate : addAddressDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.getProfileAPICalled()
        self.getAllCountriesAPICalled()
    }
    
    override func viewWillAppear(_ _animated: Bool) {
        super.viewWillAppear(_animated)
        self.navigationController?.navigationBar.isHidden = true
    }
    
    //MARK: API Calling
    func getProfileAPICalled(){
        let parameters : Parameters = [
            "cacheignore" : "\(Int(NSDate().timeIntervalSince1970))" //To ignore cache from api...
        ]
        AppDelegate.showHUD(inView: self.view, message:"Loading....")
        Alamofire.request(Constants.apiURL + "webservice/getprofile.php?email=\(loggedInUser.shared.string_Email)", method: .get, parameters: parameters)
            .responseJSON { response in
                AppDelegate.hideHUD(inView: self.view)
                switch response.result {
                case .success:
                    if let json = response.result.value{
                        //print("JSON Response : \(json)")
                        guard let _ : NSDictionary = json as? NSDictionary else {
                            return
                        }
                        do {
                            // make sure this JSON is in the format we expect
                            let jsonData =  try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
                            let decoded = try JSONSerialization.jsonObject(with: jsonData, options: []) as! NSDictionary
                            if decoded["msg"] as? String == "success"{
                                guard let arrayUser : NSArray = decoded["data"] as? NSArray else {
                                    return
                                }
                                if arrayUser.count > 0{
                                    let dict_UserRecord : NSDictionary = arrayUser[0] as! NSDictionary
                                    do {
                                        let userDefaults = UserDefaults.standard
                                        let encodedData: Data = try NSKeyedArchiver.archivedData(withRootObject: dict_UserRecord, requiringSecureCoding: false)
                                        userDefaults.removeObject(forKey: "loggedInUserRecord")
                                        userDefaults.set(encodedData, forKey: "loggedInUserRecord")
                                        userDefaults.synchronize()
                                    }
                                    catch {
                                        print("Couldn't archive data")
                                    }
                                    
                                    loggedInUser.shared.initializeUserDetails(dicuserdetail: dict_UserRecord as! Dictionary<String, Any>)
                                    
                                    self.textField_firstName.text = "\(loggedInUser.shared.string_firstName)"
                                    self.textField_lastName.text = "\(loggedInUser.shared.string_lastName)"
                                    self.textField_mobileNumber.text = "\(loggedInUser.shared.string_mobileNumber)"
                                }
                                else{
                                    Utility.showAlert(message: decoded["msg"] as? String ??  "حدث خطأ - يرجي اعادة المحاولة", controller: self)//Something went wrong, please try again.
                                }
                                
                            }else{
                                Utility.showAlert(message: decoded["message"] as? String ??  "حدث خطأ - يرجي اعادة المحاولة", controller: self)//Something went wrong, please try again.
                                
                            }
                        }
                        catch let error as NSError {
                            print("Failed to load: \(error.localizedDescription)")
                        }
                    }
                case .failure( _):
                    //Utility.showAlert(message: error.localizedDescription, controller: self)
                    Utility.showAlert(message: "حدث خطأ - يرجي اعادة المحاولة", controller: self) //Something went wrong, please try again.
                }
        }
    }
    
    func getAllCountriesAPICalled(){
        let parameters : Parameters = [
            "cacheignore" : "\(Int(NSDate().timeIntervalSince1970))" //To ignore cache from api...
        ]
        AppDelegate.showHUD(inView: self.view, message:"Loading....")
        Alamofire.request(Constants.apiURL + "webservice/countrylist.php", method: .get, parameters: parameters)
            .responseJSON { response in
                AppDelegate.hideHUD(inView: self.view)
                switch response.result {
                case .success:
                    if let json = response.result.value{
                        guard let _ : NSDictionary = json as? NSDictionary else {
                            return
                        }
                        do {
                            // make sure this JSON is in the format we expect
                            let jsonData =  try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
                            let decoded = try JSONSerialization.jsonObject(with: jsonData, options: []) as! NSDictionary
                            // here "decoded" is of type `Any`, decoded from JSON data
                            guard let dataArray : NSArray = decoded["data"] as? NSArray else {
                                return
                            }
                            if dataArray.count > 0 {
                                self.array_CountryList.addObjects(from: (dataArray.reversed()) )
                                self.setCountryNameField(self.array_CountryList)
                            }
                        } catch let error as NSError {
                            print("Failed to load: \(error.localizedDescription)")
                        }
                    }
                case .failure( _):
                    //Utility.showAlert(message: error.localizedDescription, controller: self)
                    Utility.showAlert(message: "حدث خطأ - يرجي اعادة المحاولة", controller: self) //Something went wrong, please try again.
                }
        }
    }
    
    func setCountryNameField(_ arrayCountries: NSArray) {
        
        var  dictCountry : NSMutableDictionary = NSMutableDictionary()
        let  string_country = "\(loggedInUser.shared.string_country)"
        
        if arrayCountries.count > 0 && string_country == "" {
            let dictEventDetail : NSDictionary = arrayCountries[0] as! NSDictionary
            self.setCountryCode(dictEventDetail)
        }else {
            for indx in 0 ..< arrayCountries.count{//Check here logged in user's mobile number's cel code i.e (+91,+966,+98,etc...) where exists in country list...
                let country_Obj = arrayCountries[indx] as? NSDictionary ?? NSDictionary()
                let str_countryCelCode : String = country_Obj["cel_code"] as? String ?? ""
                if string_country == str_countryCelCode {
                    dictCountry = country_Obj.mutableCopy() as! NSMutableDictionary
                    break
                }
            }
            
            //Country Name + Country Code...(i.e. India ( +91 )
            let str_countryName : String = dictCountry["name"] as? String ?? ""
            var str_countrycelCode : String = dictCountry["cel_code"] as? String ?? ""
            str_countrycelCode =  str_countrycelCode.replacingOccurrences(of: "+", with: "")
            textField_Country.text = "(" + str_countrycelCode + "+) " + str_countryName
            textField_Country.accessibilityLabel = dictCountry["code"] as? String //Country Code i.e(SA,IN...)
            textField_Country.accessibilityHint = dictCountry["cel_code"] as? String //Cell code i.e.(+91,+966,+98,etc...)
            var str_placeHolder = dictCountry["placeholder"] as? String
            str_placeHolder =  str_placeHolder?.replacingOccurrences(of: " ", with: "")
            textField_mobileNumber.placeholder = str_placeHolder
            if let Maxlgth  = str_placeHolder?.count{
                maxlength_MobileNumberfield = Maxlgth
            }
        }
        textField_mobileNumber.text = "\(loggedInUser.shared.string_mobileNumber)"
    }
    
    func addDeliveryAddressAPICalled(){
        guard let str_countryCode = textField_Country.accessibilityHint?.trimmingCharacters(in: .whitespacesAndNewlines) else{
            return
        }//accessibilityHint is country cell code i.e(+91)
        guard let str_mobileNumber = textField_mobileNumber.text?.trimmingCharacters(in: .whitespacesAndNewlines) else{
            return
        }
        let str_mobileNumberWithCountryCode : String = str_countryCode +  str_mobileNumber
        
        let parameters : Parameters = [
            
            "customer_id" : loggedInUser.shared.string_userID,
            "firstname" : textField_firstName.text!,
            "lastname" : textField_lastName.text!,
            "street" : textField_Street.text!,
            "city" : textField_City.text!,
            "country_id" : textField_Country.accessibilityLabel!,
            "mobilenumber" : str_mobileNumberWithCountryCode,
            "cacheignore" : "\(Int(NSDate().timeIntervalSince1970))" //To ignore cache from api...
        ]
        
        AppDelegate.showHUD(inView: self.view, message:"Loading....")
        Alamofire.request(Constants.apiURL + "webservice/addaddress.php", method: .post, parameters: parameters)
            .responseJSON { response in
                AppDelegate.hideHUD(inView: self.view)
                switch response.result {
                case .success:
                    if let json = response.result.value{
                        print("JSON Response : \(json)")
                        guard let _ : NSDictionary = json as? NSDictionary else {
                            return
                        }
                        do {
                            // make sure this JSON is in the format we expect
                            let jsonData =  try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
                            let decoded = try JSONSerialization.jsonObject(with: jsonData, options: []) as! NSDictionary
                            if decoded["msg"] as? String == "success"{
                                self.delegate?.addressAddeddSuccessfully()
                                self.navigationController?.popViewController(animated: false)
                            }else{
                                iToast.show("حدث خطأ - يرجي اعادة المحاولة")//An error has occurred - please try again
                            }
                        } catch let error as NSError {
                            print("Failed to load: \(error.localizedDescription)")
                        }
                    }
                case .failure( _):
                    //Utility.showAlert(message: error.localizedDescription, controller: self)
                    Utility.showAlert(message: "حدث خطأ - يرجي اعادة المحاولة", controller: self) //Something went wrong, please try again.
                }
        }
    }
    
    func sendOTPAPICalled(){
        //Country Code + Mobile Number...(i.e. +919773658682)
        guard let str_countryCode = textField_Country.accessibilityHint?.trimmingCharacters(in: .whitespacesAndNewlines) else{return}
        guard var str_mobileNumber = textField_mobileNumber.text?.trimmingCharacters(in: .whitespacesAndNewlines)else{
            return}
        
        let str_mobileNumberWithOutStartingZero =  String(str_mobileNumber.prefix(upTo: str_mobileNumber.index(str_mobileNumber.startIndex, offsetBy: 1))) //Check 0 exist in begining with mobile number
        if str_mobileNumberWithOutStartingZero == "0"{//Remove 0 from starting, if exists...
            str_mobileNumber =  String(str_mobileNumber.dropFirst())}
        let str_mobileNumberWithCountryCode : String = str_countryCode +  str_mobileNumber
        
        let parameters : Parameters = [
            "mobile" : str_mobileNumberWithCountryCode,
            "resend" : "0",
            "cacheignore" : "\(Int(NSDate().timeIntervalSince1970))" //To ignore cache from api...
        ]
        
        AppDelegate.showHUD(inView: self.view, message:"Loading....")
        Alamofire.request(Constants.mediaSourceURL + "vsms/otp/sendaddress/", method: .post, parameters: parameters)
            .responseJSON { response in
                AppDelegate.hideHUD(inView: self.view)
                switch response.result {
                case .success:
                    if let json = response.result.value{
                        guard let _ : NSDictionary = json as? NSDictionary else {
                            return
                        }
                        do {
                            let jsonData =  try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
                            let decoded = try JSONSerialization.jsonObject(with: jsonData, options: []) as! NSDictionary
                            if decoded["success"] as! Bool {
                                self.showOTPView()
                            }
                            else{
                                Utility.showAlert(message: decoded["msg"] as? String ?? "فشل الارسال -اعادة المحاولة", controller: self)// OTP not sent, please try again
                            }
                        }
                        catch let error as NSError {
                            print("Failed to load: \(error.localizedDescription)")
                        }
                    }
                case .failure( _):
                    //Utility.showAlert(message: error.localizedDescription, controller: self)
                    Utility.showAlert(message: "حدث خطأ - يرجي اعادة المحاولة", controller: self) //Something went wrong, please try again.
                }
        }
    }
    
    func verifyOTPAPICalled(){
        
        guard let strOtp = textFieldEnterOTP.text?.trimmingCharacters(in: .whitespacesAndNewlines) else{
            return
        }
        
        //Country Code + Mobile Number...(i.e. +919773658682)
        guard let str_countryCode = textField_Country.accessibilityHint?.trimmingCharacters(in: .whitespacesAndNewlines) else{
            return
        }
        guard var str_mobileNumber = textField_mobileNumber.text?.trimmingCharacters(in: .whitespacesAndNewlines) else{
            return
        }
        let str_mobileNumberWithOutStartingZero =  String(str_mobileNumber.prefix(upTo: str_mobileNumber.index(str_mobileNumber.startIndex, offsetBy: 1))) //Check 0 exist in begining with mobile number
        if str_mobileNumberWithOutStartingZero == "0"{//Remove 0 from starting, if exists...
            str_mobileNumber =  String(str_mobileNumber.dropFirst())}
        let str_mobileNumberWithCountryCode : String = str_countryCode +  str_mobileNumber
        
        
        let parameters : Parameters = [
            
            "mobile" : str_mobileNumberWithCountryCode,
            "otp" : strOtp,
            "cacheignore" : "\(Int(NSDate().timeIntervalSince1970))" //To ignore cache from api...
            
        ]
        
        AppDelegate.showHUD(inView: self.view, message:"Loading....")
        
        Alamofire.request(Constants.mediaSourceURL + "vsms/otp/verifyaddress/", method: .post, parameters: parameters)
            
            .responseJSON { response in
                
                AppDelegate.hideHUD(inView: self.view)
                
                switch response.result {
                case .success:
                    
                    if let json = response.result.value{
                        
                        //print("JSON Response : \(json)")
                        guard let _ : NSDictionary = json as? NSDictionary else {
                            return
                        }
                        
                        
                        do {
                            // make sure this JSON is in the format we expect
                            
                            let jsonData =  try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
                            let decoded = try JSONSerialization.jsonObject(with: jsonData, options: []) as! NSDictionary
                            // here "decoded" is of type `Any`, decoded from JSON data
                            
                            if decoded["success"] as! Bool {
                                
                                self.addDeliveryAddressAPICalled()
                                
                            }else{
                                
                                iToast.show(decoded["msg"] as? String)
                            }
                            
                        } catch let error as NSError {
                            
                            print("Failed to load: \(error.localizedDescription)")
                        }
                        
                    }
                case .failure( _):
                    //Utility.showAlert(message: error.localizedDescription, controller: self)
                    Utility.showAlert(message: "حدث خطأ - يرجي اعادة المحاولة", controller: self) //Something went wrong, please try again.
                }
        }
        
        
    }
    
    func resendOTPAPICalled(){
        
        guard let str_countryCode = textField_Country.accessibilityHint?.trimmingCharacters(in: .whitespacesAndNewlines) else{
            return
        }
        guard var str_mobileNumber = textField_mobileNumber.text?.trimmingCharacters(in: .whitespacesAndNewlines) else{
            return
        }
        let str_mobileNumberWithOutStartingZero =  String(str_mobileNumber.prefix(upTo: str_mobileNumber.index(str_mobileNumber.startIndex, offsetBy: 1))) //Check 0 exist in begining with mobile number
        if str_mobileNumberWithOutStartingZero == "0"{//Remove 0 from starting, if exists...
            str_mobileNumber =  String(str_mobileNumber.dropFirst())}
        let str_mobileNumberWithCountryCode : String = str_countryCode +  str_mobileNumber
        
        
        let parameters : Parameters = [
            "mobile" : str_mobileNumberWithCountryCode,
            "resend" : "1",
            "cacheignore" : "\(Int(NSDate().timeIntervalSince1970))" //To ignore cache from api...
        ]
        
        AppDelegate.showHUD(inView: self.view, message:"Loading....")
        Alamofire.request(Constants.mediaSourceURL + "vsms/otp/sendaddress/", method: .post, parameters: parameters)
            .responseJSON { response in
                AppDelegate.hideHUD(inView: self.view)
                switch response.result {
                case .success:
                    if let json = response.result.value{
                        //print("JSON Response : \(json)")
                        guard let _ : NSDictionary = json as? NSDictionary else {
                            return
                        }
                        do {
                            // make sure this JSON is in the format we expect
                            let jsonData =  try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
                            let decoded = try JSONSerialization.jsonObject(with: jsonData, options: []) as! NSDictionary
                            // here "decoded" is of type `Any`, decoded from JSON data
                            
                            if decoded["success"] as! Bool {
                                iToast.show("تم ارسال الكود بنجاح")//OTP sent successfully
                            }
                            else {
                                Utility.showAlert(message: decoded["msg"] as? String ?? "فشل الارسال -اعادة المحاولة", controller: self)//OTP not sent, please try again
                            }
                        }
                        catch let error as NSError {
                            print("Failed to load: \(error.localizedDescription)")
                        }
                    }
                case .failure( _):
                    //Utility.showAlert(message: error.localizedDescription, controller: self)
                    Utility.showAlert(message: "حدث خطأ - يرجي اعادة المحاولة", controller: self) //Something went wrong, please try again.
                }
        }
    }
    
    func showOTPView() {
        value_Timer = 30
        buttonasfullScreenForOTP = UIButton()
        buttonasfullScreenForOTP.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        buttonasfullScreenForOTP.frame = CGRect(x: 0, y: 0, width: UIApplication.shared.keyWindow?.bounds.size.width ?? 0.0, height: UIApplication.shared.keyWindow?.bounds.size.height ?? 0.0)
        buttonasfullScreenForOTP.addTarget(self, action: #selector(removebuttonasfullScreenForOTP), for: .touchUpInside)
        self.view.addSubview(buttonasfullScreenForOTP)
        
        let viewOTP = UIView()
        if UIDevice.current.userInterfaceIdiom == .pad{
            viewOTP.frame = CGRect(x: 20, y: (buttonasfullScreenForOTP.bounds.size.height - 352) / 2 - 64, width: buttonasfullScreenForOTP.bounds.size.width - 40, height: 280)
        }
        else {
            viewOTP.frame = CGRect(x: 20, y: (buttonasfullScreenForOTP.bounds.size.height - 216) / 2 - 40, width: buttonasfullScreenForOTP.bounds.size.width - 40, height: 240)
        }
        viewOTP.backgroundColor = UIColor.white
        buttonasfullScreenForOTP.addSubview(viewOTP)
        
        let labelTitle = UILabel()
        labelTitle.frame = CGRect(x: 30, y: 0, width: viewOTP.bounds.size.width - 60, height: 40)
        labelTitle.font = UIFont (name: Constants.helvetica_bold_font, size: 14)
        labelTitle.text = "التحقق من رقم الجوال" //"Mobile number verification
        labelTitle.numberOfLines = 1
        labelTitle.textAlignment = .center
        labelTitle.backgroundColor = UIColor.clear
        viewOTP.addSubview(labelTitle)
        
        let labelForgotPassword = UILabel()
        labelForgotPassword.frame = CGRect(x: 30, y: 40, width: viewOTP.bounds.size.width - 60, height: 60)
        guard let str_countryCode = textField_Country.accessibilityHint?.trimmingCharacters(in: .whitespacesAndNewlines) else{
            return
        }
        guard let str_mobileNumber = textField_mobileNumber.text?.trimmingCharacters(in: .whitespacesAndNewlines) else{
            return
        }
        
        labelForgotPassword.text = "تم ارسال رسالة نصية لكم بالكود السري مكون من 4 ارقام"
        // labelForgotPassword.text = "A text message with a 4-digit verification code has been sent to \(str_mobileNumberWithCountryCode)" //A text message with a 4-digit verification code has been sent to
        //labelForgotPassword.text = "A text message with a 4-digit verification code has been sent to +919781116773)"
        labelForgotPassword.font = UIFont (name: Constants.helvetica_regular_font, size: 14)
        labelForgotPassword.numberOfLines = 3
        labelForgotPassword.textAlignment = .center
        labelForgotPassword.backgroundColor = UIColor.clear
        viewOTP.addSubview(labelForgotPassword)
        
        textFieldEnterOTP = UITextField()
        textFieldEnterOTP.frame = CGRect(x: 30, y: 100, width: viewOTP.bounds.size.width - 60, height: 40)
        textFieldEnterOTP.font = UIFont.systemFont(ofSize: 16)
        textFieldEnterOTP.placeholder = "الكود السري"//Secret Code...
        textFieldEnterOTP.autocorrectionType = .no
        textFieldEnterOTP.autocapitalizationType = .none
        textFieldEnterOTP.backgroundColor = UIColor.white
        textFieldEnterOTP.keyboardType = .numberPad
        textFieldEnterOTP.clearButtonMode = .whileEditing
        textFieldEnterOTP.becomeFirstResponder()
        textFieldEnterOTP.layer.cornerRadius = 2
        viewOTP.addSubview(textFieldEnterOTP)
        let leftViewEmailId = UIView(frame: CGRect(x: 15, y: 0, width: 7, height: 26))
        leftViewEmailId.backgroundColor = UIColor.clear
        textFieldEnterOTP.leftView = leftViewEmailId
        textFieldEnterOTP.leftViewMode = .always
        textFieldEnterOTP.delegate = self
        
        label_ForTimerCountDown = UILabel()
        label_ForTimerCountDown.frame = CGRect(x: 30, y: 150, width: viewOTP.bounds.size.width - 60, height: 20)
        label_ForTimerCountDown.text = "اعادة ارسال الكود بعد \(value_Timer) ثانية" //"Resend after \(value_Timer) seconds"
        label_ForTimerCountDown.font = UIFont (name: Constants.helvetica_regular_font, size: 12)
        label_ForTimerCountDown.textColor = UIColor.lightGray
        label_ForTimerCountDown.numberOfLines = 1
        label_ForTimerCountDown.textAlignment = .center
        label_ForTimerCountDown.backgroundColor = UIColor.clear
        viewOTP.addSubview(label_ForTimerCountDown)
        
        buttonResendOTP = UIButton(type: .system)
        buttonResendOTP.frame = CGRect(x: 30, y: 180, width: viewOTP.bounds.size.width/2 - 40, height: 40)
        buttonResendOTP.setTitle("اعادة ارسال الكود", for: .normal)
        buttonResendOTP.titleLabel?.font = UIFont (name: Constants.helvetica_regular_font, size: 16)
        buttonResendOTP.setTitleColor(UIColor.white, for: .normal)
        buttonResendOTP.layer.cornerRadius = 2
        buttonResendOTP.addTarget(self, action: #selector(buttonResendOTPClicked), for: .touchUpInside)
        viewOTP.addSubview(buttonResendOTP)
        self.enableUserInteractionReSenOTPButton(isEnable: false)
        
        buttonVerifyOTP = UIButton(type: .system)
        buttonVerifyOTP.frame = CGRect(x: viewOTP.bounds.size.width/2 + 10, y: 180, width: viewOTP.bounds.size.width/2 - 40, height: 40)
        buttonVerifyOTP.setTitle("ادخال الكود", for: .normal) //Verify OTP
        buttonVerifyOTP.titleLabel?.font = UIFont (name: Constants.helvetica_regular_font, size: 16)
        buttonVerifyOTP.setTitleColor(UIColor.white, for: .normal)
        buttonVerifyOTP.backgroundColor = Constants.default_ThemeColor
        buttonVerifyOTP.layer.cornerRadius = 2
        buttonVerifyOTP.addTarget(self, action: #selector(buttonVerifyOTPClicked), for: .touchUpInside)
        viewOTP.addSubview(buttonVerifyOTP)
        
        otp_Timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updatelabel_ForTimerCountDown), userInfo: nil, repeats: true)
    }
    
    @objc func removebuttonasfullScreenForOTP() {
        if (otp_Timer != nil){
            otp_Timer?.invalidate()
        }
        buttonasfullScreenForOTP.removeFromSuperview()
    }
    
    @objc func buttonVerifyOTPClicked(){
        if textFieldEnterOTP.text == "" {
            Utility.showAlert(message:"يرجى ادخال الكود", controller: self)//Please enter OTP.
            return
        }
        if(textFieldEnterOTP.isFirstResponder) {
            textFieldEnterOTP.resignFirstResponder()
        }
        self.verifyOTPAPICalled()
    }
    
    @objc func buttonResendOTPClicked(){
        value_Timer = 30
        otp_Timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updatelabel_ForTimerCountDown), userInfo: nil, repeats: true)
        label_ForTimerCountDown.isHidden = false
        self.enableUserInteractionReSenOTPButton(isEnable: false)
        
        if(textFieldEnterOTP.isFirstResponder){textFieldEnterOTP.resignFirstResponder()}
        self.resendOTPAPICalled()
    }
    
    func enableUserInteractionReSenOTPButton(isEnable: Bool){
        if isEnable {
            buttonResendOTP.backgroundColor = Constants.default_ThemeColor
            buttonResendOTP.isUserInteractionEnabled = true
        }
        else {
            buttonResendOTP.backgroundColor = UIColor(red: 83/255, green: 83/255, blue: 83/255, alpha: 1.0)
            buttonResendOTP.isUserInteractionEnabled = false
        }
    }
    
    @objc func updatelabel_ForTimerCountDown(){
        if (otp_Timer != nil){
            value_Timer = value_Timer - 1
            label_ForTimerCountDown.text = "اعادة ارسال الكود بعد \(value_Timer) ثانية" //"Resend after \(value_Timer) seconds"
            if value_Timer == 0{
                otp_Timer!.invalidate()
                self.enableUserInteractionReSenOTPButton(isEnable: true)
                label_ForTimerCountDown.isHidden = true
            }
        }
    }
    
    //MARK: - UITableView DataSource
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 30
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return array_CountryList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "SelectionTreatMentCell"
        var cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier)
        if cell == nil {
            cell = tableviewCell(withReuseIdentifierSelection: cellIdentifier, tablview: tableView, with: indexPath)
        }
        
        self.configureCellSelectionProcedure(cell, for: indexPath, withSection: indexPath.section, tableView: tableView)
        
        cell!.selectionStyle = .none
        return (cell ?? nil) ?? UITableViewCell()
    }
    
    // MARK: For Selection Only
    func tableviewCell(withReuseIdentifierSelection identifier: String?, tablview: UITableView?, with indxPath: IndexPath?) -> UITableViewCell? {
        let cell = UITableViewCell(style: .default, reuseIdentifier: identifier)
        
        //UILabel Selection TreatMent...
        let labelSelectionTreatMent = UILabel()
        labelSelectionTreatMent.frame = CGRect(x: 10, y: 0, width: (tablview?.bounds.size.width ?? 0.0) - 20, height: 30)
        labelSelectionTreatMent.textColor = UIColor.black
        labelSelectionTreatMent.tag = TAGPPICKERFORSELECTION
        labelSelectionTreatMent.numberOfLines = 0
        labelSelectionTreatMent.textAlignment = .center
        labelSelectionTreatMent.backgroundColor = UIColor.white
        labelSelectionTreatMent.isUserInteractionEnabled = true
        cell.contentView.addSubview(labelSelectionTreatMent)
        
        return cell
        
    }
    
    func configureCellSelectionProcedure(_ cell: UITableViewCell?, for indexPath: IndexPath?, withSection section: Int, tableView: UITableView?) {
        let labelSelectionEvent = cell?.contentView.viewWithTag(TAGPPICKERFORSELECTION) as? UILabel
        let dictCountry : NSDictionary = array_CountryList[indexPath!.row] as! NSDictionary
        //labelSelectionEvent?.text = dict["name"] as? String
        let str_countryName : String = dictCountry["name"] as? String ?? ""
        var str_countrycelCode : String = dictCountry["cel_code"] as? String ?? ""
        str_countrycelCode =  str_countrycelCode.replacingOccurrences(of: "+", with: "")
        labelSelectionEvent?.text = "(" + str_countrycelCode + "+) " + str_countryName
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dictEventDetail : NSDictionary = array_CountryList[indexPath.row] as! NSDictionary
        self.setCountryCode(dictEventDetail)
        self.removePickerFromScreenForEvent()
    }
    
    func setCountryCode(_ dictCountry: NSDictionary) {
        //textField_Country.text = dictCountry["name"] as? String
        //textField_Country.accessibilityLabel = dictCountry["code"] as? String
        
        //Country Name + Country Code...(i.e. India ( +91 )
        let str_countryName : String = dictCountry["name"] as? String ?? ""
        var str_countrycelCode : String = dictCountry["cel_code"] as? String ?? ""
        str_countrycelCode =  str_countrycelCode.replacingOccurrences(of: "+", with: "")
        textField_Country.text = "(" + str_countrycelCode + "+) " + str_countryName
        textField_Country.accessibilityLabel = dictCountry["code"] as? String //Country Code i.e(SA,IN...)
        textField_Country.accessibilityHint = dictCountry["cel_code"] as? String //Cell code i.e.(+91,+966,+98,etc...)
        var str_placeHolder = dictCountry["placeholder"] as? String
        str_placeHolder =  str_placeHolder?.replacingOccurrences(of: " ", with: "")
        textField_mobileNumber.placeholder = str_placeHolder
        if let Maxlgth  = str_placeHolder?.count {
            maxlength_MobileNumberfield = Maxlgth
        }
        textField_mobileNumber.text = ""
    }
    
    //MARK: - UITextField Delegate
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == textFieldEnterOTP {
            if CharacterSet(charactersIn: "0123456789").isSuperset(of: CharacterSet(charactersIn: string)) {
                let currentString: NSString = textField.text! as NSString
                let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
                return newString.length <= 6
            }else{return false}
        }
        else if textField.tag ==  5 {
            // Check for valid input characters
            if CharacterSet(charactersIn: "0123456789").isSuperset(of: CharacterSet(charactersIn: string)) {
                return true
            }else {
                return false
            }
        }
        return true
    }
    
    func showCountryList(){
        if array_CountryList.count == 0 {
            //For Event...
            return
        }
        
        if (textField_firstName.isFirstResponder){
            textField_firstName.resignFirstResponder()
        }
        if (textField_lastName.isFirstResponder){
            textField_lastName.resignFirstResponder()
        }
        if (textField_Street.isFirstResponder){
            textField_Street.resignFirstResponder()
        }
        if (textField_City.isFirstResponder){
            textField_City.resignFirstResponder()
        }
        if (textField_mobileNumber.isFirstResponder){
            textField_mobileNumber.resignFirstResponder()
        }
        
        buttonasfullScreenForCountrySelection = UIButton()
        buttonasfullScreenForCountrySelection.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        buttonasfullScreenForCountrySelection.frame = CGRect(x: 0, y: 0, width: UIApplication.shared.keyWindow?.bounds.size.width ?? 0.0, height: UIApplication.shared.keyWindow?.bounds.size.height ?? 0.0)
        buttonasfullScreenForCountrySelection.addTarget(self, action: #selector(removePickerFromScreenForEvent), for: .touchUpInside)
        UIApplication.shared.keyWindow?.addSubview(buttonasfullScreenForCountrySelection)
        
        let viewForShadow = UIView()
        if UIDevice.current.userInterfaceIdiom == .phone{
            viewForShadow.frame = CGRect(x: (buttonasfullScreenForCountrySelection.bounds.size.width - 350) / 2, y: (buttonasfullScreenForCountrySelection.bounds.size.height - 216) / 2, width: 350, height: 216)
        }
        else {
            viewForShadow.frame = CGRect(x: buttonasfullScreenForCountrySelection.bounds.size.width / 4, y: buttonasfullScreenForCountrySelection.bounds.size.height / 4, width: buttonasfullScreenForCountrySelection.bounds.size.width / 2, height: buttonasfullScreenForCountrySelection.bounds.size.height / 2)
            
        }
        buttonasfullScreenForCountrySelection.addSubview(viewForShadow)
        
        tableViewForCountrySelection = UITableView()
        tableViewForCountrySelection.tag = 100
        tableViewForCountrySelection.cellLayoutMarginsFollowReadableWidth = false
        tableViewForCountrySelection.frame = CGRect(x: 0, y: 0, width: viewForShadow.bounds.size.width, height: viewForShadow.bounds.size.height)
        tableViewForCountrySelection.delegate = self
        tableViewForCountrySelection.dataSource = self
        tableViewForCountrySelection.separatorStyle = .none
        tableViewForCountrySelection.tableFooterView = UIView(frame: CGRect.zero)
        viewForShadow.addSubview(tableViewForCountrySelection)
        tableViewForCountrySelection.reloadData()
    }
    
    @objc func removePickerFromScreenForEvent() {
        buttonasfullScreenForCountrySelection.removeFromSuperview()
    }
    
    //MARK: UIButton Clicks
    @IBAction func button_CountryClicked(_ sender: UIButton) {
        self.showCountryList()
    }
    
    @IBAction func button_saveDeliveryAddressClicked(_ sender: UIButton) {
//                self.addDeliveryAddressAPICalled()
//                return
           
        if textField_firstName.text == ""{
            Utility.showAlert(message:"برجاء كتابة الاسم الاول", controller: self)
            return  
            
        } 
        if textField_lastName.text == ""{
            Utility.showAlert(message:"برجاء كتابة الاسم الاخير", controller: self)
            return
        }
        if textField_Country.text == "" || textField_Country.accessibilityLabel == ""{
            Utility.showAlert(message:"برجاء اختيار الدولة", controller: self)
            return
        }
        if textField_City.text == ""{
            Utility.showAlert(message:"برجاء كتابة المدينة", controller: self)
            return
        }
        if textField_Street.text == ""{
            Utility.showAlert(message:"برجاء كتابة اسم الشارع", controller: self)
            return
        }
        if textField_mobileNumber.text == ""{
            Utility.showAlert(message:"من فضلك أدخل رقم الجوال", controller: self)
            return
        }
        
//        guard let str_countryCode = textField_Country.accessibilityHint?.trimmingCharacters(in: .whitespacesAndNewlines) else{
//            return
//        }//accessibilityHint is country cell code i.e(+91)
//
//        guard let str_mobileNumber = textField_mobileNumber.text?.trimmingCharacters(in: .whitespacesAndNewlines) else{
//            return
//        }
        
//        let str_mobileNumberWithCountryCode : String = str_countryCode +  str_mobileNumber
        
//        if str_mobileNumberWithCountryCode == loggedInUser.shared.string_country + loggedInUser.shared.string_mobileNumber {//If user enters another number, which is not associated with an account then need to verify via OTP...
//            self.addDeliveryAddressAPICalled()
//        }
//        else {
//            self.sendOTPAPICalled()
//        }
        self.sendOTPAPICalled()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

