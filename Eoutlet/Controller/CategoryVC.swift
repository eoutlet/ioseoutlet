import Alamofire
import SDWebImage
class CategoryTableViewCell: UITableViewCell{
    
    @IBOutlet weak var view_forShadow: UIView!
    @IBOutlet weak var constraint_topHeight: NSLayoutConstraint!
    @IBOutlet weak var constraint_bottomHeight: NSLayoutConstraint!
    
    
    @IBOutlet weak var imageview_tickArrow: UIImageView!
    @IBOutlet weak var label_Title: UILabel!
    
}

class CategoryVC: UIViewController,UITableViewDataSource,UITableViewDelegate {
    @IBOutlet weak var tableView_Category: UITableView!
    var array_Category : NSMutableArray = NSMutableArray()
    var clickedSection : Int = 99999 //This default value to set all category closed
    var clickedRow : Int = 99999 //This default value to set all category closed

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView_Category.tableFooterView = UIView()
        
        //Disable swipe gesture for popviewcontroller...
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        
        //getAllCategories...
        self.getAllCategories()
    }
    
 
     override func viewWillAppear(_ animated: Bool) {
            super.viewWillAppear(true)
            self.navigationController?.navigationBar.isHidden = true
            
    //        // we are call api for reloading cart item count.....
    //        AppDelegate.shared.getAllCartItemAPICalled()
            
               //set cart item count on home screen navigation & tab bar...
               if AppDelegate.cart_item_count != 0 {
                   if let tabItems = self.tabBarController?.tabBar.items {
                       let tabItem = tabItems[0]
                       tabItem.badgeValue = "\(AppDelegate.cart_item_count)"
                   }
               }
           }
    
    //MARK: - UIButton Clicks
    @IBAction func buttonSearchClicked(_ sender: UIButton) {
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "searchVC") as! SearchVC
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    //MARK: API Calling...
    func getAllCategories(){
        
        let parameters : Parameters = [
            "cacheignore" : "\(Int(NSDate().timeIntervalSince1970))" //To ignore cache from api...
        ]
        
        AppDelegate.showHUD(inView: self.view, message:"Loading....")
        Alamofire.request(Constants.apiURL+"webservice/categoryapi.php", method: .get, parameters: parameters)
            .responseJSON { response in
                AppDelegate.hideHUD(inView: self.view)
                switch response.result {
                case .success:
                    if let json = response.result.value{
                        // print("JSON Response : \(json)")
                        guard let _ : NSDictionary = json as? NSDictionary else {
                            return
                        }
                        do {
                            // make sure this JSON is in the format we expect
                            let jsonData =  try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
                            let decoded = try JSONSerialization.jsonObject(with: jsonData, options: []) as! NSDictionary
                            // here "decoded" is of type `Any`, decoded from JSON data
                            
                            let arrayofGetData = decoded["categorydata"] as! NSArray
                            if arrayofGetData.count > 0{
                                
                                if(self.array_Category.count>0) {
                                    self.array_Category.removeAllObjects()
                                }
                                
                                self.array_Category.addObjects(from: (arrayofGetData ) as! [Any])
                                self.tableView_Category.reloadData()
                            }
                        }
                        catch let error as NSError {
                            print("Failed to load: \(error.localizedDescription)")
                        }
                    }
                case .failure( _):
                    //Utility.showAlert(message: error.localizedDescription, controller: self)
                    Utility.showAlert(message: "حدث خطأ - يرجي اعادة المحاولة", controller: self) //Something went wrong, please try again.
                }
        }
    }
    
    //MARK: - UITableView DataSource
    func numberOfSections(in tableView: UITableView) -> Int {
        return array_Category.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let dict_Record = array_Category[section] as! NSDictionary
        let view_background = UIView(frame: CGRect(x: 5, y: 50, width: tableView.bounds.size.width - 10, height: 100))
        if #available(iOS 13.0, *) {
            view_background.backgroundColor = UIColor.systemBackground
        } else {
            view_background.backgroundColor = UIColor.white
        } //your background color...

        let view_holdingAllControls = UIView(frame: CGRect(x: 5, y: 10, width: view_background.bounds.size.width, height: 80))
        view_holdingAllControls.backgroundColor = UIColor.white //your background color...
        view_holdingAllControls.cornerRadius = 10
        view_holdingAllControls.setShadow(x: -5, y: -5, width: view_holdingAllControls.bounds.width + 10, height: view_holdingAllControls.bounds.height + 10, radius: 5)
        view_background.addSubview(view_holdingAllControls)
       
        
        //Create Name Label...
        let label_Name = UILabel(frame: CGRect(x: view_holdingAllControls.bounds.size.width - 260, y: 15, width: 180, height: 25))
        label_Name.backgroundColor = UIColor.clear
        label_Name.font = UIFont(name:Constants.helvetica_bold_font, size: 14)
        label_Name.textAlignment = .right
        label_Name.textColor = Constants.default_ThemeColor
        label_Name.text = dict_Record["name"] as? String
        view_holdingAllControls.addSubview(label_Name)
      
        
        let array_ofSubCategory = dict_Record["data"] as! NSArray //If no sub category exists then dropdown arrow will be hidden.
        //Create Sub Categories Label...
        let label_totalSubCategories = UILabel(frame: CGRect(x: 10, y: 20, width: 100, height: 20))
        label_totalSubCategories.backgroundColor = UIColor.clear
        label_totalSubCategories.font = UIFont(name:Constants.helvetica_bold_font, size: 8)
        label_totalSubCategories.textAlignment = .left
        label_totalSubCategories.textColor = UIColor.lightGray
        label_totalSubCategories.text = "الفئات \(array_ofSubCategory.count)"
        view_holdingAllControls.addSubview(label_totalSubCategories)
        
        
        //Create Caption Label...
        let label_Caption = UILabel(frame: CGRect(x: 45, y: 40, width: view_holdingAllControls.bounds.size.width - 125, height: 25))
        label_Caption.backgroundColor = UIColor.clear
        label_Caption.font = UIFont(name:Constants.helvetica_bold_font, size: 12)
        label_Caption.textAlignment = .right
        label_Caption.adjustsFontSizeToFitWidth = true
        label_Caption.textColor = UIColor.systemGray
        label_Caption.text = dict_Record["caption"] as? String
        view_holdingAllControls.addSubview(label_Caption)
        
        let imgDropDown = UIImageView(frame: CGRect(x: 10, y: 45, width: 18, height: 18))
        if (clickedSection == section){
            imgDropDown.image = UIImage(named: "category_uparrow")
        }else{
            imgDropDown.image = UIImage(named: "category_downarrow")
        }
        if(array_ofSubCategory.count == 0){
            imgDropDown.isHidden = true
            label_totalSubCategories.isHidden = true
        }else{
            imgDropDown.isHidden = false
            label_totalSubCategories.isHidden = false

        }
        imgDropDown.backgroundColor = UIColor.clear
        view_holdingAllControls.addSubview(imgDropDown)
        
        
        let imageview_Category = UIImageView(frame: CGRect(x: view_holdingAllControls.bounds.size.width - 70, y: 10, width: view_holdingAllControls.bounds.size.height - 20, height: view_holdingAllControls.bounds.size.height - 20))
        imageview_Category.sd_imageIndicator = SDWebImageActivityIndicator.gray
        imageview_Category.sd_imageIndicator?.startAnimatingIndicator()
        imageview_Category.layer.cornerRadius = 5
        imageview_Category.clipsToBounds = true

        let url = NSURL(string: dict_Record["image"] as? String ?? "" )
        imageview_Category.sd_setImage(with: url as URL?) { (image, error, cache, urls) in
            if (error != nil) {// Failed to load image
            } else {// Successful in loading image
                imageview_Category.sd_imageIndicator?.stopAnimatingIndicator()
            }}
        imageview_Category.contentMode = .scaleAspectFill
        imageview_Category.backgroundColor = UIColor.white
        view_holdingAllControls.addSubview(imageview_Category)
        
        let button = UIButton(frame: CGRect(x: 0, y: 0, width: view_holdingAllControls.bounds.size.width, height: view_holdingAllControls.bounds.size.height))
        button.backgroundColor = UIColor.clear
        button.tag = section
        button.addTarget(self, action:#selector(button_sectionClicked(sender:)), for: .touchUpInside)
        view_holdingAllControls.addSubview(button)
        
        return view_background
        
    
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        
        // return UITableView.automaticDimension
        if UIDevice.current.userInterfaceIdiom == .pad{
            return 60
        }else{
            return 50
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if clickedSection == section {
            
            let dict_Record = array_Category[section] as! NSDictionary
            let array_ofSection = dict_Record["data"] as! NSArray
            return array_ofSection.count
            
        }else{
            
            return 0
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "categoryTableViewCell", for: indexPath) as! CategoryTableViewCell
        let dict_Record = array_Category[indexPath.section] as! NSDictionary
        let array_ofSection = dict_Record["data"] as! NSArray
        let array_Dict = array_ofSection[indexPath.row] as? NSDictionary
        cell.label_Title.text = array_Dict?["name"] as? String
        if clickedRow == indexPath.row{
            cell.imageview_tickArrow.isHidden = false
        }
        else{
            cell.imageview_tickArrow.isHidden = true
        }
        
        //To set the shadow for all cells...
        if array_ofSection.count == 1{
            cell.constraint_topHeight.constant = 5
            cell.constraint_bottomHeight.constant = 5
        }else if indexPath.row == 0{
            cell.constraint_topHeight.constant = 5
            cell.constraint_bottomHeight.constant = 0
        }else if indexPath.row == array_ofSection.count - 1{
            cell.constraint_topHeight.constant = 0
            cell.constraint_bottomHeight.constant = 5
        }else{
            cell.constraint_topHeight.constant = 0
            cell.constraint_bottomHeight.constant = 0
        }
        
        cell.view_forShadow.setShadow(x: -5, y: -5, width: cell.contentView.bounds.width, height: cell.view_forShadow.bounds.height + 10, radius: 5)

        cell.layoutMargins = UIEdgeInsets.zero
        cell.separatorInset = UIEdgeInsets.zero
        return cell
        
    }
    
    //MARK: UITableView Delegate
    @objc func button_sectionClicked(sender:UIButton) ->() {
        let dict_Record = array_Category[sender.tag] as! NSDictionary
        let array_ofSubCategory = dict_Record["data"] as! NSArray
        if(array_ofSubCategory.count == 0){
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "productListVC") as! ProductListVC
            vc.dict_subCategory = dict_Record
            self.navigationController?.pushViewController(vc, animated: false)
            return
        }
        
        if clickedSection == sender.tag {
            clickedSection = 99999//This default value to set all category closed
        }else{
            clickedSection = sender.tag
        }
        
        clickedRow = 99999 //This default value to set all category unselected
        tableView_Category.reloadData()
        tableView_Category.beginUpdates()
        tableView_Category.endUpdates()

        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        clickedRow = indexPath.row
        tableView_Category.reloadData {
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "productListVC") as! ProductListVC
            let dict_Record = self.array_Category[indexPath.section] as! NSDictionary
            let array_ofSection1 = dict_Record["data"] as? NSArray
            let array_Dict : NSDictionary = array_ofSection1?[indexPath.row] as? NSDictionary ?? NSDictionary()
            vc.dict_subCategory = array_Dict
            vc.array_HeaderSubCategorytList.addObjects(from: array_ofSection1 as! [Any])
            vc.titleStr = dict_Record["name"] as? String ?? ""
            vc.subCategorySelectedCellIndex = indexPath.row
            self.navigationController?.pushViewController(vc, animated: false)
        }
    }
}


