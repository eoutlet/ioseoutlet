//
//  SizeChartWebViewVC.swift
//  Eoutlet
//
//  Created by Sudhir Rohilla on 23/06/20.
//  Copyright © 2020 Unyscape. All rights reserved.
//

import UIKit
import Alamofire
import WebKit

class SizeChartWebViewVC: UIViewController, WKNavigationDelegate {

    @IBOutlet weak var sizeChartWebView: WKWebView!
    var str_sizeChartWebViewURL = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        
        sizeChartWebView.navigationDelegate = self
        sizeChartWebView.scrollView.bounces = false
        AppDelegate.showHUD(inView: self.view, message: "")
        self.loadUrlOnWebView()
        // Do any additional setup after loading the view.

    }
  
       
    func loadUrlOnWebView() {
           
           guard let myURL = URL(string: self.str_sizeChartWebViewURL) else { return}
           let myRequest = URLRequest(url: myURL)
           self.sizeChartWebView.load(myRequest)
           
       }
   
  //MARK: - WKWebView Delegates
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        DispatchQueue.main.async {
            AppDelegate.hideHUD(inView: self.view)
        }
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        DispatchQueue.main.async {
            AppDelegate.hideHUD(inView: self.view)
        }
       
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        
        if navigationAction.navigationType == WKNavigationType.linkActivated {
            if let url = navigationAction.request.url {
                decisionHandler(.cancel)
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        }
        else {
            decisionHandler(.allow)
        }
    }
    
   //MARK: - UIButton Clicks
      @IBAction func backButtonClicked(_ sender: UIButton) {
          self.navigationController?.popViewController(animated: false)
      }

}
