import Alamofire
import AppsFlyerLib

class ProfileTableViewCell: UITableViewCell {
    
    @IBOutlet weak var view_backgroundShadow: UIView!
    @IBOutlet weak var label_Title: UILabel!
    @IBOutlet weak var imageview_Icon: UIImageView!
}

class LoginVC: UIViewController,UITableViewDataSource,UITableViewDelegate,userSignupSuccessfullyDelegate,passwordChangeDelegate,UITextFieldDelegate, sendOTPCloseTransparent {
    func closeView(idStr: String) {
        self.sendOtpApiCalled()
    }
    
    @IBOutlet weak var view_segmentedShadow: UIView!
    @IBOutlet weak var segmentControl: UISegmentedControl!
    
    @IBOutlet weak var textField_Email: UITextField!
    @IBOutlet weak var textField_Email_Password: UITextField!
    
    @IBOutlet weak var textField_Mobile_Country: UITextField!
    @IBOutlet weak var textField_Mobile: UITextField!
    @IBOutlet weak var textField_Mobile_Password: UITextField!
    @IBOutlet weak var button_Mobile_submit: UIButton!
    
    @IBOutlet weak var button_helpAndSupport: UIButton!
    @IBOutlet weak var tableView_Profile: UITableView!
    
    @IBOutlet weak var emailLoginUIView: UIView!
    @IBOutlet weak var phoneLoginUIView: UIView!
    @IBOutlet weak var menuListUIView: UIView!
    @IBOutlet weak var mobileUIViewHeight_Constraint: NSLayoutConstraint!
    @IBOutlet weak var textField_Mobile_PasswordUIViewHeight_Constraint: NSLayoutConstraint!
    @IBOutlet weak var forgotBtnTopConstraints: NSLayoutConstraint!
    
    var array_profileListTitle : NSMutableArray = NSMutableArray()
    var array_iconsName : NSMutableArray = NSMutableArray()
    var array_CountryList : NSMutableArray = NSMutableArray()
    
    var tableViewForCountrySelection  : UITableView = UITableView()
    var maxlength_MobileNumberfield : Int = 0
    var buttonasfullScreenForCountrySelection : UIButton = UIButton()
    let TAGCOUNTRYNAME = 4
    let TAGCOUNTRYRADIOBUTTON = 5 
    
    var buttonasfullScreenForOTP = UIButton()
    var textFieldEnterOTP = UITextField()
    var label_ForTimerCountDown = UILabel()
    var  buttonVerifyOTP = UIButton()
    var  buttonResendOTP = UIButton()
    var otp_Timer: Timer?
    var value_Timer : Int = 30
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView_Profile.tableFooterView = UIView()
        self.menuListUIView.isHidden = true
        self.reloadTableView()
        
        self.setDefaultThemeColorHelpAndSupportButton()
        
        //Disable swipe gesture for popviewcontroller...
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
                
        if tableView_Profile.responds(to: #selector(setter: UITableView.separatorInset)) {//To remove line space from staring...
            tableView_Profile.separatorInset = .zero
        }
        
        //segment control customization
        if UIDevice.current.userInterfaceIdiom == .pad{
            view_segmentedShadow.setShadow(x: -5, y: -5, width: self.view.bounds.size.width - 290, height: view_segmentedShadow.bounds.size.height + 10, radius: 5)
        }else{
            view_segmentedShadow.setShadow(x: -5, y: -5, width: self.view.bounds.size.width - 50, height: view_segmentedShadow.bounds.size.height + 10, radius: 5)
        }
        segmentControl.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white], for: .selected)
        segmentControl.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.black], for: .normal)
        if #available(iOS 13.0, *) {
            segmentControl.selectedSegmentTintColor = UIColor.black
            //segmentControl.backgroundColor = UIColor.white
        } else {
            // Fallback on earlier versions
        }
        segmentControl.selectedSegmentIndex = 2
        
        emailLoginUIView.isHidden = true
        phoneLoginUIView.isHidden = false
        mobileUIViewHeight_Constraint.constant = 285
        textField_Mobile_PasswordUIViewHeight_Constraint.constant = 0
        self.forgotBtnTopConstraints.constant = 400
        button_Mobile_submit.setTitle("الدخول برسالة نصية", for: .normal)
        //get all countries list api called...
        self.getAllCountriesAPICalled()
    }
    
    func setDefaultThemeColorHelpAndSupportButton() {
        
        let string_Mobile : String = "خدمة العملاء"
        var mutablestring_Email = NSMutableAttributedString()
        mutablestring_Email = NSMutableAttributedString(string: string_Mobile, attributes: [NSAttributedString.Key.font:UIFont(name: Constants.helvetica_bold_font, size: 17.0)!])
        mutablestring_Email.addAttribute(NSAttributedString.Key.foregroundColor, value: Constants.default_ThemeColor, range: NSRange(location:0,length:string_Mobile.count))
        mutablestring_Email.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: NSRange(location:0, length:string_Mobile.count))
        button_helpAndSupport.setAttributedTitle(mutablestring_Email, for: .normal)  
        
    }
    
    override func viewWillAppear(_ _animated: Bool) {
        
        super.viewWillAppear(_animated)
        self.navigationController?.navigationBar.isHidden = true
        
        //Check if user already logged in...
        let userDefaults = UserDefaults.standard
        let decoded : Data = userDefaults.data(forKey: "loggedInUserRecord") ?? Data()
        do {
            if let _ : Dictionary<String, Any> = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? Dictionary<String, Any>{
                self.menuListUIView.isHidden = false
                self.reloadTableView()
            }
        }
        catch {print("Couldn't unarchive data")}
    }
    
    func reloadTableView() {
          
        let myProfile = "حسابي / التفاصيل"          //My profile / My Details
        let myWallet = "رصيد حسابي"                 //My Wallet                     // old using
        let myOrder = "طلبات الشراء"                //My Orders
        let trackOrder = "تعقب الطلبات"             //Track my orders
        let addressBook = "عناوين الشحن"            //Address Book
        let helpAndSupport = "سياسات التطبيق"       //Help & Support                //old using
        let contactUs = "اتصل بنا"                  //Contact Us
        let logOut = "تسجيل الخروج"                 //Log Out

        array_profileListTitle = [myProfile, myWallet, myOrder, trackOrder, addressBook, helpAndSupport, contactUs, logOut]
                
        array_iconsName = ["accountdashboard", "wallet-icon", "My Orders","Track Order", "addressbook","helpandsupport","contactus","logout"]
               
             
        self.tableView_Profile.reloadData()
    }
    
    @IBAction func segmentedControlButtonClickAction(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 0 {//when phone with password section is selected
            emailLoginUIView.isHidden = true
            phoneLoginUIView.isHidden = false
            mobileUIViewHeight_Constraint.constant = 330
            textField_Mobile_PasswordUIViewHeight_Constraint.constant = 45
            self.forgotBtnTopConstraints.constant = 465
            button_Mobile_submit.setTitle("الدخول برقم الجوال", for: .normal)
        }else if sender.selectedSegmentIndex == 1 {//When email section is selected
            emailLoginUIView.isHidden = false
            phoneLoginUIView.isHidden = true
            self.forgotBtnTopConstraints.constant = 400
        }else { //when phone with otp section is selected
            emailLoginUIView.isHidden = true
            phoneLoginUIView.isHidden = false
            mobileUIViewHeight_Constraint.constant = 260
            textField_Mobile_PasswordUIViewHeight_Constraint.constant = 0
            self.forgotBtnTopConstraints.constant = 400
            button_Mobile_submit.setTitle("الدخول برسالة نصية", for: .normal)
        }
    }
    
    //MARK: Coutry List API Calling
    func getAllCountriesAPICalled(){
       
         let parameters : Parameters = [
                        "cacheignore" : "\(Int(NSDate().timeIntervalSince1970))" //To ignore cache from api...
                    ]
        
        AppDelegate.showHUD(inView: self.view, message:"Loading....")
        Alamofire.request(Constants.apiURL + "webservice/countrylist.php", method: .get, parameters: parameters)
            .responseJSON { response in
                AppDelegate.hideHUD(inView: self.view)
                switch response.result {
                case .success:
                    if let json = response.result.value{
                        //print("JSON Response : \(json)")
                        guard let _ : NSDictionary = json as? NSDictionary else {
                            return
                        }
                        do {
                            // make sure this JSON is in the format we expect
                            let jsonData =  try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
                            let decoded = try JSONSerialization.jsonObject(with: jsonData, options: []) as! NSDictionary
                            // here "decoded" is of type `Any`, decoded from JSON data
                            
                            guard let dataArray : NSArray = decoded["data"] as? NSArray else {
                                return
                            }
                            
                            if dataArray.count > 0 {
                                self.array_CountryList.addObjects(from: (dataArray.reversed()) )
                            }
                            
                            if self.array_CountryList.count>0 {
                                let dictEventDetail : NSDictionary = self.array_CountryList[0] as! NSDictionary
                                self.setCountryNameField(dictEventDetail)
                            }
                        }
                        catch let error as NSError {
                            print("Failed to load: \(error.localizedDescription)")
                        }
                    }
                case .failure( _):
                    //Utility.showAlert(message: error.localizedDescription, controller: self)
                    Utility.showAlert(message: "حدث خطأ - يرجي اعادة المحاولة", controller: self) //Something went wrong, please try again.
                }
        }
    }
    
    //MARK: API Calling...
    func getguestTokenAPICall(){
        AppDelegate.showHUD(inView: self.view, message:"Loading....")
        let parameters : Parameters = [
            "cacheignore" : "\(Int(NSDate().timeIntervalSince1970))" //To ignore cache from api...
        ]
        
        Alamofire.request(Constants.apiURL + "webservice/guesttoken.php", method: .get, parameters: parameters)
            .responseJSON { response in
                AppDelegate.hideHUD(inView: self.view)
                switch response.result {
                case .success:
                    if let json = response.result.value{
                        print("JSON Response : \(json)")
                        guard let _ : NSDictionary = json as? NSDictionary else {
                            return
                        }
                        
                        do {
                            // make sure this JSON is in the format we expect
                            let jsonData =  try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
                            let decoded = try JSONSerialization.jsonObject(with: jsonData, options: []) as! NSDictionary
                            if decoded["msg"] as? String == "success"{
                                let userDefaults = UserDefaults.standard
                                do {
                                    let encodedData: Data = try NSKeyedArchiver.archivedData(withRootObject: decoded, requiringSecureCoding: false)
                                    userDefaults.removeObject(forKey: "isguestMaskKeyAndCardIdFetched")
                                    userDefaults.set(encodedData, forKey: "isguestMaskKeyAndCardIdFetched")
                                    userDefaults.synchronize()
                                }
                                catch {
                                    print("Couldn't save data")
                                }
                                self.logoutAPICalled() //After successful getting mask key and cart id, will logout from account...
                            }
                            
                        }
                        catch _ as NSError {
                            
                        }
                    }
                case .failure( _):
                    //Utility.showAlert(message: error.localizedDescription, controller: self)
                    Utility.showAlert(message: "حدث خطأ - يرجي اعادة المحاولة", controller: self) //Something went wrong, please try again.
                }
        }
    }
    
    func getAllDeliveryAddressAPICalled(){
        
        let parameters : Parameters = [
            "customer_id" : loggedInUser.shared.string_userID,
            "cacheignore" : "\(Int(NSDate().timeIntervalSince1970))" //To ignore cache from api...
            
        ]
        
        AppDelegate.showHUD(inView: self.view, message:"Loading....")
        Alamofire.request(Constants.apiURL + "webservice/addresslist.php", method: .post, parameters: parameters)
            .responseJSON { response in
                AppDelegate.hideHUD(inView: self.view)
                switch response.result {
                case .success:
                    if let json = response.result.value{
                        //print("JSON Response : \(json)")
                        guard let _ : NSDictionary = json as? NSDictionary else {
                            return
                        }
                        do {
                            // make sure this JSON is in the format we expect
                            let jsonData =  try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
                            let decoded = try JSONSerialization.jsonObject(with: jsonData, options: []) as! NSDictionary
                            // here "decoded" is of type `Any`, decoded from JSON data
                            guard let dataArray : NSArray = decoded["data"] as? NSArray else {
                                return
                            }
                            
                            if dataArray.count > 0 {
                                
                                let array_reversed : NSArray =  dataArray.reversed() as NSArray //last addedd address will show on top...
                                guard let dictAddress = array_reversed[0] as? NSDictionary else { return }
                                let userDefaults = UserDefaults.standard
                                let encodedData: Data = try NSKeyedArchiver.archivedData(withRootObject: dictAddress,requiringSecureCoding: false)
                                userDefaults.set(encodedData, forKey: "userDefaultDeliveryAddress")
                                userDefaults.synchronize()
                            }
                            else{
                                let userDefaults_DeliveryAddress = UserDefaults.standard
                                userDefaults_DeliveryAddress.removeObject(forKey:"userDefaultDeliveryAddress")
                                userDefaults_DeliveryAddress.synchronize()
                                
                            }
                        }
                        catch let error as NSError {
                            print("Failed to load: \(error.localizedDescription)")
                        }
                    }
                case .failure( _):
                    //Utility.showAlert(message: error.localizedDescription, controller: self)
                    Utility.showAlert(message: "حدث خطأ - يرجي اعادة المحاولة", controller: self) //Something went wrong, please try again.
                }
        }
    }
    
    func mobile_loginApi(loginTypeStr : String) {
        
        if textField_Mobile.text == "" {
            Utility.showAlert(message:"من فضلك أدخل رقم الجوال", controller: self)
            return
        }
        
        if textField_Mobile_Password.text == "" && loginTypeStr == "1"  {
            Utility.showAlert(message:"برجاء كتابة كلمة السر", controller: self) //Please enter password.
            return
        }
        let userDefaults = UserDefaults.standard
        let decoded : Data = userDefaults.data(forKey: "isguestMaskKeyAndCardIdFetched") ?? Data()
        var str_cartID = ""
        do {
            if let decodedData : Dictionary<String, Any> = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? Dictionary<String, Any> {
                if let crtId =  decodedData["cart_id"]{
                    str_cartID = String(format:"\(crtId)")
                }
            }
        }
        catch {
            print("Couldn't unarchive data")
        }
        
        var str_maskKey = ""
        do {
            if let decodedData : Dictionary<String, Any> = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? Dictionary<String, Any> {
                if let mskKey =  decodedData["mask_key"]{
                    str_maskKey = String(format:"\(mskKey)")
                }
            }
        } catch {
            print("Couldn't unarchive data")
        }
        
        if(textField_Mobile.isFirstResponder){textField_Mobile.resignFirstResponder()}
        if(textField_Mobile_Password.isFirstResponder){textField_Mobile_Password.resignFirstResponder()}
        
        
        //Country Code + Mobile Number...(i.e. +919773658682)
        guard let str_countryCode = textField_Mobile_Country.accessibilityHint?.trimmingCharacters(in: .whitespacesAndNewlines) else{
            return
        }
        guard var str_mobileNumber : String = textField_Mobile.text?.trimmingCharacters(in: .whitespacesAndNewlines) else{
            return
        }
        let str_mobileNumberWithOutStartingZero =  String(str_mobileNumber.prefix(upTo: str_mobileNumber.index(str_mobileNumber.startIndex, offsetBy: 1))) //Check 0 exist in begining with mobile number
        if str_mobileNumberWithOutStartingZero == "0"{//Remove 0 from starting, if exists...
            str_mobileNumber =  String(str_mobileNumber.dropFirst())}
        let str_mobileNumberWithCountryCode : String = str_countryCode +  str_mobileNumber
        
        
        let parameters : Parameters = [
            "email" : "",
            "password" : textField_Mobile_Password.text!,
            "cart_id" : str_cartID,
            "mask_key" : str_maskKey,
            "device_id" : "\(UIDevice.current.identifierForVendor?.uuidString ?? "")",
            "devicetype" : "ios",
            "mobile_number" : str_mobileNumberWithCountryCode,
            "login_type" :  loginTypeStr,// "1", // login_type (0 -> by_email, 1-> by_mobile, 2 -> by_otp
            "cacheignore" : "\(Int(NSDate().timeIntervalSince1970))" //To ignore cache from api...
        ]
        
        AppDelegate.showHUD(inView: self.view, message:"Loading....")
        Alamofire.request(Constants.apiURL + "webservice/customerapi.php", method: .get, parameters: parameters)
            .responseJSON { response in
                AppDelegate.hideHUD(inView: self.view)
                switch response.result {
                case .success:
                    if let json = response.result.value{
                        guard let _ : NSDictionary = json as? NSDictionary else {
                            return
                        }
                        do {
                            // make sure this JSON is in the format we expect
                            let jsonData =  try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
                            let decoded = try JSONSerialization.jsonObject(with: jsonData, options: []) as! NSDictionary
                            if decoded["msg"] as? String == "success"{
                                
                                guard let arrayUser : NSArray = decoded["data"] as? NSArray else {
                                    return
                                }
                                
                                if arrayUser.count > 0{
                                    
                                    let dict_UserRecord : NSDictionary = arrayUser[0] as! NSDictionary
                                    //                                    let userDefaults = UserDefaults.standard
                                    //                                    let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: dict_UserRecord)
                                    //                                    userDefaults.set(encodedData, forKey: "loggedInUserRecord")
                                    //                                    userDefaults.synchronize()
                                    do {
                                        let userDefaults = UserDefaults.standard
                                        let encodedData: Data = try NSKeyedArchiver.archivedData(withRootObject: dict_UserRecord, requiringSecureCoding: false)
                                        userDefaults.removeObject(forKey: "loggedInUserRecord")
                                        userDefaults.set(encodedData, forKey: "loggedInUserRecord")
                                        userDefaults.synchronize()
                                        
                                    } catch {
                                        print("Couldn't archive data")
                                    }
                                    
                                    loggedInUser.shared.initializeUserDetails(dicuserdetail: dict_UserRecord as! Dictionary<String, Any>)
                                    
                                    self.textField_Email.text = ""
                                    self.textField_Email_Password.text = ""
                                    self.textField_Mobile.text = ""
                                    self.textField_Mobile_Password.text = ""
                                    self.textField_Email.resignFirstResponder()
                                    self.textField_Email_Password.resignFirstResponder()
                                    self.removebuttonasfullScreenForOTP()
                                    self.menuListUIView.isHidden = false
                                    self.reloadTableView()
                                    
                                    self.loginAppsFlyer()
                                    self.getAllDeliveryAddressAPICalled()//Set default delivery address, if user has already set before this login...
                                    
                                    self.goToCartScreenAfterLogin()
                                }else{
                                    Utility.showAlert(message: decoded["msg"] as? String ??  "حدث خطأ - يرجي اعادة المحاولة", controller: self)//Something went wrong, please try again.
                                }
                                
                            }else{
                                Utility.showAlert(message: decoded["message"] as? String ??  "حدث خطأ - يرجي اعادة المحاولة", controller: self)//Something went wrong, please try again.
                            }
                        }
                        catch let error as NSError {
                            print("Failed to load: \(error.localizedDescription)")
                        }
                    }
                case .failure( _):
                    //Utility.showAlert(message: error.localizedDescription, controller: self)
                    Utility.showAlert(message: "حدث خطأ - يرجي اعادة المحاولة", controller: self) //Something went wrong, please try again.
                }
        }
    }
    
    func sendOtpApiCalled() {
        //Country Code + Mobile Number...(i.e. +919773658682)
        guard let str_countryCode = textField_Mobile_Country.accessibilityHint?.trimmingCharacters(in: .whitespacesAndNewlines) else{
            return
        }
        guard var str_mobileNumber = textField_Mobile.text?.trimmingCharacters(in: .whitespacesAndNewlines) else{
            return
        }
        
        let str_mobileNumberWithOutStartingZero =  String(str_mobileNumber.prefix(upTo: str_mobileNumber.index(str_mobileNumber.startIndex, offsetBy: 1))) //Check 0 exist in begining with mobile number
        if str_mobileNumberWithOutStartingZero == "0"{//Remove 0 from starting, if exists...
            str_mobileNumber =  String(str_mobileNumber.dropFirst())}
        let str_mobileNumberWithCountryCode : String = str_countryCode +  str_mobileNumber
        
        
        let parameters : Parameters = [
            "mobile" : str_mobileNumberWithCountryCode,
            "resend" : "0",
            "cacheignore" : "\(Int(NSDate().timeIntervalSince1970))", //To ignore cache from api...
        ]
        
        AppDelegate.showHUD(inView: self.view, message:"Loading....")
        Alamofire.request(Constants.mediaSourceURL + "vsms/otp/sendlogin/", method: .post, parameters: parameters)
            .responseJSON { response in
                AppDelegate.hideHUD(inView: self.view)
                switch response.result {
                case .success:
                    if let json = response.result.value{
                        //print("JSON Response : \(json)")
                        guard let _ : NSDictionary = json as? NSDictionary else {
                            return
                        }
                        do {
                            // make sure this JSON is in the format we expect
                            let jsonData =  try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
                            let decoded = try JSONSerialization.jsonObject(with: jsonData, options: []) as! NSDictionary
                            
                            if decoded["success"] as! Bool {
                                
                                self.showOTPView()
                                
                            }else{
                                Utility.showAlert(message: decoded["msg"] as? String ?? "فشل الارسال -اعادة المحاولة", controller: self)// OTP not sent, please try again
                                
                            }
                        }
                        catch let error as NSError {
                            print("Failed to load: \(error.localizedDescription)")
                        }
                    }
                case .failure( _):
                    //Utility.showAlert(message: error.localizedDescription, controller: self)
                    Utility.showAlert(message: "حدث خطأ - يرجي اعادة المحاولة", controller: self) //Something went wrong, please try again.
                }
        }
    }
    
    func resendOTPAPICalled() {
        guard let str_countryCode = textField_Mobile_Country.accessibilityHint?.trimmingCharacters(in: .whitespacesAndNewlines) else{
            return
        }
        guard var str_mobileNumber = textField_Mobile.text?.trimmingCharacters(in: .whitespacesAndNewlines) else{
            return
        }
        
        let str_mobileNumberWithOutStartingZero =  String(str_mobileNumber.prefix(upTo: str_mobileNumber.index(str_mobileNumber.startIndex, offsetBy: 1))) //Check 0 exist in begining with mobile number
        if str_mobileNumberWithOutStartingZero == "0"{//Remove 0 from starting, if exists...
            str_mobileNumber =  String(str_mobileNumber.dropFirst())}
        
        let str_mobileNumberWithCountryCode : String = str_countryCode +  str_mobileNumber
        
        
        let parameters : Parameters = [
            "mobile" : str_mobileNumberWithCountryCode,
            "resend" : "1",
            "cacheignore" : "\(Int(NSDate().timeIntervalSince1970))" //To ignore cache from api...
        ]
        
        AppDelegate.showHUD(inView: self.view, message:"Loading....")
        Alamofire.request(Constants.mediaSourceURL + "vsms/otp/sendlogin/", method: .post, parameters: parameters)
            .responseJSON { response in
                AppDelegate.hideHUD(inView: self.view)
                switch response.result {
                case .success:
                    if let json = response.result.value{
                        //print("JSON Response : \(json)")
                        
                        guard let _ : NSDictionary = json as? NSDictionary else {
                            return
                        }
                        
                        
                        do {
                            // make sure this JSON is in the format we expect
                            let jsonData =  try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
                            let decoded = try JSONSerialization.jsonObject(with: jsonData, options: []) as! NSDictionary
                            // here "decoded" is of type `Any`, decoded from JSON data
                            
                            if decoded["success"] as! Bool {
                                
                                iToast.show("تم ارسال الكود بنجاح")//OTP sent successfully
                                
                            }else{
                                
                                Utility.showAlert(message: decoded["msg"] as? String ?? "فشل الارسال -اعادة المحاولة", controller: self)//OTP not sent, please try again
                                
                            }
                        }
                        catch let error as NSError {
                            print("Failed to load: \(error.localizedDescription)")
                        }
                    }
                case .failure( _):
                    //Utility.showAlert(message: error.localizedDescription, controller: self)
                    Utility.showAlert(message: "حدث خطأ - يرجي اعادة المحاولة", controller: self) //Something went wrong, please try again.
                }
        }
    }
    
    func verifyOTPAPICalled() {
        guard let strOtp = textFieldEnterOTP.text?.trimmingCharacters(in: .whitespacesAndNewlines) else{
            return
        }
        //Country Code + Mobile Number...(i.e. +919773658682)
        guard let str_countryCode = textField_Mobile_Country.accessibilityHint?.trimmingCharacters(in: .whitespacesAndNewlines) else{
            return
        }
        guard var str_mobileNumber = textField_Mobile.text?.trimmingCharacters(in: .whitespacesAndNewlines) else{
            return
        }
        
        let str_mobileNumberWithOutStartingZero =  String(str_mobileNumber.prefix(upTo: str_mobileNumber.index(str_mobileNumber.startIndex, offsetBy: 1))) //Check 0 exist in begining with mobile number
        if str_mobileNumberWithOutStartingZero == "0"{//Remove 0 from starting, if exists...
            str_mobileNumber =  String(str_mobileNumber.dropFirst())}
        
        let str_mobileNumberWithCountryCode : String = str_countryCode +  str_mobileNumber
        
        let parameters : Parameters = [
            
            "mobile" : str_mobileNumberWithCountryCode,
            "otp" : strOtp,
            "cacheignore" : "\(Int(NSDate().timeIntervalSince1970))" //To ignore cache from api...
            
        ]
        
        AppDelegate.showHUD(inView: self.view, message:"Loading....")
        Alamofire.request(Constants.mediaSourceURL + "vsms/otp/verifylogin/", method: .post, parameters: parameters)
            .responseJSON { response in
                AppDelegate.hideHUD(inView: self.view)
                switch response.result {
                case .success:
                    if let json = response.result.value{
                        //print("JSON Response : \(json)")
                        guard let _ : NSDictionary = json as? NSDictionary else {
                            return
                        }
                        do {
                            // make sure this JSON is in the format we expect
                            let jsonData =  try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
                            let decoded = try JSONSerialization.jsonObject(with: jsonData, options: []) as! NSDictionary
                            // here "decoded" is of type `Any`, decoded from JSON data
                            
                            if decoded["success"] as! Bool {
                                self.mobile_loginApi(loginTypeStr : "2") // login with mobile number & otp (without password)
                            }else{
                                iToast.show(decoded["msg"] as? String)
                            }
                        } catch let error as NSError {
                            print("Failed to load: \(error.localizedDescription)")
                        }
                    }
                case .failure( _):
                    //Utility.showAlert(message: error.localizedDescription, controller: self)
                    Utility.showAlert(message: "حدث خطأ - يرجي اعادة المحاولة", controller: self) //Something went wrong, please try again.
                }
        }
    }
    
    func logoutAPICalled(){
        
        let userDefaults_loggedInUser = UserDefaults.standard
        userDefaults_loggedInUser.removeObject(forKey:"loggedInUserRecord")
        userDefaults_loggedInUser.synchronize()
        
        let userDefaults_DeliveryAddress = UserDefaults.standard
        userDefaults_DeliveryAddress.removeObject(forKey:"userDefaultDeliveryAddress")
        userDefaults_DeliveryAddress.synchronize()
        
        let userDefaults_PayfortTokenName = UserDefaults.standard
        userDefaults_PayfortTokenName.removeObject(forKey:"token_name")
        userDefaults_PayfortTokenName.synchronize()
        
        loggedInUser.shared.deinitializeAllUserDetails()
        
        if let tabItems = self.tabBarController?.tabBar.items {
            // In this case we want to modify the badge number of the first tab:
            let tabItem = tabItems[0]
            tabItem.badgeValue =  nil
        }
        
        self.menuListUIView.isHidden =  true
        self.reloadTableView()
        AppDelegate.cart_item_count = 0
        
    }
    
    //MARK: - UITableView DataSource
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       
        if tableView == tableViewForCountrySelection {
            return 65 }else { return 65}  
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tableViewForCountrySelection {
            return array_CountryList.count
        }
        else {
            return array_profileListTitle.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == tableViewForCountrySelection {
            let cellIdentifier = "SelectionCell"
            var cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier)
            if cell == nil {
                cell = tableviewCell(withReuseIdentifierSelection: cellIdentifier, tablview: tableView, with: indexPath)
            }
            
            self.configureCellSelection(cell, for: indexPath, withSection: indexPath.section, tableView: tableView)
            cell!.selectionStyle = .none
            return (cell ?? nil) ?? UITableViewCell()
            
        }else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "profileTableViewCell", for: indexPath) as! ProfileTableViewCell
            cell.view_backgroundShadow.setShadow(x: -5, y: -5, width: tableView_Profile.bounds.size.width - 80, height: 55, radius: 5)
            cell.view_backgroundShadow.cornerRadius = 5
            cell.label_Title.text = array_profileListTitle[indexPath.row] as? String
            cell.imageview_Icon.image = UIImage(named: array_iconsName[indexPath.row] as? String ?? "")
            return cell
        }
    }
    
    //MARK: UITableView Delegate
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
            if tableView == tableViewForCountrySelection {
                let dictEventDetail : NSDictionary = array_CountryList[indexPath.row] as! NSDictionary
                self.setCountryNameField(dictEventDetail)
                self.removePickerFromScreenForCountrySelection()
            }else {
                    if indexPath.row == 0 {
                        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "editProfileVC") as! EditProfileVC
                        vc.delegate_passwordChange = self
                        self.navigationController?.pushViewController(vc, animated: false)
                    }else if indexPath.row == 1 {
                        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "myWalletViewController") as! MyWalletViewController
                        self.navigationController?.pushViewController(vc, animated: false)
                    }else if indexPath.row == 2 {
                        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "myOrderVC") as! MyOrderVC
                        self.navigationController?.pushViewController(vc, animated: false)
                    }else if indexPath.row == 3 {
                        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "trackOrderVC") as! TrackOrderVC
                        self.navigationController?.pushViewController(vc, animated: false)
                    }else if indexPath.row == 4 {
                        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "allDeliveryAddressessVC") as! AllDeliveryAddressessVC
                        self.navigationController?.pushViewController(vc, animated: false)
                    }
    //                else if indexPath.row == 5 {
    //
    //                }
                    else if indexPath.row == 5 {
                        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "helpAndSupportVC") as? HelpAndSupportVC
                        self.navigationController?.pushViewController(vc!, animated: false)
                    }else if indexPath.row == 6 {
                        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "contactUsVC") as! ContactUsVC
                        self.navigationController?.pushViewController(vc, animated: false)
                    }else if indexPath.row == 7 {
                        self.logOutOptionClicked()
                    }
            }
            
        }
    
    //MARK: PasswordChangedSuccessfullyDelegate
    func passwordChangedSuccessfullyDelegate(){
        self.getguestTokenAPICall() //Need to reset mask key and cart id again....
        //self.logoutAPICalled()
    }
    
    func logOutOptionClicked(){
        let alertController = UIAlertController(title: Constants.msg_Alert, message: "هل أنت متأكد من تسجيل الخروج؟", preferredStyle: .alert)//Are you sure to logout?
        
        alertController.addAction(UIAlertAction(title: "لا", style: .default, handler: { action in
            switch action.style{
            case .default:
                print("default")
            case .cancel:
                print("cancel")
                
            case .destructive:
                print("destructive")
            @unknown default: break
                
            }}))
        
        alertController.addAction(UIAlertAction(title: "نعم", style: .default, handler: { action in
            switch action.style{
            case .default:
                print("default")
                
                //self.logoutAPICalled()
                self.getguestTokenAPICall() //Need to reset mask key and cart id again....
                
            case .cancel:
                print("cancel")
                
            case .destructive:
                print("destructive")
                
            @unknown default: break
                
            }}))
        
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    //MARK: User SignupSucessFully Delegate
    func userSignupSuccessfully(userEmail: String, password: String) {
        let userDefaults = UserDefaults.standard
        let decoded : Data = userDefaults.data(forKey: "isguestMaskKeyAndCardIdFetched") ?? Data()
        var str_cartID = ""
        do {
            
            if let decodedData : Dictionary<String, Any> = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? Dictionary<String, Any> {
                if let crtId =  decodedData["cart_id"]{
                    str_cartID = String(format:"\(crtId)")
                }
            }
        }
        catch {
            print("Couldn't unarchive data")
        }
        
        var str_maskKey = ""
        do {
            
            if let decodedData : Dictionary<String, Any> = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? Dictionary<String, Any> {
                if let mskKey =  decodedData["mask_key"]{
                    str_maskKey = String(format:"\(mskKey)")
                }
            }
        }
        catch {
            print("Couldn't unarchive data")
        }
        
        
        if(textField_Email.isFirstResponder){textField_Email.resignFirstResponder()}
        if(textField_Email_Password.isFirstResponder){textField_Email_Password.resignFirstResponder()}
        
        let parameters : Parameters = [
            
            "email" : userEmail,
            "password" : password,
            "cart_id" : str_cartID,
            "mask_key" : str_maskKey,
            "device_id" : "\(UIDevice.current.identifierForVendor?.uuidString ?? "")",
            "devicetype" : "ios",
            "mobile_number" : "",
            "login_type" : "0", // login_type (0 -> by_email, 1-> by_mobile, 2 -> by_otp
            "cacheignore" : "\(Int(NSDate().timeIntervalSince1970))" //To ignore cache from api...
        ]
        
        AppDelegate.showHUD(inView: self.view, message:"Loading....")
        Alamofire.request(Constants.apiURL + "webservice/customerapi.php", method: .get, parameters: parameters)
            .responseJSON { response in
                AppDelegate.hideHUD(inView: self.view)
                switch response.result {
                case .success:
                    if let json = response.result.value{
                        guard let _ : NSDictionary = json as? NSDictionary else {
                            return
                        }
                        do {
                            // make sure this JSON is in the format we expect
                            let jsonData =  try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
                            let decoded = try JSONSerialization.jsonObject(with: jsonData, options: []) as! NSDictionary
                            if decoded["msg"] as? String == "success"{
                                guard let arrayUser : NSArray = decoded["data"] as? NSArray else {
                                    return
                                }
                                
                                if arrayUser.count > 0{
                                    
                                    let dict_UserRecord : NSDictionary = arrayUser[0] as! NSDictionary
                                    //                                    let userDefaults = UserDefaults.standard
                                    //                                    let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: dict_UserRecord)
                                    //                                    userDefaults.set(encodedData, forKey: "loggedInUserRecord")
                                    //                                    userDefaults.synchronize()
                                    
                                    do {
                                        let userDefaults = UserDefaults.standard
                                        let encodedData: Data = try NSKeyedArchiver.archivedData(withRootObject: dict_UserRecord, requiringSecureCoding: false)
                                        userDefaults.removeObject(forKey: "loggedInUserRecord")
                                        userDefaults.set(encodedData, forKey: "loggedInUserRecord")
                                        userDefaults.synchronize()
                                    }
                                    catch {
                                        print("Couldn't archive data")
                                    }
                                    
                                    loggedInUser.shared.initializeUserDetails(dicuserdetail: dict_UserRecord as! Dictionary<String, Any>)
                                    
                                    self.textField_Email.text = ""
                                    self.textField_Email_Password.text = ""
                                    self.textField_Email.resignFirstResponder()
                                    self.textField_Email_Password.resignFirstResponder()
                                    self.menuListUIView.isHidden = false
                                    self.reloadTableView()
                                    
                                    self.goToCartScreenAfterLogin()
                                    
                                }else{
                                    Utility.showAlert(message: "لم تقم بتسجيل الدخول بشكل صحيح أو أن حسابك معطل مؤقتاً.", controller: self)//You are not logged in correctly or your account is temporarily disabled.
                                }
                            }else{
                                Utility.showAlert(message: decoded["message"] as? String ??  "حدث خطأ - يرجي اعادة المحاولة", controller: self)//Something went wrong, please try again.
                            }
                        }
                        catch let error as NSError {
                            print("Failed to load: \(error.localizedDescription)")
                        }
                    }
                case .failure( _):
                    //Utility.showAlert(message: error.localizedDescription, controller: self)
                    Utility.showAlert(message: "حدث خطأ - يرجي اعادة المحاولة", controller: self) //Something went wrong, please try again.
                }
        }
    }
    
    //MARK: UIButton Clicks
    @IBAction func button_emailLoggedInClicked(_ sender: UIButton) {
        
        
        if textField_Email.text == "" || self.isValidEmail(emailStr:textField_Email.text!) == false{
            Utility.showAlert(message:"الرجاء إدخال بريد إلكتروني صحيح", controller: self) //Please enter valid email.
            return
        }
        
        if textField_Email_Password.text == ""{
            Utility.showAlert(message:"برجاء كتابة كلمة السر", controller: self) //Please enter password.
            return
        }
        
        let userDefaults = UserDefaults.standard
        let decoded : Data = userDefaults.data(forKey: "isguestMaskKeyAndCardIdFetched") ?? Data()
        var str_cartID = ""
        do {
            if let decodedData : Dictionary<String, Any> = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? Dictionary<String, Any> {
                if let crtId =  decodedData["cart_id"]{
                    str_cartID = String(format:"\(crtId)")
                }
            }
        }
        catch {
            print("Couldn't unarchive data")
        }
        
        var str_maskKey = ""
        do {
            if let decodedData : Dictionary<String, Any> = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? Dictionary<String, Any> {
                if let mskKey =  decodedData["mask_key"]{
                    str_maskKey = String(format:"\(mskKey)")
                }
            }
        } catch {
            print("Couldn't unarchive data")
        }
        
        if(textField_Email.isFirstResponder){textField_Email.resignFirstResponder()}
        if(textField_Email_Password.isFirstResponder){textField_Email_Password.resignFirstResponder()}
        
        //Country Code + Mobile Number...(i.e. +919773658682)
        guard let str_countryCode = textField_Mobile_Country.accessibilityHint?.trimmingCharacters(in: .whitespacesAndNewlines) else{
            return
        }
        guard let str_mobileNumber = textField_Mobile.text?.trimmingCharacters(in: .whitespacesAndNewlines) else{
            return
        }
        let str_mobileNumberWithCountryCode : String = str_countryCode +  str_mobileNumber
        
        
        
        
        let parameters : Parameters = [
            "email" : textField_Email.text!,
            "password" : textField_Email_Password.text!,
            "cart_id" : str_cartID,
            "mask_key" : str_maskKey,
            "device_id" : "\(UIDevice.current.identifierForVendor?.uuidString ?? "")",
            "devicetype" : "ios",
            "mobile_number" : str_mobileNumberWithCountryCode,
            "login_type" : "0", // login_type (0 -> by_email, 1-> by_mobile, 2 -> by_otp
            "cacheignore" : "\(Int(NSDate().timeIntervalSince1970))" //To ignore cache from api...
        ]
        
        AppDelegate.showHUD(inView: self.view, message:"Loading....")
        Alamofire.request(Constants.apiURL + "webservice/customerapi.php", method: .get, parameters: parameters)
            .responseJSON { response in
                AppDelegate.hideHUD(inView: self.view)
                switch response.result {
                case .success:
                    if let json = response.result.value{
                        guard let _ : NSDictionary = json as? NSDictionary else {
                            return
                        }
                        do {
                            // make sure this JSON is in the format we expect
                            let jsonData =  try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
                            let decoded = try JSONSerialization.jsonObject(with: jsonData, options: []) as! NSDictionary
                            if decoded["msg"] as? String == "success"{
                                
                                guard let arrayUser : NSArray = decoded["data"] as? NSArray else {
                                    return
                                }
                                
                                if arrayUser.count > 0{
                                    
                                    let dict_UserRecord : NSDictionary = arrayUser[0] as! NSDictionary
                                    //                                    let userDefaults = UserDefaults.standard
                                    //                                    let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: dict_UserRecord)
                                    //                                    userDefaults.set(encodedData, forKey: "loggedInUserRecord")
                                    //                                    userDefaults.synchronize()
                                    do {
                                        
                                        let userDefaults = UserDefaults.standard
                                        let encodedData: Data = try NSKeyedArchiver.archivedData(withRootObject: dict_UserRecord, requiringSecureCoding: false)
                                        userDefaults.removeObject(forKey: "loggedInUserRecord")
                                        userDefaults.set(encodedData, forKey: "loggedInUserRecord")
                                        userDefaults.synchronize()
                                    }
                                    catch {
                                        print("Couldn't archive data")
                                    }
                                    
                                    loggedInUser.shared.initializeUserDetails(dicuserdetail: dict_UserRecord as! Dictionary<String, Any>)
                                    
                                    self.textField_Email.text = ""
                                    self.textField_Email_Password.text = ""
                                    self.textField_Email.resignFirstResponder()
                                    self.textField_Email_Password.resignFirstResponder()
                                    self.menuListUIView.isHidden = false
                                    self.reloadTableView()
                                    
                                    self.loginAppsFlyer()
                                    self.getAllDeliveryAddressAPICalled()//Set default delivery address, if user has already set before this login...
                                    
                                    self.goToCartScreenAfterLogin()
                                    
                                }else{
                                    Utility.showAlert(message: decoded["msg"] as? String ??  "حدث خطأ - يرجي اعادة المحاولة", controller: self)//Something went wrong, please try again.
                                }
                                
                            }else{
                                Utility.showAlert(message: decoded["message"] as? String ??  "حدث خطأ - يرجي اعادة المحاولة", controller: self)//Something went wrong, please try again.
                                
                            }
                        }
                        catch let error as NSError {
                            print("Failed to load: \(error.localizedDescription)")
                        }
                    }
                case .failure( _):
                    //Utility.showAlert(message: error.localizedDescription, controller: self)
                    Utility.showAlert(message: "حدث خطأ - يرجي اعادة المحاولة", controller: self) //Something went wrong, please try again.
                }
        }
    }
    
    // this method will check if user coming from checkout screen then user will redirect to checkout screen after login...
    func goToCartScreenAfterLogin() {
        if Constants.isComingFromCartScreen == true {
            Constants.isComingFromCartScreen = false
            selectTabBar(index: 0)
        }
    }
    
    @IBAction func button_mobileLoggedInClicked(_ sender: UIButton) {
        if self.segmentControl.selectedSegmentIndex == 2 { // login with otp
            if textField_Mobile.text == "" {
                Utility.showAlert(message:"من فضلك أدخل رقم الجوال", controller: self)
                return
            }
            self.showGetTheCodeAlert()
        }
        else {
            self.mobile_loginApi(loginTypeStr: "1") // login with mobile number & password
        }
    }
    
    
    
    //    @IBAction func button_mobileLoggedInOTPClicked(_ sender: UIButton) {
    //        if textField_Mobile.text == ""  || textField_Mobile.text!.count != maxlength_MobileNumberfield {
    //            Utility.showAlert(message:"من فضلك أدخل رقم الجوال", controller: self)
    //            return
    //        }
    //        self.showGetTheCodeAlert()
    //    }
    
    func showGetTheCodeAlert(){
//        let alertController = UIAlertController(title: "التأكد من صحة رقم الجوال", message: "", preferredStyle: .alert)//"Make sure the phone number is correct",
//
//        alertController.addAction(UIAlertAction(title: "الحصول على الكود", style: .default, handler: { action in //"Get the code",
//            switch action.style{
//            case .default:
//                print("default")
//                self.sendOtpApiCalled()
//            case .cancel:
//                print("cancel")
//
//            case .destructive:
//                print("destructive")
//
//            @unknown default: break
//
//            }}))
//        self.present(alertController, animated: true, completion: nil)
        
           let modalViewController = AlertViewSendOTPViewController()
                modalViewController.modalPresentationStyle = .overFullScreen // .overCurrentContext
        //        modalViewController.modalTransitionStyle =  UIModalTransitionStyle.flipHorizontal
                modalViewController.delegate = self
                self.present(modalViewController, animated: false)
    }
    
    @IBAction func button_signUPClicked(_ sender: UIButton) {
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "signUPVC") as? SignUPVC
        vc?.userSignupSuccessDelegate = self
        self.navigationController?.pushViewController(vc!, animated: false)
    }
    
    @IBAction func button_forgetYourPassword(_ sender: UIButton) {
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "forgotPasswordVC") as? ForgotPasswordVC
        self.navigationController?.pushViewController(vc!, animated: false)
        
    }
    
    @IBAction func buttonSearchClicked(_ sender: UIButton) {
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "searchVC") as! SearchVC
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    @IBAction func button_HelpAndSupport(_ sender: UIButton) {
        
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "helpAndSupportVC") as? HelpAndSupportVC
        self.navigationController?.pushViewController(vc!, animated: false)
    }
    
    //MARK: - For Choose Country Only
    func tableviewCell(withReuseIdentifierSelection identifier: String?, tablview: UITableView?, with indxPath: IndexPath?) -> UITableViewCell? {
        
        
        let cell = UITableViewCell(style: .default, reuseIdentifier: identifier)
        cell.backgroundColor = UIColor.clear
        //UILabel Selection TreatMent...
        
        let radioButtonImageView = UIImageView.init(frame: CGRect.init(x: 10, y: 17, width: 25, height: 25))
        radioButtonImageView.tag = TAGCOUNTRYRADIOBUTTON
        radioButtonImageView.isUserInteractionEnabled = false
        cell.contentView.addSubview(radioButtonImageView)
        
        let labelSelection = UILabel.init(frame: CGRect(x: 40, y: 0, width: (tablview?.bounds.size.width ?? 0.0) - 45, height: 65))
        labelSelection.textColor = UIColor.white
        labelSelection.tag = TAGCOUNTRYNAME
        labelSelection.numberOfLines = 0
        labelSelection.textAlignment = .right
        labelSelection.isUserInteractionEnabled = true
        cell.contentView.addSubview(labelSelection)
        
        return cell
    }
    
    func configureCellSelection(_ cell: UITableViewCell?, for indexPath: IndexPath?, withSection section: Int, tableView: UITableView?) {
        
        let dictCountry : NSDictionary = array_CountryList[indexPath!.row] as! NSDictionary
        let str_countryName : String = dictCountry["name"] as? String ?? ""
        var str_countrycelCode : String = dictCountry["cel_code"] as? String ?? ""
        str_countrycelCode =  str_countrycelCode.replacingOccurrences(of: "+", with: "")
        
        
        let labelSelectionEvent = cell?.contentView.viewWithTag(TAGCOUNTRYNAME) as? UILabel
        labelSelectionEvent?.text = "(" + str_countrycelCode + "+) " + str_countryName
        labelSelectionEvent?.textColor = UIColor.white
        
        let radioImageSelectionEvent = cell?.contentView.viewWithTag(TAGCOUNTRYRADIOBUTTON) as? UIImageView
        if labelSelectionEvent?.text == textField_Mobile_Country.text {
            radioImageSelectionEvent?.image = UIImage(named: "radio-on-white")
        }
        else {
            radioImageSelectionEvent?.image = UIImage(named: "radio-off-white")
        }
        
    }
    
    func setCountryNameField(_ dictCountry: NSDictionary) {
        
        //Country Name + Country Code...(i.e. India ( +91 )
        let str_countryName : String = dictCountry["name"] as? String ?? ""
        var str_countrycelCode : String = dictCountry["cel_code"] as? String ?? ""
        str_countrycelCode =  str_countrycelCode.replacingOccurrences(of: "+", with: "")
        textField_Mobile_Country.text = "(" + str_countrycelCode + "+) " + str_countryName
        textField_Mobile_Country.accessibilityLabel = dictCountry["code"] as? String ?? ""//Country Code i.e(SA,IN,etc...)
        textField_Mobile_Country.accessibilityHint = dictCountry["cel_code"] as? String //Cell code i.e.(+91,+966,+98,etc...)
        var str_placeHolder = dictCountry["placeholder"] as? String
        str_placeHolder =  str_placeHolder?.replacingOccurrences(of: " ", with: "")
        textField_Mobile.placeholder = str_placeHolder
        if let Maxlgth  = str_placeHolder?.count{
            maxlength_MobileNumberfield = Maxlgth
        }
        textField_Mobile.text = ""
    }
    
    
    //MARK: - UITextField Delegate
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if  textField == textField_Mobile_Country {
            self.showCountryList()
            return false
        }
        return true
        
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField ==  textField_Mobile {
            // Check for valid input characters
            if CharacterSet(charactersIn: "0123456789").isSuperset(of: CharacterSet(charactersIn: string)) {
                return true
            }else{return false}
            
        }
        else if textField == textFieldEnterOTP {
            if CharacterSet(charactersIn: "0123456789").isSuperset(of: CharacterSet(charactersIn: string)) {
                let currentString: NSString = textField.text! as NSString
                let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
                return newString.length <= 6
            }else{return false}
        }
        return true
        
    }
    
    func showCountryList(){
        if array_CountryList.count == 0 {
            //For Event...
            return
        }
        
        if (textField_Mobile.isFirstResponder){
            textField_Mobile.resignFirstResponder()
        }
        if (textField_Mobile_Password.isFirstResponder){
            textField_Mobile_Password.resignFirstResponder()
        }
        
        buttonasfullScreenForCountrySelection = UIButton()
        buttonasfullScreenForCountrySelection.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        buttonasfullScreenForCountrySelection.frame = CGRect(x: 0, y: 0, width: UIApplication.shared.keyWindow?.bounds.size.width ?? 0.0, height: UIApplication.shared.keyWindow?.bounds.size.height ?? 0.0)
        buttonasfullScreenForCountrySelection.addTarget(self, action: #selector(removePickerFromScreenForCountrySelection), for: .touchUpInside)
        UIApplication.shared.keyWindow?.addSubview(buttonasfullScreenForCountrySelection)
        
        
        let viewForShadow = UIView()
        if UIDevice.current.userInterfaceIdiom == .phone{
            
            viewForShadow.frame = CGRect(x: (buttonasfullScreenForCountrySelection.bounds.size.width - 350) / 2, y: (buttonasfullScreenForCountrySelection.bounds.size.height - 500) / 2, width: 350, height: 500)
            
        } else {
            
            viewForShadow.frame = CGRect(x: buttonasfullScreenForCountrySelection.bounds.size.width / 4, y: buttonasfullScreenForCountrySelection.bounds.size.height / 4, width: buttonasfullScreenForCountrySelection.bounds.size.width / 2, height: buttonasfullScreenForCountrySelection.bounds.size.height / 2)
            
        }
        viewForShadow.autoresizingMask = [.flexibleTopMargin, .flexibleBottomMargin, .flexibleLeftMargin, .flexibleRightMargin]
        buttonasfullScreenForCountrySelection.addSubview(viewForShadow)
        
        tableViewForCountrySelection = UITableView()
        tableViewForCountrySelection.tag = 100
        tableViewForCountrySelection.cellLayoutMarginsFollowReadableWidth = false
        tableViewForCountrySelection.frame = CGRect(x: 0, y: 0, width: viewForShadow.bounds.size.width, height: viewForShadow.bounds.size.height)
        tableViewForCountrySelection.delegate = self
        tableViewForCountrySelection.dataSource = self
        tableViewForCountrySelection.separatorStyle = UITableViewCell.SeparatorStyle.singleLine
        tableViewForCountrySelection.separatorColor = UIColor.gray
        tableViewForCountrySelection.tableFooterView = UIView(frame: CGRect.zero)
        tableViewForCountrySelection.backgroundColor = UIColor.black
        viewForShadow.addSubview(tableViewForCountrySelection)
        tableViewForCountrySelection.reloadData()
    }
    
    @objc func removePickerFromScreenForCountrySelection() {
        buttonasfullScreenForCountrySelection.removeFromSuperview()
    }
    
    //OTP Functionality...
    func showOTPView(){
        
        value_Timer = 30
        
        buttonasfullScreenForOTP = UIButton()
        buttonasfullScreenForOTP.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        buttonasfullScreenForOTP.frame = CGRect(x: 0, y: 0, width: UIApplication.shared.keyWindow?.bounds.size.width ?? 0.0, height: UIApplication.shared.keyWindow?.bounds.size.height ?? 0.0)
        buttonasfullScreenForOTP.addTarget(self, action: #selector(removebuttonasfullScreenForOTP), for: .touchUpInside)
        self.view.addSubview(buttonasfullScreenForOTP)
        
        let viewOTP = UIView()
        if UIDevice.current.userInterfaceIdiom == .pad{
            
            viewOTP.frame = CGRect(x: 20, y: (buttonasfullScreenForOTP.bounds.size.height - 352) / 2 - 64, width: buttonasfullScreenForOTP.bounds.size.width - 40, height: 280)
        } else {
            
            
            viewOTP.frame = CGRect(x: 20, y: (buttonasfullScreenForOTP.bounds.size.height - 216) / 2 - 40, width: buttonasfullScreenForOTP.bounds.size.width - 40, height: 240)
        }
        viewOTP.backgroundColor = UIColor.white
        buttonasfullScreenForOTP.addSubview(viewOTP)
        
        let labelTitle = UILabel()
        labelTitle.frame = CGRect(x: 30, y: 0, width: viewOTP.bounds.size.width - 60, height: 40)
        labelTitle.font = UIFont (name: Constants.helvetica_bold_font, size: 14)
        labelTitle.text = "التحقق من رقم الجوال" //"Mobile number verification
        labelTitle.numberOfLines = 1
        labelTitle.textAlignment = .center
        labelTitle.backgroundColor = UIColor.clear
        viewOTP.addSubview(labelTitle)
        
        
        let labelForgotPassword = UILabel()
        labelForgotPassword.frame = CGRect(x: 30, y: 40, width: viewOTP.bounds.size.width - 60, height: 60)
        guard let str_countryCode = textField_Mobile_Country.accessibilityHint?.trimmingCharacters(in: .whitespacesAndNewlines) else{
            return
        }
        guard let str_mobileNumber = textField_Mobile.text?.trimmingCharacters(in: .whitespacesAndNewlines) else{
            return
        }
        labelForgotPassword.text = "تم ارسال رسالة نصية لكم بالكود السري مكون من 4 ارقام" //A text message with a 4-digit verification code has been sent to
        //labelForgotPassword.text = "A text message with a 4-digit verification code has been sent to +919781116773)"
        labelForgotPassword.font = UIFont (name: Constants.helvetica_regular_font, size: 14)
        labelForgotPassword.numberOfLines = 3
        labelForgotPassword.textAlignment = .center
        labelForgotPassword.backgroundColor = UIColor.clear
        viewOTP.addSubview(labelForgotPassword)
        
        
        textFieldEnterOTP = UITextField()
        textFieldEnterOTP.frame = CGRect(x: 30, y: 100, width: viewOTP.bounds.size.width - 60, height: 40)
        textFieldEnterOTP.font = UIFont.systemFont(ofSize: 16)
        textFieldEnterOTP.placeholder = "برجاء كتابة رمز التحقق" //Please Enter OTP...
        textFieldEnterOTP.autocorrectionType = .no
        textFieldEnterOTP.autocapitalizationType = .none
        textFieldEnterOTP.backgroundColor = UIColor.white
        textFieldEnterOTP.keyboardType = .numberPad
        textFieldEnterOTP.clearButtonMode = .whileEditing
        textFieldEnterOTP.becomeFirstResponder()
        textFieldEnterOTP.layer.cornerRadius = 2
        viewOTP.addSubview(textFieldEnterOTP)
        let leftViewEmailId = UIView(frame: CGRect(x: 15, y: 0, width: 7, height: 26))
        leftViewEmailId.backgroundColor = UIColor.clear
        textFieldEnterOTP.leftView = leftViewEmailId
        textFieldEnterOTP.leftViewMode = .always
        textFieldEnterOTP.delegate = self

        label_ForTimerCountDown = UILabel()
        label_ForTimerCountDown.frame = CGRect(x: 30, y: 150, width: viewOTP.bounds.size.width - 60, height: 20)
        label_ForTimerCountDown.text = "اعادة ارسال الكود بعد \(value_Timer) ثانية" //"Resend after \(value_Timer) seconds"
        label_ForTimerCountDown.font = UIFont (name: Constants.helvetica_regular_font, size: 12)
        label_ForTimerCountDown.textColor = UIColor.lightGray
        label_ForTimerCountDown.numberOfLines = 1
        label_ForTimerCountDown.textAlignment = .center
        label_ForTimerCountDown.backgroundColor = UIColor.clear
        viewOTP.addSubview(label_ForTimerCountDown)
        
        buttonResendOTP = UIButton(type: .system)
        buttonResendOTP.frame = CGRect(x: 30, y: 180, width: viewOTP.bounds.size.width/2 - 40, height: 40)
        buttonResendOTP.setTitle("اعادة ارسال الكود", for: .normal) // Resend OTP
        buttonResendOTP.setTitleColor(UIColor.white, for: .normal)
        buttonResendOTP.layer.cornerRadius = 2
        buttonResendOTP.addTarget(self, action: #selector(buttonResendOTPClicked), for: .touchUpInside)
        viewOTP.addSubview(buttonResendOTP)
        self.enableUserInteractionReSenOTPButton(isEnable: false)
        
        
        buttonVerifyOTP = UIButton(type: .system)
        buttonVerifyOTP.frame = CGRect(x: viewOTP.bounds.size.width/2 + 10, y: 180, width: viewOTP.bounds.size.width/2 - 40, height: 40)
        buttonVerifyOTP.setTitle("ادخال الكود", for: .normal) //Verify OTP
        buttonVerifyOTP.setTitleColor(UIColor.white, for: .normal)
        buttonVerifyOTP.backgroundColor = Constants.default_ThemeColor
        buttonVerifyOTP.layer.cornerRadius = 2
        buttonVerifyOTP.addTarget(self, action: #selector(buttonVerifyOTPClicked), for: .touchUpInside)
        viewOTP.addSubview(buttonVerifyOTP)
        
        otp_Timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updatelabel_ForTimerCountDown), userInfo: nil, repeats: true)
        
    }
    
    @objc func buttonResendOTPClicked(){
        value_Timer = 30
        otp_Timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updatelabel_ForTimerCountDown), userInfo: nil, repeats: true)
        label_ForTimerCountDown.isHidden = false
        self.enableUserInteractionReSenOTPButton(isEnable: false)
        
        if(textFieldEnterOTP.isFirstResponder){textFieldEnterOTP.resignFirstResponder()}
        self.resendOTPAPICalled()
    }
    
    func enableUserInteractionReSenOTPButton(isEnable: Bool){
        if isEnable{
            buttonResendOTP.backgroundColor = Constants.default_ThemeColor
            buttonResendOTP.isUserInteractionEnabled = true
        }
        else{
            buttonResendOTP.backgroundColor = UIColor(red: 83/255, green: 83/255, blue: 83/255, alpha: 1.0)
            buttonResendOTP.isUserInteractionEnabled = false
        }
    }
    
    @objc func updatelabel_ForTimerCountDown(){
        if (otp_Timer != nil){
            value_Timer = value_Timer - 1
            label_ForTimerCountDown.text = "اعادة ارسال الكود بعد \(value_Timer) ثانية" //"Resend after \(value_Timer) seconds"
            if value_Timer == 0{
                otp_Timer!.invalidate()
                self.enableUserInteractionReSenOTPButton(isEnable: true)
                label_ForTimerCountDown.isHidden = true
            }
        }
    }
    
    @objc func buttonVerifyOTPClicked() {
        if textFieldEnterOTP.text == "" {
            Utility.showAlert(message:"يرجى ادخال الكود", controller: self)//Please enter OTP.
            return
        }
        if(textFieldEnterOTP.isFirstResponder){textFieldEnterOTP.resignFirstResponder()}
        self.verifyOTPAPICalled()
    }
    
    @objc func removebuttonasfullScreenForOTP() {
        if (otp_Timer != nil){
            otp_Timer?.invalidate()
        }
        buttonasfullScreenForOTP.removeFromSuperview()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

//for appsflyer
extension LoginVC {
    func loginAppsFlyer() {
        let productInfo : NSMutableDictionary =  NSMutableDictionary()
        productInfo.setValue(loggedInUser.shared.string_userID, forKey: "user_id")
        analyticsAppsFlyer.shared.AnalyticsEventLogin(paramas: productInfo)
    }
}


