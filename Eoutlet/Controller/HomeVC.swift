import Alamofire
import ImageSlideshow
import AppsFlyerLib
import SDWebImage
import FirebaseAnalytics

//First Category...
class FirstTopHeaderCategoryCollectionViewCell : UICollectionViewCell
{
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var label_Title: UILabel!
}

//Feature List...
class FeatureListCollectionViewCell: UICollectionViewCell
{
    @IBOutlet weak var imageView_Icon: UIImageView!
}

//Recommended...
class RecommendedCollectionViewCell : UICollectionViewCell
{
    @IBOutlet weak var imageView_Icon: UIImageView!
}

//New Arrivals...
class NewArrivalsCollectinViewCell: UICollectionViewCell
{
    @IBOutlet weak var imageView_Icon: UIImageView!
}

//Editors Choice...
class EditorsChoiceCollectionViewCell: UICollectionViewCell
{
    @IBOutlet weak var imageView_Icon: UIImageView!
}

class HomeVC: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,AppsFlyerTrackerDelegate {
    @IBOutlet weak var collectionView_TopHeaderCategory: UICollectionView!
    @IBOutlet weak var constraints_heightTopHeaderCategory: NSLayoutConstraint!
    @IBOutlet weak var collectionView_ProductList: UICollectionView!
    @IBOutlet weak var constraint_heightCollectinView_ProductList: NSLayoutConstraint!
    @IBOutlet weak var constraint_heightFeaturedUIView1: NSLayoutConstraint!
    @IBOutlet weak var constraint_heightFeaturedUIView2: NSLayoutConstraint!

    @IBOutlet weak var collectionView_FeatureList: UICollectionView!
    @IBOutlet weak var constraint_heightCollectinView_FeatureList: NSLayoutConstraint!

    @IBOutlet weak var collectionView_Recommended: UICollectionView!
    @IBOutlet weak var constraint_heightCollectinView_Recommended: NSLayoutConstraint!
    
    @IBOutlet weak var collectionView_NewArrivals: UICollectionView!
    @IBOutlet weak var constraint_heightCollectinView_NewArrivals: NSLayoutConstraint!
    @IBOutlet weak var newArrivalPageControl: UIPageControl!

    @IBOutlet weak var collectionView_EditorsChoice: UICollectionView!
    @IBOutlet weak var constraint_heightCollectinView_EditorsChoice: NSLayoutConstraint!

    @IBOutlet weak var newSliderSlideShow: ImageSlideshow!
    
    @IBOutlet weak var categoryLbl: UILabel!
    @IBOutlet weak var featuredLbl: UILabel!
    @IBOutlet weak var recommendedLbl: UILabel!
    @IBOutlet weak var newArrivalLbl: UILabel!
    @IBOutlet weak var ourEditorsChoiceLbl: UILabel!

    var array_AllSlidingBanners : NSMutableArray = NSMutableArray()
    var array_TopHeaderCategory : NSMutableArray = NSMutableArray()
    var indexPath_TopHeaderCategoryClicked: IndexPath = IndexPath(item: 1000, section: 0)
    var array_BannerList : NSMutableArray = NSMutableArray()
    var array_ProductList : NSMutableArray = NSMutableArray()
    var array_FeaturedList : NSMutableArray = NSMutableArray()
    var array_FeatureListCollectionView : NSMutableArray = NSMutableArray()
    var array_RecommendedList : NSMutableArray = NSMutableArray()
    var array_NewArrivals : NSMutableArray = NSMutableArray()
    var array_OurEditorChoice : NSMutableArray = NSMutableArray()
    
    @IBOutlet weak var mainBannerImageView: UIImageView!
    @IBOutlet weak var relatedProductListOfferImageView: UIImageView!
    
    @IBOutlet weak var featuredImageViewFirst: UIImageView!
    @IBOutlet weak var featuredImageViewSecond: UIImageView!
    @IBOutlet weak var featuredImageViewThird: UIImageView!
    @IBOutlet weak var featuredImageViewFourth: UIImageView!
    @IBOutlet weak var featuredImageViewFifth: UIImageView!
    @IBOutlet weak var featuredImageViewSixth: UIImageView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView_TopHeaderCategory.transform = CGAffineTransform(scaleX: -1, y: 1)
        collectionView_FeatureList.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
        collectionView_EditorsChoice.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)

        self.contentViewAppsFlyer()
        AppsFlyerTracker.shared().delegate = self
        self.categoryLbl.isHidden = true
        self.featuredLbl.isHidden = true
        self.recommendedLbl.isHidden = true
        self.newArrivalLbl.isHidden = true
        self.ourEditorsChoiceLbl.isHidden = true

        
        self.newArrivalPageControl.isHidden = true
        if UIDevice.current.userInterfaceIdiom == .phone  {
            self.constraint_heightCollectinView_NewArrivals.constant = 250
        }else {
            self.constraint_heightCollectinView_NewArrivals.constant = 400
        }
        
        
        //Disable swipe gesture for popviewcontroller...
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        
        //will get sliding banner, category from this api...
        self.getAllCategoryAndProductsAPICalled()
        
        //will get main banner, related product offer banner, new arrival product list from this api...
        self.getAllNewArrivalProductListAPICalled()
        
        //will get featured and recommended product from this api...
        self.getAllFeaturedAndRecommendedAPICalled()
        
        //Check if user already logged in...
        let userDefaults = UserDefaults.standard
        let decoded : Data = userDefaults.data(forKey: "loggedInUserRecord") ?? Data()
        do {
            if let decodedData : Dictionary<String, Any> = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? Dictionary<String, Any> {
                loggedInUser.shared.initializeUserDetails(dicuserdetail: decodedData)
            }
        }catch {
            print("Couldn't unarchive data")
        }
        
        /*
         Here we check current app version is updated or not(i.e force appupdate)
         */
        self.getAppVersionAPICall()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        self.AppsFlyerRedirection()//when user clicks any appsflyer ad, when app's instance is terminated/killed. Then after screen appering "HomeVC.swift" will redirect to AppsFlyer's detail page...
    }
    
    func AppsFlyerRedirection(){
        
        let delegate = UIApplication.shared.delegate as! AppDelegate
        let data = delegate.data_AppsFlyerCompaignData as NSDictionary
        
        if data != NSDictionary(){
            
            let opencartview : String = data["opencartview"] as? String ?? "" //If opencartview is appearing in data with "yes", it means need to open view cart tab...
            if opencartview == "yes"{ selectTabBar(index: 0)
                return }
            
            guard let _ = data["id"], let _ = data["name"] else{return}//If id and name keys are missing then no need to redirect productListVC...
            
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "productListVC") as! ProductListVC
            let dict_Cat : NSMutableDictionary =  NSMutableDictionary() //id, name fetching from data set in appsflyer's account for particular compaign...(i.e Attribution Link Parameters)
            dict_Cat.setValue("\(data["id"] as? String ?? "")", forKey: "id")
            dict_Cat.setValue("\(data["name"] as? String ?? "")", forKey: "name")
            vc.dict_subCategory = dict_Cat
            let string_Children =  data["children"] as? String ?? ""
            let array_ChildrenFromAppsFlyer =  string_Children.components(separatedBy: "|")
            if(string_Children != "" && array_ChildrenFromAppsFlyer.count > 0 ){
                
                let array_ChildrenFinal : NSMutableArray = NSMutableArray()
                for indx in 0 ..< array_ChildrenFromAppsFlyer.count  {
                    
                    let dict0 : NSMutableDictionary = NSMutableDictionary()
                    let indexData = array_ChildrenFromAppsFlyer[indx] as  String
                    let array_IdAndName =  indexData.components(separatedBy: ",")
                    
                    dict0.setValue(array_IdAndName[0], forKey: "id")//Default
                    dict0.setValue(array_IdAndName[1], forKey: "name")
                    array_ChildrenFinal.add(dict0)
                }
                vc.array_HeaderSubCategorytList.addObjects(from: array_ChildrenFinal as! [Any])
            }
            self.navigationController?.pushViewController(vc, animated: false)
            
            let delegate = UIApplication.shared.delegate as! AppDelegate
            delegate.data_AppsFlyerCompaignData = NSMutableDictionary()//Need to reset as blank record after appearing appsflyer's screen...
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.isHidden = true
        
        //Check valid maskKey and cart id already exits...
        let userDefaults = UserDefaults.standard
        guard let decoded = userDefaults.data(forKey: "isguestMaskKeyAndCardIdFetched")else{
            self.getguestTokenAPICall()
            return
        }
        do {
            guard let _ : Dictionary<String, Any> = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? Dictionary<String, Any> else {
                self.getguestTokenAPICall()
                return
            }
        }
        catch {
            print("Couldn't unarchive data")
        }
        self.addNavigationControllerActivities()
    }
    
    //MARK: Navigation Settings
    func addNavigationControllerActivities(){
        //set cart item count on home screen navigation & tab bar...
        if let tabItems = self.tabBarController?.tabBar.items {
            let tabItem = tabItems[0]
            tabItem.badgeValue = "\(AppDelegate.cart_item_count)"
        }
    }
    
    //MARK: API Calling...
    func getAppVersionAPICall(){
        AppDelegate.showHUD(inView: self.view, message:"Loading....")
        let parameters : Parameters = [
            "cacheignore" : "\(Int(NSDate().timeIntervalSince1970))" //To ignore cache from api...
        ]
        Alamofire.request(Constants.apiURL + "webservice/getcurrentversion.php", method: .get, parameters: parameters)
            .responseJSON { response in
                AppDelegate.hideHUD(inView: self.view)
                switch response.result {
                case .success:
                    if let json = response.result.value{
                        print("JSON Response : \(json)")
                        guard let _ : NSDictionary = json as? NSDictionary else {
                            return
                        }
                        do {
                            // make sure this JSON is in the format we expect
                            let jsonData =  try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
                            let decoded = try JSONSerialization.jsonObject(with: jsonData, options: []) as! NSDictionary
                            if decoded["msg"] as? String == "success"{
                                //check app current version with appstore version
                                let serverVersion = decoded["ios_version"] as! String
                                let currentVersion = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as! String
                                if currentVersion < serverVersion { //If installed app version is less that server updated app version then we have to forcefully update the app...
                                    self.forceUpdateAppPopUp(currentVersion : currentVersion, serverVersion: serverVersion)
                                }
                            }
                        }
                        catch _ as NSError {
                            
                        }
                    }
                case .failure( _):
                    //Utility.showAlert(message: error.localizedDescription, controller: self)
                    Utility.showAlert(message: "حدث خطأ - يرجي اعادة المحاولة", controller: self) //Something went wrong, please try again.
                }
            }
    }
    
    func forceUpdateAppPopUp(currentVersion: String, serverVersion: String){
        let alertController = UIAlertController(title: "رساله", message: "هناك إصدار جديد من هذا التطبيق متاح", preferredStyle: .alert)//Please update your app."
        alertController.addAction(UIAlertAction(title: "تحديث", style: .default, handler: { action in //Cancel
                                                    switch action.style{
                                                    case .cancel:
                                                        print("cancel")
                                                    case .destructive:
                                                        print("destructive")
                                                    case .default:
                                                        print(" default")
                                                        let url  = URL(string: "https://apps.apple.com/app/id1484601181")
                                                        
                                                        self.updateAppsFlyer(currentVersion: currentVersion, serverVersion: serverVersion)
                                                        if UIApplication.shared.canOpenURL(url!) {
                                                            UIApplication.shared.open(url!, options: [:], completionHandler: { action in
                                                                exit(0);
                                                            })
                                                        }
                                                    @unknown default:
                                                        print("unknown default")
                                                    }}))
        self.present(alertController, animated: true, completion: nil)
    }
    
    func getguestTokenAPICall(){
        
        AppDelegate.showHUD(inView: self.view, message:"Loading....")
        let parameters : Parameters = [
            "cacheignore" : "\(Int(NSDate().timeIntervalSince1970))" //To ignore cache from api...
        ]
        Alamofire.request(Constants.apiURL + "webservice/guesttoken.php", method: .get, parameters: parameters)
            .responseJSON { response in
                AppDelegate.hideHUD(inView: self.view)
                switch response.result {
                case .success:
                    if let json = response.result.value{
                        print("JSON Response : \(json)")
                        guard let _ : NSDictionary = json as? NSDictionary else {
                            return
                        }
                        do {
                            // make sure this JSON is in the format we expect
                            let jsonData =  try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
                            let decoded = try JSONSerialization.jsonObject(with: jsonData, options: []) as! NSDictionary
                            if decoded["msg"] as? String == "success" {
                                do {
                                    let userDefaults = UserDefaults.standard
                                    let encodedData: Data = try NSKeyedArchiver.archivedData(withRootObject: decoded, requiringSecureCoding: false)
                                    userDefaults.removeObject(forKey: "isguestMaskKeyAndCardIdFetched")
                                    userDefaults.set(encodedData, forKey: "isguestMaskKeyAndCardIdFetched")
                                    userDefaults.synchronize()
                                }
                                catch {
                                    print("Couldn't archive data")
                                }
                            }
                        }
                        catch _ as NSError {
                            
                        }
                    }
                case .failure( _):
                    //Utility.showAlert(message: error.localizedDescription, controller: self)
                    Utility.showAlert(message: "حدث خطأ - يرجي اعادة المحاولة", controller: self) //Something went wrong, please try again.
                }
            }
    }
    
    
    func setAllSlidingBannersFromServer(array_slidingBanners: NSArray){
        //autoscroll collectionview
        newSliderSlideShow.slideshowInterval = 3.5
        //        newSliderSlideShow.pageIndicatorPosition = .init(horizontal: .center, vertical: .customBottom(padding: 5))
        //        newSliderSlideShow.pageIndicator?.view.tintColor = UIColor.clear
        newSliderSlideShow.contentScaleMode = .scaleAspectFill
        //newSliderSlideShow.contentScaleMode = .scaleToFill
        //        let pageControl = UIPageControl()
        //        pageControl.currentPageIndicatorTintColor = UIColor.clear
        //        pageControl.pageIndicatorTintColor = UIColor.clear
        //        newSliderSlideShow.pageIndicator = pageControl
        newSliderSlideShow.pageIndicator = nil
        newSliderSlideShow.activityIndicator = DefaultActivityIndicator()
        //newSliderSlideShow.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi))
        //newSliderSlideShow.scrollView.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi))
        //newSliderSlideShow.scrollView.transform = CGAffineTransform(scaleX: -1, y: 1)
        let recognizer = UITapGestureRecognizer(target: self, action: #selector(didTapSliderImage))
        newSliderSlideShow.addGestureRecognizer(recognizer)
        newSliderSlideShow.currentPageChanged = { page in
        }
        //        let localSource = [BundleImageSource(imageString: "newsliderImage"), BundleImageSource(imageString: "newsliderImage1"), BundleImageSource(imageString: "newsliderImage2"), BundleImageSource(imageString: "newsliderImage3")]
        ////        var arr = [AlamofireSource]()
        ////        arr.append(AlamofireSource(urlString: PICTURE_URL + "service/" + serviceDetails.cover_photo)!)
        ////        for imageStr in serviceDetails.gallery {
        ////            arr.append(AlamofireSource(urlString: PICTURE_URL + "service/" + imageStr)!)
        ////        }
        //        self.newSliderSlideShow.setImageInputs(localSource)
        
        //        self.newSliderSlideShow.setImageInputs([
        //
        ////            ImageSource(image: (UIImage(named: "newsliderImage")?.flipImage())!),
        ////            ImageSource(image: (UIImage(named: "newsliderImage1")?.flipImage())!),
        ////            ImageSource(image: (UIImage(named: "newsliderImage2")?.flipImage())!),
        ////            ImageSource(image: (UIImage(named: "newsliderImage3")?.flipImage())!),
        //            ImageSource(image: image123!.flipImage()!),
        //            ImageSource(image: image123!.flipImage()!),
        //            ImageSource(image: image123!.flipImage()!),
        //            ImageSource(image: image123!.flipImage()!),
        //
        //
        //        ])
        //
        //        var arr_Banners = [ImageSource]()
        //     //   DispatchQueue.global(qos: .background).async {
        //
        //            for obj_Indx in 0 ..< array_slidingBanners.count{
        //                let dict : NSDictionary = array_slidingBanners[obj_Indx] as! NSDictionary
        //                let urlString = NSURL(string: dict["image"] as? String ?? "" )
        //
        //                var imgBanner: UIImage?
        //                if let imageData: NSData = NSData(contentsOf: urlString! as URL) {
        //                    imgBanner = UIImage(data: imageData as Data)
        //                }
        //
        //                let img_Source : ImageSource =  ImageSource(image: imgBanner!.flipImage()!)
        //                arr_Banners.append(img_Source)
        //            }
        //
        //    //        DispatchQueue.main.async {
        //                self.newSliderSlideShow.setImageInputs(arr_Banners)
        //                self.newSliderSlideShow.scrollView.transform = CGAffineTransform(scaleX: -1, y: 1)
        //         // }
        //       // }
        
        var arr_Banners = [ImageSource]()
        
        for obj_Indx in 0 ..< array_slidingBanners.count{
            
            let dict : NSDictionary = array_slidingBanners[obj_Indx] as! NSDictionary
            let urlString = NSURL(string: dict["image"] as? String ?? "" )
            
            var imgBanner: UIImage?
            if let imageData: NSData = NSData(contentsOf: urlString! as URL) {
                let image : UIImage = UIImage(data: imageData as Data) ?? UIImage()
                
                imgBanner = image.resizedImage(to: newSliderSlideShow.bounds.size)  // resize image as per home screen
            }
            
            let img_Source : ImageSource =  ImageSource(image: imgBanner!.flipImage()!)
            arr_Banners.append(img_Source)
        }
        
        self.newSliderSlideShow.setImageInputs(arr_Banners)
        newSliderSlideShow.scrollView.transform = CGAffineTransform(scaleX: -1, y: 1)
    }
    
    @objc func didTapSliderImage() {
        
        print(newSliderSlideShow.currentPage)
        if array_AllSlidingBanners.count > newSliderSlideShow.currentPage{
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "productListVC") as! ProductListVC
            let dict_Cat : NSDictionary = array_AllSlidingBanners[newSliderSlideShow.currentPage] as! NSDictionary
            vc.dict_subCategory = dict_Cat
            let array_ofSection = NSMutableArray()
            array_ofSection.addObjects(from: dict_Cat["children"] as? NSArray as! [Any])
            vc.array_HeaderSubCategorytList.addObjects(from: array_ofSection as! [Any])
            self.navigationController?.pushViewController(vc, animated: false)
        }
    }
    
    //MARK: API Calling...
    func getAllCategoryAndProductsAPICalled(){
        
        let parameters : Parameters = [
            "cacheignore" : "\(Int(NSDate().timeIntervalSince1970))" //To ignore cache from api...
        ]
        
        AppDelegate.showHUD(inView: self.view, message:"Loading....")
        Alamofire.request(Constants.apiURL+"webservice/categoryapi.php", method: .get, parameters: parameters)
            .responseJSON { response in
                AppDelegate.hideHUD(inView: self.view)
                switch response.result {
                case .success:
                    if let json = response.result.value{
                        guard let _ : NSDictionary = json as? NSDictionary else {
                            return
                        }
                        do {
                            // make sure this JSON is in the format we expect
                            let jsonData =  try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
                            let decoded = try JSONSerialization.jsonObject(with: jsonData, options: []) as! NSDictionary
                            // here "decoded" is of type `Any`, decoded from JSON data
                            
                            guard let dataArray : NSArray = decoded["categorydata"] as? NSArray else {
                                return
                            }
                            
                            if dataArray.count > 0 {
                                self.categoryLbl.isHidden = false
                                self.array_TopHeaderCategory.addObjects(from: (dataArray) as! [Any])
                            }
                            
                            
                            self.reloadTopHeaderCategories()
                            
                            
                            
                            let arrayof_slidingBanners = decoded["slidingbanners"] as? NSArray ?? NSArray()
                            if arrayof_slidingBanners.count > 0{//Need to set all sliding banners above new arrival section....
                                self.array_AllSlidingBanners.addObjects(from: (arrayof_slidingBanners ) as! [Any])
                                self.setAllSlidingBannersFromServer(array_slidingBanners: self.array_AllSlidingBanners)
                            }
                        }
                        catch let error as NSError {
                            print("Failed to load: \(error.localizedDescription)")
                        }
                    }
                case .failure( _):
                    Utility.showAlert(message: "حدث خطأ - يرجي اعادة المحاولة", controller: self) //Something went wrong, please try again.
                }
            }
    }
    func reloadTopHeaderCategories(){
        
        if self.array_TopHeaderCategory.count % 3 == 0 {
            let noOfLines = self.array_TopHeaderCategory.count / 3
            self.constraints_heightTopHeaderCategory.constant = CGFloat(50 * noOfLines) + CGFloat(10 * (noOfLines - 1))
        }else {
            let noOfLines = ((self.array_TopHeaderCategory.count / 3) + 1)
            self.constraints_heightTopHeaderCategory.constant = CGFloat(50 * noOfLines) + CGFloat(10 * (noOfLines - 1))
            // self.constraints_heightTopHeaderCategory.constant = 400
        }
        
        self.collectionView_TopHeaderCategory.reloadData()
        
    }
    
 
    func getAllNewArrivalProductListAPICalled(){
        
        let parameters : Parameters = [
            "cacheignore" : "\(Int(NSDate().timeIntervalSince1970))" //To ignore cache from api...
        ]
        
        AppDelegate.showHUD(inView: self.view, message:"Loading....")
        Alamofire.request(Constants.apiURL+"webservice/getnewarrival.php", method: .get, parameters: parameters)
            .responseJSON { response in
                AppDelegate.hideHUD(inView: self.view)
                switch response.result {
                case .success:
                    if let json = response.result.value{
                        guard let _ : NSDictionary = json as? NSDictionary else {
                            return
                        }
                        do {
                            // make sure this JSON is in the format we expect
                            let jsonData =  try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
                            let decoded = try JSONSerialization.jsonObject(with: jsonData, options: []) as! NSDictionary
                            // here "decoded" is of type `Any`, decoded from JSON data
                            
                            guard let arrayofBannerData : NSArray = decoded["banners"] as? NSArray else {
                                return
                            }
                            
                            if arrayofBannerData.count > 0 {
                                self.array_BannerList.removeAllObjects()
                                self.array_BannerList.addObjects(from: arrayofBannerData as! [Any])
                            }
                            
                            ////set main banner & related product list banner  images .....
                            if self.array_BannerList.count>0 {
                                let dict : NSDictionary = self.array_BannerList[0] as! NSDictionary
                                self.mainBannerImageView.sd_imageIndicator = SDWebImageActivityIndicator.gray
                                self.mainBannerImageView.sd_imageIndicator?.startAnimatingIndicator()
                                let url = NSURL(string: dict["img"] as? String ?? "")
                                self.mainBannerImageView.sd_setImage(with: url as URL?) { (image, error, cache, urls) in
                                    if (error != nil) {// Failed to load image
                                    } else {// Successful in loading image
                                        self.mainBannerImageView.sd_imageIndicator?.stopAnimatingIndicator()
                                    }}
                                
                                //image action
                                let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.mainBannerImageViewTapped))
                                self.mainBannerImageView.isUserInteractionEnabled = true
                                self.mainBannerImageView.addGestureRecognizer(tapGestureRecognizer)
                            }
                            if self.array_BannerList.count>1 {
                                let dict : NSDictionary = self.array_BannerList[1] as! NSDictionary
                                self.relatedProductListOfferImageView.sd_imageIndicator = SDWebImageActivityIndicator.gray
                                self.relatedProductListOfferImageView.sd_imageIndicator?.startAnimatingIndicator()
                                let url = NSURL(string: dict["img"] as? String ?? "")
                                self.relatedProductListOfferImageView.sd_setImage(with: url as URL?) { (image, error, cache, urls) in
                                    if (error != nil) {// Failed to load image
                                    } else {// Successful in loading image
                                        self.relatedProductListOfferImageView.sd_imageIndicator?.stopAnimatingIndicator()
                                    }}
                                
                                //image action
                                let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector( self.relatedProductListBannerImageViewTapped))
                                self.relatedProductListOfferImageView.isUserInteractionEnabled = true
                                self.relatedProductListOfferImageView.addGestureRecognizer(tapGestureRecognizer)
                            }
                            
                            //related product list set up...........................................
                            guard let dataArray : NSArray = decoded["data"] as? NSArray else {
                                return
                            }
                            
                            if dataArray.count > 0 {
                                self.array_ProductList.removeAllObjects()
                                self.array_ProductList.addObjects(from: dataArray as! [Any])
                            }
                            
                            self.collectionView_ProductList.reloadData()
                            
                        }
                        catch let error as NSError {
                            print("Failed to load: \(error.localizedDescription)")
                        }
                    }
                case .failure( _):
                    Utility.showAlert(message: "حدث خطأ - يرجي اعادة المحاولة", controller: self) //Something went wrong, please try again.
                }
            }
    }
    
    func getAllFeaturedAndRecommendedAPICalled(){
        let parameters : Parameters = [
            "cacheignore" : "\(Int(NSDate().timeIntervalSince1970))" //To ignore cache from api...
        ]
        AppDelegate.showHUD(inView: self.view, message:"Loading....")
        Alamofire.request(Constants.apiURL+"webservice/getnewrecommended.php", method: .get, parameters: parameters)
            .responseJSON { response in
                AppDelegate.hideHUD(inView: self.view)
                switch response.result {
                case .success:
                    if let json = response.result.value{
                        // print("JSON Response : \(json)")
                        guard let _ : NSDictionary = json as? NSDictionary else {
                            return
                        }
                        do {
                            // make sure this JSON is in the format we expect
                            let jsonData =  try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
                            let decoded = try JSONSerialization.jsonObject(with: jsonData, options: []) as! NSDictionary
                            // here "decoded" is of type `Any`, decoded from JSON data
                            // you can now cast it with the right type
                            //if let dictFromJSON = decoded as? [String:String] {
                            let arrayofGetDataFeatured = decoded["featured"] as! NSArray
                            if arrayofGetDataFeatured.count > 0{
                                self.featuredLbl.isHidden = false
                                self.array_FeaturedList.addObjects(from: (arrayofGetDataFeatured) as! [Any])
                            }
                            self.reloadFeatureList()
                         
                            self.array_FeatureListCollectionView.add(arrayofGetDataFeatured[3])
                            self.array_FeatureListCollectionView.add(arrayofGetDataFeatured[4])
                            self.array_FeatureListCollectionView.add(arrayofGetDataFeatured[5])
                            self.array_FeatureListCollectionView.add(arrayofGetDataFeatured[6])
                            self.reloadFeatureListCollectionView()

                            let arrayofGetDataRecommended = decoded["recommendedlist"] as! NSArray
                            if arrayofGetDataRecommended.count > 0{
                                self.recommendedLbl.isHidden = false
                                self.array_RecommendedList.addObjects(from: (arrayofGetDataRecommended ) as! [Any])
                            }
                            self.collectionView_Recommended.reloadData()
                            
                            let arrayofGetDataNewArrivals = decoded["newarrival"] as! NSArray
                            if arrayofGetDataNewArrivals.count > 0{
                                self.newArrivalLbl.isHidden = false
                                self.array_NewArrivals.addObjects(from: (arrayofGetDataNewArrivals ) as! [Any])
                                self.newArrivalPageControl.isHidden = false
                                self.newArrivalPageControl.currentPage = 0
                                self.newArrivalPageControl.numberOfPages = self.array_NewArrivals.count
                                self.collectionView_NewArrivals.reloadData()
                               
                            }
                            self.collectionView_NewArrivals.reloadData()
                            
                            
                            let arrayofGetDataOurEditorChoice = decoded["editor-choice"] as! NSArray
                            if arrayofGetDataOurEditorChoice.count > 0{
                                self.ourEditorsChoiceLbl.isHidden = false
                                self.array_OurEditorChoice.addObjects(from: (arrayofGetDataOurEditorChoice ) as! [Any])
                           
                            }
                            self.reloadOurEditorsChoice()


                        }
                        catch let error as NSError {
                            print("Failed to load: \(error.localizedDescription)")
                        }
                    }
                case .failure( _):
                    //Utility.showAlert(message: error.localizedDescription, controller: self)
                    Utility.showAlert(message: "حدث خطأ - يرجي اعادة المحاولة", controller: self) //Something went wrong, please try again.
                }
            }
    }
    
    func reloadFeatureList(){

        //set hegith for featured view...
        self.constraint_heightFeaturedUIView1.constant = UIScreen.main.bounds.size.width/1.25
        self.constraint_heightFeaturedUIView2.constant = UIScreen.main.bounds.size.width/1.25

        ////set featured images .....
        if self.array_FeaturedList.count>0 {
            let dict : NSDictionary = self.array_FeaturedList[0] as! NSDictionary
            self.featuredImageViewFirst.sd_imageIndicator = SDWebImageActivityIndicator.gray
            self.featuredImageViewFirst.sd_imageIndicator?.startAnimatingIndicator()
            let url = NSURL(string: dict["img"] as? String ?? "")
            self.featuredImageViewFirst.sd_setImage(with: url as URL?) { (image, error, cache, urls) in
                if (error != nil) {// Failed to load image
                } else {// Successful in loading image
                    self.featuredImageViewFirst.sd_imageIndicator?.stopAnimatingIndicator()
                }}
            
            //image action
            let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.featuredImageViewTapped))
            self.featuredImageViewFirst.addGestureRecognizer(tapGestureRecognizer)
        }
        
        if self.array_FeaturedList.count>1 {
            let dict : NSDictionary = self.array_FeaturedList[1] as! NSDictionary
            self.featuredImageViewSecond.sd_imageIndicator = SDWebImageActivityIndicator.gray
            self.featuredImageViewSecond.sd_imageIndicator?.startAnimatingIndicator()
            let url = NSURL(string: dict["img"] as? String ?? "")
            self.featuredImageViewSecond.sd_setImage(with: url as URL?) { (image, error, cache, urls) in
                if (error != nil) {// Failed to load image
                } else {// Successful in loading image
                    self.featuredImageViewSecond.sd_imageIndicator?.stopAnimatingIndicator()
                }}
            
            //image action
            let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.featuredImageViewTapped))
            self.featuredImageViewSecond.addGestureRecognizer(tapGestureRecognizer)
        }
        if self.array_FeaturedList.count>2{
            let dict : NSDictionary = self.array_FeaturedList[2] as! NSDictionary
            self.featuredImageViewThird.sd_imageIndicator = SDWebImageActivityIndicator.gray
            self.featuredImageViewThird.sd_imageIndicator?.startAnimatingIndicator()
            let url = NSURL(string: dict["img"] as? String ?? "")
            self.featuredImageViewThird.sd_setImage(with: url as URL?) { (image, error, cache, urls) in
                if (error != nil) {// Failed to load image
                } else {// Successful in loading image
                    self.featuredImageViewThird.sd_imageIndicator?.stopAnimatingIndicator()
                }}
            
            //image action
            let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.featuredImageViewTapped))
            self.featuredImageViewThird.addGestureRecognizer(tapGestureRecognizer)
        }
        
        if self.array_FeaturedList.count>7{
            let dict : NSDictionary = self.array_FeaturedList[7] as! NSDictionary
            self.featuredImageViewFourth.sd_imageIndicator = SDWebImageActivityIndicator.gray
            self.featuredImageViewFourth.sd_imageIndicator?.startAnimatingIndicator()
            let url = NSURL(string: dict["img"] as? String ?? "")
            self.featuredImageViewFourth.sd_setImage(with: url as URL?) { (image, error, cache, urls) in
                if (error != nil) {// Failed to load image
                } else {// Successful in loading image
                    self.featuredImageViewFourth.sd_imageIndicator?.stopAnimatingIndicator()
                }}
            
            //image action
            let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.featuredImageViewTapped))
            self.featuredImageViewFourth.addGestureRecognizer(tapGestureRecognizer)
        }
        
        if self.array_FeaturedList.count>8{
            let dict : NSDictionary = self.array_FeaturedList[8] as! NSDictionary
            self.featuredImageViewFifth.sd_imageIndicator = SDWebImageActivityIndicator.gray
            self.featuredImageViewFifth.sd_imageIndicator?.startAnimatingIndicator()
            let url = NSURL(string: dict["img"] as? String ?? "")
            self.featuredImageViewFifth.sd_setImage(with: url as URL?) { (image, error, cache, urls) in
                if (error != nil) {// Failed to load image
                } else {// Successful in loading image
                    self.featuredImageViewFifth.sd_imageIndicator?.stopAnimatingIndicator()
                }}
            
            //image action
            let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.featuredImageViewTapped))
            self.featuredImageViewFifth.addGestureRecognizer(tapGestureRecognizer)
        }
        
        if self.array_FeaturedList.count>9{
            let dict : NSDictionary = self.array_FeaturedList[9] as! NSDictionary
            self.featuredImageViewSixth.sd_imageIndicator = SDWebImageActivityIndicator.gray
            self.featuredImageViewSixth.sd_imageIndicator?.startAnimatingIndicator()
            let url = NSURL(string: dict["img"] as? String ?? "")
            self.featuredImageViewSixth.sd_setImage(with: url as URL?) { (image, error, cache, urls) in
                if (error != nil) {// Failed to load image
                } else {// Successful in loading image
                    self.featuredImageViewSixth.sd_imageIndicator?.stopAnimatingIndicator()
                }}
            
            //image action
            let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.featuredImageViewTapped))
            self.featuredImageViewSixth.addGestureRecognizer(tapGestureRecognizer)
        }
    }
    
    func reloadOurEditorsChoice(){

         if UIDevice.current.userInterfaceIdiom == .phone  {
             if self.array_OurEditorChoice.count % 2 == 0 {
                 let noOfLines = self.array_OurEditorChoice.count / 2
                 self.constraint_heightCollectinView_EditorsChoice.constant = CGFloat((Int(self.collectionView_EditorsChoice.frame.width) / 2) * noOfLines)
             }else {
                 let noOfLines = ((self.array_OurEditorChoice.count / 2) + 1)
                 self.constraint_heightCollectinView_EditorsChoice.constant = CGFloat((Int(self.collectionView_EditorsChoice.frame.width) / 2) * noOfLines)
             }
         } else {
             if self.array_OurEditorChoice.count % 3 == 0 {
                 let noOfLines = self.array_OurEditorChoice.count / 3
                 self.constraint_heightCollectinView_EditorsChoice.constant = CGFloat((Int(self.collectionView_EditorsChoice.frame.width) / 3) * noOfLines)
             }else {
                 let noOfLines = ((self.array_OurEditorChoice.count / 3) + 1)
                 self.constraint_heightCollectinView_EditorsChoice.constant = CGFloat((Int(self.collectionView_EditorsChoice.frame.width) / 3) * noOfLines)
             }
         }
        
        self.collectionView_EditorsChoice.reloadData()

     }
    
   func reloadFeatureListCollectionView(){
        
        if UIDevice.current.userInterfaceIdiom == .phone  {
            if self.array_FeatureListCollectionView.count % 2 == 0 {
                let noOfLines = self.array_FeatureListCollectionView.count / 2
                self.constraint_heightCollectinView_FeatureList.constant = CGFloat((Int(self.collectionView_FeatureList.frame.width) / 2) * noOfLines)
            }else {
                let noOfLines = ((self.array_FeatureListCollectionView.count / 2) + 1)
                self.constraint_heightCollectinView_FeatureList.constant = CGFloat((Int(self.collectionView_FeatureList.frame.width) / 2) * noOfLines)
            }
        } else {
            if self.array_FeatureListCollectionView.count % 3 == 0 {
                let noOfLines = self.array_FeatureListCollectionView.count / 3
                self.constraint_heightCollectinView_FeatureList.constant = CGFloat((Int(self.collectionView_FeatureList.frame.width) / 3) * noOfLines)
            }else {
                let noOfLines = ((self.array_FeatureListCollectionView.count / 3) + 1)
                self.constraint_heightCollectinView_FeatureList.constant = CGFloat((Int(self.collectionView_FeatureList.frame.width) / 3) * noOfLines)
            }
        }
       
       self.collectionView_FeatureList.reloadData()

    }
    //MARK: Images tab gesture...
    ////featured image tap gesture methods
    @objc func featuredImageViewTapped(gesture:UIGestureRecognizer){
    
        print(gesture.view!.tag)
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "productListVC") as! ProductListVC
        let dict_Record = self.array_FeaturedList[gesture.view!.tag] as! NSDictionary
        vc.dict_subCategory = dict_Record
        let array_ofSection = dict_Record["children"] as? NSArray
        vc.array_HeaderSubCategorytList.addObjects(from: array_ofSection as! [Any])
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    ////banner image tap gesture methods
    @objc func mainBannerImageViewTapped(){
        if self.array_BannerList.count > 0 {
            /////main banner click action
        }
    }
    
    @objc func relatedProductListBannerImageViewTapped(){
        if self.array_BannerList.count > 1 {
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "productListVC") as! ProductListVC
            vc.dict_subCategory = array_BannerList[1] as? NSDictionary ?? NSDictionary()
            self.navigationController?.pushViewController(vc, animated: false)
        }
    }
    
    //MARK: UICollection View...
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView.tag == 0 {//Top header category...
            return array_TopHeaderCategory.count
        }
        else if collectionView.tag == 1 { // product list
            return array_ProductList.count
        }else if collectionView.tag == 2 { // Feature list
            return array_FeatureListCollectionView.count
        }else if(collectionView.tag == 3){//Recommended list
            return array_RecommendedList.count
        }else if collectionView.tag == 4 {//New Arrivals...
            return array_NewArrivals.count
        }else if collectionView.tag == 5 { // Editors choice list
            return array_OurEditorChoice.count
        }
        
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if(collectionView.tag == 0){//FirstTopHeaderCategory
       
            let width =  Int((self.view.bounds.size.width-60) / 3)
            return CGSize(width: width, height: 50)
            
        }else  if(collectionView.tag == 1) {
            var width: CGFloat = 0
            if UIDevice.current.userInterfaceIdiom == .phone{
                width = (collectionView_ProductList.bounds.size.width - 40) / 1.65
                constraint_heightCollectinView_ProductList.constant = width + 75
                return CGSize(width: width, height: width + 75)
            }
            else {
                
                width = (collectionView_ProductList.bounds.size.width - 40) / 3
                constraint_heightCollectinView_ProductList.constant = width + 105
                return CGSize(width: width, height: width + 105)
            }
        }else if(collectionView.tag == 2) {//Featured List ...
            return UIDevice.current.userInterfaceIdiom == .phone ? CGSize(width: (collectionView.frame.width / 2), height: (collectionView.frame.width / 2)) : CGSize(width: (collectionView.frame.width / 3), height: (collectionView.frame.width / 3))
        }else if(collectionView.tag == 3) {//Recommended
            var width: CGFloat = 0
            if UIDevice.current.userInterfaceIdiom == .phone{
                width = (collectionView_Recommended.bounds.size.width - 40) / 1.5
                self.constraint_heightCollectinView_Recommended.constant = width - 10
                return CGSize(width: width, height: width - 10)
            }
            else{
                width = (collectionView_Recommended.bounds.size.width - 40) / 2.5
                self.constraint_heightCollectinView_Recommended.constant = width + 20
                return CGSize(width: width, height: width + 20)
            }
        }else if(collectionView.tag == 4) {//New Arrivals
            return CGSize(width: collectionView.frame.width, height: collectionView.frame.height - 10)
        }else if(collectionView.tag == 5) {//Our Editor's Choice
            return UIDevice.current.userInterfaceIdiom == .phone ? CGSize(width: (collectionView.frame.width / 2), height: (collectionView.frame.width / 2)) : CGSize(width: (collectionView.frame.width / 3), height: (collectionView.frame.width / 3))
        }else {
            return CGSize.zero
        }
    }
    
    internal func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if(collectionView.tag == 0){
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "firstTopHeaderCategoryCollectionViewCell", for: indexPath) as! FirstTopHeaderCategoryCollectionViewCell
            let dict : NSDictionary = array_TopHeaderCategory[indexPath.row] as! NSDictionary
            cell.label_Title.text = dict["name"] as? String
          
            if indexPath_TopHeaderCategoryClicked.item == indexPath.item {
                cell.cardView.backgroundColor = UIColor(red: 0.808, green: 0.624, blue: 0.376, alpha: 1)
                cell.label_Title.textColor = .white
            }else {
                cell.cardView.backgroundColor = .white
                cell.label_Title.textColor = .black
            }
            
            cell.cardView.dropShadow()
            cell.contentView.transform = CGAffineTransform(scaleX: -1, y: 1)
            return cell
            
        }else if(collectionView.tag == 1) {//Product List
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "productListCollectionViewCell", for: indexPath) as! ProductListCollectionViewCell
            let dict : NSDictionary = array_ProductList[indexPath.row] as! NSDictionary
            cell.label_Title.text = dict["name"] as? String
            cell.label_OldPrice.isHidden = false
            cell.label_NewFrontPrice.isHidden = true
            
            if let intValue = (dict["old_price"] as? Int) {
                
                if(intValue == 0){
                    cell.label_OldPrice.isHidden = true
                    cell.label_NewFrontPrice.isHidden = false
                }
                cell.label_OldPrice.text = String(format: "SAR %d", intValue)
                
            }else if let strValue = (dict["old_price"] as? String){
                
                if((strValue.replacingOccurrences(of: ",", with: "")) == "0"){
                    cell.label_OldPrice.isHidden = true
                    cell.label_NewFrontPrice.isHidden = false
                }
                cell.label_OldPrice.text = String(format: "SAR %d", (strValue.replacingOccurrences(of: ",", with: "") as NSString).integerValue)
                
            }else{
                print("some other data types exists.")
            }
            
            
            if let intValue = (dict["price"] as? Int) {
                cell.label_NewPrice.text = String(format: "SAR %d", intValue)
                cell.label_NewFrontPrice.text = String(format: "SAR %d", intValue)
            }else if let strValue = (dict["price"] as? String){
                cell.label_NewPrice.text = String(format: "SAR %d", (strValue.replacingOccurrences(of: ",", with: "") as NSString).integerValue)
                cell.label_NewFrontPrice.text = String(format: "SAR %d", (strValue.replacingOccurrences(of: ",", with: "") as NSString).integerValue)
            }else{
                print("some other data types exists.")
            }
            
            cell.imageView_Icon.sd_imageIndicator = SDWebImageActivityIndicator.gray
            cell.imageView_Icon.sd_imageIndicator?.startAnimatingIndicator()
            let url = NSURL(string: dict["img"] as? String ?? "")
            cell.imageView_Icon.sd_setImage(with: url as URL?) { (image, error, cache, urls) in
                if (error != nil) {// Failed to load image
                } else {// Successful in loading image
                    cell.imageView_Icon.sd_imageIndicator?.stopAnimatingIndicator()
                }}
            
            //set shadow on product list cell collectionview
            cell.layoutIfNeeded()
            cell.base_View.backgroundColor = UIColor.white //your background color...
            cell.base_View.cornerRadius = 5
            cell.base_View.setShadow(x: -5, y: -5, width: cell.base_View.bounds.width + 10, height: cell.base_View.bounds.height + 10, radius: 5)
            return cell
        }else if(collectionView.tag == 2){ //Feature List
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FeatureListCollectionViewCell", for: indexPath) as! FeatureListCollectionViewCell
            
            let dict : NSDictionary = array_FeatureListCollectionView[indexPath.row] as! NSDictionary
            
            cell.imageView_Icon.sd_imageIndicator = SDWebImageActivityIndicator.gray
            cell.imageView_Icon.sd_imageIndicator?.startAnimatingIndicator()
            let url = NSURL(string: dict["img"] as? String ?? "")
            cell.imageView_Icon.sd_setImage(with: url as URL?) { (image, error, cache, urls) in
                if (error != nil) {// Failed to load image
                } else {// Successful in loading image
                    cell.imageView_Icon.sd_imageIndicator?.stopAnimatingIndicator()
                }}
            cell.contentView.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)

            return cell

        }else if(collectionView.tag == 3){ //Recommended
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "recommendedCollectionViewCell", for: indexPath) as! RecommendedCollectionViewCell
            let dict : NSDictionary = array_RecommendedList[indexPath.row] as! NSDictionary
            cell.imageView_Icon.sd_imageIndicator = SDWebImageActivityIndicator.gray
            cell.imageView_Icon.sd_imageIndicator?.startAnimatingIndicator()
            let url = NSURL(string: dict["img"] as? String ?? "" )
            cell.imageView_Icon.sd_setImage(with: url as URL?) { (image, error, cache, urls) in
                if (error != nil) {// Failed to load image
                } else {// Successful in loading image
                    cell.imageView_Icon.sd_imageIndicator?.stopAnimatingIndicator()
                }}
            return cell
        }else if(collectionView.tag == 4){ //New Arrivals
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "NewArrivalsCollectinViewCell", for: indexPath) as! NewArrivalsCollectinViewCell
            let dict : NSDictionary = array_NewArrivals[indexPath.row] as! NSDictionary
            
            cell.imageView_Icon.sd_imageIndicator = SDWebImageActivityIndicator.gray
            cell.imageView_Icon.sd_imageIndicator?.startAnimatingIndicator()
            let url = NSURL(string: dict["img"] as? String ?? "")
            cell.imageView_Icon.sd_setImage(with: url as URL?) { (image, error, cache, urls) in
                if (error != nil) {// Failed to load image
                } else {// Successful in loading image
                    cell.imageView_Icon.sd_imageIndicator?.stopAnimatingIndicator()
                }}
            return cell

            
        }else if(collectionView.tag == 5){ //Our Editor's Choice
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "EditorsChoiceCollectionViewCell", for: indexPath) as! EditorsChoiceCollectionViewCell
            
            let dict : NSDictionary = array_OurEditorChoice[indexPath.row] as! NSDictionary
            
            cell.imageView_Icon.sd_imageIndicator = SDWebImageActivityIndicator.gray
            cell.imageView_Icon.sd_imageIndicator?.startAnimatingIndicator()
            let url = NSURL(string: dict["img"] as? String ?? "")
            cell.imageView_Icon.sd_setImage(with: url as URL?) { (image, error, cache, urls) in
                if (error != nil) {// Failed to load image
                } else {// Successful in loading image
                    cell.imageView_Icon.sd_imageIndicator?.stopAnimatingIndicator()
                }}
            cell.contentView.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
            return cell

        }
        return UICollectionViewCell()
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        if let collectionView = scrollView as? UICollectionView {
            if collectionView.tag == 4 {
                let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
                self.newArrivalPageControl.currentPage = Int(pageNumber)
            }
        }
}
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView.tag == 0{
           
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "productListVC") as! ProductListVC
            let dict_Record = self.array_TopHeaderCategory[indexPath.item] as! NSDictionary
            vc.dict_subCategory = dict_Record
            let array_ofSection = dict_Record["data"] as? NSArray
            vc.array_HeaderSubCategorytList.addObjects(from: array_ofSection as! [Any])
            self.navigationController?.pushViewController(vc, animated: false)
            indexPath_TopHeaderCategoryClicked = IndexPath(item: indexPath.item, section: 0)
            let deadlineTime = DispatchTime.now() + .seconds(0)
            DispatchQueue.main.asyncAfter(deadline: deadlineTime) {
                self.collectionView_TopHeaderCategory.reloadData()
            }
        }else if (collectionView.tag == 1){
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "productDetailVC") as? ProductDetailVC
            vc?.dict_Product = array_ProductList[indexPath.row] as! NSDictionary
            self.navigationController?.pushViewController(vc!, animated: false)
        }else if(collectionView.tag == 2){
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "productListVC") as! ProductListVC
            let dict_Record : NSDictionary = self.array_FeatureListCollectionView[indexPath.item] as! NSDictionary
            vc.dict_subCategory = dict_Record
            let array_ofSection = dict_Record["children"] as? NSArray
            vc.array_HeaderSubCategorytList.addObjects(from: array_ofSection as! [Any])
            self.navigationController?.pushViewController(vc, animated: true)
        }else if(collectionView.tag == 3){
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "productListVC") as! ProductListVC
            let dict_Record = self.array_RecommendedList[indexPath.row] as! NSDictionary
            vc.dict_subCategory = dict_Record
            let array_ofSection = dict_Record["children"] as? NSArray
            vc.array_HeaderSubCategorytList.addObjects(from: array_ofSection as! [Any])
            self.navigationController?.pushViewController(vc, animated: false)
        }else if(collectionView.tag == 4){
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "productListVC") as! ProductListVC
            let dict_Record : NSDictionary = self.array_NewArrivals[indexPath.item] as! NSDictionary
            vc.dict_subCategory = dict_Record
            let array_ofSection = dict_Record["children"] as? NSArray
            vc.array_HeaderSubCategorytList.addObjects(from: array_ofSection as! [Any])
            self.navigationController?.pushViewController(vc, animated: true)
        }else if(collectionView.tag == 5){
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "productListVC") as! ProductListVC
            let dict_Record : NSDictionary = self.array_OurEditorChoice[indexPath.item] as! NSDictionary
            vc.dict_subCategory = dict_Record
            let array_ofSection = dict_Record["children"] as? NSArray
            vc.array_HeaderSubCategorytList.addObjects(from: array_ofSection as! [Any])
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func labelWidth(categoryname: NSString, fontSize:Int) -> CGFloat {
        
        //Calculate the expected size based on the font and linebreak mode of your label
        var labelSize: CGSize = CGSize()
        labelSize = categoryname.size(withAttributes: [NSAttributedString.Key.font: UIFont(name: Constants.helvetica_bold_font, size: CGFloat(fontSize)) ?? 0])
        
        return labelSize.width
        
    }
    
    //MARK: - UIButton Clicks
    @IBAction func buttonSearchClicked(_ sender: UIButton) {
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "searchVC") as! SearchVC
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    
    @IBAction func buttonOffersClicked(_ sender: UIButton) {
        
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "productListVC") as! ProductListVC
        let dict_Cat : NSMutableDictionary = NSMutableDictionary()
        dict_Cat.setValue("492", forKey: "id")
        vc.dict_subCategory = dict_Cat
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    
    //MARK: - AppsFlyerTrackerDelegate
    func onConversionDataSuccess(_ installData: [AnyHashable: Any]) {
        
     
    }
    
    func onConversionDataFail(_ error: Error) {
       
    }
    
    func onAppOpenAttribution(_ attributionData: [AnyHashable : Any]) {//This delegate will call only when app's instance is alive...
      
        if let data : NSDictionary = attributionData as NSDictionary?{
            
            let deadlineTime = DispatchTime.now() + .seconds(1)
            DispatchQueue.main.asyncAfter(deadline: deadlineTime) {
                
                self.selectTabBar(index: 4)
                self.navigationController?.popToRootViewController(animated: false)
                
                let opencartview : String = data["opencartview"] as? String ?? "" //If opencartview is appearing in data with "yes", it means need to open view cart tab...
                if opencartview == "yes"{ self.selectTabBar(index: 0)
                    return }
                
                guard let _ = data["id"], let _ = data["name"] else{return}//If id and name keys are missing then no need to redirect productListVC...
                
                let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "productListVC") as! ProductListVC
                let dict_Cat : NSMutableDictionary =  NSMutableDictionary() //id, name fetching from data set in appsflyer's account for particular compaign...(i.e Attribution Link Parameters)
                dict_Cat.setValue("\(data["id"] as? String ?? "")", forKey: "id")
                dict_Cat.setValue("\(data["name"] as? String ?? "")", forKey: "name")
                vc.dict_subCategory = dict_Cat
                
                let string_Children =  data["children"] as? String ?? ""
                let array_ChildrenFromAppsFlyer =  string_Children.components(separatedBy: "|")
                if(string_Children != "" && array_ChildrenFromAppsFlyer.count > 0 ){
                    
                    let array_ChildrenFinal : NSMutableArray = NSMutableArray()
                    for indx in 0 ..< array_ChildrenFromAppsFlyer.count  {
                        
                        let dict0 : NSMutableDictionary = NSMutableDictionary()
                        let indexData = array_ChildrenFromAppsFlyer[indx] as  String
                        let array_IdAndName =  indexData.components(separatedBy: ",")
                        
                        dict0.setValue(array_IdAndName[0], forKey: "id")//Default
                        dict0.setValue(array_IdAndName[1], forKey: "name")
                        array_ChildrenFinal.add(dict0)
                    }
                    vc.array_HeaderSubCategorytList.addObjects(from: array_ChildrenFinal as! [Any])
                }
                self.navigationController?.pushViewController(vc, animated: false)
            }
        }
    }
    
    func onAppOpenAttributionFailure(_ error: Error) {
    }
}

//For Appsflyer
extension HomeVC {
    func contentViewAppsFlyer() {
        let productInfo : NSMutableDictionary =  NSMutableDictionary()
        productInfo.setValue(loggedInUser.shared.string_userID, forKey: "user_id")
        analyticsAppsFlyer.shared.AnalyticsEventContentView(paramas: productInfo)
    }
    
    func updateAppsFlyer(currentVersion: String, serverVersion: String) {
        let productInfo : NSMutableDictionary =  NSMutableDictionary()
        productInfo.setValue(currentVersion, forKey: "old_version")
        productInfo.setValue(serverVersion, forKey: "new_version")
        analyticsAppsFlyer.shared.AnalyticsEventUpdate(paramas: productInfo)
    }
}


