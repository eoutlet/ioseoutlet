
import UIKit
import Alamofire
import AppsFlyerLib
import StoreKit

class ThankYouVC: UIViewController {
    @IBOutlet weak var imagview_statusIcon: UIImageView!
    @IBOutlet weak var label_congratulation: UILabel!
    @IBOutlet weak var label_orderPlacedSuccessfully: UILabel!
    @IBOutlet weak var label_OrderId: UILabel!
    var string_orderStatus : String = ""
    var string_OrderId : String = ""
    var string_Price : String = ""
    var string_Coupon : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //post notification will called when thankyou screen show, using this gift checkbox will reset on cart screen.
        NotificationCenter.default.post(name: Notification.Name("giftBoxReset"), object: nil)

        
        AppDelegate.cart_item_count = 0  // we are clearing cart item when thankyou screen will come...
        
        if(string_orderStatus == "cancelled") {
            imagview_statusIcon.image = UIImage(named: "cross")
            label_congratulation.text = "طلب ملغي"
            label_orderPlacedSuccessfully.text = "نأسف لقد تم الغاء طلبك"
        }
        else if(string_orderStatus == "failed") {
            imagview_statusIcon.image = UIImage(named: "cross")
            label_congratulation.text = "فشل"
            label_orderPlacedSuccessfully.text = "آسف فشل طلبك"
        }else {
            self.purchaseAppsFlyer()
            self.eventPurchaseFirebaseAnalytics()
        }
        
        label_OrderId.text = "Order ID : \(string_OrderId)"
        
        self.navigationController?.navigationBar.isHidden = true
        
        if let tabItems = self.tabBarController?.tabBar.items {
            // In this case we want to modify the badge number of the first tab:
            let tabItem = tabItems[0]
            tabItem.badgeValue = "0"
        }
        self.getcustomerTokenAPICall()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(false)
        
        if(string_orderStatus == "success") || (string_orderStatus == "") {//If order status success then star rating screen will appear...
//        let modalViewController = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "starRatingOnThankuPageVC") as! StarRatingOnThankuPageVC
//            modalViewController.modalPresentationStyle = .overFullScreen // .overCurrentContext
//            self.present(modalViewController, animated: true)
            SKStoreReviewController.requestReview()
        }
    }
    
    @IBAction func ratingButtonClicked(_ sender: UIButton) { 
        
    }
    
    //MARK: API Calling...
    func getcustomerTokenAPICall(){
        AppDelegate.showHUD(inView: self.view, message:"Loading....")
        let parameters : Parameters = [
            "customer_id" : loggedInUser.shared.string_userID,
            "cacheignore" : "\(Int(NSDate().timeIntervalSince1970))" //To ignore cache from api...
        ]
        Alamofire.request(Constants.apiURL + "webservice/customertoken.php", method: .get, parameters: parameters)
            .responseJSON { response in
                AppDelegate.hideHUD(inView: self.view)
                switch response.result {
                case .success:
                    if let json = response.result.value{
                        print("JSON Response : \(json)")
                        guard let _ : NSDictionary = json as? NSDictionary else {
                            return
                        }
                        do {
                            // make sure this JSON is in the format we expect
                            let jsonData =  try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
                            let decoded = try JSONSerialization.jsonObject(with: jsonData, options: []) as! NSDictionary
                            if decoded["msg"] as? String == "success"{
                                let userDefaults = UserDefaults.standard
                                do {
                                    let encodedData: Data = try NSKeyedArchiver.archivedData(withRootObject: decoded, requiringSecureCoding: false)
                                    userDefaults.removeObject(forKey: "isguestMaskKeyAndCardIdFetched")
                                    userDefaults.set(encodedData, forKey: "isguestMaskKeyAndCardIdFetched")
                                    userDefaults.synchronize()
                                }
                                catch {
                                    print("Couldn't save data")
                                }
                                // self.logoutAPICalled() //After successful getting mask key and cart id, will logout from account...
                            }
                        }
                        catch _ as NSError {
                            
                        }
                    }
                case .failure( _):
                    //Utility.showAlert(message: error.localizedDescription, controller: self)
                    Utility.showAlert(message: "حدث خطأ - يرجي اعادة المحاولة", controller: self) //Something went wrong, please try again.
                }
        }
    }
}

//for appsflyer
extension ThankYouVC {
    func purchaseAppsFlyer() {
        let productInfo : NSMutableDictionary =  NSMutableDictionary()
        productInfo.setValue("SAR", forKey: "currency")
        productInfo.setValue(string_Price, forKey: "price")
        productInfo.setValue(string_orderStatus, forKey: "order_status")
        productInfo.setValue(loggedInUser.shared.string_userID, forKey: "user_id")
        productInfo.setValue(string_OrderId, forKey: "order_id")
        productInfo.setValue(string_Coupon, forKey: "coupon")
        productInfo.setValue(string_Price, forKey: "revenue")
        analyticsAppsFlyer.shared.AnalyticsEventPurchase(paramas: productInfo)
    }
    
    func eventPurchaseFirebaseAnalytics(){
        let productInfo : NSMutableDictionary =  NSMutableDictionary()
        productInfo.setValue(string_Coupon, forKey: "coupon")
        productInfo.setValue("SAR", forKey: "currency")
        productInfo.setValue(Int(string_Price), forKey: "value")
        productInfo.setValue("", forKey: "tax")
        productInfo.setValue("", forKey: "shipping")
        productInfo.setValue(string_OrderId, forKey: "transactionid") //order id is transactionid here...
        analyticsFirebase.shared.AnalyticsEventEcommercePurchase(paramas: productInfo as NSDictionary)
    }
}
