//
//  Wallet3DSecureURLWebViewVC.swift
//  Eoutlet
//
//  Created by Sudhir Rohilla on 23/06/20.
//  Copyright © 2020 Unyscape. All rights reserved.
//

import UIKit
import Alamofire
import WebKit

class Checkout3DSecureURLWebViewVC: UIViewController, WKNavigationDelegate {

    @IBOutlet weak var threeDSecureWebView: WKWebView!
    var str_threeDSecureURL = ""
    var str_orderID = ""
    var is_Partial = ""
    var amount_Final = 0
    var str_Coupon_Code = ""
    var tokenNameStr = ""
    
    var timer = Timer()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        threeDSecureWebView.navigationDelegate = self
        threeDSecureWebView.scrollView.bounces = false

        self.loadUrlOnWebView()
        timer = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(self.getOrderStatus), userInfo: nil, repeats: true)
        
        // Do any additional setup after loading the view.
        
    }
    override func viewWillAppear(_ _animated: Bool) {
           super.viewWillAppear(_animated)
           self.navigationController?.navigationBar.isHidden = true
       }
       
       override func viewDidAppear(_ animated: Bool) {
           super.viewDidAppear(true)
       
        AppDelegate.showHUD(inView: self.view, message:"Loading....")
       }
    
    override func viewDidDisappear(_ animated: Bool) {
            self.timer.invalidate()
    }
    
    func loadUrlOnWebView() {
           
           guard let myURL = URL(string: self.str_threeDSecureURL) else { return}
           let myRequest = URLRequest(url: myURL)
           self.threeDSecureWebView.load(myRequest)
           
       }
   
    //MARK: API Calling...
    @objc func getOrderStatus() {
            
            let parameters : Parameters = [
                "order_id" : self.str_orderID,
                "token_name" : tokenNameStr,
                "is_wallet" : "0",
                "devicetype" : "ios",
                "cacheignore" : "\(Int(NSDate().timeIntervalSince1970))" //To ignore cache from api...
            ]
             
            Alamofire.request(Constants.apiURL+"webservice/checkout_newpaymentstatus.php", method: .post, parameters: parameters)
                .responseJSON { response in
                    switch response.result {
                    case .success:
                        if let json = response.result.value{
                            guard let _ : NSDictionary = json as? NSDictionary else {
                                return
                            }
                            do {
                                // make sure this JSON is in the format we expect
                                let jsonData =  try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
                                let decoded = try JSONSerialization.jsonObject(with: jsonData, options: []) as! NSDictionary
                                // here "decoded" is of type `Any`, decoded from JSON data
                                
                                if decoded["msg"] as? String == "success"{
                                    
                                    AppDelegate.hideHUD(inView: self.view)
                                    self.timer.invalidate()
                                    let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "thankYouVC") as! ThankYouVC
                                    vc.string_orderStatus = "success"
                                    vc.string_OrderId = "\(self.str_orderID)"
                                    vc.string_Price = "\(self.amount_Final)"
                                    vc.string_Coupon = "\(self.str_Coupon_Code)"
                                    self.navigationController?.pushViewController(vc, animated: false)
                                    
                                }else if(decoded["msg"] as? String == "failed"){
                                    
                                    
                                    self.orderStatusUpdateApiCalled(order_id: "\(self.str_orderID)", status: "failed", transactionId: "")

                                    AppDelegate.hideHUD(inView: self.view)
                                    self.timer.invalidate()
                                    let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "thankYouVC") as! ThankYouVC
                                    vc.string_orderStatus = "failed"
                                    vc.string_OrderId = "\(self.str_orderID)"
                                    vc.string_Price = "\(self.amount_Final)"
                                    vc.string_Coupon = "\(self.str_Coupon_Code)"
                                    self.navigationController?.pushViewController(vc, animated: false)
                                    
                                }
                                
                            }
                            catch _ as NSError {
                            }
                        }
                    case .failure( _): break
                       // Utility.showAlert(message: "حدث خطأ - يرجي اعادة المحاولة", controller: self) //Something went wrong, please try again.
                    }
            }
        }
    
    func orderStatusUpdateApiCalled(order_id: String, status: String, transactionId : String){
       
           let parameters : Parameters = [
              
            "order_id" : self.str_orderID,
            "token_name" : tokenNameStr,
            "is_wallet" : "0",
            "devicetype" : "ios",
            "cacheignore" : "\(Int(NSDate().timeIntervalSince1970))" //To ignore cache from api...
           ]
           
           Alamofire.request(Constants.apiURL+"webservice/checkout_statusupdate.php", method: .post, parameters: parameters)
               .responseJSON { response in
                   AppDelegate.hideHUD(inView: self.view)
                   switch response.result {
                   case .success:
                       if let json = response.result.value{
                           guard let _ : NSDictionary = json as? NSDictionary else {
                               return
                           }
                           do {
                               // make sure this JSON is in the format we expect
                               let jsonData =  try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
                               let decoded = try JSONSerialization.jsonObject(with: jsonData, options: []) as! NSDictionary
                               // here "decoded" is of type `Any`, decoded from JSON data
                               if decoded["msg"] as? String == "success"{
                                 print("success")  //Sucessfully status updated...

                               }
                           }
                           catch let error as NSError {
                               print("Failed to load: \(error.localizedDescription)")
                           }
                       }
                   case .failure( _):
                       
                    print("حدث خطأ - يرجي اعادة المحاولة")  //Something went wrong, please try again.

                     //  self.showAlertForMoveBackToCart(message: "حدث خطأ - يرجي اعادة المحاولة")  //Something went wrong, please try again.
                   }
           }
       }
       
    
        
  //MARK: - WKWebView Delegates
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        DispatchQueue.main.async {
            AppDelegate.hideHUD(inView: self.view)
        }
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        DispatchQueue.main.async {
            AppDelegate.hideHUD(inView: self.view)
        }
       
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        
        if navigationAction.navigationType == WKNavigationType.linkActivated {
            if let url = navigationAction.request.url {
                decisionHandler(.cancel)
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        }
        else {
            decisionHandler(.allow)
        }
    }
   
}
