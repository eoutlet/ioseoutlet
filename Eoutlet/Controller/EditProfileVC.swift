import Alamofire

protocol passwordChangeDelegate {
    
    func passwordChangedSuccessfullyDelegate()
    
}

class EditProfileVC: UIViewController,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate{
    var delegate_passwordChange : passwordChangeDelegate?
    var array_CountryList : NSMutableArray = NSMutableArray()
    var maxlength_MobileNumberfield : Int = 0
    var buttonasfullScreenForCountrySelection : UIButton = UIButton()
    var tableViewForCountrySelection  : UITableView = UITableView()
    let TAGCOUNTRYNAME = 4
    let TAGCOUNTRYRADIOBUTTON = 5
    
    @IBOutlet weak var label_firstlastName: UILabel!
    @IBOutlet weak var label_address: UILabel!
    
    @IBOutlet weak var textField_firstName: UITextField!
    @IBOutlet weak var textField_lastName: UITextField!
    @IBOutlet weak var textField_Email: UITextField!
    @IBOutlet weak var textField_Country: UITextField!
    @IBOutlet weak var textField_mobileNumber: UITextField!
    @IBOutlet weak var textField_NewPassword: UITextField!
    @IBOutlet weak var textField_confirmPassword: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.getProfileAPICalled()
    }
    
    override func viewWillAppear(_ _animated: Bool) {
        super.viewWillAppear(_animated)
        self.navigationController?.navigationBar.isHidden = true
    }
    
    //MARK: API Calling...
    func getProfileAPICalled(){
        let parameters : Parameters = [
            "cacheignore" : "\(Int(NSDate().timeIntervalSince1970))" //To ignore cache from api...
        ]
        AppDelegate.showHUD(inView: self.view, message:"Loading....")
        Alamofire.request(Constants.apiURL + "webservice/getprofile.php?email=\(loggedInUser.shared.string_Email)", method: .get, parameters: parameters)
            .responseJSON { response in
                AppDelegate.hideHUD(inView: self.view)
                switch response.result {
                case .success:
                    if let json = response.result.value{
                        //print("JSON Response : \(json)")
                        guard let _ : NSDictionary = json as? NSDictionary else {
                            return
                        }
                        do {
                            // make sure this JSON is in the format we expect
                            let jsonData =  try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
                            let decoded = try JSONSerialization.jsonObject(with: jsonData, options: []) as! NSDictionary
                            if decoded["msg"] as? String == "success"{
                                guard let arrayUser : NSArray = decoded["data"] as? NSArray else {
                                    return
                                }
                                if arrayUser.count > 0{
                                    let dict_UserRecord : NSDictionary = arrayUser[0] as! NSDictionary
                                    do {
                                        let userDefaults = UserDefaults.standard
                                        let encodedData: Data = try NSKeyedArchiver.archivedData(withRootObject: dict_UserRecord, requiringSecureCoding: false)
                                        userDefaults.removeObject(forKey: "loggedInUserRecord")
                                        userDefaults.set(encodedData, forKey: "loggedInUserRecord")
                                        userDefaults.synchronize()
                                    }
                                    catch {
                                        print("Couldn't archive data")
                                    }
                                    
                                    loggedInUser.shared.initializeUserDetails(dicuserdetail: dict_UserRecord as! Dictionary<String, Any>)
                                    
                                    self.label_firstlastName.text =  loggedInUser.shared.string_firstName + " " + loggedInUser.shared.string_lastName
                                    self.label_address.text = loggedInUser.shared.string_Email
                                    self.textField_firstName.text =  loggedInUser.shared.string_firstName
                                    self.textField_lastName.text =  loggedInUser.shared.string_lastName
                                    self.textField_Email.text =  loggedInUser.shared.string_Email
                                    self.textField_Country.text =  loggedInUser.shared.string_country
                                    self.textField_mobileNumber.text =  loggedInUser.shared.string_mobileNumber
                                    
                                    self.getAllCountriesAPICalled()
                                }
                                else{
                                    Utility.showAlert(message: decoded["msg"] as? String ??  "حدث خطأ - يرجي اعادة المحاولة", controller: self)//Something went wrong, please try again.
                                }
                                
                            }else{
                                Utility.showAlert(message: decoded["message"] as? String ??  "حدث خطأ - يرجي اعادة المحاولة", controller: self)//Something went wrong, please try again.
                                
                            }
                        }
                        catch let error as NSError {
                            print("Failed to load: \(error.localizedDescription)")
                        }
                    }
                case .failure( _):
                    //Utility.showAlert(message: error.localizedDescription, controller: self)
                    Utility.showAlert(message: "حدث خطأ - يرجي اعادة المحاولة", controller: self) //Something went wrong, please try again.
                }
        }
    }
    
    func getAllCountriesAPICalled(){
        let parameters : Parameters = [
            "cacheignore" : "\(Int(NSDate().timeIntervalSince1970))" //To ignore cache from api...
        ]
        
        AppDelegate.showHUD(inView: self.view, message:"Loading....")
        Alamofire.request(Constants.apiURL + "webservice/countrylist.php", method: .get, parameters: parameters)
            .responseJSON { response in
                AppDelegate.hideHUD(inView: self.view)
                switch response.result {
                case .success:
                    if let json = response.result.value{
                        //print("JSON Response : \(json)")
                        guard let _ : NSDictionary = json as? NSDictionary else {
                            return
                        }
                        do {
                            // make sure this JSON is in the format we expect
                            let jsonData =  try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
                            let decoded = try JSONSerialization.jsonObject(with: jsonData, options: []) as! NSDictionary
                            // here "decoded" is of type `Any`, decoded from JSON data
                            
                            guard let dataArray : NSArray = decoded["data"] as? NSArray else {
                                return
                            }
                            
                            if dataArray.count > 0 {
                                self.array_CountryList.addObjects(from: (dataArray.reversed()) )
                                //                                let dictEventDetail : NSDictionary = self.array_CountryList[0] as! NSDictionary
                                self.setCountryNameField(self.array_CountryList)
                            }
                        }
                        catch let error as NSError {
                            print("Failed to load: \(error.localizedDescription)")
                        }
                    }
                case .failure( _):
                    //Utility.showAlert(message: error.localizedDescription, controller: self)
                    Utility.showAlert(message: "حدث خطأ - يرجي اعادة المحاولة", controller: self) //Something went wrong, please try again.
                }
        }
    }
    
    //MARK: - UITableView DataSource
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return array_CountryList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "SelectionCell"
        var cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier)
        if cell == nil {
            cell = tableviewCell(withReuseIdentifierSelection: cellIdentifier, tablview: tableView, with: indexPath)
        }
        self.configureCellSelection(cell, for: indexPath, withSection: indexPath.section, tableView: tableView)
        cell!.selectionStyle = .none
        return (cell ?? nil) ?? UITableViewCell()
    }
    
    //MARK: - For Choose Country Only
    func tableviewCell(withReuseIdentifierSelection identifier: String?, tablview: UITableView?, with indxPath: IndexPath?) -> UITableViewCell? {
        
        
        let cell = UITableViewCell(style: .default, reuseIdentifier: identifier)
        cell.backgroundColor = UIColor.clear
        //UILabel Selection TreatMent...
        
        let radioButtonImageView = UIImageView.init(frame: CGRect.init(x: 10, y: 17, width: 25, height: 25))
        radioButtonImageView.tag = TAGCOUNTRYRADIOBUTTON
        radioButtonImageView.isUserInteractionEnabled = false
        cell.contentView.addSubview(radioButtonImageView)
        
        let labelSelection = UILabel.init(frame: CGRect(x: 40, y: 0, width: (tablview?.bounds.size.width ?? 0.0) - 45, height: 65))
        labelSelection.textColor = UIColor.white
        labelSelection.tag = TAGCOUNTRYNAME
        labelSelection.numberOfLines = 0
        labelSelection.textAlignment = .right
        labelSelection.isUserInteractionEnabled = true
        cell.contentView.addSubview(labelSelection)
        
        return cell
    }
    
    func configureCellSelection(_ cell: UITableViewCell?, for indexPath: IndexPath?, withSection section: Int, tableView: UITableView?) {
        
        let dictCountry : NSDictionary = array_CountryList[indexPath!.row] as! NSDictionary
        let str_countryName : String = dictCountry["name"] as? String ?? ""
        var str_countrycelCode : String = dictCountry["cel_code"] as? String ?? ""
        str_countrycelCode =  str_countrycelCode.replacingOccurrences(of: "+", with: "")
        
        
        let labelSelectionEvent = cell?.contentView.viewWithTag(TAGCOUNTRYNAME) as? UILabel
        labelSelectionEvent?.text = "(" + str_countrycelCode + "+) " + str_countryName
        labelSelectionEvent?.textColor = UIColor.white
        
        let radioImageSelectionEvent = cell?.contentView.viewWithTag(TAGCOUNTRYRADIOBUTTON) as? UIImageView
        if labelSelectionEvent?.text == textField_Country.text {
            radioImageSelectionEvent?.image = UIImage(named: "radio-on-white")
        }
        else {
            radioImageSelectionEvent?.image = UIImage(named: "radio-off-white")
        }
    }
    
    func setCountryNameField(_ arrayCountries: NSArray) {
        var  dictCountry : NSMutableDictionary = NSMutableDictionary()
        let  string_country = "\(loggedInUser.shared.string_country)"
        
        if arrayCountries.count > 0 && string_country == "" {
            //            let dictEventDetail : NSDictionary = arrayCountries[0] as! NSDictionary
            //            self.setCountryCode(dictEventDetail)
        }
        else {
            for indx in 0 ..< arrayCountries.count{//Check here logged in user's mobile number's cel code i.e (+91,+966,+98,etc...) where exists in country list...
                let country_Obj = arrayCountries[indx] as? NSDictionary ?? NSDictionary()
                let str_countryCelCode : String = country_Obj["cel_code"] as? String ?? ""
                if string_country == str_countryCelCode {
                    
                    //                string_mobileNumber =  "\(loggedInUser.shared.string_mobileNumber)"
                    dictCountry = country_Obj.mutableCopy() as! NSMutableDictionary
                    break
                }
            }
            
            //Country Name + Country Code...(i.e. India ( +91 )
            let str_countryName : String = dictCountry["name"] as? String ?? ""
            var str_countrycelCode : String = dictCountry["cel_code"] as? String ?? ""
            str_countrycelCode =  str_countrycelCode.replacingOccurrences(of: "+", with: "")
            textField_Country.text = "(" + str_countrycelCode + "+) " + str_countryName
            textField_Country.accessibilityLabel = dictCountry["code"] as? String //Country Code i.e(SA,IN...)
            textField_Country.accessibilityHint = dictCountry["cel_code"] as? String //Cell code i.e.(+91,+966,+98,etc...)
            var str_placeHolder = dictCountry["placeholder"] as? String
            str_placeHolder =  str_placeHolder?.replacingOccurrences(of: " ", with: "")
            textField_mobileNumber.placeholder = str_placeHolder
            if let Maxlgth  = str_placeHolder?.count{
                maxlength_MobileNumberfield = Maxlgth
            }
        }
        textField_mobileNumber.text = "\(loggedInUser.shared.string_mobileNumber)"
    }    
    
    func setCountryCode(_ dictCountry: NSDictionary) {
        //textField_Country.text = dictCountry["name"] as? String
        //textField_Country.accessibilityLabel = dictCountry["code"] as? String
        
        //Country Name + Country Code...(i.e. India ( +91 )
        let str_countryName : String = dictCountry["name"] as? String ?? ""
        var str_countrycelCode : String = dictCountry["cel_code"] as? String ?? ""
        str_countrycelCode =  str_countrycelCode.replacingOccurrences(of: "+", with: "")
        textField_Country.text = "(" + str_countrycelCode + "+) " + str_countryName
        textField_Country.accessibilityLabel = dictCountry["code"] as? String //Country Code i.e(SA,IN...)
        textField_Country.accessibilityHint = dictCountry["cel_code"] as? String //Cell code i.e.(+91,+966,+98,etc...)
        var str_placeHolder = dictCountry["placeholder"] as? String
        str_placeHolder =  str_placeHolder?.replacingOccurrences(of: " ", with: "")
        textField_mobileNumber.placeholder = str_placeHolder
        if let Maxlgth  = str_placeHolder?.count {
            maxlength_MobileNumberfield = Maxlgth
        }
        textField_mobileNumber.text = ""
    }
    
    //MARK: UITableView Delegate
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        let dictEventDetail : NSDictionary = array_CountryList[indexPath.row] as! NSDictionary
        self.setCountryCode(dictEventDetail)
        self.removePickerFromScreenForCountrySelection()
    }
    
    //MARK: - UITextField Delegate
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField.tag ==  0 {//First name...
            // Check for valid input characters
            if CharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ ").isSuperset(of: CharacterSet(charactersIn: string)) {
                return true
            }else{return false}
            
        }else if textField.tag ==  1 {//Last name...
            // Check for valid input characters
            if CharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ ").isSuperset(of: CharacterSet(charactersIn: string)) {
                return true
            }else{return false}
            
        }
        else if textField.tag ==  3 {
            if CharacterSet(charactersIn: "0123456789").isSuperset(of: CharacterSet(charactersIn: string)) {
                let currentString: NSString = textField.text! as NSString
                let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
                return newString.length <= maxlength_MobileNumberfield
            }else{return false}
        }
        
        return true
        
    }
    
    func showCountryList(){
        if array_CountryList.count == 0 {
            //For Event...
            return
        }
        
        if (textField_firstName.isFirstResponder) {
            textField_firstName.resignFirstResponder()
        }
        if (textField_lastName.isFirstResponder) {
            textField_lastName.resignFirstResponder()
        }
        if (textField_Email.isFirstResponder) {
            textField_Email.resignFirstResponder()
        }
        if (textField_NewPassword.isFirstResponder) {
            textField_NewPassword.resignFirstResponder()
        }
        if (textField_confirmPassword.isFirstResponder) {
            textField_confirmPassword.resignFirstResponder()
        }
        if (textField_mobileNumber.isFirstResponder) {
            textField_mobileNumber.resignFirstResponder()
        }  
        buttonasfullScreenForCountrySelection = UIButton()
        buttonasfullScreenForCountrySelection.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        buttonasfullScreenForCountrySelection.frame = CGRect(x: 0, y: 0, width: UIApplication.shared.keyWindow?.bounds.size.width ?? 0.0, height: UIApplication.shared.keyWindow?.bounds.size.height ?? 0.0)
        buttonasfullScreenForCountrySelection.addTarget(self, action: #selector(removePickerFromScreenForCountrySelection), for: .touchUpInside)
        UIApplication.shared.keyWindow?.addSubview(buttonasfullScreenForCountrySelection)
        
        
        let viewForShadow = UIView()
        if UIDevice.current.userInterfaceIdiom == .phone{
            
            viewForShadow.frame = CGRect(x: (buttonasfullScreenForCountrySelection.bounds.size.width - 350) / 2, y: (buttonasfullScreenForCountrySelection.bounds.size.height - 500) / 2, width: 350, height: 500)
            
        } else {
            
            viewForShadow.frame = CGRect(x: buttonasfullScreenForCountrySelection.bounds.size.width / 4, y: buttonasfullScreenForCountrySelection.bounds.size.height / 4, width: buttonasfullScreenForCountrySelection.bounds.size.width / 2, height: buttonasfullScreenForCountrySelection.bounds.size.height / 2)
            
        }
        viewForShadow.autoresizingMask = [.flexibleTopMargin, .flexibleBottomMargin, .flexibleLeftMargin, .flexibleRightMargin]
        buttonasfullScreenForCountrySelection.addSubview(viewForShadow)
        
        tableViewForCountrySelection = UITableView()
        tableViewForCountrySelection.tag = 100
        tableViewForCountrySelection.cellLayoutMarginsFollowReadableWidth = false
        tableViewForCountrySelection.frame = CGRect(x: 0, y: 0, width: viewForShadow.bounds.size.width, height: viewForShadow.bounds.size.height)
        tableViewForCountrySelection.delegate = self
        tableViewForCountrySelection.dataSource = self
        tableViewForCountrySelection.separatorStyle = UITableViewCell.SeparatorStyle.singleLine
        tableViewForCountrySelection.separatorColor = UIColor.gray
        tableViewForCountrySelection.tableFooterView = UIView(frame: CGRect.zero)
        tableViewForCountrySelection.backgroundColor = UIColor.black
        viewForShadow.addSubview(tableViewForCountrySelection)
        tableViewForCountrySelection.reloadData()
    }
    
    @objc func removePickerFromScreenForCountrySelection() {
        buttonasfullScreenForCountrySelection.removeFromSuperview()
    }
    
    //MARK: UIButton Clicks
    @IBAction func backButtonClicked(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func buttonSearchClicked(_ sender: UIButton) {
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "searchVC") as! SearchVC
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    @IBAction func button_firstNameEditClicked(_ sender: UIButton) {
        textField_firstName.becomeFirstResponder()
    }
    
    @IBAction func button_lastNameEditClicked(_ sender: UIButton) {
        textField_lastName.becomeFirstResponder()
    }
    
    @IBAction func button_emailEditClicked(_ sender: UIButton) {
        textField_Email.becomeFirstResponder()
    }
    
    @IBAction func button_CountryClicked(_ sender: UIButton) {
        self.showCountryList()
    }
    
    @IBAction func button_phoneEditClicked(_ sender: UIButton) {
        textField_mobileNumber.becomeFirstResponder()
    }
    
    @IBAction func button_newpasswordEditClicked(_ sender: UIButton) {
        textField_NewPassword.becomeFirstResponder()
    }
    
    @IBAction func button_confirmPasswordEditClicked(_ sender: UIButton) {
        textField_confirmPassword.becomeFirstResponder()
    }
    
    @IBAction func button_updateProfileClicked(_ sender: UIButton) {
        var countryStr = ""
        var phoneStr = ""
        
        if textField_firstName.text == ""{
            Utility.showAlert(message:"برجاء كتابة الاسم الاول", controller: self)
            return
        }
        if textField_lastName.text == ""{
            Utility.showAlert(message:"برجاء كتابة الاسم الاخير", controller: self)
            return
        }
        if textField_Email.text == "" || self.isValidEmail(emailStr:textField_Email.text!) == false{
            Utility.showAlert(message:"الرجاء إدخال بريد إلكتروني صحيح", controller: self)
            return
        }
        if textField_Country.text != "" {
            countryStr = (textField_Country.accessibilityHint?.trimmingCharacters(in: .whitespacesAndNewlines))!
        }
        
        if textField_mobileNumber.text != "" && textField_mobileNumber.text!.count != maxlength_MobileNumberfield {
            Utility.showAlert(message:"من فضلك أدخل رقم الجوال", controller: self)
            return
        }
        
        if textField_mobileNumber.text != "" && textField_mobileNumber.text!.count == maxlength_MobileNumberfield {
            phoneStr = (textField_mobileNumber.text?.trimmingCharacters(in: .whitespacesAndNewlines))!
        }
        
        var str_mobileNumberWithCountryCode : String = ""
        if textField_Country.text != "" && textField_mobileNumber.text != "" {
            str_mobileNumberWithCountryCode = countryStr +  phoneStr
        }
        //        if str_mobileNumberWithCountryCode == "\(loggedInUser.shared.string_mobileNumber)" {//If
        //        }
        //        else {
        //
        //        }
        //
        let parameters : Parameters = [
            
            "customer_id" : loggedInUser.shared.string_userID,
            "firstname" : textField_firstName.text!,
            "lastname" :textField_lastName.text!,
            "email" : textField_Email.text!,
            "mobilenumber" : str_mobileNumberWithCountryCode,
            "cacheignore" : "\(Int(NSDate().timeIntervalSince1970))" //To ignore cache from api...
            
        ]
        
        AppDelegate.showHUD(inView: self.view, message: "Loading...")
        //Alamofire.request(Constants.apiURL+"user/register", method: .post, parameters: parameters, encoding: JSONEncoding.default)
        //Alamofire.request(Constants.apiURL+"rest/V1/customers", method: .post, parameters: parameters, headers: headers)
        // Alamofire.request(Constants.apiURL+"rest/V1/customers", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers)
        Alamofire.request(Constants.apiURL + "webservice/updateprofile.php", method: .post, parameters: parameters)
            .responseJSON { response in
                
                AppDelegate.hideHUD(inView: self.view)
                
                switch response.result {
                case .success:
                    
                    if let json = response.result.value{
                        
                        print("JSON Response : \(json)")
                        
                        
                        guard let _ : NSDictionary = json as? NSDictionary else {
                            return
                        }
                        
                        do {
                            // make sure this JSON is in the format we expect
                            
                            let jsonData =  try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
                            let decoded = try JSONSerialization.jsonObject(with: jsonData, options: []) as! NSDictionary
                            if decoded["msg"] as? String == "success"{
                                //setting country empty if phone is empty in locally
                                if phoneStr == "" {
                                    countryStr = ""
                                }
                                
                                let  jsonDict : NSMutableDictionary = NSMutableDictionary()
                                jsonDict.setValue("\(self.textField_firstName.text!)", forKey: "fname")
                                jsonDict.setValue("\(self.textField_lastName.text!)", forKey: "lname")
                                jsonDict.setValue("\(self.textField_Email.text!)", forKey: "email")
                                jsonDict.setValue("\(phoneStr)", forKey: "mob")
                                jsonDict.setValue("\(countryStr)", forKey: "country_code")
                                jsonDict.setValue("\(loggedInUser.shared.string_userID)", forKey: "id")
                                do {  
                                    
                                    let userDefaults = UserDefaults.standard
                                    let encodedData: Data = try NSKeyedArchiver.archivedData(withRootObject: jsonDict, requiringSecureCoding: false)
                                    userDefaults.removeObject(forKey: "loggedInUserRecord")
                                    userDefaults.set(encodedData, forKey: "loggedInUserRecord")
                                    userDefaults.synchronize()
                                    
                                } catch {
                                    print("Couldn't archive data")
                                }
                                
                                loggedInUser.shared.initializeUserDetails(dicuserdetail: jsonDict as! Dictionary<String, Any>)
                                iToast.show("تم تحديث الملف الشخصي بنجاح")//Profile updated successfully.
                                
                                
                            }else{
                                
                                iToast.show("حدث خطأ - يرجي اعادة المحاولة")//An error has occurred - please try again
                                
                            }
                            
                        } catch let error as NSError {
                            
                            print("Failed to load: \(error.localizedDescription)")
                        }
                        
                    }
                case .failure( _):
                    //Utility.showAlert(message: error.localizedDescription, controller: self)
                    Utility.showAlert(message: "حدث خطأ - يرجي اعادة المحاولة", controller: self) //Something went wrong, please try again.
                }
        }
        
        
    }
    
    @IBAction func button_changePasswordlicked(_ sender: UIButton) {
        guard let str_Password = textField_NewPassword.text?.trimmingCharacters(in: .whitespacesAndNewlines) else{
            return
        }
        
        if str_Password.count < 7 {
            Utility.showAlert(message:"كلمة السر يجب أن لا تقل عن 7 أرقام", controller: self)
            return
        }
        
        guard let str_ConfirmPassword = textField_confirmPassword.text?.trimmingCharacters(in: .whitespacesAndNewlines) else{
            return
        }
        
        if str_ConfirmPassword == ""{
            Utility.showAlert(message:"برجاء ادخال كود التأكيد", controller: self)
            return
        }
        
        if str_Password != str_ConfirmPassword{
            Utility.showAlert(message:"كلمة المرور لا تتطابق مع الكلمة الاولى", controller: self)
            return
        }
        
        let parameters : Parameters = [
            "customer_id" : loggedInUser.shared.string_userID,
            "newpassword" : str_ConfirmPassword,
            "cacheignore" : "\(Int(NSDate().timeIntervalSince1970))" //To ignore cache from api...
        ]
        
        AppDelegate.showHUD(inView: self.view, message: "Loading...")
        //Alamofire.request(Constants.apiURL+"user/register", method: .post, parameters: parameters, encoding: JSONEncoding.default)
        //Alamofire.request(Constants.apiURL+"rest/V1/customers", method: .post, parameters: parameters, headers: headers)
        // Alamofire.request(Constants.apiURL+"rest/V1/customers", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers)
        Alamofire.request(Constants.apiURL + "webservice/changepassword.php", method: .post, parameters: parameters)
            .responseJSON { response in
                AppDelegate.hideHUD(inView: self.view)
                switch response.result {
                case .success:
                    if let json = response.result.value{
                        print("JSON Response : \(json)")
                        guard let _ : NSDictionary = json as? NSDictionary else {
                            return
                        }
                        do {
                            let jsonData =  try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
                            let decoded = try JSONSerialization.jsonObject(with: jsonData, options: []) as! NSDictionary
                            if decoded["msg"] as? String == "success" {
                                iToast.show("تم تغيير كلمة السر بنجاح")//Password changed successfully.
                                self.delegate_passwordChange?.passwordChangedSuccessfullyDelegate()
                                self.navigationController?.popToRootViewController(animated: false)
                            }
                            else{
                                iToast.show(decoded["msg"] as? String)
                            }
                        }
                        catch _ as NSError {
                        }
                    }
                case .failure( _):
                    //Utility.showAlert(message: error.localizedDescription, controller: self)
                    Utility.showAlert(message: "حدث خطأ - يرجي اعادة المحاولة", controller: self) //Something went wrong, please try again.
                }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
