import Alamofire

protocol deliveryAddressChangeDelegate {
    func deliveryAddressChangeSuccessfully()
}

protocol AllDeliveryAddressessCellDelegate {
    func buttonDeleteClicked(sender: UIButton)
}

class AllDeliveryAddressessCell : UITableViewCell {
    @IBOutlet weak var baseView: UIView!
    @IBOutlet weak var label_Name: UILabel!
    @IBOutlet weak var label_Street: UILabel!
    @IBOutlet weak var label_City: UILabel!
    @IBOutlet weak var label_Country: UILabel!
    @IBOutlet weak var label_mobileNumber: UILabel!
    @IBOutlet weak var button_Delete: UIButton!
    
    var delegate: AllDeliveryAddressessCellDelegate?
    
    @IBAction func button_deleteClicked(_ sender: UIButton) {
        delegate?.buttonDeleteClicked(sender: sender)
    }
}

class AllDeliveryAddressessVC: UIViewController,UITableViewDataSource,UITableViewDelegate,AllDeliveryAddressessCellDelegate,addAddressDelegate {
    @IBOutlet weak var table_ViewAllDeliveryAddressess: UITableView!
    @IBOutlet weak var table_ViewHeightConstraints: NSLayoutConstraint!

    var delegateDeliveryAddressChange : deliveryAddressChangeDelegate?
    
    var array_AllDeliveryAddressess : NSMutableArray = NSMutableArray()
    var refreshControl = UIRefreshControl()
    var str_SelectedAddressID = ""
    var cellHeight = 160
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(refresh), for: UIControl.Event.valueChanged)
        table_ViewAllDeliveryAddressess.addSubview(refreshControl) // not required when using UITableViewController
                
        let userDefaults = UserDefaults.standard
        let decoded = userDefaults.data(forKey: "userDefaultDeliveryAddress")
        if (decoded != nil) {
            if let dict_Address : NSDictionary = NSKeyedUnarchiver.unarchiveObject(with: decoded!) as? NSDictionary {
                str_SelectedAddressID = dict_Address["address_id"] as! String
            }
        }
        
        //getAllDeliveryAddressess API Called...
        self.getAllDeliveryAddressAPICalled()
    }
    
    @objc func refresh(sender:AnyObject) {
        //getAllDeliveryAddressess API Called...
        self.getAllDeliveryAddressAPICalled()
    }
    
    override func viewWillAppear(_ _animated: Bool) {
        super.viewWillAppear(_animated)
        self.navigationController?.navigationBar.isHidden = true
    }
    
   //MARK: - UIButton Clicks
    @IBAction func backButtonClicked(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func buttonSearchClicked(_ sender: UIButton) {
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "searchVC") as! SearchVC
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    //MARK: API Calling
    func getAllDeliveryAddressAPICalled(){
        let parameters : Parameters = [
            
            "customer_id" : loggedInUser.shared.string_userID,
            "cacheignore" : "\(Int(NSDate().timeIntervalSince1970))" //To ignore cache from api...
            
        ]
        
        AppDelegate.showHUD(inView: self.view, message:"Loading....")
        Alamofire.request(Constants.apiURL + "webservice/addresslist.php", method: .post, parameters: parameters)
            .responseJSON { response in
                AppDelegate.hideHUD(inView: self.view)
                switch response.result {
                case .success:
                    if let json = response.result.value{
                        //print("JSON Response : \(json)")
                        guard let _ : NSDictionary = json as? NSDictionary else {
                            return
                        }
                        do {
                            // make sure this JSON is in the format we expect
                            let jsonData =  try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
                            let decoded = try JSONSerialization.jsonObject(with: jsonData, options: []) as! NSDictionary
                            // here "decoded" is of type `Any`, decoded from JSON data
                            
                            guard let dataArray : NSArray = decoded["data"] as? NSArray else {
                                return
                            }
                            
                            self.array_AllDeliveryAddressess.removeAllObjects()
                            
                            if dataArray.count > 0 {
                                self.array_AllDeliveryAddressess.addObjects(from: (dataArray.reversed()) ) //last addedd address will show on top...

                                self.table_ViewHeightConstraints.constant = CGFloat(self.cellHeight * self.array_AllDeliveryAddressess.count)
                            }
                            else{
                                let userDefaults_DeliveryAddress = UserDefaults.standard
                                userDefaults_DeliveryAddress.removeObject(forKey:"userDefaultDeliveryAddress")
                                userDefaults_DeliveryAddress.synchronize()
                                
                                self.table_ViewHeightConstraints.constant = 0
                            }
                            
                            self.refreshControl.endRefreshing()
                            self.table_ViewAllDeliveryAddressess.reloadData()
                        }
                        catch let error as NSError {
                            print("Failed to load: \(error.localizedDescription)")
                        }
                    }
               case .failure( _):
                    //Utility.showAlert(message: error.localizedDescription, controller: self)
                    Utility.showAlert(message: "حدث خطأ - يرجي اعادة المحاولة", controller: self) //Something went wrong, please try again.
                }
        }
    }
    
    func deleteAddressApiCalled(parameters: Parameters){
        AppDelegate.showHUD(inView: self.view, message:"Loading....")
        Alamofire.request(Constants.apiURL + "webservice/deleteaddress.php", method: .post, parameters: parameters)
            .responseJSON { response in
                AppDelegate.hideHUD(inView: self.view)
                switch response.result {
                case .success:
                    if let json = response.result.value{
                        // print("JSON Response : \(json)")
                        guard let _ : NSDictionary = json as? NSDictionary else {
                            return
                        }
                        do {
                            // make sure this JSON is in the format we expect
                            let jsonData =  try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
                            if (try JSONSerialization.jsonObject(with: jsonData, options: []) as? NSDictionary) != nil {
                                self.getAllDeliveryAddressAPICalled()
                            }
                            else{
                                Utility.showAlert(message: "حدث خطأ - يرجي اعادة المحاولة", controller: self)//An error has occurred - please try again
                            }
                        }
                        catch let error as NSError {
                            print("Failed to load: \(error.localizedDescription)")
                        }
                        
                        //                        if (jsonDict["status"] as! String == "ok"){
                        //
                        //                            let userDefaults = UserDefaults.standard
                        //                            let dataUser = jsonDict["user"] as! NSDictionary
                        //                            let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: dataUser)
                        //                            userDefaults.set(encodedData, forKey: "loggedInUserRecord")
                        //                            userDefaults.synchronize()
                        //                        }else{
                        //                            Utility.showAlert(message: jsonDict["message"] as! String, controller: self)
                        //                        }
                    }
                case .failure( _):
                    //Utility.showAlert(message: error.localizedDescription, controller: self)
                    Utility.showAlert(message: "حدث خطأ - يرجي اعادة المحاولة", controller: self) //Something went wrong, please try again.
                }
        }
    }
        
    //MARK: - Address Added Successfully...
    func addressAddeddSuccessfully() {
        self.getAllDeliveryAddressAPICalled()
    }
    
    //MARK: - UITableView DataSource
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(cellHeight)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return array_AllDeliveryAddressess.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "allDeliveryAddressessCell", for: indexPath) as! AllDeliveryAddressessCell
        cell.delegate = self as? AllDeliveryAddressessCellDelegate
        let dict_Address = array_AllDeliveryAddressess[indexPath.row] as? NSDictionary
        cell.label_Name.text = "\(dict_Address?["firstname"] as! String) \(dict_Address?["lastname"] as! String)"
          cell.label_Street.text = dict_Address?["street"] as? String ?? ""
        cell.label_City.text = dict_Address?["city"] as? String ?? ""
        cell.label_Country.text = dict_Address?["country"] as? String ?? ""
        cell.label_mobileNumber.text = dict_Address?["telephone"] as? String ?? ""
     
        cell.button_Delete.accessibilityLabel = String(format: "%d", indexPath.row)
        
        cell.baseView.layer.borderWidth = 1.0
        cell.baseView.layer.borderColor = UIColor.init(red: 214/255, green: 222/255, blue: 230/255, alpha: 1.0).cgColor
        
        if str_SelectedAddressID == dict_Address?["address_id"] as? String{
            cell.baseView.layer.borderWidth = 1.0
            cell.baseView.layer.borderColor = UIColor.init(red: 216/255, green: 166/255, blue: 100/255, alpha: 1.0).cgColor
        }
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let dictAddress = array_AllDeliveryAddressess[indexPath.row] as? NSDictionary else { return }
        let userDefaults = UserDefaults.standard
        let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: dictAddress)
        userDefaults.set(encodedData, forKey: "userDefaultDeliveryAddress")
        userDefaults.synchronize()
        str_SelectedAddressID = dictAddress["address_id"] as! String
        table_ViewAllDeliveryAddressess.reloadData()
        
        if (self.delegateDeliveryAddressChange != nil){
            self.delegateDeliveryAddressChange?.deliveryAddressChangeSuccessfully()
            self.navigationController?.popViewController(animated: false)
        }
    }
        
    func buttonDeleteClicked(sender: UIButton) {
        
        let indxPath : Int = Int((sender.accessibilityLabel)!) ?? 0
        let dict_Address = self.array_AllDeliveryAddressess[indxPath] as! NSDictionary
        
        let params : Parameters = [
            
            "customer_id" : loggedInUser.shared.string_userID,
            "address_id" : dict_Address["address_id"] as? String ?? "",
              "cacheignore" : "\(Int(NSDate().timeIntervalSince1970))" //To ignore cache from api...
        ]
        
        if str_SelectedAddressID == dict_Address["address_id"] as? String{
            
            let userDefaults_DeliveryAddress = UserDefaults.standard
            userDefaults_DeliveryAddress.removeObject(forKey:"userDefaultDeliveryAddress")
            userDefaults_DeliveryAddress.synchronize()
            
        }
        
        self.deleteAddressApiCalled(parameters: params)
    }
    
    @IBAction func button_addNewDeliveryAddressClicked(_ sender: UIButton) {
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "addressBookVC") as! AddressBookVC
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
