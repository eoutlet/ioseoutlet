import UIKit
import Alamofire
import MonthYearPicker
import Frames

class CheckoutRememberMeCardTableViewCell: UITableViewCell{
    @IBOutlet weak var base_View : UIView!
    @IBOutlet weak var circle_Image_RememberMeCard : UIImageView!       //Circle Image icons
    @IBOutlet weak var card_NameLbl: UILabel!
    @IBOutlet weak var card_NumberLbl: UILabel!
    @IBOutlet weak var card_CVVTextField: UITextField!
}

class CheckOutPaymentOptionsVC: UIViewController, UITextFieldDelegate, UITableViewDataSource, UITableViewDelegate, PKPaymentAuthorizationControllerDelegate,PKPaymentAuthorizationViewControllerDelegate {
    
    @IBOutlet weak var label_totalAmount : UILabel!
    @IBOutlet weak var view_Base : UIView!
    @IBOutlet weak var view_creditDebitCard : UIView!
    @IBOutlet weak var view_creditDebitCardForm : UIView!
    @IBOutlet weak var view_cardFormHeightConstraints : NSLayoutConstraint!
    
    @IBOutlet weak var view_applePay : UIView!
    
    @IBOutlet weak var view_Wallet : UIView!
    @IBOutlet weak var label_walletBalanceValue: UILabel!
    @IBOutlet weak var view_walletHeightConstraints : NSLayoutConstraint!
    
    @IBOutlet weak var view_STC : UIView!
    @IBOutlet weak var view_STCHeightConstraints : NSLayoutConstraint!
    @IBOutlet weak var view_STCTopConstraints : NSLayoutConstraint!

    @IBOutlet weak var view_leftWalletBalance : UIView!
    @IBOutlet weak var label_leftWalletBalanceValue : UILabel!
    
    @IBOutlet weak var view_COD : UIView!
    @IBOutlet weak var label_CODBalanceValue: UILabel!
    @IBOutlet weak var view_codHeightConstraints : NSLayoutConstraint!
    
    @IBOutlet weak var rememberMe_Card_TableView : UITableView!
    @IBOutlet weak var rememberMe_Card_TableViewHeightConstraints: NSLayoutConstraint!
    
    //Circle Image icons
    @IBOutlet weak var image_creditDebitCard : UIImageView!
    @IBOutlet weak var image_applePay : UIImageView!
    @IBOutlet weak var image_STC : UIImageView!
    @IBOutlet weak var image_wallet : UIImageView!
    @IBOutlet weak var image_COD : UIImageView!
    
    @IBOutlet weak var card_NameTextField : UITextField!
    @IBOutlet weak var card_NumberTextField : UITextField!
    @IBOutlet weak var card_ExpiryTextField : UITextField!
    @IBOutlet weak var card_SecurityCodeTextField : UITextField!
    @IBOutlet weak var finalAmountUIView : UIView!
    @IBOutlet weak var view_creditCardAndCODButton: UIView!
    @IBOutlet weak var view_applePayButton: UIView!
    
    @IBOutlet weak var image_autoSavedCheckBox : UIImageView!
    
    @IBOutlet weak var stcPhoneNumberTextField : UITextField!
    
    var buttonasfullScreenForOTP = UIButton()
    var textFieldEnterOTP = UITextField()
    var label_ForTimerCountDown = UILabel()
    var  buttonVerifyOTP = UIButton()
    var  buttonResendOTP = UIButton()
    var otp_Timer: Timer?
    var value_Timer : Int = 30
    
    var dict_CompleteCartRecord : NSMutableDictionary = NSMutableDictionary()
    var array_CartItem : NSMutableArray = NSMutableArray()
    // var paymentMethod = "" //i.e. cashondelivery, credit card.
    var first_name = ""
    var last_name = ""
    var street = ""
    var city = ""
    var country = ""
    var country_id = ""
    var mobile = ""
    var str_Coupon_Code = ""
    var shippingCharge = 0
    var amount_Final = 0
    var amount_Wallet = 0
    var is_WalletApplied : Bool = false
    var shippingMethod = ""
    var is_ApplePayChoose : Bool = false  // it will set to true when payemnt by apple pay is done...
    
    var is_GiftApplied : String = "0" // set default gift not applied
    
    var str_rememberMe = "NO"
    
    var array_rememberMeCards = NSMutableArray()
    
    var selectedRememberMeCardIndex : Int = -1
    
    var expirationDatePicker = MonthYearPickerView()
    
    var maxlength_CardNumberfield : Int = 16
    var maxlength_CartCVVNumberfield : Int = 4
    var maxlength_STCPhoneNumberfield : Int = 0
    
    var OTPReferenceSTC : String = ""
    var STCPaymentReferenceSTC : String = ""
    var OrderIdSTC : String = ""
    
    var checkoutAPIClient: CheckoutAPIClient {
        return CheckoutAPIClient(publicKey: Constants.KCheckOutApiKey, environment: Constants.KCheckOutEnviroment)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //        if loggedInUser.shared.string_mobileNumber.count != 0 {
        //            maxlength_STCPhoneNumberfield = loggedInUser.shared.string_country.count +  loggedInUser.shared.string_mobileNumber.count
        //        }
        //        else {
        //            maxlength_STCPhoneNumberfield = 15  //set default 15 if user have not phone number in profile
        //        }
        maxlength_STCPhoneNumberfield = 10  //set default 15 if user have not phone number in profile
        stcPhoneNumberTextField.text = "" //loggedInUser.shared.string_mobileNumber
        
        self.view_applePayButton.isHidden = true
        label_totalAmount.text = "\(amount_Final)"
        
        self.rememberMe_Card_TableViewHeightConstraints.constant = CGFloat(80 * array_rememberMeCards.count)
        self.rememberMe_Card_TableView.tableFooterView = UIView()
        
        if(amount_Final == 0){//If total payable amount is zero, this can be achieve after applying coupon code...
            self.createOrderAPICalled_CheckOut(paymentMethod: "free", isPartial: "0", cardDetail: NSDictionary() as! NSMutableDictionary, token_name: "")
        }else{//Fetch Already Saved Cards For Transactions...
            self.getRememberMeCardsAPICalled()
        }
        self.adjustAllUIElements()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        self.view_Base.setShadow(shadowSize: 10.0, radius: 10.0)
        self.view_creditDebitCard.setShadow(shadowSize: 10.0, radius: 10.0)
        self.view_Wallet.setShadow(shadowSize: 10.0, radius: 10.0)
        self.finalAmountUIView.setShadow(shadowSize: 10.0, radius: 10.0)
        
        //Set COD UI View Hidden or Not
        if self.country_id == "SA" && amount_Final <= 3000  && is_GiftApplied == "0" {//COD option will show only when --> country == "SA" and gift box option is not selected and final amount upto 3000 SAR only...
            self.view_COD.setShadow(shadowSize: 10.0, radius: 10.0)
        }else {
            self.view_COD.setShadow(shadowSize: 0.0, radius: 0.0)
        }
        self.view_applePay.setShadow(shadowSize: 10.0, radius: 10.0)
        
        if self.country_id == "SA"  {
            self.view_STC.setShadow(shadowSize: 10.0, radius: 10.0)
        }
        else {
            self.view_STC.setShadow(shadowSize: 0.0, radius: 0.0)
        }
    }
    
    func adjustAllUIElements(){
        
        view_cardFormHeightConstraints.constant = 0
        
        view_walletHeightConstraints.constant = 50
        view_STCHeightConstraints.constant = 50
        view_leftWalletBalance.isHidden = true
        self.getWalletBalanceAPICalled()
        
        //Set COD UI View Hidden or Not
        if self.country_id == "SA" && amount_Final <= 3000 && is_GiftApplied == "0" {//COD option will show only when --> country == "SA" and gift box option is not selected and final amount upto 3000 SAR only...
            view_codHeightConstraints.constant = 50
            label_CODBalanceValue.text = "\(dict_CompleteCartRecord["cod_label"] as! String)"
        }
        else {
            view_codHeightConstraints.constant = 0
            self.view_COD.isHidden = true
        }
        
        self.stcHideShow()
    }
    
    override func viewWillAppear(_ _animated: Bool) {
        super.viewWillAppear(_animated)
        self.navigationController?.navigationBar.isHidden = true
    }
    
    func stcHideShow() {
        if self.country_id == "SA"  {
            self.view_STC.setShadow(shadowSize: 10.0, radius: 10.0)
            self.view_STC.isHidden = false
            self.view_STCHeightConstraints.constant = 50
            self.view_STCTopConstraints.constant = 10
        }
        else {
            self.view_STC.setShadow(shadowSize: 0.0, radius: 0.0)
            self.view_STC.isHidden = true
            self.view_STCHeightConstraints.constant = 0
            self.view_STCTopConstraints.constant = 0
        }
    }
    
    //MARK: API Calling...
    func getRememberMeCardsAPICalled() {
        
        var str_userID = ""
        if !loggedInUser.shared.string_userID.isEmpty && loggedInUser.shared.string_userID.count != 0{
            str_userID = String(format:"\(loggedInUser.shared.string_userID)")
        }
        let parameters : Parameters = [
            
            "customer_id" : str_userID,
            "cacheignore" : "\(Int(NSDate().timeIntervalSince1970))" //To ignore cache from api...
        ]
        
        
        AppDelegate.showHUD(inView: self.view, message:"Loading....")
        Alamofire.request(Constants.apiURL+"webservice/getremembermecards.php", method: .post, parameters: parameters)
            .responseJSON { response in
                AppDelegate.hideHUD(inView: self.view)
                switch response.result {
                case .success:
                    if let json = response.result.value{
                        print("JSON Response : \(json)")
                        guard let _ : NSDictionary = json as? NSDictionary else {
                            return
                        }
                        do {
                            // make sure this JSON is in the format we expect
                            let jsonData =  try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
                            let decoded = try JSONSerialization.jsonObject(with: jsonData, options: []) as! NSDictionary
                            if decoded["status"] as? String == "success"{
                                let arrayofGetData = decoded["data"] as? NSArray ?? NSArray()
                                if arrayofGetData.count > 0{
                                    self.array_rememberMeCards.addObjects(from: arrayofGetData as! [Any])
                                    self.rememberMe_Card_TableViewHeightConstraints.constant = CGFloat(80*self.array_rememberMeCards.count)
                                    self.rememberMe_Card_TableView.reloadData()
                                }
                            }
                        }
                        catch _ as NSError {
                        }
                    }
                case .failure( _):
                    Utility.showAlert(message: "حدث خطأ - يرجي اعادة المحاولة", controller: self) //Something went wrong, please try again.
                }
        }
        
    }
    
    func getWalletBalanceAPICalled(){
        
        let parameters : Parameters = [
            "customer_id" : loggedInUser.shared.string_userID,
            "cacheignore" : "\(Int(NSDate().timeIntervalSince1970))" //To ignore cache from api...
        ]
        
        AppDelegate.showHUD(inView: self.view, message:"")
        Alamofire.request(Constants.apiURL+"webservice/getcustomerwallet.php", method: .get, parameters: parameters)
            .responseJSON { response in
                AppDelegate.hideHUD(inView: self.view)
                switch response.result {
                case .success:
                    if let json = response.result.value{
                        //print("JSON Response : \(json)")
                        guard let _ : NSDictionary = json as? NSDictionary else {
                            return
                        }
                        do {
                            // make sure this JSON is in the format we expect
                            let jsonData =  try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
                            let decoded = try JSONSerialization.jsonObject(with: jsonData, options: []) as! NSDictionary
                            
                            guard let dataDict : NSDictionary = decoded["data"] as? NSDictionary else {
                                return
                                
                            }
                            
                            if let intValue = (dataDict["wallet_amount"] as? Int) {
                                self.label_walletBalanceValue.text = "\(intValue)"
                                self.amount_Wallet = intValue
                            }else if let strValue = (dataDict["wallet_amount"] as? String){
                                self.label_walletBalanceValue.text = "\(strValue)"
                                self.amount_Wallet = Int(strValue) ?? 0
                            }
                            
                        }
                        catch let error as NSError {
                            print("Failed to load: \(error.localizedDescription)")
                        }
                    }
                case .failure( _):
                    //Utility.showAlert(message: error.localizedDescription, controller: self)
                    Utility.showAlert(message: "حدث خطأ - يرجي اعادة المحاولة", controller: self) //Something went wrong, please try again.
                }
        }
    }
    
    func createOrderAPICalled_CheckOut(paymentMethod: NSString, isPartial: NSString, cardDetail:  NSMutableDictionary, token_name: NSString) {
        
        //Payment
        let dict_Payment : NSMutableDictionary = NSMutableDictionary()
        dict_Payment.setValue("\(loggedInUser.shared.string_firstName)", forKey: "firstname")
        dict_Payment.setValue("\(loggedInUser.shared.string_lastName)", forKey: "lastname")
        dict_Payment.setValue("\(city)", forKey: "city")
        dict_Payment.setValue("\(country_id)", forKey: "country_id")//accessibilityLabel is coutry id
        dict_Payment.setValue("\(country_id)", forKey: "region")
        dict_Payment.setValue("\(loggedInUser.shared.string_mobileNumber)", forKey: "telephone")
        let dict_StreetPayment : NSMutableDictionary = NSMutableDictionary()
        dict_StreetPayment.setValue("\(street)", forKey: "0")
        dict_StreetPayment.setValue("", forKey: "1")
        dict_Payment.setValue(dict_StreetPayment, forKey: "street")
        
        //Shipping
        let dict_Shipping : NSMutableDictionary = NSMutableDictionary()
        dict_Shipping.setValue(first_name, forKey: "firstname")//accessibilityLabel is firstname...
        dict_Shipping.setValue(last_name, forKey: "lastname")//accessibilityHint is lastname
        dict_Shipping.setValue("\(city)", forKey: "city")
        dict_Shipping.setValue("\(country_id)", forKey: "country_id") //accessibilityLabel is coutry id
        dict_Shipping.setValue("\(country_id)", forKey: "region")
        dict_Shipping.setValue("\(mobile)", forKey: "telephone")
        let dict_ShippingStreet : NSMutableDictionary = NSMutableDictionary()
        dict_ShippingStreet.setValue("\(street)", forKey: "0")
        dict_ShippingStreet.setValue("", forKey: "1")
        dict_Shipping.setValue(dict_ShippingStreet, forKey: "street")
        
        
        let arry_Products : NSMutableArray = NSMutableArray()
        for indx in 0 ..< array_CartItem.count{
            
            let dict_Product : NSMutableDictionary = NSMutableDictionary()
            
            let dict_Item : NSDictionary = array_CartItem[indx] as! NSDictionary
            var qty : String = String()
            
            if let intValue = (dict_Item["qty"] as? Int) {
                qty = String(format: "%d", intValue)
            }
            else if let strValue = (dict_Item["qty"] as? String) {
                qty = String(format: "%d", (strValue as NSString).integerValue)
            }
            dict_Product.setValue("\(qty)", forKey: "qty")
            
            var product_id : String = String()
            if let intValue = (dict_Item["product_id"] as? Int) {
                product_id = String(format: "%d", intValue)
            }
            else if let strValue = (dict_Item["product_id"] as? String) {
                product_id = String(format: "%@", (strValue as NSString))
            }
            dict_Product.setValue("\(product_id)", forKey: "product_id")
            
            let arry_SuperAttribute : NSMutableArray = NSMutableArray()
            
            if dict_Item["type"] as! String == "configurable"{
                
                let dict_superAttribute : NSMutableDictionary = NSMutableDictionary()
                
                guard let ary_Options : NSArray = dict_Item["option"] as? NSArray else {
                    return
                }
                
                guard let dict_ColorAndSize : NSDictionary =  ary_Options[0] as? NSDictionary else {
                    return
                }
                
                guard let obj_Color : NSDictionary =  dict_ColorAndSize["color"] as? NSDictionary else {
                    return
                }
                
                if let id = obj_Color["id"] as? Int{
                    if let value = obj_Color["value"] as? Int{
                        
                        dict_superAttribute.setValue("\(value)", forKey: "\(id)")
                        
                    }
                }
                
                guard let obj_Size : NSDictionary =  dict_ColorAndSize["size"] as? NSDictionary else {
                    return
                }
                if let id = obj_Size["id"] as? Int{
                    if let value = obj_Size["value"] as? Int{
                        dict_superAttribute.setValue("\(value)", forKey:"\(id)")
                    }
                    
                }
                
                //arry_SuperAttribute.add(dict_superAttribute)
                dict_Product.setValue(dict_superAttribute, forKey: "super_attribute")
            }
            else {
                dict_Product.setValue(arry_SuperAttribute, forKey: "super_attribute")
            }
            arry_Products.add(dict_Product)
        }
        
        var couponCode = ""
        if (str_Coupon_Code != ""){
            couponCode = str_Coupon_Code.trimmingCharacters(in: .whitespacesAndNewlines)
        }
        
        
        
        let str_labelTotalAmountValue : NSString = NSString(string: label_totalAmount.text ?? "")
        
        let parameters : Parameters = [
            
            "user_id" : loggedInUser.shared.string_userID,
            "payment_method" : paymentMethod,
            "payment" : dict_Payment,
            "shipping_method" : shippingMethod,
            "shipping" : dict_Shipping,
            "products" : arry_Products,
            "transaction_id" : "",//At this time this will be blank, because transaction is pending now.
            "coupon" : couponCode,
            "is_partial" : isPartial,
            "devicetype" : "ios",
            "card_detail" : cardDetail,
            "token_name" : token_name,
            "amount" : "\(str_labelTotalAmountValue.integerValue)",
            "gift_wrap_fee" : is_GiftApplied,
            "remember_me" : str_rememberMe,
            "cacheignore" : "\(Int(NSDate().timeIntervalSince1970))" //To ignore cache from api...
        ]
        
        let headers : HTTPHeaders = [
            "Content-Type" : "application/json"
        ]
        
        AppDelegate.showHUD(inView: self.view, message:"Loading....")
        Alamofire.request(Constants.apiURL + "webservice/createorderwithcheckout.php", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers)
            .responseJSON{ response in
                AppDelegate.hideHUD(inView: self.view)
                switch response.result {
                case .success:
                    if let json = response.result.value{
                        print("JSON Response : \(json)")
                        guard let _ : NSDictionary = json as? NSDictionary else {
                            return
                        }
                        do {
                            // make sure this JSON is in the format we expect
                            let jsonData =  try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
                            let decoded = try JSONSerialization.jsonObject(with: jsonData, options: []) as! NSDictionary
                            if decoded["msg"] as? String == "success"{
                                self.initiateCheckOutAppsFlyer(coupanCode: self.str_Coupon_Code)
                                self.addPaymentInfoAppsFlyper()
                                self.beginCheckoutFirebaseAnylatics()
                                self.addPaymentInfoFirebaseAnylatics()
                                
                                if let _ = decoded["redirect_link"], let _ =  decoded["order_id"] {
                                    self.initiateCheckOutAppsFlyer(coupanCode: self.str_Coupon_Code)
                                    self.addPaymentInfoAppsFlyper()
                                    self.beginCheckoutFirebaseAnylatics()
                                    self.addPaymentInfoFirebaseAnylatics()
                                    
                                    let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "checkout3DSecureURLWebViewVC") as! Checkout3DSecureURLWebViewVC
                                    if self.str_rememberMe == "YES" {
                                        vc.tokenNameStr = token_name as String
                                    }
                                    vc.str_threeDSecureURL = decoded["redirect_link"] as? String ?? ""
                                    vc.str_orderID = decoded["order_id"] as? String ?? ""
                                    vc.amount_Final = self.amount_Final
                                    vc.str_Coupon_Code = "\(self.str_Coupon_Code)"
                                    self.navigationController?.pushViewController(vc, animated: false)
                                }
                                else if (decoded["payment_method"] as? String == "checkoutcom_apple_pay") {
                                    self.openApplePayController(orderId: decoded["order_id"] as! String)
                                }
                                else if (decoded["payment_method"] as? String == "msp_cashondelivery") && (decoded["is_partial"] as? String == "1"){
                                    self.orderStatusUpdateApiCalled(order_id: "\(decoded["order_id"] as! String)", status: "success", transactionId: "")
                                }
                                else if let _ = decoded["response_message"]{
                                    iToast.show(decoded["response_message"] as? String)
                                }else{
                                    let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "thankYouVC") as! ThankYouVC
                                    vc.string_OrderId = decoded["order_id"] as! String
                                    vc.string_Price = "\(self.amount_Final)"
                                    vc.string_Coupon = "\(self.str_Coupon_Code)"
                                    self.navigationController?.pushViewController(vc, animated: false)
                                }
                            }else{
                                self.showAlertForMoveBackToCart(message: "حدث خطأ - يرجي اعادة المحاولة")  //Something went wrong, please try again.
                            }
                        }
                        catch let error as NSError {
                            print("Failed to load: \(error.localizedDescription)")
                        }
                    }
                case .failure( _):
                    self.showAlertForMoveBackToCart(message: "حدث خطأ - يرجي اعادة المحاولة")  //Something went wrong, please try again.
                }
        }
    }
    
    func createOrderAPICalled_STCPay(paymentMethod: NSString, isPartial: NSString) {
        //Payment
        let dict_Payment : NSMutableDictionary = NSMutableDictionary()
        dict_Payment.setValue("\(loggedInUser.shared.string_firstName)", forKey: "firstname")
        dict_Payment.setValue("\(loggedInUser.shared.string_lastName)", forKey: "lastname")
        dict_Payment.setValue("\(city)", forKey: "city")
        dict_Payment.setValue("\(country_id)", forKey: "country_id")//accessibilityLabel is coutry id
        dict_Payment.setValue("\(country_id)", forKey: "region")
        dict_Payment.setValue("\(loggedInUser.shared.string_mobileNumber)", forKey: "telephone")
        let dict_StreetPayment : NSMutableDictionary = NSMutableDictionary()
        dict_StreetPayment.setValue("\(street)", forKey: "0")
        dict_StreetPayment.setValue("", forKey: "1")
        dict_Payment.setValue(dict_StreetPayment, forKey: "street")
        
        //Shipping
        let dict_Shipping : NSMutableDictionary = NSMutableDictionary()
        dict_Shipping.setValue(first_name, forKey: "firstname")//accessibilityLabel is firstname...
        dict_Shipping.setValue(last_name, forKey: "lastname")//accessibilityHint is lastname
        dict_Shipping.setValue("\(city)", forKey: "city")
        dict_Shipping.setValue("\(country_id)", forKey: "country_id") //accessibilityLabel is coutry id
        dict_Shipping.setValue("\(country_id)", forKey: "region")
        dict_Shipping.setValue("\(mobile)", forKey: "telephone")
        let dict_ShippingStreet : NSMutableDictionary = NSMutableDictionary()
        dict_ShippingStreet.setValue("\(street)", forKey: "0")
        dict_ShippingStreet.setValue("", forKey: "1")
        dict_Shipping.setValue(dict_ShippingStreet, forKey: "street")
        
        let arry_Products : NSMutableArray = NSMutableArray()
        for indx in 0 ..< array_CartItem.count{
            let dict_Product : NSMutableDictionary = NSMutableDictionary()
            
            let dict_Item : NSDictionary = array_CartItem[indx] as! NSDictionary
            var qty : String = String()
            
            if let intValue = (dict_Item["qty"] as? Int) {
                qty = String(format: "%d", intValue)
            }
            else if let strValue = (dict_Item["qty"] as? String) {
                qty = String(format: "%d", (strValue as NSString).integerValue)
            }
            dict_Product.setValue("\(qty)", forKey: "qty")
            
            var product_id : String = String()
            if let intValue = (dict_Item["product_id"] as? Int) {
                product_id = String(format: "%d", intValue)
            }
            else if let strValue = (dict_Item["product_id"] as? String) {
                product_id = String(format: "%@", (strValue as NSString))
            }
            dict_Product.setValue("\(product_id)", forKey: "product_id")
            
            let arry_SuperAttribute : NSMutableArray = NSMutableArray()
            
            if dict_Item["type"] as! String == "configurable"{
                
                let dict_superAttribute : NSMutableDictionary = NSMutableDictionary()
                
                guard let ary_Options : NSArray = dict_Item["option"] as? NSArray else {
                    return
                }
                guard let dict_ColorAndSize : NSDictionary =  ary_Options[0] as? NSDictionary else {
                    return
                }
                guard let obj_Color : NSDictionary =  dict_ColorAndSize["color"] as? NSDictionary else {
                    return
                }
                
                if let id = obj_Color["id"] as? Int{
                    if let value = obj_Color["value"] as? Int{
                        dict_superAttribute.setValue("\(value)", forKey: "\(id)")
                    }
                }
                
                guard let obj_Size : NSDictionary =  dict_ColorAndSize["size"] as? NSDictionary else {
                    return
                }
                if let id = obj_Size["id"] as? Int{
                    if let value = obj_Size["value"] as? Int{
                        dict_superAttribute.setValue("\(value)", forKey:"\(id)")  }  }
                
                //arry_SuperAttribute.add(dict_superAttribute)
                dict_Product.setValue(dict_superAttribute, forKey: "super_attribute")
            }
            else {
                dict_Product.setValue(arry_SuperAttribute, forKey: "super_attribute")
            }
            arry_Products.add(dict_Product)
        }
        
        var couponCode = ""
        if (str_Coupon_Code != ""){
            couponCode = str_Coupon_Code.trimmingCharacters(in: .whitespacesAndNewlines)
        }
        
        let str_labelTotalAmountValue : NSString = NSString(string: label_totalAmount.text ?? "")
        
        let parameters : Parameters = [
            "user_id" : loggedInUser.shared.string_userID,
            "payment_method" : paymentMethod,
            "payment" : dict_Payment,
            "shipping_method" : shippingMethod,
            "shipping" : dict_Shipping,
            "products" : arry_Products,
            "transaction_id" : "",//At this time this will be blank, because transaction is pending now.
            "coupon" : couponCode,
            "is_partial" : isPartial,
            "devicetype" : "ios",
            "gift_wrap_fee" : is_GiftApplied,
            "stcmobile" : "+966" + (stcPhoneNumberTextField.text ?? ""), // "+966539396141" 
            "amount" : "\(str_labelTotalAmountValue.integerValue)",
            "cacheignore" : "\(Int(NSDate().timeIntervalSince1970))" //To ignore cache from api...
        ]
        
        let headers : HTTPHeaders = [
            "Content-Type" : "application/json"
        ]
        
        AppDelegate.showHUD(inView: self.view, message:"Loading....")
        Alamofire.request(Constants.apiURL + "webservice/createorderwithstc.php", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers)
            .responseJSON{ response in
                DispatchQueue.main.async {
                    AppDelegate.hideHUD(inView: self.view)
                }
                switch response.result {
                case .success:
                    if let json = response.result.value{
                        print("JSON Response : \(json)")
                        guard let _ : NSDictionary = json as? NSDictionary else {
                            return
                        }
                        do {
                            // make sure this JSON is in the format we expect
                            let jsonData =  try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
                            let decoded = try JSONSerialization.jsonObject(with: jsonData, options: []) as! NSDictionary
                            if decoded["msg"] as? String == "success"{
                                if (decoded["payment_method"] as? String == "stcpay") {
                                    self.OTPReferenceSTC = decoded["OtpReference"] as? String ?? ""
                                    self.STCPaymentReferenceSTC = decoded["STCPayPmtReference"] as? String ?? ""
                                    self.OrderIdSTC = decoded["order_id"] as? String ?? ""
                                    self.showOTPView()
                                }
                            }
                            else {
                                Utility.showAlert(message: decoded["data"] as? String ?? "حدث خطأ - يرجي اعادة المحاولة", controller: self)//Something went wrong, please try again.
                            }
                        }
                        catch let error as NSError {
                            print("Failed to load: \(error.localizedDescription)")
                        }
                    }
                case .failure( _):
                    Utility.showAlert(message: "حدث خطأ - يرجي اعادة المحاولة", controller: self)  //Something went wrong, please try again.
                }
        }
    }
    
    func STCResponseAPICalled(OtpReference: String, STCPayPmtReference: String, order_id : String) {
        
        let parameters : Parameters = [
            "OtpReference" : OtpReference,
            "OtpValue" : textFieldEnterOTP.text ?? "",
            "STCPayPmtReference" : STCPayPmtReference,
            "order_id" : order_id,
            "devicetype" : "ios",
            "cacheignore" : "\(Int(NSDate().timeIntervalSince1970))" //To ignore cache from api...
        ]
        
        let headers : HTTPHeaders = [
            "Content-Type" : "application/json"
        ]
        AppDelegate.showHUD(inView: self.view, message:"Loading....")
        Alamofire.request(Constants.apiURL + "webservice/stc_response.php", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers)
            .responseJSON{ response in
                AppDelegate.hideHUD(inView: self.view)
                switch response.result {
                case .success:
                    if let json = response.result.value{
                        print("JSON Response : \(json)")
                        guard let _ : NSDictionary = json as? NSDictionary else {
                            return
                        }
                        do {
                            // make sure this JSON is in the format we expect
                            let jsonData =  try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
                            let decoded = try JSONSerialization.jsonObject(with: jsonData, options: []) as! NSDictionary
                            if decoded["msg"] as? String == "success"{
                                if (self.otp_Timer != nil){
                                    self.otp_Timer?.invalidate()
                                }
                                self.buttonasfullScreenForOTP.removeFromSuperview()
                                
                                self.initiateCheckOutAppsFlyer(coupanCode: self.str_Coupon_Code)
                                self.addPaymentInfoAppsFlyper()
                                self.beginCheckoutFirebaseAnylatics()
                                self.addPaymentInfoFirebaseAnylatics()
                                
                                let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "thankYouVC") as! ThankYouVC
                                vc.string_OrderId = decoded["order_id"] as! String
                                vc.string_Price = "\(self.amount_Final)"
                                vc.string_Coupon = "\(self.str_Coupon_Code)"
                                self.navigationController?.pushViewController(vc, animated: false)
                            }
                            else {
                                if (self.otp_Timer != nil){
                                    self.otp_Timer?.invalidate()
                                }
                                self.buttonasfullScreenForOTP.removeFromSuperview()
                                
                                Utility.showAlert(message: decoded["data"] as? String ?? "حدث خطأ - يرجي اعادة المحاولة", controller: self)//Something went wrong, please try again.
                                //                                self.showAlertForMoveBackToCart(message: decoded["data"] as? String ?? "حدث خطأ - يرجي اعادة المحاولة")//Something went wrong, please try again.
                            } 
                        }
                        catch let error as NSError {
                            print("Failed to load: \(error.localizedDescription)")
                        }
                    }
                case .failure( _):
                    self.showAlertForMoveBackToCart(message: "حدث خطأ - يرجي اعادة المحاولة")  //Something went wrong, please try again.
                }
        }
    }
    
    func resendSTCOtpAPICalled() {
        let parameters : Parameters = [
            "stcmobile" : textFieldEnterOTP.text ?? "",
            "order_id" : OrderIdSTC,
            "cacheignore" : "\(Int(NSDate().timeIntervalSince1970))" //To ignore cache from api...
        ]
        
        let headers : HTTPHeaders = [
            "Content-Type" : "application/json"
        ]
        AppDelegate.showHUD(inView: self.view, message:"Loading....")
        Alamofire.request(Constants.apiURL + "webservice/stc_resendotp.php", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers)
            .responseJSON{ response in
                AppDelegate.hideHUD(inView: self.view)
                switch response.result {
                case .success:
                    if let json = response.result.value{
                        print("JSON Response : \(json)")
                        guard let _ : NSDictionary = json as? NSDictionary else {
                            return
                        }
                        do {
                            // make sure this JSON is in the format we expect
                            let jsonData =  try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
                            let decoded = try JSONSerialization.jsonObject(with: jsonData, options: []) as! NSDictionary
                            if decoded["msg"] as? String == "success" {
                                iToast.show("تم ارسال الكود بنجاح")//OTP sent successfully
                            }
                            else {
                                Utility.showAlert(message: decoded["msg"] as? String ?? "فشل الارسال -اعادة المحاولة", controller: self)//OTP not sent, please try again
                            }
                        }
                        catch let error as NSError {
                            print("Failed to load: \(error.localizedDescription)")
                        }
                    }
                case .failure( _):
                    self.showAlertForMoveBackToCart(message: "حدث خطأ - يرجي اعادة المحاولة")  //Something went wrong, please try again.
                }
        }
    }
    
    //OTP Functionality...
    func showOTPView(){
        
        value_Timer = 30
        
        buttonasfullScreenForOTP = UIButton()
        buttonasfullScreenForOTP.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        buttonasfullScreenForOTP.frame = CGRect(x: 0, y: 0, width: UIApplication.shared.keyWindow?.bounds.size.width ?? 0.0, height: UIApplication.shared.keyWindow?.bounds.size.height ?? 0.0)
        buttonasfullScreenForOTP.addTarget(self, action: #selector(removebuttonasfullScreenForOTP), for: .touchUpInside)
        self.view.addSubview(buttonasfullScreenForOTP)
        
        let viewOTP = UIView()
        if UIDevice.current.userInterfaceIdiom == .pad{
            viewOTP.frame = CGRect(x: 20, y: (buttonasfullScreenForOTP.bounds.size.height - 352) / 2 - 64, width: buttonasfullScreenForOTP.bounds.size.width - 40, height: 280)
        }else {
            viewOTP.frame = CGRect(x: 20, y: (buttonasfullScreenForOTP.bounds.size.height - 216) / 2 - 40, width: buttonasfullScreenForOTP.bounds.size.width - 40, height: 240)
        }
        viewOTP.backgroundColor = UIColor.white
        buttonasfullScreenForOTP.addSubview(viewOTP)
        
        let labelTitle = UILabel()
        labelTitle.frame = CGRect(x: 30, y: 0, width: viewOTP.bounds.size.width - 60, height: 40)
        labelTitle.font = UIFont (name: Constants.helvetica_bold_font, size: 14)
        labelTitle.text = "التحقق من رقم الجوال" //"Mobile number verification
        labelTitle.numberOfLines = 1
        labelTitle.textAlignment = .center
        labelTitle.backgroundColor = UIColor.clear
        viewOTP.addSubview(labelTitle)
        
        let labelForgotPassword = UILabel()
        labelForgotPassword.frame = CGRect(x: 30, y: 40, width: viewOTP.bounds.size.width - 60, height: 60)
        labelForgotPassword.text = "تم ارسال رسالة نصية لكم بالكود السري مكون من 4 ارقام" //A text message with a 4-digit verification code has been sent to
        //labelForgotPassword.text = "A text message with a 4-digit verification code has been sent to +919781116773)"
        labelForgotPassword.font = UIFont (name: Constants.helvetica_regular_font, size: 14)
        labelForgotPassword.numberOfLines = 3
        labelForgotPassword.textAlignment = .center
        labelForgotPassword.backgroundColor = UIColor.clear
        viewOTP.addSubview(labelForgotPassword)
        
        textFieldEnterOTP = UITextField()
        textFieldEnterOTP.frame = CGRect(x: 30, y: 100, width: viewOTP.bounds.size.width - 60, height: 40)
        textFieldEnterOTP.font = UIFont.systemFont(ofSize: 16)
        textFieldEnterOTP.placeholder = "برجاء كتابة رمز التحقق" //Please Enter OTP...
        textFieldEnterOTP.autocorrectionType = .no
        textFieldEnterOTP.autocapitalizationType = .none
        textFieldEnterOTP.backgroundColor = UIColor.white
        textFieldEnterOTP.keyboardType = .numberPad
        textFieldEnterOTP.clearButtonMode = .whileEditing
        textFieldEnterOTP.becomeFirstResponder()
        textFieldEnterOTP.layer.cornerRadius = 2
        viewOTP.addSubview(textFieldEnterOTP)
        let leftViewEmailId = UIView(frame: CGRect(x: 15, y: 0, width: 7, height: 26))
        leftViewEmailId.backgroundColor = UIColor.clear
        textFieldEnterOTP.leftView = leftViewEmailId
        textFieldEnterOTP.leftViewMode = .always
        textFieldEnterOTP.delegate = self
        
        label_ForTimerCountDown = UILabel()
        label_ForTimerCountDown.frame = CGRect(x: 30, y: 150, width: viewOTP.bounds.size.width - 60, height: 20)
        label_ForTimerCountDown.text = "اعادة ارسال الكود بعد \(value_Timer) ثانية" //"Resend after \(value_Timer) seconds"
        label_ForTimerCountDown.font = UIFont (name: Constants.helvetica_regular_font, size: 12)
        label_ForTimerCountDown.textColor = UIColor.lightGray
        label_ForTimerCountDown.numberOfLines = 1
        label_ForTimerCountDown.textAlignment = .center
        label_ForTimerCountDown.backgroundColor = UIColor.clear
        viewOTP.addSubview(label_ForTimerCountDown)
        
        buttonResendOTP = UIButton(type: .system)
        buttonResendOTP.frame = CGRect(x: 30, y: 180, width: viewOTP.bounds.size.width/2 - 40, height: 40)
        buttonResendOTP.setTitle("اعادة ارسال الكود", for: .normal) // Resend OTP
        buttonResendOTP.setTitleColor(UIColor.white, for: .normal)
        buttonResendOTP.layer.cornerRadius = 2
        buttonResendOTP.addTarget(self, action: #selector(buttonResendOTPClicked), for: .touchUpInside)
        viewOTP.addSubview(buttonResendOTP)
        self.enableUserInteractionReSenOTPButton(isEnable: false)
        
        
        buttonVerifyOTP = UIButton(type: .system)
        buttonVerifyOTP.frame = CGRect(x: viewOTP.bounds.size.width/2 + 10, y: 180, width: viewOTP.bounds.size.width/2 - 40, height: 40)
        buttonVerifyOTP.setTitle("ادخال الكود", for: .normal) //Verify OTP
        buttonVerifyOTP.setTitleColor(UIColor.white, for: .normal)
        buttonVerifyOTP.backgroundColor = Constants.default_ThemeColor
        buttonVerifyOTP.layer.cornerRadius = 2
        buttonVerifyOTP.addTarget(self, action: #selector(buttonVerifyOTPClicked), for: .touchUpInside)
        viewOTP.addSubview(buttonVerifyOTP)
        
        otp_Timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updatelabel_ForTimerCountDown), userInfo: nil, repeats: true)
    }
    
    @objc func buttonVerifyOTPClicked() {
        
        let trimmedCardName = textFieldEnterOTP.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        if textFieldEnterOTP.text == "" || trimmedCardName == "" {
            Utility.showAlert(message:"يرجى ادخال الكود", controller: self)//Please enter OTP.
            return
        }
        if(textFieldEnterOTP.isFirstResponder){textFieldEnterOTP.resignFirstResponder()}
        self.STCResponseAPICalled(OtpReference: OTPReferenceSTC, STCPayPmtReference: STCPaymentReferenceSTC, order_id : OrderIdSTC)
        
    }
    
    @objc func removebuttonasfullScreenForOTP() {
        if (otp_Timer != nil){
            otp_Timer?.invalidate()
        }
        self.stcCartRetainAPICalled()
    }
    
    func stcCartRetainAPICalled() {
        let parameters : Parameters = [
            "cacheignore" : "\(Int(NSDate().timeIntervalSince1970))" //To ignore cache from api...
        ]
        
        AppDelegate.showHUD(inView: self.view, message:"Loading....")
        Alamofire.request(Constants.apiURL + "webservice/stc_cartretain.php?order_id=\(OrderIdSTC)&devicetype=ios", method: .get, parameters: parameters)
            .responseJSON { response in
                AppDelegate.hideHUD(inView: self.view)
                switch response.result {
                case .success:
                    if let json = response.result.value{
                        print("JSON Response : \(json)")
                        guard let _ : NSDictionary = json as? NSDictionary else {
                            return
                        }
                        do {
                            // make sure this JSON is in the format we expect
                            let jsonData =  try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
                            let decoded = try JSONSerialization.jsonObject(with: jsonData, options: []) as! NSDictionary
                            self.buttonasfullScreenForOTP.removeFromSuperview()
                            
                            if decoded["msg"] as? String == "success" {
                                //                                iToast.show("تم ارسال الكود بنجاح")//OTP sent successfully
                            }
                            else {
                                //                                Utility.showAlert(message: decoded["data"] as? String ?? "حدث خطأ - يرجي اعادة المحاولة", controller: self)//Something went wrong, please try again.
                            }
                        }
                        catch let error as NSError {
                            print("Failed to load: \(error.localizedDescription)")
                        }
                    }
                case .failure( _):
                    self.showAlertForMoveBackToCart(message: "حدث خطأ - يرجي اعادة المحاولة")  //Something went wrong, please try again.
                }
        }
    }
    
    @objc func updatelabel_ForTimerCountDown(){
        if (otp_Timer != nil){
            value_Timer = value_Timer - 1
            label_ForTimerCountDown.text = "اعادة ارسال الكود بعد \(value_Timer) ثانية" //"Resend after \(value_Timer) seconds"
            if value_Timer == 0{
                otp_Timer!.invalidate()
                self.enableUserInteractionReSenOTPButton(isEnable: true)
                label_ForTimerCountDown.isHidden = true
            }
        }
    }
    
    @objc func buttonResendOTPClicked(){
        value_Timer = 30
        otp_Timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updatelabel_ForTimerCountDown), userInfo: nil, repeats: true)
        label_ForTimerCountDown.isHidden = false
        self.enableUserInteractionReSenOTPButton(isEnable: false)
        
        if(textFieldEnterOTP.isFirstResponder){textFieldEnterOTP.resignFirstResponder()}
        self.resendSTCOtpAPICalled()
    }
    
    func enableUserInteractionReSenOTPButton(isEnable: Bool){
        if isEnable{
            buttonResendOTP.backgroundColor = Constants.default_ThemeColor
            buttonResendOTP.isUserInteractionEnabled = true
        }
        else{
            buttonResendOTP.backgroundColor = UIColor(red: 83/255, green: 83/255, blue: 83/255, alpha: 1.0)
            buttonResendOTP.isUserInteractionEnabled = false
        }
    }
    
    func createOrderAPICalled(paymentMethod: NSString, isPartial: NSString) {
        
        //Payment
        let dict_Payment : NSMutableDictionary = NSMutableDictionary()
        dict_Payment.setValue("\(loggedInUser.shared.string_firstName)", forKey: "firstname")
        dict_Payment.setValue("\(loggedInUser.shared.string_lastName)", forKey: "lastname")
        dict_Payment.setValue("\(city)", forKey: "city")
        dict_Payment.setValue("\(country_id)", forKey: "country_id")//accessibilityLabel is coutry id
        dict_Payment.setValue("\(country_id)", forKey: "region")
        dict_Payment.setValue("\(loggedInUser.shared.string_mobileNumber)", forKey: "telephone")
        let dict_StreetPayment : NSMutableDictionary = NSMutableDictionary()
        dict_StreetPayment.setValue("\(street)", forKey: "0")
        dict_StreetPayment.setValue("", forKey: "1")
        dict_Payment.setValue(dict_StreetPayment, forKey: "street")
        
        //Shipping
        let dict_Shipping : NSMutableDictionary = NSMutableDictionary()
        dict_Shipping.setValue(first_name, forKey: "firstname")//accessibilityLabel is firstname...
        dict_Shipping.setValue(last_name, forKey: "lastname")//accessibilityHint is lastname
        dict_Shipping.setValue("\(city)", forKey: "city")
        dict_Shipping.setValue("\(country_id)", forKey: "country_id") //accessibilityLabel is coutry id
        dict_Shipping.setValue("\(country_id)", forKey: "region")
        dict_Shipping.setValue("\(mobile)", forKey: "telephone")
        let dict_ShippingStreet : NSMutableDictionary = NSMutableDictionary()
        dict_ShippingStreet.setValue("\(street)", forKey: "0")
        dict_ShippingStreet.setValue("", forKey: "1")
        dict_Shipping.setValue(dict_ShippingStreet, forKey: "street")
        
        
        let arry_Products : NSMutableArray = NSMutableArray()
        for indx in 0 ..< array_CartItem.count{
            
            let dict_Product : NSMutableDictionary = NSMutableDictionary()
            
            let dict_Item : NSDictionary = array_CartItem[indx] as! NSDictionary
            var qty : String = String()
            
            if let intValue = (dict_Item["qty"] as? Int) {
                qty = String(format: "%d", intValue)
            }
            else if let strValue = (dict_Item["qty"] as? String) {
                qty = String(format: "%d", (strValue as NSString).integerValue)
            }
            dict_Product.setValue("\(qty)", forKey: "qty")
            
            var product_id : String = String()
            if let intValue = (dict_Item["product_id"] as? Int) {
                product_id = String(format: "%d", intValue)
            }
            else if let strValue = (dict_Item["product_id"] as? String) {
                product_id = String(format: "%@", (strValue as NSString))
            }
            dict_Product.setValue("\(product_id)", forKey: "product_id")
            
            let arry_SuperAttribute : NSMutableArray = NSMutableArray()
            
            if dict_Item["type"] as! String == "configurable"{
                
                let dict_superAttribute : NSMutableDictionary = NSMutableDictionary()
                
                guard let ary_Options : NSArray = dict_Item["option"] as? NSArray else {
                    return
                }
                
                guard let dict_ColorAndSize : NSDictionary =  ary_Options[0] as? NSDictionary else {
                    return
                }
                
                guard let obj_Color : NSDictionary =  dict_ColorAndSize["color"] as? NSDictionary else {
                    return
                }
                
                if let id = obj_Color["id"] as? Int{
                    if let value = obj_Color["value"] as? Int{
                        dict_superAttribute.setValue("\(value)", forKey: "\(id)")
                    }
                }
                
                guard let obj_Size : NSDictionary =  dict_ColorAndSize["size"] as? NSDictionary else {
                    return
                }
                if let id = obj_Size["id"] as? Int{
                    if let value = obj_Size["value"] as? Int{
                        dict_superAttribute.setValue("\(value)", forKey:"\(id)")  }  }
                
                //arry_SuperAttribute.add(dict_superAttribute)
                dict_Product.setValue(dict_superAttribute, forKey: "super_attribute")
            }
            else {
                dict_Product.setValue(arry_SuperAttribute, forKey: "super_attribute")
            }
            arry_Products.add(dict_Product)
        }
        
        var couponCode = ""
        if (str_Coupon_Code != ""){
            couponCode = str_Coupon_Code.trimmingCharacters(in: .whitespacesAndNewlines)
        }
        
        let parameters : Parameters = [
            
            "user_id" : loggedInUser.shared.string_userID,
            "payment_method" : paymentMethod,
            "payment" : dict_Payment,
            "shipping_method" : shippingMethod,
            "shipping" : dict_Shipping,
            "products" : arry_Products,
            "transaction_id" : "",//At this time this will be blank, because transaction is pending now.
            "coupon" : couponCode,
            "is_partial" : isPartial,
            "devicetype" : "ios",
            "gift_wrap_fee" : is_GiftApplied,
            "cacheignore" : "\(Int(NSDate().timeIntervalSince1970))" //To ignore cache from api...
            
        ]
        
        let headers : HTTPHeaders = [
            "Content-Type" : "application/json"
        ]
        
        AppDelegate.showHUD(inView: self.view, message:"Loading....")
        Alamofire.request(Constants.apiURL + "createorderwithcheckout.php", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers)
            .responseJSON{ response in
                AppDelegate.hideHUD(inView: self.view)
                switch response.result {
                case .success:
                    if let json = response.result.value{
                        print("JSON Response : \(json)")
                        guard let _ : NSDictionary = json as? NSDictionary else {
                            return
                        }
                        do {
                            // make sure this JSON is in the format we expect
                            let jsonData =  try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
                            let decoded = try JSONSerialization.jsonObject(with: jsonData, options: []) as! NSDictionary
                            if decoded["msg"] as? String == "success"{
                                
                                self.initiateCheckOutAppsFlyer(coupanCode: self.str_Coupon_Code)
                                self.addPaymentInfoAppsFlyper()
                                self.beginCheckoutFirebaseAnylatics()
                                self.addPaymentInfoFirebaseAnylatics()
                                
                                if (decoded["payment_method"] as? String == "checkoutcom_apple_pay") {
                                    self.openApplePayController(orderId: decoded["order_id"] as! String)
                                    
                                    //   self.getSDKTokenForApplePayApiCalled(orderId: (decoded["order_id"] as! String))
                                    
                                }else if (decoded["payment_method"] as? String == "msp_cashondelivery") && (decoded["is_partial"] as? String == "1"){
                                    
                                    self.orderStatusUpdateApiCalled(order_id: "\(decoded["order_id"] as! String)", status: "success", transactionId: "")
                                    
                                }else{
                                    
                                    let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "thankYouVC") as! ThankYouVC
                                    vc.string_OrderId = decoded["order_id"] as! String
                                    vc.string_Price = "\(self.amount_Final)"
                                    vc.string_Coupon = "\(self.str_Coupon_Code)"
                                    self.navigationController?.pushViewController(vc, animated: false)
                                }
                                
                            }else {
                                self.showAlertForMoveBackToCart(message: decoded["data"] as? String ?? "حدث خطأ - يرجي اعادة المحاولة")//Something went wrong, please try again.
                            }
                        }
                        catch let error as NSError {
                            print("Failed to load: \(error.localizedDescription)")
                        }
                    }
                case .failure( _):
                    self.showAlertForMoveBackToCart(message: "حدث خطأ - يرجي اعادة المحاولة")  //Something went wrong, please try again.
                }
        }
    }
    
    func showAlertForMoveBackToCart(message: String) {
        let alertController = UIAlertController(title: Constants.msg_Alert, message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "حسنا", style: .default, handler: { action in
            switch action.style{
            case .default:
                print("default")
                self.navigationController?.popToRootViewController(animated: false)
            case .cancel:
                print("cancel")
            case .destructive:
                print("destructive")
            @unknown default: break
            }}))
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    //MARK: - UITableView DataSource & Delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return array_rememberMeCards.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "checkoutRememberMeCardTableViewCell", for: indexPath) as! CheckoutRememberMeCardTableViewCell
        if selectedRememberMeCardIndex == indexPath.row {
            cell.circle_Image_RememberMeCard.image = UIImage.init(named: "yellow-tick")
        }else {
            cell.circle_Image_RememberMeCard.image = UIImage.init(named: "gray-tick")
        }
        cell.layoutIfNeeded()
        cell.base_View.setShadow(shadowSize: 10.0, radius: 10.0)
        let dict_Card : NSDictionary = array_rememberMeCards[indexPath.row] as! NSDictionary
        cell.card_NameLbl.text = "\(dict_Card["name"] ?? "")"
        cell.card_NumberLbl.text = "\(dict_Card["card_number"] ?? "")"
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return indexPath.row == selectedRememberMeCardIndex ? 135 : 80
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let str_labelTotalAmountValue : NSString = NSString(string: label_totalAmount.text ?? "")
        if(str_labelTotalAmountValue.integerValue == 0){
            image_wallet.image = UIImage.init(named: "gray-tick")
            label_totalAmount.text = "\(amount_Final)"
            label_walletBalanceValue.text = "\(amount_Wallet)"
        }
        
        if image_wallet.image == UIImage.init(named: "yellow-tick") {
            let str_walletLabelValue : NSString = NSString(string: label_walletBalanceValue.text ?? "")
            label_totalAmount.text = "\(amount_Final - str_walletLabelValue.integerValue)"
        }else{
            
            label_totalAmount.text = "\(amount_Final)"
            view_walletHeightConstraints.constant = 50
            view_leftWalletBalance.isHidden = true
            
        }
        
        //        if amount_Final < 200 && country_id == "SA"{//If total amount below 200SAR, then for Saudi Arabia country extra 25SAR shipping charge will be applied.
        //        label_totalAmount.text = "\(amount_Final + 25)"
        //        }
        
        self.view_cardFormHeightConstraints.constant = 0
        image_creditDebitCard.image = UIImage.init(named: "gray-tick")
        image_applePay.image = UIImage.init(named: "gray-tick")
        image_COD.image = UIImage.init(named: "gray-tick")
        image_STC.image = UIImage.init(named: "gray-tick")
        
        self.stcHideShow()
        
        self.view_applePayButton.isHidden = true
        selectedRememberMeCardIndex = indexPath.row
        self.rememberMe_Card_TableViewHeightConstraints.constant = CGFloat(80*array_rememberMeCards.count + 55)
        self.rememberMe_Card_TableView.reloadData()
        
    }
    
    
    //MARK: - UIButton Clicks
    @IBAction func backButtonClicked(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func button_creditDebitCardClicked(_ sender: UIButton) {
        if image_creditDebitCard.image == UIImage.init(named: "yellow-tick") {return}//If payment method is already selected...
        let str_labelTotalAmountValue : NSString = NSString(string: label_totalAmount.text ?? "")
        if(str_labelTotalAmountValue.integerValue == 0){
            image_wallet.image = UIImage.init(named: "gray-tick")
            label_walletBalanceValue.text = "\(amount_Wallet)"
            label_totalAmount.text = "\(amount_Final)"
        }
        
        if image_wallet.image == UIImage.init(named: "yellow-tick") {
            let str_walletLabelValue : NSString = NSString(string: label_walletBalanceValue.text ?? "")
            label_totalAmount.text = "\(amount_Final - str_walletLabelValue.integerValue)"
        }
        else{
            label_totalAmount.text = "\(amount_Final)"
            view_walletHeightConstraints.constant = 50
            view_leftWalletBalance.isHidden = true
        }
        
        image_creditDebitCard.image = UIImage.init(named: "yellow-tick")
        image_applePay.image = UIImage.init(named: "gray-tick")
        image_COD.image = UIImage.init(named: "gray-tick")
        image_STC.image = UIImage.init(named: "gray-tick")
        
        self.view_cardFormHeightConstraints.constant = 298
        self.view_applePayButton.isHidden = true
        self.stcHideShow()
        self.selectedRememberMeCardIndex = -1
        self.rememberMe_Card_TableViewHeightConstraints.constant = CGFloat(80*array_rememberMeCards.count)
        self.rememberMe_Card_TableView.reloadData()
    }
    
    @IBAction func expirationDateButtonClicked(_ sender: UIButton) {
        card_ExpiryTextField.resignFirstResponder()
        expirationDatePicker = MonthYearPickerView(frame: CGRect(origin: CGPoint(x: 0, y: (view.bounds.height - 216) / 2), size: CGSize(width: view.bounds.width, height: 216)))
        expirationDatePicker.minimumDate = Date()
        expirationDatePicker.maximumDate = Calendar.current.date(byAdding: .year, value: 10, to: Date())
        expirationDatePicker.addTarget(self, action: #selector(datePickerValueChanged(_:)), for: .valueChanged)
        view.addSubview(expirationDatePicker)
        card_ExpiryTextField?.inputView = expirationDatePicker
        card_ExpiryTextField.tintColor = UIColor.clear
        card_ExpiryTextField?.becomeFirstResponder()
    }
    
    @IBAction func button_rememberMeCheckBoxClicked(_ sender: UIButton) {
        
        if image_autoSavedCheckBox.image == UIImage.init(named: "checkBoxSelected") {
            image_autoSavedCheckBox.image = UIImage.init(named: "checkBoxUnselected")
            str_rememberMe = "NO"
            
        }else {
            image_autoSavedCheckBox.image = UIImage.init(named: "checkBoxSelected")
            str_rememberMe = "YES"
        }
    }
    
    @IBAction func button_applePayClicked(_ sender: UIButton) {
        
        if image_applePay.image == UIImage.init(named: "yellow-tick") {return}//If payment method is already selected...
        
        let str_labelTotalAmountValue : NSString = NSString(string: label_totalAmount.text ?? "")
        if(str_labelTotalAmountValue.integerValue == 0){
            image_wallet.image = UIImage.init(named: "gray-tick")
            label_walletBalanceValue.text = "\(amount_Wallet)"
            label_totalAmount.text = "\(amount_Final)"
        }
        
        if image_wallet.image == UIImage.init(named: "yellow-tick") {
            let str_walletLabelValue : NSString = NSString(string: label_walletBalanceValue.text ?? "")
            label_totalAmount.text = "\(amount_Final - str_walletLabelValue.integerValue)"
        }
        else{
            label_totalAmount.text = "\(amount_Final)"
            view_walletHeightConstraints.constant = 50
            view_leftWalletBalance.isHidden = true
        }
        
        self.view_cardFormHeightConstraints.constant = 0
        image_creditDebitCard.image = UIImage.init(named: "gray-tick")
        image_applePay.image = UIImage.init(named: "yellow-tick")
        image_STC.image = UIImage.init(named: "gray-tick")
        image_COD.image = UIImage.init(named: "gray-tick")
        self.view_applePayButton.isHidden = false
        self.stcHideShow()
        self.selectedRememberMeCardIndex = -1
        self.rememberMe_Card_TableViewHeightConstraints.constant = CGFloat(80*array_rememberMeCards.count)
        self.rememberMe_Card_TableView.reloadData()
    }
    
    @IBAction func button_stcPaymentButtonClicked(_ sender: UIButton) {
        if image_STC.image == UIImage.init(named: "yellow-tick") {return}//If payment method is already selected...
        
        let str_labelTotalAmountValue : NSString = NSString(string: label_totalAmount.text ?? "")
        if(str_labelTotalAmountValue.integerValue == 0){
            image_wallet.image = UIImage.init(named: "gray-tick")
            label_walletBalanceValue.text = "\(amount_Wallet)"
            label_totalAmount.text = "\(amount_Final)"
        }
        
        if image_wallet.image == UIImage.init(named: "yellow-tick") {
            let str_walletLabelValue : NSString = NSString(string: label_walletBalanceValue.text ?? "")
            label_totalAmount.text = "\(amount_Final - str_walletLabelValue.integerValue)"
        }
        else{
            label_totalAmount.text = "\(amount_Final)"
            view_walletHeightConstraints.constant = 50
            view_leftWalletBalance.isHidden = true
        }
        
        image_STC.image = UIImage.init(named: "yellow-tick")
        image_applePay.image = UIImage.init(named: "gray-tick")
        image_COD.image = UIImage.init(named: "gray-tick")
        image_creditDebitCard.image = UIImage.init(named: "gray-tick")
        
        self.view_cardFormHeightConstraints.constant = 0
        self.view_applePayButton.isHidden = true
        self.view_STCHeightConstraints.constant = 110
        self.view_STC.frame.size.height = 110
        self.view_STC.setShadow(shadowSize: 10.0, radius: 10.0)
        
        self.selectedRememberMeCardIndex = -1
        self.rememberMe_Card_TableViewHeightConstraints.constant = CGFloat(80*array_rememberMeCards.count)
        self.rememberMe_Card_TableView.reloadData()
    }
    
    @IBAction func button_walletClicked(_ sender: UIButton) {
        if amount_Wallet == 0{return}//If wallet balance is zero, then there is need to choose this option
        
        if image_wallet.image == UIImage.init(named: "yellow-tick") {
            
            image_wallet.image = UIImage.init(named: "gray-tick")
            view_walletHeightConstraints.constant = 50
            view_leftWalletBalance.isHidden = true
            //  self.view_Wallet.setShadow1(shadowSize: 10.0, radius: 10.0)
            
            label_totalAmount.text = "\(amount_Final)"
            label_walletBalanceValue.text = "\(amount_Wallet)"
            
            if image_COD.image == UIImage.init(named: "yellow-tick") {//If COD option is already selected
                let CODAmount : Int = self.dict_CompleteCartRecord["cod_charges"] as? Int ?? 0
                label_totalAmount.text = "\(amount_Final + CODAmount)"
            }
        }
        else {
            image_wallet.image = UIImage.init(named: "yellow-tick")
            view_walletHeightConstraints.constant = 60
            view_leftWalletBalance.isHidden = false
            //  self.view_Wallet.setShadow1(shadowSize: 10.0, radius: 10.0)
            if amount_Wallet >= amount_Final{ //If wallet amount is more than total amount. Then other payment options need to reset, because user can pay full amount from wallet...
                
                label_totalAmount.text = "0"
                label_walletBalanceValue.text = "\(amount_Final)"
                label_leftWalletBalanceValue.text = "\(amount_Wallet - amount_Final)"
                
                self.view_cardFormHeightConstraints.constant = 0
                image_creditDebitCard.image = UIImage.init(named: "gray-tick")
                image_applePay.image = UIImage.init(named: "gray-tick")
                image_STC.image = UIImage.init(named: "gray-tick")
                image_COD.image = UIImage.init(named: "gray-tick")
                self.view_applePayButton.isHidden = true
                self.stcHideShow()
                
                self.selectedRememberMeCardIndex = -1
                self.rememberMe_Card_TableViewHeightConstraints.constant = CGFloat(80*array_rememberMeCards.count)
                self.rememberMe_Card_TableView.reloadData()
            }
            else {
                label_totalAmount.text = "\(amount_Final - amount_Wallet)"
                if image_COD.image == UIImage.init(named: "yellow-tick") {//If COD option is already selected
                    let str_labelTotalAmountValue : NSString = NSString(string: label_totalAmount.text ?? "")
                    let CODAmount : Int = self.dict_CompleteCartRecord["cod_charges"] as? Int ?? 0
                    label_totalAmount.text = "\(str_labelTotalAmountValue.integerValue + CODAmount)"
                }
                label_walletBalanceValue.text = "\(amount_Wallet)"
                label_leftWalletBalanceValue.text = "0"
            }
        }
    }
    
    @IBAction func button_CODClicked(_ sender: UIButton) {
        self.stcHideShow()

        if image_COD.image == UIImage.init(named: "yellow-tick") {return}//If payment method is already selected...

        let str_labelTotalAmountValue : NSString = NSString(string: label_totalAmount.text ?? "")
        if(str_labelTotalAmountValue.integerValue == 0){
            
            image_wallet.image = UIImage.init(named: "gray-tick")
            image_STC.image = UIImage.init(named: "gray-tick")
            label_walletBalanceValue.text = "\(amount_Wallet)"
            label_totalAmount.text = "\(amount_Final)"
            self.stcHideShow()
        }
        
        let CODAmount : Int = self.dict_CompleteCartRecord["cod_charges"] as? Int ?? 0
        if image_wallet.image == UIImage.init(named: "yellow-tick") {
            let str_totalAmountValue : NSString = NSString(string: label_totalAmount.text ?? "")
            label_totalAmount.text = "\(str_totalAmountValue.integerValue + CODAmount)"
        }
        else{
            label_totalAmount.text = "\(amount_Final + CODAmount)"
            view_walletHeightConstraints.constant = 50
            view_leftWalletBalance.isHidden = true
        }
        
        
        //        if amount_Final < 200 {
        //
        //            let str_totalAmountValue : NSString = NSString(string: label_totalAmount.text ?? "")
        //            label_totalAmount.text = "\(str_totalAmountValue.integerValue - shippingCharge)"//If total amount below 200SAR, then for Saudi Arabia country shipping charge will be deducted.
        //        }
        
        self.view_cardFormHeightConstraints.constant = 0
        image_creditDebitCard.image = UIImage.init(named: "gray-tick")
        image_applePay.image = UIImage.init(named: "gray-tick")
        image_STC.image = UIImage.init(named: "gray-tick")
        image_COD.image = UIImage.init(named: "yellow-tick")
        
        self.view_applePayButton.isHidden = true
        self.selectedRememberMeCardIndex = -1
        self.rememberMe_Card_TableViewHeightConstraints.constant = CGFloat(80*array_rememberMeCards.count)
        self.rememberMe_Card_TableView.reloadData()
        
    }
    
    @IBAction func button_SaveAndContinueClicked(_ sender: UIButton) {
        
        if image_creditDebitCard.image == UIImage.init(named: "yellow-tick") {
            
            let trimmedCardName = card_NameTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            let trimmedCardNumber = card_NumberTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            let trimmedSecurityCode = card_SecurityCodeTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            let trimmedExpiry = card_ExpiryTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            
            
            if trimmedCardName == "" || trimmedCardNumber == "" || trimmedSecurityCode == "" || trimmedExpiry == ""{
                Utility.showAlert(message: "برجاء فحص بيانات البطاقة", controller: self)//Please check card detail.
                return
            }
            //Card Detail
            let dict_CardDetail : NSMutableDictionary = NSMutableDictionary()
            dict_CardDetail.setValue(trimmedCardName, forKey: "name")//card_Name
            dict_CardDetail.setValue(trimmedCardNumber, forKey: "number")//card_Number
            dict_CardDetail.setValue(trimmedSecurityCode, forKey: "securitycode")//card_SecurityCode
            dict_CardDetail.setValue(trimmedExpiry, forKey: "expiry")//card_Expiry(i.e. 2207 format)
            //            let ary_Date =  trimmedExpiry.components(separatedBy: " / ")
            //            var str_finalExpiry = String()
            //            str_finalExpiry.append(ary_Date[1])
            //            str_finalExpiry.append(ary_Date[0])
            //            dict_CardDetail.setValue("\(str_finalExpiry)", forKey: "expiry")//card_Expiry(i.e. 2207 format)
            var is_Partial : NSString = "0"
            if image_wallet.image == UIImage.init(named: "yellow-tick"){ is_Partial = "1" }
            //            self.createOrderAPICalled_Payfort(paymentMethod:"payfort_fort_cc", isPartial: is_Partial, cardDetail: dict_CardDetail, token_name: "")
            
            let card = getCardTokenRequest(cardName : trimmedCardName, cardNumber : trimmedCardNumber, expiry : trimmedExpiry, cvv: trimmedSecurityCode)
            print(card)
            AppDelegate.showHUD(inView: self.view, message:"Loading....")
            checkoutAPIClient.createCardToken(card: card, successHandler: { cardToken in
                AppDelegate.hideHUD(inView: self.view)
                self.createOrderAPICalled_CheckOut(paymentMethod:"checkoutcom_card_payment", isPartial: is_Partial, cardDetail: dict_CardDetail, token_name: cardToken.token as NSString)
            }, errorHandler: { error in
                iToast.show(error.errorType)
                AppDelegate.hideHUD(inView: self.view)
            })
            return
        }
        
        
        if image_applePay.image == UIImage.init(named: "yellow-tick") {
            
            if #available(iOS 12.1.1, *) {
                
                let canMakePayment : Bool =  PKPaymentAuthorizationController.canMakePayments(usingNetworks: [PKPaymentNetwork.visa, PKPaymentNetwork.masterCard, PKPaymentNetwork.amex,  PKPaymentNetwork.mada])
                
                if !canMakePayment{//If no apple pay card is added in wallet and apple pay in settings.
                    
                    self.showOpenSettingAppAlert()//Please add a card into your Apple Pay Wallet iPhone
                    return
                }
                
            } else {
                
                let canMakePayment : Bool = PKPaymentAuthorizationController.canMakePayments(usingNetworks: [PKPaymentNetwork.visa, PKPaymentNetwork.masterCard, PKPaymentNetwork.amex])
                
                if !canMakePayment{//If no apple pay card is added in wallet and apple pay in settings.
                    
                    self.showOpenSettingAppAlert()//Please add a card into your Apple Pay Wallet iPhone
                    return
                }
                
            }
            
            //User needs to create order first then make payment(except cash on delivery)...
            var is_Partial : NSString = "0"
            if image_wallet.image == UIImage.init(named: "yellow-tick"){ is_Partial = "1" }
            self.createOrderAPICalled_CheckOut(paymentMethod: "checkoutcom_apple_pay", isPartial: is_Partial, cardDetail: NSMutableDictionary(), token_name: "")
            // self.openApplePayController(orderId: "")
            
            return
            
        }
        
        if image_COD.image == UIImage.init(named: "yellow-tick") {
            //User needs to create order first then make payment(except cash on delivery)...
            var is_Partial : NSString = "0"
            if image_wallet.image == UIImage.init(named: "yellow-tick"){ is_Partial = "1" }
            self.createOrderAPICalled_CheckOut(paymentMethod: "msp_cashondelivery", isPartial: is_Partial, cardDetail: NSMutableDictionary(), token_name: "")
            return
            
        }
        
        if image_STC.image == UIImage.init(named: "yellow-tick") {
            let trimmedSTCPhoneNumber = stcPhoneNumberTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            if trimmedSTCPhoneNumber == "" /* || stcPhoneNumberTextField.text!.count != maxlength_STCPhoneNumberfield*/ {
                Utility.showAlert(message:"من فضلك أدخل رقم الجوال", controller: self)
                return
            }
            
            //            self.validateStcMobileApi()
            var is_Partial : NSString = "0"
            if self.image_wallet.image == UIImage.init(named: "yellow-tick"){ is_Partial = "1" }
            self.createOrderAPICalled_STCPay(paymentMethod: "stcpay", isPartial: is_Partial)
            return
        }
        
        if (selectedRememberMeCardIndex != -1){ //If any option is selected in saved cards list...
            
            let indxPath : IndexPath = IndexPath.init(row: selectedRememberMeCardIndex, section: 0)
            let cell: CheckoutRememberMeCardTableViewCell = self.rememberMe_Card_TableView.cellForRow(at: indxPath) as! CheckoutRememberMeCardTableViewCell
            let trimmedSecurityCode = cell.card_CVVTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            if trimmedSecurityCode == ""{
                Utility.showAlert(message: "برجاء فحص بيانات البطاقة", controller: self)//Please check card detail.
                return
            }
            
            let dict_Card : NSDictionary = array_rememberMeCards[selectedRememberMeCardIndex] as! NSDictionary
            //Card Detail
            let dict_CardDetail : NSMutableDictionary = NSMutableDictionary()
            dict_CardDetail.setValue("", forKey: "name")//card_Name
            dict_CardDetail.setValue("", forKey: "number")//card_Number
            dict_CardDetail.setValue("\(trimmedSecurityCode)", forKey: "securitycode")//card_SecurityCode
            dict_CardDetail.setValue("", forKey: "expiry")//card_Expiry(i.e. 2207 format)
            var is_Partial : NSString = "0"
            if image_wallet.image == UIImage.init(named: "yellow-tick"){ is_Partial = "1" }
            
            
            //            self.createOrderAPICalled_Payfort(paymentMethod: "payfort_fort_cc", isPartial: is_Partial, cardDetail: dict_CardDetail, token_name: "\(dict_Card["token_name"] ?? "")" as NSString)
            self.createOrderAPICalled_CheckOut(paymentMethod:"checkoutcom_card_payment", isPartial: is_Partial, cardDetail: dict_CardDetail, token_name: "\(dict_Card["token_name"] ?? "")" as NSString)
            
            return
        }
        
        if image_wallet.image == UIImage.init(named: "yellow-tick") {
            
            //User needs to create order first then make payment(except cash on delivery)...
            let str_labelTotalAmountValue : NSString = NSString(string: label_totalAmount.text ?? "")
            if(str_labelTotalAmountValue.integerValue != 0){//user is paying through wallet but wallet balance is less than total amount. Then he have to select another payment option.
                
                Utility.showAlert(message:"رصيدك لايكفي برجاء اختيار طريقة دفع اضافية لسداد باقي قيمة الطلب", controller: self) //The wallet doesn't have a sufficient amount, you need to select one more payment option to complete order
                return
            }
            
            // self.createOrderAPICalled(paymentMethod: "wallet", isPartial: "0")
            self.createOrderAPICalled_CheckOut(paymentMethod: "wallet", isPartial: "0", cardDetail: NSMutableDictionary(), token_name: "")
            return
        }
        
        Utility.showAlert(message:"برجاء اختيار طريقة الدفع للاستمرار", controller: self) //Please select a payment method to continue...
    }
    
    private func getCardTokenRequest(cardName : String, cardNumber : String, expiry : String, cvv: String) -> CkoCardTokenRequest {
        let cardUtils = CardUtils()
        let cardNumber = cardUtils.standardize(cardNumber: cardNumber)
        let cvv = cvv
        let (expiryMonth, expiryYear) = cardUtils.standardize(expirationDate: expiry)
        //        // create the phone number
        //        let phoneNumber = CkoPhoneNumber(countryCode: "44", number: "7777777777")
        //        // create the address
        //        let address = CkoAddress(addressLine1: "test1", addressLine2: "test2", city: "London", state: "London", zip: "N12345", country: "GB")
        return CkoCardTokenRequest(number: cardNumber, expiryMonth: expiryMonth, expiryYear: expiryYear, cvv: cvv, name: cardName, billingAddress: CkoAddress(), phone: CkoPhoneNumber())
    }
    
    func showOpenSettingAppAlert() {
        let alertController = UIAlertController(title: Constants.msg_Alert, message: "برجاء إضافة بطاقتك الائتمانية الى محفظة الايفون", preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "حسنا", style: .default, handler: { action in
            switch action.style{
            case .default:
                print("default")
                //let url = URL(string: "App-Prefs:root=PASSBOOK")
                //let url = URL(string: "prefs:root=MUSIC")
                //let url = URL(string: UIApplication.openSettingsURLString)
                let url = URL(string: "shoebox://url-scheme")
                if UIApplication.shared.canOpenURL(url!) {
                    UIApplication.shared.open(url!, options: [:], completionHandler: nil)
                }
            case .cancel:
                print("cancel")
            case .destructive:
                print("destructive")
            @unknown default: break
            }}))
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    
    
    //MARK: - UIPickerViewDelegate Delegates
    @objc func datePickerValueChanged(_ picker: MonthYearPickerView) {
        print("date changed: \(picker.date)")
        let dateFormatter: DateFormatter = DateFormatter()        // Create date formatter
        dateFormatter.dateFormat = "MM / YY"          // Set date format
        
        // Apply date format
        let selectedDate = dateFormatter.string(from: picker.date)
        print("Selected value \(selectedDate)")
        card_ExpiryTextField?.text = selectedDate
    }
    
    //    //MARK: - Apple Pay
    //    func getSDKTokenForApplePayApiCalled(orderId: String){
    //
    //        //
    //        //        let parameters : Parameters = [
    //        //
    //        //            "service_command" : SERVICE_COMMAND,
    //        //            "access_code" : Constants.ACCESS_CODE,
    //        //            "merchant_identifier" : Constants.MERCHANT_IDENTIFIER,
    //        //            "language" : LANGUAGE,
    //        //            "device_id" : DEVICE_ID,
    //        //            "signature" : SIGNATURE
    //        //
    //        //        ]
    //
    //        //let SERVICE_COMMAND = "SDK_TOKEN"
    //        //let LANGUAGE = "en"
    //        let DEVICE_ID : String = UIDevice.current.identifierForVendor?.uuidString ?? ""
    //        //TESTSHAIN=Se8GBcDzHUIqpRvyzkmT
    //
    //        //
    //        //        let SHA = SHA256.init("\(Constants.SHA_REQUEST_PHRASE)access_code=\(Constants.ACCESS_CODE)device_id=\(DEVICE_ID)language=\(LANGUAGE)merchant_identifier=\(Constants.MERCHANT_IDENTIFIER)service_command=\(SERVICE_COMMAND)\(Constants.SHA_REQUEST_PHRASE)") //order of parameters need to be fixed otherwise sdk token will not generate....
    //        //
    //        //        let SIGNATURE = SHA.digestString()
    //
    //        AppDelegate.showHUD(inView: self.view, message:"Loading....")
    //
    //        Alamofire.upload(multipartFormData: { multipartFormData in
    //            multipartFormData.append(DEVICE_ID.data(using: .utf8)! , withName: "device_id")
    //
    //        }, to: Constants.apiURL + "webservice/getapplepaytoken.php", method: .post, headers : [:],
    //           encodingCompletion: { encodingResult in
    //            switch encodingResult {
    //            case .success(let upload, _, _):
    //                print(upload.progress)
    //                upload.responseJSON { response in
    //                    AppDelegate.hideHUD(inView: self.view)
    //                    switch response.result {
    //                    case .success:
    //                        if let json = response.result.value{
    //                            guard let _ : NSDictionary = json as? NSDictionary else {
    //                                return
    //                            }
    //                            do {
    //                                // make sure this JSON is in the format we expect
    //                                let jsonData =  try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
    //                                if let decoded = try JSONSerialization.jsonObject(with: jsonData, options: []) as? NSDictionary{
    //                                    if decoded["sdk_token"] != nil{
    //                                        self.openApplePayController(orderId: orderId)
    //                                    }
    //                                    else{
    //                                        iToast.show(decoded["response_message"] as? String)
    //                                    }
    //                                }
    //                                else{
    //                                    self.showAlertForMoveBackToCart(message: "حدث خطأ - يرجي اعادة المحاولة")  //Something went wrong, please try again.
    //                                }
    //                            }
    //                            catch let error as NSError {
    //                                print("Failed to load: \(error.localizedDescription)")
    //                            }
    //                        }
    //                    case .failure( _):
    //                        self.showAlertForMoveBackToCart(message: "حدث خطأ - يرجي اعادة المحاولة")  //Something went wrong, please try again.
    //                    }
    //                }
    //            case .failure( _):
    //                self.showAlertForMoveBackToCart(message: "حدث خطأ - يرجي اعادة المحاولة")  //Something went wrong, please try again.
    //            }
    //        })
    //    }
    
    //MARK: PKPaymentAuthorizationControllerDelegate
    func paymentAuthorizationControllerDidFinish(_ controller: PKPaymentAuthorizationController) {
        print("paymentAuthorizationControllerDidFinish")
    }
    
    func openApplePayController(orderId: String){
        
        let request = PKPaymentRequest()
        request.merchantIdentifier = Constants.KApplePayMerchantIdentifier
        if #available(iOS 12.1.1, *) {
            request.supportedNetworks = [PKPaymentNetwork.visa, PKPaymentNetwork.masterCard, PKPaymentNetwork.masterCard, PKPaymentNetwork.amex, PKPaymentNetwork.mada]
        } else {
            //Fallback on earlier versions
            request.supportedNetworks = [PKPaymentNetwork.visa, PKPaymentNetwork.masterCard, PKPaymentNetwork.amex]
        }
        //request.requiredBillingContactFields = [PKContactField.name,PKContactField.phoneNumber,PKContactField.emailAddress,PKContactField.postalAddress]
        request.merchantCapabilities = PKMerchantCapability.capability3DS
        request.countryCode = "\(country_id)"
        request.currencyCode = "SAR"
        let str_totalAmountValue : NSString = NSString(string: label_totalAmount.text ?? "")
        let priceWithoutSAR : String = "\(str_totalAmountValue.integerValue)"
        let formatter = NumberFormatter()
        formatter.generatesDecimalNumbers = true
        let finalPricevalue : NSDecimalNumber = formatter.number(from: priceWithoutSAR) as? NSDecimalNumber ?? 0
        request.paymentSummaryItems = [
            PKPaymentSummaryItem(label: "eOutlet", amount: finalPricevalue)
        ]
        
        let applePayController = PKPaymentAuthorizationViewController(paymentRequest: request)
        applePayController?.delegate = self
        //        applePayController?.accessibilityHint = "\(sdkToken)" //Here accessibilityHint is used as SDK token, genertaed from payfort for apple pay.
        applePayController?.accessibilityLabel = "\(orderId)" //Here accessibilityLabel is used as order ID, genertaed from order creation API, which will be further used as merchat_reference .
        self.present(applePayController!, animated: true, completion: nil)
    }
    
    //MARK: PKPaymentAuthorizationViewControllerDelegate
    func paymentAuthorizationViewControllerDidFinish(_ controller: PKPaymentAuthorizationViewController) {
        controller.dismiss(animated: true, completion: {
           
            if self.is_ApplePayChoose == false {
                
                self.is_ApplePayChoose = true
                let str_labelTotalAmountValue : NSString = NSString(string: self.label_totalAmount.text ?? "")
                self.sendApplePayDataToCheckoutAPICalled(order_id: controller.accessibilityLabel ?? "", token_name: "", status: "failed", amount: "\(str_labelTotalAmountValue)")
            }
        })
    }
    
    func paymentAuthorizationViewController(_ controller: PKPaymentAuthorizationViewController, didAuthorizePayment payment: PKPayment, handler completion: @escaping (PKPaymentAuthorizationResult) -> Void) {
        
        completion(PKPaymentAuthorizationResult(status: PKPaymentAuthorizationStatus.success, errors: []))
        
        self.is_ApplePayChoose = true
        
        //Perform some very basic validation on the provided contact information
        let asyncSuccessful = payment.token.paymentData.count != 0
        
        if asyncSuccessful {
            
            let data = payment.token.paymentData
            checkoutAPIClient.createApplePayToken(paymentData: data, successHandler: { cardToken in
           
                let str_labelTotalAmountValue : NSString = NSString(string: self.label_totalAmount.text ?? "")
                self.sendApplePayDataToCheckoutAPICalled(order_id: controller.accessibilityLabel ?? "", token_name: (cardToken.token as NSString) as String, status: "success", amount: "\(str_labelTotalAmountValue)")
                
            }, errorHandler: { error in
                print(error)
            })
        }
        
    }
    
    func sendApplePayDataToCheckoutAPICalled(order_id: String, token_name: String, status: String, amount: String){
        
        let parameters : Parameters = [
            "order_id" : "\(order_id)",
            "status" : "\(status)",
            "token_name" : "\(token_name)",
            "amount" : "\(amount)",
            "cacheignore" : "\(Int(NSDate().timeIntervalSince1970))" //To ignore cache from api...
        ]
        
        AppDelegate.showHUD(inView: self.view, message:"Loading....")
        Alamofire.request(Constants.apiURL+"webservice/apple_response.php", method: .post, parameters: parameters)
            .responseJSON { response in
                AppDelegate.hideHUD(inView: self.view)
                switch response.result {
                case .success:
                    if let json = response.result.value{
                        guard let _ : NSDictionary = json as? NSDictionary else {
                            return
                        }
                        do {
                            // make sure this JSON is in the format we expect
                            let jsonData =  try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
                            let decoded = try JSONSerialization.jsonObject(with: jsonData, options: []) as! NSDictionary
                            // here "decoded" is of type `Any`, decoded from JSON data
                            if decoded["msg"] as? String == "success"{
                                
                                let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "thankYouVC") as! ThankYouVC
                                vc.string_orderStatus = "\(status)"
                                vc.string_OrderId = "\(order_id)"
                                vc.string_Price = "\(self.amount_Final)"
                                vc.string_Coupon = "\(self.str_Coupon_Code)"
                                self.navigationController?.pushViewController(vc, animated: false)
                                
                            }else{
                                self.showAlertForMoveBackToCart(message: "حدث خطأ - يرجي اعادة المحاولة")  //An error has occurred - please try again
                            }
                        }
                        catch let error as NSError {
                            print("Failed to load: \(error.localizedDescription)")
                        }
                    }
                case .failure( _):
                    self.showAlertForMoveBackToCart(message: "حدث خطأ - يرجي اعادة المحاولة")  //Something went wrong, please try again.
                }
        }
    }

    func orderStatusUpdateApiCalled(order_id: String, status: String, transactionId : String){
        let parameters : Parameters = [
            "order_id" : "\(order_id)",
            "status" : "\(status)",
            "transaction" : "\(transactionId)",
            "cacheignore" : "\(Int(NSDate().timeIntervalSince1970))" //To ignore cache from api...
        ]
        
        AppDelegate.showHUD(inView: self.view, message:"Loading....")
        Alamofire.request(Constants.apiURL+"webservice/statusupdate.php", method: .post, parameters: parameters)
            .responseJSON { response in
                AppDelegate.hideHUD(inView: self.view)
                switch response.result {
                case .success:
                    if let json = response.result.value{
                        guard let _ : NSDictionary = json as? NSDictionary else {
                            return
                        }
                        do {
                            // make sure this JSON is in the format we expect
                            let jsonData =  try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
                            let decoded = try JSONSerialization.jsonObject(with: jsonData, options: []) as! NSDictionary
                            // here "decoded" is of type `Any`, decoded from JSON data
                            if decoded["msg"] as? String == "success"{
                                let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "thankYouVC") as! ThankYouVC
                                vc.string_orderStatus = "\(status)"
                                vc.string_OrderId = "\(order_id)"
                                vc.string_Price = "\(self.amount_Final)"
                                vc.string_Coupon = "\(self.str_Coupon_Code)"
                                self.navigationController?.pushViewController(vc, animated: false)
                            }
                            else{
                                self.showAlertForMoveBackToCart(message: "حدث خطأ - يرجي اعادة المحاولة")  //An error has occurred - please try again
                            }
                        }
                        catch let error as NSError {
                            print("Failed to load: \(error.localizedDescription)")
                        }
                    }
                case .failure( _):
                    self.showAlertForMoveBackToCart(message: "حدث خطأ - يرجي اعادة المحاولة")  //Something went wrong, please try again.
                }
        }
    }
    
    //MARK: - UITextField Delegates
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == card_NumberTextField {
            if CharacterSet(charactersIn: "0123456789").isSuperset(of: CharacterSet(charactersIn: string)) {
                let currentString: NSString = textField.text! as NSString
                let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
                return newString.length <= maxlength_CardNumberfield
            }else{return false}
        }else if textField == card_SecurityCodeTextField {
            if CharacterSet(charactersIn: "0123456789").isSuperset(of: CharacterSet(charactersIn: string)) {
                let currentString: NSString = textField.text! as NSString
                let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
                return newString.length <= maxlength_CartCVVNumberfield
            }else{return false}
        }
        else if textField == stcPhoneNumberTextField{//TextField Within RememberMe TableView's cell
            if CharacterSet(charactersIn: "+0123456789").isSuperset(of: CharacterSet(charactersIn: string)) {
                let currentString: NSString = textField.text! as NSString
                let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
                return newString.length <= maxlength_STCPhoneNumberfield
            }else{return false}
        }
        else if textField == textFieldEnterOTP {
            if CharacterSet(charactersIn: "0123456789").isSuperset(of: CharacterSet(charactersIn: string)) {
                let currentString: NSString = textField.text! as NSString
                let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
                return newString.length <= 6
            }else{return false}
        }
        else if textField.tag == 99{//TextField Within RememberMe TableView's cell
            if CharacterSet(charactersIn: "0123456789").isSuperset(of: CharacterSet(charactersIn: string)) {
                let currentString: NSString = textField.text! as NSString
                let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
                return newString.length <= maxlength_CartCVVNumberfield
            }else{return false}
        }
        return true
    }
}

//for appsflyer analytics
extension CheckOutPaymentOptionsVC {
    func initiateCheckOutAppsFlyer(coupanCode:String) {
        let productInfo : NSMutableDictionary =  NSMutableDictionary()
        productInfo.setValue("\(amount_Final)", forKey: "price")
        productInfo.setValue("SAR", forKey: "currency")
        productInfo.setValue(loggedInUser.shared.string_userID, forKey: "user_id")
        productInfo.setValue(coupanCode, forKey: "coupon")
        analyticsAppsFlyer.shared.AnalyticsEventInitiateCheckout(paramas: productInfo)
    }
    
    func addPaymentInfoAppsFlyper() {
        let productInfo : NSMutableDictionary =  NSMutableDictionary()
        productInfo.setValue("\(amount_Final)", forKey: "price")
        productInfo.setValue("SAR", forKey: "currency")
        productInfo.setValue(loggedInUser.shared.string_userID, forKey: "user_id")
        analyticsAppsFlyer.shared.AnalyticsEventAddPaymentInfo(paramas: productInfo)
    }
}

//For Firebase Analytics
extension CheckOutPaymentOptionsVC {
    func beginCheckoutFirebaseAnylatics(){
        let productInfo : NSMutableDictionary =  NSMutableDictionary()
        productInfo.setValue("SAR", forKey: "currency")
        productInfo.setValue("\(amount_Final)", forKey: "value")
        productInfo.setValue(self.str_Coupon_Code, forKey: "coupon")
        analyticsFirebase.shared.AnalyticsEventBeginCheckout(paramas: productInfo as NSDictionary)
    }
    
    func addPaymentInfoFirebaseAnylatics() {
        analyticsFirebase.shared.AnalyticsEventAddPaymentInfo(paramas: [:])
    }
}

