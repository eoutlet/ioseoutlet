

import Alamofire
import SDWebImage

class HeaderSubCategorytListCollectionViewCell : UICollectionViewCell
{
    
    @IBOutlet weak var view_Background: UIView!
    @IBOutlet weak var label_Title: UILabel!
    
}

class ProductListCollectionViewCell : UICollectionViewCell
{
    @IBOutlet weak var base_View: UIView!
    @IBOutlet weak var imageView_Icon: UIImageView!
    @IBOutlet weak var label_Title: UILabel!
    @IBOutlet weak var label_OldPrice: UILabel!
    @IBOutlet weak var label_NewPrice: UILabel!
    @IBOutlet weak var label_NewFrontPrice: UILabel! // for showing in middle when old price is hide
    
}
class ProductListVC: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,UITableViewDataSource,UITableViewDelegate,applyfilterDelegate {
    
    @IBOutlet weak var label_title: UILabel!
    @IBOutlet weak var collectionViewHeaderSubCategorytList: UICollectionView!
    
    @IBOutlet weak var view_Sorting: UIView!
    @IBOutlet weak var sorting_IconImageView: UIImageView!
    @IBOutlet weak var sorting_titleUILabel: UILabel!
    @IBOutlet weak var constraints_viewWidthSorting: NSLayoutConstraint!
    @IBOutlet weak var constraints_viewSortingLeading: NSLayoutConstraint!
    
    @IBOutlet weak var view_Filter: UIView!
    @IBOutlet weak var filter_IconImageView: UIImageView!
    @IBOutlet weak var filter_titleUILabel: UILabel!
    @IBOutlet weak var constraints_viewWidthFilter: NSLayoutConstraint!
    @IBOutlet weak var constraints_viewFilterLeading: NSLayoutConstraint!
    
    
    @IBOutlet weak var collectionViewProductList: UICollectionView!
    var array_HeaderSubCategorytList : NSMutableArray = NSMutableArray()
    var array_ProductList : NSMutableArray = NSMutableArray()
    
    var dict_subCategory : NSDictionary = NSDictionary()
    var refreshControl = UIRefreshControl()
    var array_MainFiltersFromFiltersScreen : NSMutableArray = NSMutableArray()
    
    
    var array_Sorting : NSMutableArray = NSMutableArray()
    var array_appliedFilter : NSMutableArray = NSMutableArray()
    var buttonasfullScreenForSorting : UIButton = UIButton()
    var tableViewForSorting  : UITableView = UITableView()
    let TAGPPICKERFORSELECTION = 1
    var pageNumber : Int = 1
    var isProductListAPICalled : Bool = true
    var isItemListFirebaseAnylaticsCalled = false
    var sort_by = 99//99:this is for specific category, 0:default, 1:recently arrived, 2:low to high, 3:high to low
    var subCategorySelectedCellIndex = -1 // for unselect collectionview cell its default  // sort 9999, filter 8888
    var clickedRow : Int = 99 //This default value to set all category closed
    var lastContentOffset : CGFloat = 0
    var titleStr = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        label_title.text = titleStr
        if titleStr == "" {  // if not compes in category list
            if let strValue = (dict_subCategory["name"] as? String){
                label_title.text = strValue
            }
        }
        else { // when comes from category list
             collectionViewHeaderSubCategorytList.reloadData()
            self.collectionViewHeaderSubCategorytList.scrollToItem(at:IndexPath(item: subCategorySelectedCellIndex, section: 0), at: .centeredHorizontally, animated: true)
        }
        
        sorting_titleUILabel.text = "ترتيب حسب"
        filter_titleUILabel.text = "فلاتر البحث"
        
        let swipeLeft_Sorting = UISwipeGestureRecognizer(target: self, action: #selector(leftSwipeGesture_Sorting(_:)))
        swipeLeft_Sorting.direction = UISwipeGestureRecognizer.Direction.left
        view_Sorting.addGestureRecognizer(swipeLeft_Sorting)
        
        let swipeRight_Sorting = UISwipeGestureRecognizer(target: self, action: #selector(rightSwipeGesture_Sorting(_:)))
        swipeRight_Sorting.direction = UISwipeGestureRecognizer.Direction.right
        view_Sorting.addGestureRecognizer(swipeRight_Sorting)
        
        
        
        let swipeLeft_Filter = UISwipeGestureRecognizer(target: self, action: #selector(leftSwipeGesture_Filter(_:)))
        swipeLeft_Filter.direction = UISwipeGestureRecognizer.Direction.left
        view_Filter.addGestureRecognizer(swipeLeft_Filter)
        
        let swipeRight_Filter = UISwipeGestureRecognizer(target: self, action: #selector(rightSwipeGesture_Filter(_:)))
        swipeRight_Filter.direction = UISwipeGestureRecognizer.Direction.right
        view_Filter.addGestureRecognizer(swipeRight_Filter)
        
        let sortTap = UITapGestureRecognizer(target: self, action: #selector(self.sortViewTap(_:)))
        view_Sorting.addGestureRecognizer(sortTap)
        
        
        let filterTap = UITapGestureRecognizer(target: self, action: #selector(self.filterViewTap(_:)))
        view_Filter.addGestureRecognizer(filterTap)
        collectionViewHeaderSubCategorytList.transform = CGAffineTransform(scaleX: -1, y: 1)
  
        refreshControl.addTarget(self, action: #selector(pullToRefreshCollectionView), for: UIControl.Event.valueChanged)
        collectionViewProductList.addSubview(refreshControl) //Pull to refresh in UICollectionViewController
        
        
        //Get All productList API Called...
        self.getAllProductsAPICalled(categoryId: dict_subCategory["id"] as! NSString, withPageNumber: pageNumber)
        
        let dict0 : NSMutableDictionary = NSMutableDictionary()
        dict0.setValue("الافتراضي", forKey: "name")//Default
        dict0.setValue("0", forKey: "id")
        array_Sorting.add(dict0)
        
        let dict1 : NSMutableDictionary = NSMutableDictionary()
        dict1.setValue("وصل حديثا", forKey: "name") //Recently arrived
        dict1.setValue("1", forKey: "id")
        array_Sorting.add(dict1)
        
        let dict2 : NSMutableDictionary = NSMutableDictionary()
        dict2.setValue(" السعر من الأدني الي الاعلي", forKey: "name") //Low to high
        dict2.setValue("2", forKey: "id")
        array_Sorting.add(dict2)
        
        let dict3 : NSMutableDictionary = NSMutableDictionary()
        dict3.setValue("السعر من الأعلي الي الادني", forKey: "name") //High to low
        dict3.setValue("3", forKey: "id")
        array_Sorting.add(dict3)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.isHidden = true
        
        //set cart item count on home screen navigation & tab bar...
        if AppDelegate.cart_item_count != 0 {
            if let tabItems = self.tabBarController?.tabBar.items {
                let tabItem = tabItems[0]
                tabItem.badgeValue = "\(AppDelegate.cart_item_count)"
            }
        }
    }
    
    @objc func pullToRefreshCollectionView(sender:AnyObject) {
        
        //        self.array_MainFiltersFromFiltersScreen.removeAllObjects()
        //        self.setFilterCounter(array_MainFilters: self.array_MainFiltersFromFiltersScreen)//Need to show number of applied filters's counter...
        
        //sort_by = 0 //Need to reset sort_by, as default
        pageNumber = 1 //Need to reset page number 1, this is will fetch initial level data
        //   isProductListAPICalled  = true //Need to set because this will give initial level data
        
        //Get All productList API Called...
        if isProductListAPICalled{
            //Get All productList API Called...
            self.getAllProductsAPICalled(categoryId: dict_subCategory["id"] as! NSString, withPageNumber: pageNumber)
        }else{
            //Get All Filter API Called...
            self.applyFilterAPICalled()
        }
    }
    
    //MARK: - UIButton Clicks
    @IBAction func backButtonClicked(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func buttonSearchClicked(_ sender: UIButton) {
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "searchVC") as! SearchVC
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    //MARK: API Calling...
    func getAllProductsAPICalled(categoryId: NSString, withPageNumber: Int){
        
        let parameters : Parameters = [
            
            "cat_id" : categoryId,
            "page" : withPageNumber,
            "sort_by" : "\(sort_by)",
            "cacheignore" : "\(Int(NSDate().timeIntervalSince1970))" //To ignore cache from api...
            
        ]
        
        AppDelegate.showHUD(inView: self.view, message:"Loading....")
        Alamofire.request(Constants.apiURL+"webservice/productlistapi.php", method: .get, parameters: parameters)
            .responseJSON { response in
                AppDelegate.hideHUD(inView: self.view)
                switch response.result {
                case .success:
                    if let json = response.result.value{
                        guard let _ : NSDictionary = json as? NSDictionary else {
                            return
                        }
                        do {
                            // make sure this JSON is in the format we expect
                            let jsonData =  try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
                            let decoded = try JSONSerialization.jsonObject(with: jsonData, options: []) as! NSDictionary
                            // here "decoded" is of type `Any`, decoded from JSON data
                            guard let dataArray : NSArray = decoded["data"] as? NSArray else {
                                return
                            }
                            
                            self.pageNumber = decoded["page"] as! Int
                            if dataArray.count > 0 {
                                if(self.array_ProductList.count > 0 && self.pageNumber == 1){//When page number == 1, then need to remove all old data
                                    self.array_ProductList.removeAllObjects()
                                }
                                
                                self.array_ProductList.addObjects(from: (dataArray ) as! [Any])
                                if(!self.isItemListFirebaseAnylaticsCalled){ //viewItemListFirebaseAnylatics not called then, have to called only once...
                                    let first_Product = self.array_ProductList[0] as! Parameters
                                    self.viewItemListFirebaseAnylatics(paramas:first_Product)
                                }
                                
                                self.collectionViewProductList.reloadData()
                                self.refreshControl.endRefreshing()
                            }
                        }
                        catch let error as NSError {
                            print("Failed to load: \(error.localizedDescription)")
                        }
                    }
                case .failure( _):
                    Utility.showAlert(message: "حدث خطأ - يرجي اعادة المحاولة", controller: self) //Something went wrong, please try again.
                }
        }
    }
    
    func applyFilterAPICalled(){
        
        let parameters : Parameters = [
            
            "cat_id" : dict_subCategory["id"] as! NSString,
            "data" : array_appliedFilter,
            "sort_by" : "\(sort_by)",
            "page" : pageNumber,
            "cacheignore" : "\(Int(NSDate().timeIntervalSince1970))" //To ignore cache from api...
            
        ]
        
        let headers : HTTPHeaders = [
            "Content-Type" : "application/json"
        ]
        
        AppDelegate.showHUD(inView: self.view, message:"Loading....")
        Alamofire.request(Constants.apiURL + "webservice/filter.php", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers)
            .responseJSON { response in
                AppDelegate.hideHUD(inView: self.view)
                switch response.result {
                case .success:
                    if let json = response.result.value{
                        //print("JSON Response : \(json)")
                        guard let _ : NSDictionary = json as? NSDictionary else {
                            return
                        }
                        do {
                            // make sure this JSON is in the format we expect
                            let jsonData =  try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
                            let decoded = try JSONSerialization.jsonObject(with: jsonData, options: []) as! NSDictionary
                            // here "decoded" is of type `Any`, decoded from JSON data
                            guard let dataArray : NSArray = decoded["data"] as? NSArray else {
                                return
                            }
                            
                            self.pageNumber = decoded["page"] as? Int ?? 0
                            if dataArray.count > 0 {
                                if(self.array_ProductList.count>0 && self.pageNumber == 1){
                                    self.array_ProductList.removeAllObjects()
                                }
                                self.array_ProductList.addObjects(from: (dataArray ) as! [Any])
                                self.collectionViewProductList.reloadData()
                                self.refreshControl.endRefreshing()
                            }
                        }
                        catch let error as NSError {
                            print("Failed to load: \(error.localizedDescription)")
                        }
                    }
                case .failure( _):
                    //Utility.showAlert(message: error.localizedDescription, controller: self)
                    Utility.showAlert(message: "حدث خطأ - يرجي اعادة المحاولة", controller: self) //Something went wrong, please try again.
                }
        }
    }
    
    //MARK: UICollection View...
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView.tag == 0{
            return array_HeaderSubCategorytList.count
        }
        else{
            return array_ProductList.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        if collectionView.tag == 0{
            return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        }
        else{
            return UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView.tag == 0{
            
            var width: CGFloat = 0
            let dict : NSDictionary = self.array_HeaderSubCategorytList[indexPath.row] as! NSDictionary
            width = self.labelWidth(categoryname: dict["name"] as? NSString ?? "")
            width += 15
            
            return CGSize(width: width, height: 30)
        }else{
            var width: CGFloat = 0
            if UIDevice.current.userInterfaceIdiom == .phone{
                width = (collectionViewProductList.bounds.size.width - 30) / 2
            }
            else {
                width = (collectionViewProductList.bounds.size.width - 40) / 3
            }
            return CGSize(width: width, height: width + 120)
        }
    }
    
    func labelWidth(categoryname: NSString) -> CGFloat {
        
        //Calculate the expected size based on the font and linebreak mode of your label
        var labelSize: CGSize = CGSize()
        labelSize = categoryname.size(withAttributes: [NSAttributedString.Key.font: UIFont(name: Constants.helvetica_bold_font, size: 10) ?? 0])
        
        return labelSize.width
        
    }
    
    internal func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView.tag == 0 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "headerSubCategorytListCollectionViewCell", for: indexPath) as! HeaderSubCategorytListCollectionViewCell
            cell.view_Background.backgroundColor = UIColor.white
            cell.label_Title.textColor = UIColor.black
            let dict : NSDictionary = array_HeaderSubCategorytList[indexPath.row] as! NSDictionary
            cell.label_Title.text =  dict["name"] as? String
            cell.label_Title.textAlignment = NSTextAlignment.center
            if subCategorySelectedCellIndex == indexPath.item {
                cell.view_Background.backgroundColor = UIColor.black
                cell.label_Title.textColor = UIColor.white
            }
            else {
                cell.view_Background.backgroundColor = UIColor.white
                cell.label_Title.textColor = UIColor.black
            }
            cell.contentView.transform = CGAffineTransform(scaleX: -1, y: 1)
            return cell
            
        }else{
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "productListCollectionViewCell", for: indexPath) as! ProductListCollectionViewCell
            let dict : NSDictionary = array_ProductList[indexPath.row] as! NSDictionary
            cell.label_Title.text = dict["name"] as? String
            cell.label_OldPrice.isHidden = false
            cell.label_NewFrontPrice.isHidden = true
            //cell.base_View.setShadow(shadowSize: 10.0, radius: 10.0)
            
            if let intValue = (dict["old_price"] as? Int) {
                
                if(intValue == 0){
                    cell.label_OldPrice.isHidden = true
                    cell.label_NewFrontPrice.isHidden = false
                }
                cell.label_OldPrice.text = String(format: "SAR %d", intValue)
                
            }else if let strValue = (dict["old_price"] as? String){
                
                if((strValue.replacingOccurrences(of: ",", with: "")) == "0"){
                    cell.label_OldPrice.isHidden = true
                    cell.label_NewFrontPrice.isHidden = false
                }
                cell.label_OldPrice.text = String(format: "SAR %d", (strValue.replacingOccurrences(of: ",", with: "") as NSString).integerValue)
                
            }else{
                print("some other data types exists.")
            }
            
            
            if let intValue = (dict["price"] as? Int) {
                cell.label_NewPrice.text = String(format: "SAR %d", intValue)
                cell.label_NewFrontPrice.text = String(format: "SAR %d", intValue)
            }else if let strValue = (dict["price"] as? String){
                cell.label_NewPrice.text = String(format: "SAR %d", (strValue.replacingOccurrences(of: ",", with: "") as NSString).integerValue)
                cell.label_NewFrontPrice.text = String(format: "SAR %d", (strValue.replacingOccurrences(of: ",", with: "") as NSString).integerValue)
            }else{
                print("some other data types exists.")
            }
            
            cell.imageView_Icon.sd_imageIndicator = SDWebImageActivityIndicator.gray
            cell.imageView_Icon.sd_imageIndicator?.startAnimatingIndicator()
            let url = NSURL(string: dict["img"] as? String ?? "")
            cell.imageView_Icon.sd_setImage(with: url as URL?) { (image, error, cache, urls) in
                if (error != nil) {// Failed to load image
                } else {// Successful in loading image
                    cell.imageView_Icon.sd_imageIndicator?.stopAnimatingIndicator()
                }}
            
            //set shadow on product list cell collectionview
            cell.layoutIfNeeded()
            cell.base_View.backgroundColor = UIColor.white //your background color...
            cell.base_View.cornerRadius = 5
            cell.base_View.setShadow(x: -5, y: -5, width: cell.base_View.bounds.width + 10, height: cell.base_View.bounds.height + 10, radius: 5)
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView.tag == 0{
            
            DispatchQueue.main.async {
                
                self.array_MainFiltersFromFiltersScreen.removeAllObjects()
                self.setFilterCounter(array_MainFilters: self.array_MainFiltersFromFiltersScreen)//Need to show number of applied filters's counter...
                self.array_ProductList.removeAllObjects()
                // self.array_ProductListToHoldValues.removeAllObjects()
                self.collectionViewProductList.reloadData()
                
            }
            
            sort_by = 0//Need to reset sort_by as default
            pageNumber = 1 //Need to reset page number 1, this is will fetch initial level data
            //Get All productList API Called...
            isProductListAPICalled  = true //Need to set because this will give initial level data
            
            let dict : NSDictionary = array_HeaderSubCategorytList[indexPath.item] as! NSDictionary
            dict_subCategory = dict
            self.title = dict_subCategory["name"] as! NSString as String
            self.getAllProductsAPICalled(categoryId: dict_subCategory["id"] as! NSString, withPageNumber: pageNumber)
            
            
            self.resetSortViewUIChanges()
            self.resetFilterViewUIChanges()
            subCategorySelectedCellIndex = indexPath.item
            
            self.collectionViewHeaderSubCategorytList.reloadData()
        }
        else{
            
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "productDetailVC") as? ProductDetailVC
            vc?.dict_Product = array_ProductList[indexPath.row] as! NSDictionary
            self.navigationController?.pushViewController(vc!, animated: false)
        }
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if scrollView.tag == 0 { // only for sub category
            if !decelerate {
                
                if (self.lastContentOffset > scrollView.contentOffset.x) {
                    
                    // handle dragging to the left
                    print("left")
                    UIView.animate(withDuration: 0.01, animations: {
                        self.constraints_viewWidthSorting.constant = 90
                        self.constraints_viewWidthFilter.constant = 30 + self.labelWidth(categoryname: self.filter_titleUILabel.text! as NSString)
                        self.view.layoutIfNeeded()
                        self.constraints_viewSortingLeading.constant = 10
                        self.constraints_viewFilterLeading.constant = 10
                    })
                    
                } else if (self.lastContentOffset < scrollView.contentOffset.x) {
                    print("right")
                    UIView.animate(withDuration: 0.01, animations: {
                        self.constraints_viewWidthSorting.constant = 30
                        self.constraints_viewSortingLeading.constant = 0
                        
                        let text = self.filter_titleUILabel.text! as NSString
                        let replaced = text.replacingOccurrences(of: "فلاتر البحث", with: "")
                        
                        if self.filter_titleUILabel.text! == "فلاتر البحث" {
                            self.constraints_viewFilterLeading.constant = 0
                            self.constraints_viewWidthFilter.constant = 30 + self.labelWidth(categoryname: replaced as NSString)
                        }
                        else {
                            self.constraints_viewFilterLeading.constant = 10
                            self.constraints_viewWidthFilter.constant = 30 + 8 + self.labelWidth(categoryname: replaced as NSString)
                        }
                        self.view.layoutIfNeeded()
                    })
                }
                
                self.lastContentOffset = scrollView.contentOffset.x
            }
        }
    }
    
    func  scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        if (scrollView.tag == 1){//Only for product list
            
            if collectionViewProductList.contentOffset.y >= (collectionViewProductList.contentSize.height - collectionViewProductList.frame.size.height) {//you reached end of the UICollectionview
                
                pageNumber += 1
                if isProductListAPICalled{
                    
                    //Get All productList API Called...
                    self.getAllProductsAPICalled(categoryId: dict_subCategory["id"] as! NSString, withPageNumber: pageNumber)
                    
                }else{
                    
                    //Get All Filter API Called...
                    self.applyFilterAPICalled()
                }
                
            }
            
        }
        
    }
    
    //MARK: - Fiter & Sort View Methods
    @objc func sortViewTap(_ sender: UITapGestureRecognizer? = nil) {
        self.resetFilterViewUIChanges()
        if sorting_IconImageView.image?.pngData() == UIImage(named: "swap")?.pngData() {
            subCategorySelectedCellIndex = 9999
            sorting_IconImageView.image = UIImage(named: "swap-white")
            view_Sorting.backgroundColor = UIColor.black
            self.sorting_titleUILabel.textColor = UIColor.white
        }

        self.showSortingView()
        self.collectionViewHeaderSubCategorytList.reloadData()
    }
    
    @objc func filterViewTap(_ sender: UITapGestureRecognizer? = nil) {
        self.resetSortViewUIChanges()
        if filter_IconImageView.image?.pngData() == UIImage(named: "filter")?.pngData() {
            subCategorySelectedCellIndex = 8888
            filter_IconImageView.image = UIImage(named: "filter-white")
            view_Filter.backgroundColor = UIColor.black
            self.filter_titleUILabel.textColor = UIColor.white
        }
        self.collectionViewHeaderSubCategorytList.reloadData()

        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "filterVC") as! FilterVC
        vc.str_catID = dict_subCategory["id"] as! NSString
        vc.delegate_Filter = self
        if (self.array_MainFiltersFromFiltersScreen.count != 0){
            vc.array_tempFilterHolder = self.array_MainFiltersFromFiltersScreen
        }
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    
    func resetSortViewUIChanges() {
        subCategorySelectedCellIndex = -1
        sorting_IconImageView.image = UIImage(named: "swap")
        view_Sorting.backgroundColor = UIColor.white
        self.sorting_titleUILabel.textColor = UIColor.black
    }
    
    func resetFilterViewUIChanges() {
        subCategorySelectedCellIndex = -1
        filter_IconImageView.image = UIImage(named: "filter")
        view_Filter.backgroundColor = UIColor.white
        self.filter_titleUILabel.textColor = UIColor.black
    }
    
    @objc func leftSwipeGesture_Sorting(_ sender: UISwipeGestureRecognizer){
        UIView.animate(withDuration: 0.01, animations: {
            self.constraints_viewWidthSorting.constant = 90
            self.constraints_viewWidthFilter.constant = 30 + self.labelWidth(categoryname: self.filter_titleUILabel.text! as NSString)
            self.constraints_viewSortingLeading.constant = 10
            self.constraints_viewFilterLeading.constant = 10
            self.view.layoutIfNeeded()
        })
    }
    
    @objc func rightSwipeGesture_Sorting(_ sender: UISwipeGestureRecognizer){
        UIView.animate(withDuration: 0.01, animations: {
            self.constraints_viewWidthSorting.constant = 30
            self.constraints_viewSortingLeading.constant = 0
            
            let text = self.filter_titleUILabel.text! as NSString
            let replaced = text.replacingOccurrences(of: "فلاتر البحث", with: "")
            
            if self.filter_titleUILabel.text! == "فلاتر البحث" {
                self.constraints_viewFilterLeading.constant = 0
                self.constraints_viewWidthFilter.constant = 30 + self.labelWidth(categoryname: replaced as NSString)
            }
            else {
                self.constraints_viewFilterLeading.constant = 10
                self.constraints_viewWidthFilter.constant = 30 + 8 + self.labelWidth(categoryname: replaced as NSString)
            }
            self.view.layoutIfNeeded()
        })
    }
    
    @objc func leftSwipeGesture_Filter(_ sender: UISwipeGestureRecognizer){
        UIView.animate(withDuration: 0.01, animations: {
            self.constraints_viewWidthSorting.constant = 90
            self.constraints_viewWidthFilter.constant = 30 + self.labelWidth(categoryname: self.filter_titleUILabel.text! as NSString)
            self.constraints_viewSortingLeading.constant = 10
            self.constraints_viewFilterLeading.constant = 10
            self.view.layoutIfNeeded()
        })
    }
    
    @objc func rightSwipeGesture_Filter(_ sender: UISwipeGestureRecognizer){
        UIView.animate(withDuration: 0.01, animations: {
            self.constraints_viewWidthSorting.constant = 30
            self.constraints_viewSortingLeading.constant = 0
            
            let text = self.filter_titleUILabel.text! as NSString
            let replaced = text.replacingOccurrences(of: "فلاتر البحث", with: "")
            
            if self.filter_titleUILabel.text! == "فلاتر البحث" {
                self.constraints_viewFilterLeading.constant = 0
                self.constraints_viewWidthFilter.constant = 30 + self.labelWidth(categoryname: replaced as NSString)
            }
            else {
                self.constraints_viewFilterLeading.constant = 10
                self.constraints_viewWidthFilter.constant = 30 + 8 + self.labelWidth(categoryname: replaced as NSString)
            }
            self.view.layoutIfNeeded()
        })
    }
    
    
    //MARK: - UITableView DataSource
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return array_Sorting.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellIdentifier = "SelectionTreatMentCell"
        
        var cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier)
        
        if cell == nil {
            
            cell = tableviewCell(withReuseIdentifierSelectionProcedure: cellIdentifier, tablview: tableView, with: indexPath)
        }
        
        self.configureCellSelectionProcedure(cell, for: indexPath, withSection: indexPath.section, tableView: tableView)
        
        cell!.selectionStyle = .none
        
        return (cell ?? nil) ?? UITableViewCell()
        
    }
    
    //MARK: - UITableView DataSource
    // MARK: ************---------For Choose Procedure Only----------************
    func tableviewCell(withReuseIdentifierSelectionProcedure identifier: String?, tablview: UITableView?, with indxPath: IndexPath?) -> UITableViewCell? {
        
        
        let cell = UITableViewCell(style: .default, reuseIdentifier: identifier)
        
        //UILabel Selection TreatMent...
        let labelSelectionTreatMent = UILabel()
        labelSelectionTreatMent.frame = CGRect(x: 50, y: 0, width: (tablview?.bounds.size.width ?? 0.0) - 100, height: 40)
        labelSelectionTreatMent.tag = TAGPPICKERFORSELECTION
        labelSelectionTreatMent.numberOfLines = 0
        labelSelectionTreatMent.textColor = UIColor.init(red: 119/255, green: 131/255, blue: 143/255, alpha: 1.0)
        labelSelectionTreatMent.textAlignment = .right
        labelSelectionTreatMent.isUserInteractionEnabled = true
        labelSelectionTreatMent.font = UIFont.init(name: Constants.helvetica_regular_font, size: 12)
        cell.contentView.addSubview(labelSelectionTreatMent)
        
        
        
        if clickedRow == indxPath?.row {
            //UIImageView Selection TreatMent...
            let tickImageView = UIImageView()
            tickImageView.frame = CGRect(x: 10, y: (40-12)/2, width: 12, height: 12)
            tickImageView.tag = TAGPPICKERFORSELECTION
            tickImageView.image = #imageLiteral(resourceName: "category_tick")
            tickImageView.isUserInteractionEnabled = false
            cell.contentView.addSubview(tickImageView)
        }
        else{//UIImageView Selection TreatMent...
            let tickImageView = UIImageView()
            tickImageView.frame = CGRect(x: 10, y: (40-12)/2, width: 12, height: 12)
            tickImageView.tag = TAGPPICKERFORSELECTION
            tickImageView.isUserInteractionEnabled = false
            cell.contentView.addSubview(tickImageView)
        }
        
        
        return cell
        
        
    }
    
    func configureCellSelectionProcedure(_ cell: UITableViewCell?, for indexPath: IndexPath?, withSection section: Int, tableView: UITableView?) {
        
        let labelSelectionEvent = cell?.contentView.viewWithTag(TAGPPICKERFORSELECTION) as? UILabel
        let dict : NSDictionary = array_Sorting[indexPath!.row] as! NSDictionary
        labelSelectionEvent?.text = dict["name"] as? String
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        clickedRow = indexPath.row
        pageNumber = 1
        sort_by = indexPath.row
        
        tableViewForSorting.reloadData {
            self.removePickerFromScreenForSorting()
            self.perform(#selector(self.scrollCollectionViewOnTop), with: nil, afterDelay: 0.3)
            
            //Get All productList API Called...
            if self.isProductListAPICalled{
                
                //Get All productList API Called...
                self.getAllProductsAPICalled(categoryId: self.dict_subCategory["id"] as! NSString, withPageNumber: self.pageNumber)
                
            }else{
                
                //Get All Filter API Called...
                self.applyFilterAPICalled()
            }
        }
        
        
    }
    
    
    @objc func scrollCollectionViewOnTop(){
        
        if(array_ProductList.count > 0){
            
            let indxPath = IndexPath.init(row: 0, section: 0)
            collectionViewProductList.scrollToItem(at: indxPath, at: .top, animated: false)
        }
        
        
    }
    
    func showSortingView(){
        
        buttonasfullScreenForSorting = UIButton()
        buttonasfullScreenForSorting.backgroundColor = UIColor.clear
        buttonasfullScreenForSorting.frame = CGRect(x: 0, y: 0, width: UIApplication.shared.keyWindow?.bounds.size.width ?? 0.0, height: UIApplication.shared.keyWindow?.bounds.size.height ?? 0.0)
        buttonasfullScreenForSorting.autoresizingMask = [.flexibleTopMargin, .flexibleBottomMargin, .flexibleLeftMargin, .flexibleRightMargin, .flexibleWidth, .flexibleHeight]
        buttonasfullScreenForSorting.addTarget(self, action: #selector(removePickerFromScreenForSorting), for: .touchUpInside)
        UIApplication.shared.keyWindow?.addSubview(buttonasfullScreenForSorting)
        
        
        let viewForShadow = UIView()
        viewForShadow.backgroundColor = UIColor.white
        if UIDevice.current.userInterfaceIdiom == .phone{
            
            viewForShadow.frame = CGRect(x: 20, y: buttonasfullScreenForSorting.bounds.size.height, width: self.buttonasfullScreenForSorting.bounds.size.width - 40, height: 200)
            
        } else {
            
            viewForShadow.frame = CGRect(x: buttonasfullScreenForSorting.bounds.size.width / 4, y: buttonasfullScreenForSorting.bounds.size.height, width: buttonasfullScreenForSorting.bounds.size.width / 2, height: 200)
            
        }
        viewForShadow.autoresizingMask = [.flexibleTopMargin, .flexibleBottomMargin, .flexibleLeftMargin, .flexibleRightMargin]
        buttonasfullScreenForSorting.addSubview(viewForShadow)
        
        
        
        tableViewForSorting = UITableView()
        tableViewForSorting.tag = 100
        tableViewForSorting.cellLayoutMarginsFollowReadableWidth = false
        tableViewForSorting.frame = CGRect(x: 0, y: 0, width: viewForShadow.bounds.size.width, height: 160)
        tableViewForSorting.delegate = self
        tableViewForSorting.dataSource = self
        tableViewForSorting.separatorInset = UIEdgeInsets.zero
        tableViewForSorting.separatorStyle = .singleLine
        tableViewForSorting.tableFooterView = UIView(frame: CGRect.zero)
        viewForShadow.addSubview(tableViewForSorting)
        tableViewForSorting.reloadData()
        
        //        UIView.animate(withDuration: 0.3,  delay: 0.0, options: .curveEaseInOut, animations: {
        
        //            viewForShadow.layoutIfNeeded()
        if UIDevice.current.userInterfaceIdiom == .phone{
            
            viewForShadow.frame = CGRect(x: 20, y: self.collectionViewProductList.frame.origin.y + 10, width: self.buttonasfullScreenForSorting.bounds.size.width - 40, height: 160)
            
        } else {
            
            viewForShadow.frame = CGRect(x: self.buttonasfullScreenForSorting.bounds.size.width / 4, y: self.collectionViewProductList.frame.origin.y + 10, width: self.buttonasfullScreenForSorting.bounds.size.width / 2, height: 160)
            
        }
        //
        //        }, completion: { (finishedSecondAnimation) in
        //
        //
        //        })
        //
        viewForShadow.setShadow(shadowSize: 10.0, radius: 5.0)
        
        
    }
    
    @objc func removePickerFromScreenForSorting() {
        
        buttonasfullScreenForSorting.removeFromSuperview()
        
    }
    
    //MARK: - Filter Applied Successfully
    func fileteredAppliedSuccessfully(array_filteredToBeApplied : NSArray, array_MainFiltersFromFilterScreen: NSArray){
        
        self.array_MainFiltersFromFiltersScreen.removeAllObjects()
        self.array_MainFiltersFromFiltersScreen.addObjects(from: array_MainFiltersFromFilterScreen as! [Any])
        
        DispatchQueue.main.async {
            
            //self.setAppliedFiltersCounter()//Need to show number of applied filters's counter...
            self.setFilterCounter(array_MainFilters: self.array_MainFiltersFromFiltersScreen)
            self.array_ProductList.removeAllObjects()
            //self.array_ProductListToHoldValues.removeAllObjects()
            self.collectionViewProductList.reloadData()
        };
        
        isProductListAPICalled  = false
        pageNumber = 1 //Need to reset page number 1, this is will fetch initial level data
        self.array_appliedFilter = array_filteredToBeApplied.mutableCopy() as! NSMutableArray
        self.applyFilterAPICalled()
        
        
    }
    
    func setFilterCounter(array_MainFilters : NSArray){
        
        var total_AppliedFiltersCount : Int = 0
        
        for indx in 0 ..< array_MainFilters.count{
            
            let dict_Address = array_MainFilters[indx] as? NSDictionary
            if let dataArray = dict_Address?["data"] as? NSArray {
                
                let resultPredicate = NSPredicate(format: "isSelected contains[c] %@", "yes" )
                let array_filtered = dataArray.filtered(using: resultPredicate)
                
                total_AppliedFiltersCount += array_filtered.count
                
            }
            
        }
        
        if total_AppliedFiltersCount == 0{
            self.filter_titleUILabel.text = "فلاتر البحث"
            UIView.animate(withDuration: 0.01, animations: {
                self.constraints_viewWidthFilter.constant = 30 + self.labelWidth(categoryname: self.filter_titleUILabel.text! as NSString)
                self.view.layoutIfNeeded()
            })
        }
        else{
            self.filter_titleUILabel.text = "فلاتر البحث (\(total_AppliedFiltersCount))"
            UIView.animate(withDuration: 0.01, animations: {
                self.constraints_viewWidthFilter.constant = 30 + self.labelWidth(categoryname: self.filter_titleUILabel.text! as NSString)
                self.view.layoutIfNeeded()
            })
            
        }
        
        // collectionViewHeaderSubCategorytList.reloadData()
    }
    
}

//for firebase
extension ProductListVC {
    
    func viewItemListFirebaseAnylatics(paramas: Parameters){
        
        isItemListFirebaseAnylaticsCalled = true
        let productInfo : NSMutableDictionary =  NSMutableDictionary()
        productInfo.setValue(paramas["category_name"] as? String ?? "", forKey: "category_name")
        analyticsFirebase.shared.AnalyticsEventViewItemList(paramas: productInfo as NSDictionary)
        
    }
    
}

