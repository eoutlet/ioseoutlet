
import Alamofire

class AllBrandsTableViewCell: UITableViewCell{
    @IBOutlet weak var label_Title: UILabel!
}

class AToZVC: UIViewController, UITableViewDataSource,UITableViewDelegate  {
    @IBOutlet weak var allBrandTableView: UITableView!
    var array_AllBrands : NSMutableArray = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.getAllBransAPICalled()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.isHidden = true
        
        //set cart item count on home screen navigation & tab bar...
        if AppDelegate.cart_item_count != 0 {
            if let tabItems = self.tabBarController?.tabBar.items {
                let tabItem = tabItems[0]
                tabItem.badgeValue = "\(AppDelegate.cart_item_count)"
            }
        }
    }
    
    //MARK: - UIButton Clicks
    @IBAction func buttonSearchClicked(_ sender: UIButton) {
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "searchVC") as! SearchVC
        self.navigationController?.pushViewController(vc, animated: false)
    }   
    
    //MARK: API Calling...
    func getAllBransAPICalled(){
        
        let parameters : Parameters = [
            "cacheignore" : "\(Int(NSDate().timeIntervalSince1970))" //To ignore cache from api...
        ]
        AppDelegate.showHUD(inView: self.view, message:"Loading....")
        Alamofire.request(Constants.apiURL+"webservice/brandcatapi.php", method: .get, parameters: parameters)
            .responseJSON { response in
                AppDelegate.hideHUD(inView: self.view)
                switch response.result {
                case .success:
                    if let json = response.result.value{
                        guard let _ : NSDictionary = json as? NSDictionary else {
                            return
                        }
                        do {
                            let jsonData =  try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
                            let decoded = try JSONSerialization.jsonObject(with: jsonData, options: []) as! NSDictionary
                            // here "decoded" is of type `Any`, decoded from JSON data
                            // you can now cast it with the right type
                            let arrayofGetData = decoded["arr"] as! NSArray
                            if arrayofGetData.count > 0{
                                self.array_AllBrands.addObjects(from: arrayofGetData as! [Any])
                                self.allBrandTableView.reloadData()
                            }
                        }
                        catch let error as NSError {
                            print("Failed to load: \(error.localizedDescription)")
                        }
                    }
                case .failure( _):
                    //Utility.showAlert(message: error.localizedDescription, controller: self)
                    Utility.showAlert(message: "حدث خطأ - يرجي اعادة المحاولة", controller: self) //Something went wrong, please try again.
                }
        }
    }
    
    
    //MARK: - UITableView DataSource
    func numberOfSections(in tableView: UITableView) -> Int {
        return array_AllBrands.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let dict_Record = array_AllBrands[section] as! NSDictionary
        let label = UILabel(frame: CGRect(x: 10, y: 0, width: tableView.frame.size.width, height: 20))
        label.font = UIFont(name:Constants.montserrat_bold_font, size: 14.0)
        label.textAlignment = .left
        label.textColor = Constants.default_ThemeColor
        label.text = dict_Record["alphabetic"] as? String
        
        return label
        
        
        
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        
        return 40
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let dict_Record = array_AllBrands[section] as! NSDictionary
        let array_ofSection = dict_Record["data"] as! NSArray
        return array_ofSection.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "allBrandsTableViewCell", for: indexPath) as! AllBrandsTableViewCell
        let dict_Record = array_AllBrands[indexPath.section] as! NSDictionary
        let array_ofSection = dict_Record["data"] as! NSArray
        let array_Dict = array_ofSection[indexPath.row] as? NSDictionary
        cell.label_Title.text = array_Dict?["name"] as? String
        return cell
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "productListVC") as! ProductListVC
        let dict_Record = array_AllBrands[indexPath.section] as? NSDictionary
        let array_Data = dict_Record?["data"] as? NSArray
        let dict_Cat = array_Data?[indexPath.row] as? NSDictionary
        vc.dict_subCategory = dict_Cat ?? NSDictionary()
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
}

