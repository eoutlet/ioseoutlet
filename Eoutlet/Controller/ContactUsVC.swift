//
//  ContactUsVC.swift
//  Eoutlet
//
//  Created by Unyscape Infocom on 18/10/19.
//  Copyright © 2019 Unyscape. All rights reserved.
//
import Alamofire

class ContactUsVC: UIViewController, UIGestureRecognizerDelegate {
    @IBOutlet weak var whatsappView: UIView!
    @IBOutlet weak var callView: UIView!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var commentTextView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // ADD Corner radius
        whatsappView.layer.cornerRadius = 10.0
        whatsappView.layer.borderWidth = 2.0
        whatsappView.layer.borderColor = UIColor.lightGray.cgColor
        
        callView.layer.cornerRadius = 10.0
        callView.layer.borderWidth = 2.0
        callView.layer.borderColor = UIColor.lightGray.cgColor
        
        commentTextView.layer.cornerRadius = 5.0
        commentTextView.layer.borderWidth = 1.0
        commentTextView.layer.borderColor = UIColor.lightGray.cgColor
        
        // ADD UITapGesture
        let whatsappTap = UITapGestureRecognizer(target: self, action: #selector(self.whatsppClicked(_:)))
        whatsappTap.numberOfTouchesRequired = 1
        whatsappTap.numberOfTapsRequired = 1
        whatsappView.addGestureRecognizer(whatsappTap)
        
        let phoneTap = UITapGestureRecognizer(target: self, action: #selector(self.phoneClicked(_:)))
        phoneTap.numberOfTouchesRequired = 1
        phoneTap.numberOfTapsRequired = 1
        callView.addGestureRecognizer(phoneTap)
    }
    
     override func viewWillAppear(_ _animated: Bool) {
                super.viewWillAppear(_animated)
                self.navigationController?.navigationBar.isHidden = true
            }
          
            //MARK: - UIButton Clicks
            @IBAction func backButtonClicked(_ sender: UIButton) {
                self.navigationController?.popViewController(animated: false)
            }
    
    @objc func whatsppClicked(_ sender: UITapGestureRecognizer? = nil) {
       // let urlWhats = "whatsapp://send?phone=+966532631188"
       // let urlWhats = "whatsapp://send?phone=+966532631188&text=مرحبا فريق اي اوتلت"
        let urlWhats = "https://wa.me/+966532631188?text=مرحبا فريق اي اوتلت"
        if let urlString = urlWhats.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed){
            if let whatsappURL = URL(string: urlString) {
                if UIApplication.shared.canOpenURL(whatsappURL){
                    if #available(iOS 10.0, *) {
                        UIApplication.shared.open(whatsappURL, options: [:], completionHandler: nil)
                    } else {
                        UIApplication.shared.openURL(whatsappURL)
                    }
                }
                else {
                    Utility.showAlert(message: "آسف ، لم يتم تثبيت واتس اب.", controller: self) //"Sorry, whatsapp is not installed."
                }
            }
        }
    }
    
    @objc func phoneClicked(_ sender: UITapGestureRecognizer? = nil) {
        if let phoneCallURL = URL(string: "telprompt://+966920017422") {
            
            let application:UIApplication = UIApplication.shared
            if (application.canOpenURL(phoneCallURL)) {
                application.open(phoneCallURL, options: [:], completionHandler: nil)
            }else {
                Utility.showAlert(message: "آسف ، مكالمة هاتفية غير ممكنة.", controller: self) //Sorry, phone call is not possible.
            }
        }
    }
    
    //MARK: API Calling...
    func contactUSAPICalled(){
        guard let str_name = nameTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) else{
            return
        }
        guard let str_email = emailTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) else{
            return
        }
        guard let str_mobileNumber = phoneTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) else{
            return
        }
        
        guard let str_Message = commentTextView.text?.trimmingCharacters(in: .whitespacesAndNewlines) else{
            return
        }
        
        let parameters : Parameters = [
            
            "name" : str_name,
            "email" :str_email,
            "telephone" : str_mobileNumber,
            "comment" : str_Message,
            "cacheignore" : "\(Int(NSDate().timeIntervalSince1970))" //To ignore cache from api...

        ]
        
        
        AppDelegate.showHUD(inView: self.view, message: "Loading...")
        
        //Alamofire.request(Constants.apiURL+"user/register", method: .post, parameters: parameters, encoding: JSONEncoding.default)
        //Alamofire.request(Constants.apiURL+"rest/V1/customers", method: .post, parameters: parameters, headers: headers)
        // Alamofire.request(Constants.apiURL+"rest/V1/customers", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers)
        Alamofire.request(Constants.apiURL + "webservice/contact.php", method: .get, parameters: parameters)
            
            .responseJSON { response in
                
                AppDelegate.hideHUD(inView: self.view)
                
                switch response.result {
                case .success:
                    
                    if let json = response.result.value{
                        
                        print("JSON Response : \(json)")
                        
                        guard let _ : NSDictionary = json as? NSDictionary else {
                            return
                        }
                        
                        do {
                            
                            // make sure this JSON is in the format we expect
                            let jsonData =  try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
                            let decoded = try JSONSerialization.jsonObject(with: jsonData, options: []) as! NSDictionary
                            
                            print("API Data Response: \(decoded)")
                            
                            if decoded["msg"] as? String == "success"{
                                
                                iToast.show("شكرا لإتصالك بنا")
                                self.nameTextField.text = ""
                                self.emailTextField.text = ""
                                self.phoneTextField.text = ""
                                self.commentTextView.text = ""

                            }else{
                                
                                iToast.show("حدث خطأ - يرجي اعادة المحاولة")//An error has occurred - please try again

                            }
                            
                            
                        } catch let error as NSError {
                            
                            print("Failed to load: \(error.localizedDescription)")
                        }
                        
                    }
                case .failure( _):
                        //Utility.showAlert(message: error.localizedDescription, controller: self)
                        Utility.showAlert(message: "حدث خطأ - يرجي اعادة المحاولة", controller: self) //Something went wrong, please try again.
                    }
        }
    }
    
    
    @IBAction func sendBtnTap(_ sender: UIButton) {
        
      
        if nameTextField.text == ""{
            
            Utility.showAlert(message:"اكتب اسمك الاول", controller: self)
            return
            
        }
        
        if emailTextField.text == "" || self.isValidEmail(emailStr:emailTextField.text!) == false{
            
            Utility.showAlert(message:"الرجاء إدخال بريد إلكتروني صحيح", controller: self) //Please enter valid email.
            return
            
        }
        
        if phoneTextField.text == ""{
            
            Utility.showAlert(message:"من فضلك أدخل رقم الجوال", controller: self)
            return
            
        }

        self.contactUSAPICalled()
      
    }
}

