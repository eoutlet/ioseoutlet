import Alamofire

class ForgotPasswordVC: UIViewController,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate, sendOTPCloseTransparent {
    func closeView(idStr: String) {
        self.sendOTPAPICalled()
    }
    var array_CountryList : NSMutableArray = NSMutableArray()
    var buttonasfullScreenForCountrySelection : UIButton = UIButton()
    var tableViewForCountrySelection  : UITableView = UITableView()
    let TAGCOUNTRYNAME = 4
    let TAGCOUNTRYRADIOBUTTON = 5
    
    var buttonasfullScreenForOTP = UIButton()
    var textFieldEnterOTP = UITextField()
    var label_ForTimerCountDown = UILabel()
    var  buttonVerifyOTP = UIButton()
    var  buttonResendOTP = UIButton()
    var otp_Timer: Timer?
    var value_Timer : Int = 30
    var str_defaultTabSelected = "mobile"
    var maxlength_MobileNumberfield : Int = 0
   
    @IBOutlet weak var view_segmentedShadow: UIView!
    @IBOutlet weak var segmentControl: UISegmentedControl!
    @IBOutlet weak var constraints_dropDownWidth: NSLayoutConstraint!
    @IBOutlet weak var imageview_iconEmail: UIImageView!
    @IBOutlet weak var textField_countryorEmail: UITextField!
    @IBOutlet weak var view_shadowMobile: UIView!
    @IBOutlet weak var constraint_heightViewShadowMobile: NSLayoutConstraint!
    @IBOutlet weak var textField_mobileNumber: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setDefaultThemeColorOfEmailButton()
        
        //get all countries list api called...
        self.getAllCountriesAPICalled()
        
       //segment control customization
        if UIDevice.current.userInterfaceIdiom == .pad{
            view_segmentedShadow.setShadow(x: -5, y: -5, width: self.view.bounds.size.width - 290, height: view_segmentedShadow.bounds.size.height + 10, radius: 5)
        }else{
            view_segmentedShadow.setShadow(x: -5, y: -5, width: self.view.bounds.size.width - 90, height: view_segmentedShadow.bounds.size.height + 10, radius: 5)
        }
        segmentControl.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white], for: .selected)
        segmentControl.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.black], for: .normal)
        if #available(iOS 13.0, *) {
            segmentControl.selectedSegmentTintColor = UIColor.black
            //segmentControl.backgroundColor = UIColor.white
        } else {
            // Fallback on earlier versions
        }
        segmentControl.selectedSegmentIndex = 0
        constraints_dropDownWidth.constant = 40

    }
    
    //MARK: API Calling...
    func getAllCountriesAPICalled() {
       
         let parameters : Parameters = [
                        "cacheignore" : "\(Int(NSDate().timeIntervalSince1970))" //To ignore cache from api...
                    ]
        
        AppDelegate.showHUD(inView: self.view, message:"Loading....")
        Alamofire.request(Constants.apiURL + "webservice/countrylist.php", method: .get, parameters: parameters)
            .responseJSON { response in
                AppDelegate.hideHUD(inView: self.view)
                switch response.result {
                case .success:
                    if let json = response.result.value{
                        //print("JSON Response : \(json)")
                        guard let _ : NSDictionary = json as? NSDictionary else {
                            return
                        }
                        do {
                            // make sure this JSON is in the format we expect
                            let jsonData =  try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
                            let decoded = try JSONSerialization.jsonObject(with: jsonData, options: []) as! NSDictionary
                            // here "decoded" is of type `Any`, decoded from JSON data
                            guard let dataArray : NSArray = decoded["data"] as? NSArray else {
                                return
                            }
                            if dataArray.count > 0 {
                                self.array_CountryList.addObjects(from: (dataArray.reversed()) )
                            }
                            if self.array_CountryList.count>0 {
                                let dictEventDetail : NSDictionary = self.array_CountryList[0] as! NSDictionary
                                self.setCountryNameField(dictEventDetail)
                            }
                        }
                        catch let error as NSError {
                            print("Failed to load: \(error.localizedDescription)")
                        }
                    }
                case .failure( _):
                    //Utility.showAlert(message: error.localizedDescription, controller: self)
                    Utility.showAlert(message: "حدث خطأ - يرجي اعادة المحاولة", controller: self) //Something went wrong, please try again.
                }
        }
    }
    
    func sendOTPAPICalled(){
        //Country Code + Mobile Number...(i.e. +919773658682)
        guard let str_countryCode = textField_countryorEmail.accessibilityHint?.trimmingCharacters(in: .whitespacesAndNewlines) else {
            return
        }
        guard let str_mobileNumber = textField_mobileNumber.text?.trimmingCharacters(in: .whitespacesAndNewlines) else{
            return
        }
        let str_mobileNumberWithCountryCode : String = str_countryCode +  str_mobileNumber
        
        let parameters : Parameters = [
            "mobile" : str_mobileNumberWithCountryCode,
            "resend" : "0",
            "cacheignore" : "\(Int(NSDate().timeIntervalSince1970))" //To ignore cache from api...
        ]
        AppDelegate.showHUD(inView: self.view, message:"Loading....")
        Alamofire.request(Constants.mediaSourceURL + "vsms/otp/sendforgotpassword/", method: .post, parameters: parameters)
            .responseJSON { response in
                AppDelegate.hideHUD(inView: self.view)
                switch response.result {
                case .success:
                    if let json = response.result.value{
                        //print("JSON Response : \(json)")
                        guard let _ : NSDictionary = json as? NSDictionary else {
                            return
                        }
                        do {
                            // make sure this JSON is in the format we expect
                            let jsonData =  try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
                            let decoded = try JSONSerialization.jsonObject(with: jsonData, options: []) as! NSDictionary
                            if decoded["success"] as! Bool {
                                self.showOTPView()
                            }
                            else {
                                Utility.showAlert(message: decoded["msg"] as? String ?? "فشل الارسال -اعادة المحاولة", controller: self)//OTP not sent, please try again
                            }
                        }
                        catch let error as NSError {
                            print("Failed to load: \(error.localizedDescription)")
                        }
                    }
                case .failure( _):
                    //Utility.showAlert(message: error.localizedDescription, controller: self)
                    Utility.showAlert(message: "حدث خطأ - يرجي اعادة المحاولة", controller: self) //Something went wrong, please try again.
                }
        }
    }
    
    func verifyOTPAPICalled() {
        guard let strOtp = textFieldEnterOTP.text?.trimmingCharacters(in: .whitespacesAndNewlines) else{
            return
        }
        //Country Code + Mobile Number...(i.e. +919773658682)
        guard let str_countryCode = textField_countryorEmail.accessibilityHint?.trimmingCharacters(in: .whitespacesAndNewlines) else {
            return
        }
        guard let str_mobileNumber = textField_mobileNumber.text?.trimmingCharacters(in: .whitespacesAndNewlines) else{
            return
        }
        let str_mobileNumberWithCountryCode : String = str_countryCode +  str_mobileNumber
        
        let parameters : Parameters = [
            "mobile" : str_mobileNumberWithCountryCode,
            "otp" : strOtp,
            "cacheignore" : "\(Int(NSDate().timeIntervalSince1970))" //To ignore cache from api...
        ]
        AppDelegate.showHUD(inView: self.view, message:"Loading....")
        Alamofire.request(Constants.mediaSourceURL + "vsms/otp/verifyforgotpassword/", method: .post, parameters: parameters)
            .responseJSON { response in
                AppDelegate.hideHUD(inView: self.view)
                switch response.result {
                case .success:
                    if let json = response.result.value {
                        //print("JSON Response : \(json)")
                        guard let _ : NSDictionary = json as? NSDictionary else {
                            return
                        }
                        do {
                            // make sure this JSON is in the format we expect
                            let jsonData =  try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
                            let decoded = try JSONSerialization.jsonObject(with: jsonData, options: []) as! NSDictionary
                            // here "decoded" is of type `Any`, decoded from JSON data
                            if decoded["success"] as! Bool {
                                let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "resetPasswordVC") as? ResetPasswordVC
                                vc?.dict_verifyOTPAPIResponse = decoded
                                self.navigationController?.pushViewController(vc!, animated: false)
                            }
                            else {
                                iToast.show(decoded["msg"] as? String)
                            }
                        }
                        catch let error as NSError {
                            print("Failed to load: \(error.localizedDescription)")
                        }
                    }
                case .failure( _):
                    //Utility.showAlert(message: error.localizedDescription, controller: self)
                    Utility.showAlert(message: "حدث خطأ - يرجي اعادة المحاولة", controller: self) //Something went wrong, please try again.
                }
        }
    }
    
    func resendOTPAPICalled(){
        guard let str_countryCode = textField_countryorEmail.accessibilityHint?.trimmingCharacters(in: .whitespacesAndNewlines) else {
            return
        }
        guard let str_mobileNumber = textField_mobileNumber.text?.trimmingCharacters(in: .whitespacesAndNewlines) else{
            return
        }
        let str_mobileNumberWithCountryCode : String = str_countryCode +  str_mobileNumber
        
        let parameters : Parameters = [
            "mobile" : str_mobileNumberWithCountryCode,
            "resend" : "1",
            "cacheignore" : "\(Int(NSDate().timeIntervalSince1970))" //To ignore cache from api...
        ]
        AppDelegate.showHUD(inView: self.view, message:"Loading....")
        Alamofire.request(Constants.mediaSourceURL + "vsms/otp/sendforgotpassword/", method: .post, parameters: parameters)
            .responseJSON { response in
                AppDelegate.hideHUD(inView: self.view)
                switch response.result {
                case .success:
                    if let json = response.result.value{
                        guard let _ : NSDictionary = json as? NSDictionary else {
                            return
                        }
                        do {
                            // make sure this JSON is in the format we expect
                            let jsonData =  try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
                            let decoded = try JSONSerialization.jsonObject(with: jsonData, options: []) as! NSDictionary
                            if decoded["success"] as! Bool {
                                iToast.show("تم ارسال الكود بنجاح")//OTP sent successfully
                            }
                            else {
                                Utility.showAlert(message: decoded["msg"] as? String ?? "فشل الارسال -اعادة المحاولة", controller: self)//OTP not sent, please try again
                            }
                        }
                        catch let error as NSError {
                            print("Failed to load: \(error.localizedDescription)")
                        }
                    }
                case .failure( _):
                    //Utility.showAlert(message: error.localizedDescription, controller: self)
                    Utility.showAlert(message: "حدث خطأ - يرجي اعادة المحاولة", controller: self) //Something went wrong, please try again.
                }
        }
    }
    
    func showOTPView(){
        value_Timer = 30
        buttonasfullScreenForOTP = UIButton()
        buttonasfullScreenForOTP.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        buttonasfullScreenForOTP.frame = CGRect(x: 0, y: 0, width: UIApplication.shared.keyWindow?.bounds.size.width ?? 0.0, height: UIApplication.shared.keyWindow?.bounds.size.height ?? 0.0)
        buttonasfullScreenForOTP.addTarget(self, action: #selector(removebuttonasfullScreenForOTP), for: .touchUpInside)
        self.view.addSubview(buttonasfullScreenForOTP)
        
        let viewOTP = UIView()
        if UIDevice.current.userInterfaceIdiom == .pad{
            
            viewOTP.frame = CGRect(x: 20, y: (buttonasfullScreenForOTP.bounds.size.height - 352) / 2 - 64, width: buttonasfullScreenForOTP.bounds.size.width - 40, height: 280)
        } else {
            
            
            viewOTP.frame = CGRect(x: 20, y: (buttonasfullScreenForOTP.bounds.size.height - 216) / 2 - 40, width: buttonasfullScreenForOTP.bounds.size.width - 40, height: 240)
        }
        viewOTP.backgroundColor = UIColor.white
        buttonasfullScreenForOTP.addSubview(viewOTP)
        
        let labelTitle = UILabel()
        labelTitle.frame = CGRect(x: 30, y: 0, width: viewOTP.bounds.size.width - 60, height: 40)
        labelTitle.font = UIFont (name: Constants.helvetica_bold_font, size: 14)
        labelTitle.text = "التحقق من رقم الجوال" //"Mobile number verification
        labelTitle.numberOfLines = 1
        labelTitle.textAlignment = .center
        labelTitle.backgroundColor = UIColor.clear
        viewOTP.addSubview(labelTitle)
        
        let labelForgotPassword = UILabel()
        labelForgotPassword.frame = CGRect(x: 30, y: 40, width: viewOTP.bounds.size.width - 60, height: 60)
        guard let str_countryCode = textField_countryorEmail.accessibilityHint?.trimmingCharacters(in: .whitespacesAndNewlines) else{
            return
        }
        guard let str_mobileNumber = textField_mobileNumber.text?.trimmingCharacters(in: .whitespacesAndNewlines) else{
            return
        }
        let str_mobileNumberWithCountryCode : String = str_countryCode +  str_mobileNumber
        labelForgotPassword.text = "تم ارسال رسالة نصية لكم بالكود السري مكون من 4 ارقام" //A text message with a 4-digit verification code has been sent to
        //labelForgotPassword.text = "A text message with a 4-digit verification code has been sent to +919781116773)"
        labelForgotPassword.font = UIFont (name: Constants.helvetica_regular_font, size: 14)
        labelForgotPassword.numberOfLines = 3
        labelForgotPassword.textAlignment = .center
        labelForgotPassword.backgroundColor = UIColor.clear
        viewOTP.addSubview(labelForgotPassword)
        
        textFieldEnterOTP = UITextField()
        textFieldEnterOTP.frame = CGRect(x: 30, y: 100, width: viewOTP.bounds.size.width - 60, height: 40)
        textFieldEnterOTP.font = UIFont.systemFont(ofSize: 16)
        textFieldEnterOTP.placeholder = "برجاء كتابة رمز التحقق" //Please Enter OTP...
        textFieldEnterOTP.autocorrectionType = .no
        textFieldEnterOTP.autocapitalizationType = .none
        textFieldEnterOTP.backgroundColor = UIColor.white
        textFieldEnterOTP.keyboardType = .numberPad
        textFieldEnterOTP.clearButtonMode = .whileEditing
        textFieldEnterOTP.becomeFirstResponder()
        textFieldEnterOTP.layer.cornerRadius = 2
        viewOTP.addSubview(textFieldEnterOTP)
        let leftViewEmailId = UIView(frame: CGRect(x: 15, y: 0, width: 7, height: 26))
        leftViewEmailId.backgroundColor = UIColor.clear
        textFieldEnterOTP.leftView = leftViewEmailId
        textFieldEnterOTP.leftViewMode = .always
        textFieldEnterOTP.delegate = self

        label_ForTimerCountDown = UILabel()
        label_ForTimerCountDown.frame = CGRect(x: 30, y: 150, width: viewOTP.bounds.size.width - 60, height: 20)
        label_ForTimerCountDown.text = "اعادة ارسال الكود بعد \(value_Timer) ثانية" //"Resend after \(value_Timer) seconds"
        label_ForTimerCountDown.font = UIFont (name: Constants.helvetica_regular_font, size: 12)
        label_ForTimerCountDown.textColor = UIColor.lightGray
        label_ForTimerCountDown.numberOfLines = 1
        label_ForTimerCountDown.textAlignment = .center
        label_ForTimerCountDown.backgroundColor = UIColor.clear
        viewOTP.addSubview(label_ForTimerCountDown)
        
        buttonResendOTP = UIButton(type: .system)
        buttonResendOTP.frame = CGRect(x: 30, y: 180, width: viewOTP.bounds.size.width/2 - 40, height: 40)
        buttonResendOTP.setTitle("اعادة ارسال الكود", for: .normal) // Resend OTP
        buttonResendOTP.setTitleColor(UIColor.white, for: .normal)
        buttonResendOTP.layer.cornerRadius = 2
        buttonResendOTP.addTarget(self, action: #selector(buttonResendOTPClicked), for: .touchUpInside)
        viewOTP.addSubview(buttonResendOTP)
        self.enableUserInteractionReSenOTPButton(isEnable: false)
        
        buttonVerifyOTP = UIButton(type: .system)
        buttonVerifyOTP.frame = CGRect(x: viewOTP.bounds.size.width/2 + 10, y: 180, width: viewOTP.bounds.size.width/2 - 40, height: 40)
        buttonVerifyOTP.setTitle("ادخال الكود", for: .normal) //Verify OTP
        buttonVerifyOTP.setTitleColor(UIColor.white, for: .normal)
        buttonVerifyOTP.backgroundColor = Constants.default_ThemeColor
        buttonVerifyOTP.layer.cornerRadius = 2
        buttonVerifyOTP.addTarget(self, action: #selector(buttonVerifyOTPClicked), for: .touchUpInside)
        viewOTP.addSubview(buttonVerifyOTP)
        
        otp_Timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updatelabel_ForTimerCountDown), userInfo: nil, repeats: true)
    }
    
    @objc func removebuttonasfullScreenForOTP() {
        if (otp_Timer != nil){
            otp_Timer?.invalidate()
        }
        buttonasfullScreenForOTP.removeFromSuperview()
    }
    
    @objc func buttonVerifyOTPClicked(){
        if textFieldEnterOTP.text == ""{
            Utility.showAlert(message:"يرجى ادخال الكود", controller: self)//Please enter OTP.
            return
        }
        if(textFieldEnterOTP.isFirstResponder){textFieldEnterOTP.resignFirstResponder()}
        self.verifyOTPAPICalled()
    }
    
    @objc func buttonResendOTPClicked(){
        value_Timer = 30
        otp_Timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updatelabel_ForTimerCountDown), userInfo: nil, repeats: true)
        label_ForTimerCountDown.isHidden = false
        self.enableUserInteractionReSenOTPButton(isEnable: false)
        
        if(textFieldEnterOTP.isFirstResponder){textFieldEnterOTP.resignFirstResponder()}
        self.resendOTPAPICalled()
    }
    
    func enableUserInteractionReSenOTPButton(isEnable: Bool){
        if isEnable{
            buttonResendOTP.backgroundColor = Constants.default_ThemeColor
            buttonResendOTP.isUserInteractionEnabled = true
        }
        else{
            buttonResendOTP.backgroundColor = UIColor(red: 83/255, green: 83/255, blue: 83/255, alpha: 1.0)
            buttonResendOTP.isUserInteractionEnabled = false
        }
    }
    
    @objc func updatelabel_ForTimerCountDown(){
        if (otp_Timer != nil) {
            value_Timer = value_Timer - 1
            label_ForTimerCountDown.text = "اعادة ارسال الكود بعد \(value_Timer) ثانية" //"Resend after \(value_Timer) seconds"
            
            if value_Timer == 0 {
                otp_Timer!.invalidate()
                self.enableUserInteractionReSenOTPButton(isEnable: true)
                label_ForTimerCountDown.isHidden = true
            }
        }
    }
    
    func forgotPasswordAPICalled(){
        let parameters : Parameters = [
            "email" : textField_countryorEmail.text!,
            "cacheignore" : "\(Int(NSDate().timeIntervalSince1970))" //To ignore cache from api...
        ]
        AppDelegate.showHUD(inView: self.view, message: "Loading...")
        Alamofire.request(Constants.apiURL+"webservice/forgotpwd.php", method: .post, parameters: parameters)
            .responseJSON { response in
                AppDelegate.hideHUD(inView: self.view)
                switch response.result {
                case .success:
                    if let json = response.result.value{
                        print("JSON Response : \(json)")
                        guard let _ : NSDictionary = json as? NSDictionary else {
                            return
                        }
                        do {
                            // make sure this JSON is in the format we expect
                            let jsonData =  try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
                            let decoded = try JSONSerialization.jsonObject(with: jsonData, options: []) as! NSDictionary
                            if decoded["msg"] as? String == "success"{
                                self.backToScreenWithMessage()
                            }
                            else{
                                iToast.show("حدث خطأ - يرجي اعادة المحاولة")//An error has occurred - please try again
                            }
                        }
                        catch let error as NSError {
                            print("Failed to load: \(error.localizedDescription)")
                        }
                    }
                case .failure( _):
                    //Utility.showAlert(message: error.localizedDescription, controller: self)
                    Utility.showAlert(message: "حدث خطأ - يرجي اعادة المحاولة", controller: self) //Something went wrong, please try again.
                }
        }
    }
    
    //MARK: - UITableView DataSource
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return array_CountryList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "SelectionCell"
        var cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier)
        if cell == nil {
            cell = tableviewCell(withReuseIdentifierSelection: cellIdentifier, tablview: tableView, with: indexPath)
        }
        self.configureCellSelection(cell, for: indexPath, withSection: indexPath.section, tableView: tableView)
        cell!.selectionStyle = .none
        return (cell ?? nil) ?? UITableViewCell()
    }
    
    //MARK: - For Choose Country Only
    func tableviewCell(withReuseIdentifierSelection identifier: String?, tablview: UITableView?, with indxPath: IndexPath?) -> UITableViewCell? {
        let cell = UITableViewCell(style: .default, reuseIdentifier: identifier)
        cell.backgroundColor = UIColor.clear
        //UILabel Selection TreatMent...
        
        let radioButtonImageView = UIImageView.init(frame: CGRect.init(x: 10, y: 17, width: 25, height: 25))
        radioButtonImageView.tag = TAGCOUNTRYRADIOBUTTON
        radioButtonImageView.isUserInteractionEnabled = false
        cell.contentView.addSubview(radioButtonImageView)
        
        let labelSelection = UILabel.init(frame: CGRect(x: 40, y: 0, width: (tablview?.bounds.size.width ?? 0.0) - 45, height: 65))
        labelSelection.textColor = UIColor.white
        labelSelection.tag = TAGCOUNTRYNAME
        labelSelection.numberOfLines = 0
        labelSelection.textAlignment = .right
        labelSelection.isUserInteractionEnabled = true
        cell.contentView.addSubview(labelSelection)
        
        return cell
    }
    
    func configureCellSelection(_ cell: UITableViewCell?, for indexPath: IndexPath?, withSection section: Int, tableView: UITableView?) {
        
        let dictCountry : NSDictionary = array_CountryList[indexPath!.row] as! NSDictionary
        let str_countryName : String = dictCountry["name"] as? String ?? ""
        var str_countrycelCode : String = dictCountry["cel_code"] as? String ?? ""
        str_countrycelCode =  str_countrycelCode.replacingOccurrences(of: "+", with: "")
        
        let labelSelectionEvent = cell?.contentView.viewWithTag(TAGCOUNTRYNAME) as? UILabel
        labelSelectionEvent?.text = "(" + str_countrycelCode + "+) " + str_countryName
        labelSelectionEvent?.textColor = UIColor.white
        
        let radioImageSelectionEvent = cell?.contentView.viewWithTag(TAGCOUNTRYRADIOBUTTON) as? UIImageView
        if labelSelectionEvent?.text == textField_countryorEmail.text {
            radioImageSelectionEvent?.image = UIImage(named: "radio-on-white")
        }
        else {
            radioImageSelectionEvent?.image = UIImage(named: "radio-off-white")
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dictEventDetail : NSDictionary = array_CountryList[indexPath.row] as! NSDictionary
        self.setCountryNameField(dictEventDetail)
        self.removePickerFromScreenForEvent()
    }
    
    func setCountryNameField(_ dictCountry: NSDictionary) {
        //Country Name + Country Code...(i.e. India ( +91 )
        let str_countryName : String = dictCountry["name"] as? String ?? ""
        var str_countrycelCode : String = dictCountry["cel_code"] as? String ?? ""
        str_countrycelCode =  str_countrycelCode.replacingOccurrences(of: "+", with: "")
        textField_countryorEmail.text = "(" + str_countrycelCode + "+) " + str_countryName
        textField_countryorEmail.accessibilityLabel = dictCountry["code"] as? String ?? ""//Country Code i.e(SA,IN,etc...)
        textField_countryorEmail.accessibilityHint = dictCountry["cel_code"] as? String //Cell code i.e.(+91,+966,+98,etc...)
        var str_placeHolder = dictCountry["placeholder"] as? String
        str_placeHolder =  str_placeHolder?.replacingOccurrences(of: " ", with: "")
        textField_mobileNumber.placeholder = str_placeHolder
        if let Maxlgth  = str_placeHolder?.count{
            maxlength_MobileNumberfield = Maxlgth
        }
        textField_mobileNumber.text = ""
    }
    
    //MARK: - UITextField Delegate
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if  textField.tag ==  0  && str_defaultTabSelected == "mobile"{
            self.showCountryList()
            return false
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == textFieldEnterOTP {
            if CharacterSet(charactersIn: "0123456789").isSuperset(of: CharacterSet(charactersIn: string)) {
                let currentString: NSString = textField.text! as NSString
                let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
                return newString.length <= 6
            }else{return false}
        }
        else if textField.tag ==  6 {
            // Check for valid input characters
            if CharacterSet(charactersIn: "0123456789").isSuperset(of: CharacterSet(charactersIn: string)) {
                let currentString: NSString = textField.text! as NSString
                let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
                return newString.length <= maxlength_MobileNumberfield
            }
            else {
                return false
            }
        }
        return true
    }
    
    func showCountryList(){
        if array_CountryList.count == 0 {
            //For Event...
            return
        }
        if (textField_countryorEmail.isFirstResponder){
            textField_countryorEmail.resignFirstResponder()
        }
        if (textField_mobileNumber.isFirstResponder){
            textField_mobileNumber.resignFirstResponder()
        }
        
        buttonasfullScreenForCountrySelection = UIButton()
        buttonasfullScreenForCountrySelection.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        buttonasfullScreenForCountrySelection.frame = CGRect(x: 0, y: 0, width: UIApplication.shared.keyWindow?.bounds.size.width ?? 0.0, height: UIApplication.shared.keyWindow?.bounds.size.height ?? 0.0)
        buttonasfullScreenForCountrySelection.addTarget(self, action: #selector(removePickerFromScreenForEvent), for: .touchUpInside)
        UIApplication.shared.keyWindow?.addSubview(buttonasfullScreenForCountrySelection)
        
        let viewForShadow = UIView()
        if UIDevice.current.userInterfaceIdiom == .phone{
            viewForShadow.frame = CGRect(x: (buttonasfullScreenForCountrySelection.bounds.size.width - 350) / 2, y: (buttonasfullScreenForCountrySelection.bounds.size.height - 500) / 2, width: 350, height: 500)
        }
        else {
            viewForShadow.frame = CGRect(x: buttonasfullScreenForCountrySelection.bounds.size.width / 4, y: buttonasfullScreenForCountrySelection.bounds.size.height / 4, width: buttonasfullScreenForCountrySelection.bounds.size.width / 2, height: buttonasfullScreenForCountrySelection.bounds.size.height / 2)
            
        }
        viewForShadow.autoresizingMask = [.flexibleTopMargin, .flexibleBottomMargin, .flexibleLeftMargin, .flexibleRightMargin]
        buttonasfullScreenForCountrySelection.addSubview(viewForShadow)
        
        tableViewForCountrySelection = UITableView()
        tableViewForCountrySelection.tag = 100
        tableViewForCountrySelection.cellLayoutMarginsFollowReadableWidth = false
        tableViewForCountrySelection.frame = CGRect(x: 0, y: 0, width: viewForShadow.bounds.size.width, height: viewForShadow.bounds.size.height)
        tableViewForCountrySelection.delegate = self
        tableViewForCountrySelection.dataSource = self
        tableViewForCountrySelection.separatorStyle = UITableViewCell.SeparatorStyle.singleLine
        tableViewForCountrySelection.separatorColor = UIColor.gray
        tableViewForCountrySelection.tableFooterView = UIView(frame: CGRect.zero)
        tableViewForCountrySelection.backgroundColor = UIColor.black
        viewForShadow.addSubview(tableViewForCountrySelection)
        tableViewForCountrySelection.reloadData()
    }
    
    @objc func removePickerFromScreenForEvent() {
        buttonasfullScreenForCountrySelection.removeFromSuperview()
    }
    
    func setDefaultThemeColorOfMobileButton() {
        let string_Mobile : String = "رقم الجوال"
        var mutablestring_Email = NSMutableAttributedString()
        mutablestring_Email = NSMutableAttributedString(string: string_Mobile, attributes: [NSAttributedString.Key.font:UIFont(name: Constants.helvetica_bold_font, size: 17.0)!])
        mutablestring_Email.addAttribute(NSAttributedString.Key.foregroundColor, value: Constants.default_ThemeColor, range: NSRange(location:0,length:string_Mobile.count))
        mutablestring_Email.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: NSRange(location:0, length:string_Mobile.count))
        //        button_Mobile.setAttributedTitle(mutablestring_Email, for: .normal)
    }
    
    func setBlackColorOfMobileButton() {
        let string_Mobile : String = "رقم الجوال"
        var mutablestring_Email = NSMutableAttributedString()
        mutablestring_Email = NSMutableAttributedString(string: string_Mobile, attributes: [NSAttributedString.Key.font:UIFont(name: Constants.helvetica_bold_font, size: 17.0)!])
        mutablestring_Email.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.black, range: NSRange(location:0,length:string_Mobile.count))
        //mutablestring_Email.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: NSRange(location:0, length:string_Mobile.count))
        //        button_Mobile.setAttributedTitle(mutablestring_Email, for: .normal)
    }
    
    func setDefaultThemeColorOfEmailButton() {
        let string_Email : String = "البريد الإلكتروني"
        var mutablestring_Email = NSMutableAttributedString()
        mutablestring_Email = NSMutableAttributedString(string: string_Email, attributes: [NSAttributedString.Key.font:UIFont(name: Constants.helvetica_bold_font, size: 17.0)!])
        mutablestring_Email.addAttribute(NSAttributedString.Key.foregroundColor, value: Constants.default_ThemeColor, range: NSRange(location:0,length:string_Email.count))
        mutablestring_Email.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: NSRange(location:0, length:string_Email.count))
        //        button_Email.setAttributedTitle(mutablestring_Email, for: .normal)
    }
    
    func setBlackColorOfEmailButton() {
        let string_Email : String = "البريد الإلكتروني"
        var mutablestring_Email = NSMutableAttributedString()
        mutablestring_Email = NSMutableAttributedString(string: string_Email, attributes: [NSAttributedString.Key.font:UIFont(name: Constants.helvetica_bold_font, size: 17.0)!])
        mutablestring_Email.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.black, range: NSRange(location:0,length:string_Email.count))
        //mutablestring_Email.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: NSRange(location:0, length:string_Email.count))
        //        button_Email.setAttributedTitle(mutablestring_Email, for: .normal)
    }
    
    //MARK: UIButton Clicks
    @IBAction func segmentedControlButtonClickAction(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 0 {//When mobile section is selected
            
            if(textField_mobileNumber.becomeFirstResponder()){textField_mobileNumber.resignFirstResponder()}
            constraints_dropDownWidth.constant = 40
            str_defaultTabSelected = "mobile"
            imageview_iconEmail.image = UIImage(named: "signup_country")
            textField_mobileNumber.isHidden = false
            constraint_heightViewShadowMobile.constant = 45
            view_shadowMobile.isHidden = false
            textField_countryorEmail.text = ""
            textField_countryorEmail.placeholder = "بلد"
            textField_countryorEmail.textAlignment = NSTextAlignment.center
            self.setBlackColorOfEmailButton()
            self.setDefaultThemeColorOfMobileButton()
            
            if array_CountryList.count>0 {
                let dictEventDetail : NSDictionary = array_CountryList[0] as! NSDictionary
                self.setCountryNameField(dictEventDetail)
            }
            
        }else { //when email section is selected
            
            constraints_dropDownWidth.constant = 0
            str_defaultTabSelected = "email"
            imageview_iconEmail.image = UIImage(named: "signup_email")
            textField_mobileNumber.isHidden = true
            constraint_heightViewShadowMobile.constant = 0
            view_shadowMobile.isHidden = true
            textField_countryorEmail.text = ""
            textField_countryorEmail.placeholder = "بريدك الالكتروني"  // Your Email Address
            textField_countryorEmail.textAlignment = NSTextAlignment.right
            self.setBlackColorOfMobileButton()
            self.setDefaultThemeColorOfEmailButton()
        }
    }
    
    @IBAction func button_resetPasswordClicked(_ sender: UIButton) {
        if str_defaultTabSelected == "email"{
            if textField_countryorEmail.text == "" || self.isValidEmail(emailStr:textField_countryorEmail.text!) == false {
                Utility.showAlert(message:"الرجاء إدخال بريد إلكتروني صحيح", controller: self)
                return
            }
            self.forgotPasswordAPICalled()
        }
        else{
            if textField_countryorEmail.text == "" || textField_countryorEmail.accessibilityHint == "" {
                Utility.showAlert(message:"برجاء اختيار الدولة", controller: self)
                return
            }
            if textField_mobileNumber.text == ""  || textField_mobileNumber.text!.count != maxlength_MobileNumberfield {
                Utility.showAlert(message:"من فضلك أدخل رقم الجوال", controller: self)
                return
            }
            value_Timer = 30
            self.showGetTheCodeAlert()
        }
    }
    
    func showGetTheCodeAlert(){
//        let alertController = UIAlertController(title: "التأكد من صحة رقم الجوال", message: "", preferredStyle: .alert)//"Make sure the phone number is correct",
//        alertController.addAction(UIAlertAction(title: "الحصول على الكود", style: .default, handler: { action in //"Get the code",
//            switch action.style{
//            case .default:
//                print("default")
//                self.sendOTPAPICalled()
//            case .cancel:
//                print("cancel")
//            case .destructive:
//                print("destructive")
//            @unknown default: break
//            }}))
//        self.present(alertController, animated: true, completion: nil)
        
        let modalViewController = AlertViewSendOTPViewController()
                       modalViewController.modalPresentationStyle = .overFullScreen // .overCurrentContext
               //        modalViewController.modalTransitionStyle =  UIModalTransitionStyle.flipHorizontal
                       modalViewController.delegate = self
                       self.present(modalViewController, animated: false)
    }
    
    func backToScreenWithMessage(){
        let alertController = UIAlertController(title: Constants.msg_Alert, message: "تم إرسال بريد إلكتروني لإعادة تعين كلمة السر", preferredStyle: .alert) //Reset Password sent to your email 
        alertController.addAction(UIAlertAction(title: "حسنا", style: .default, handler: { action in
            switch action.style{
            case .default:
                print("default")
                self.navigationController?.popToRootViewController(animated: false)
            case .cancel:
                print("cancel")
            case .destructive:
                print("destructive")
            @unknown default: break
                
            }}))
        self.present(alertController, animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

