//
//  ReturnOrderVC.swift
//  Eoutlet
//
//  Created by Unyscape Infocom on 06/02/20.
//  Copyright © 2020 Unyscape. All rights reserved.
//

import UIKit

class ReturnOrderTableViewCell : UITableViewCell
{
    @IBOutlet weak var cellUIView: UIView!
    @IBOutlet weak var order_Number: UILabel!
    @IBOutlet weak var order_title: UILabel!
    @IBOutlet weak var order_amount: UILabel!
    @IBOutlet weak var return_button: UIView!
}

class ReturnOrderVC: UIViewController, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet weak var returnOrderTableView: UITableView!
    let cell_Height = 160
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.addNavigationControllerActivities()
    }
    
    override func viewWillAppear(_ _animated: Bool) {
        super.viewWillAppear(_animated)
        self.navigationController?.navigationBar.isHidden = false
    }
    
    //MARK: Navigation Settings
    func addNavigationControllerActivities(){
        let backImg: UIImage = UIImage(named: "backarrow")!
        let button_Back = UIButton(type: .custom)
        button_Back.frame = CGRect(x: 0, y: 0, width: 32, height: 22)
        button_Back.addTarget(self, action: #selector(backClicked), for: .touchUpInside)
        button_Back.setBackgroundImage(backImg, for: .normal)
        let buttonItem = UIBarButtonItem(customView: button_Back)
        navigationItem.leftBarButtonItem = buttonItem
        self.title = "استبدال او استرجاع" // RETURN ORDER
    }
    
    @objc func backClicked() {
        self.navigationController?.popViewController(animated: false)
    }
    
    //set the number of section of tableview
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    //set the number of row as per section
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 20
    }
    
    //configure the tableview row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "returnOrderTableViewCell", for: indexPath) as! ReturnOrderTableViewCell
        
        //row shadow
        let shadowSize : CGFloat = 5.0
        let shadowPath = UIBezierPath(rect: CGRect(x: Int(-shadowSize / 2),
                                                   y: 2,
                                                   width: Int(self.view.frame.size.width),
                                                   height: cell_Height-10))
        cell.cellUIView.layer.masksToBounds = false
        cell.cellUIView.layer.shadowColor = UIColor.lightGray.cgColor
        cell.cellUIView.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        cell.cellUIView.layer.shadowOpacity = 0.5
        cell.cellUIView.layer.shadowRadius = 5.0
        cell.cellUIView.layer.shadowPath = shadowPath.cgPath
        return cell
    }
     
    //set tableview row height as per device like iphone or ipad
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(cell_Height)
    }
    
    //did select action when user click on tableview cell
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
}
