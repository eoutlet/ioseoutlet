//
//  Wallet3DSecureURLWebViewVC.swift
//  Eoutlet
//
//  Created by Sudhir Rohilla on 23/06/20.
//  Copyright © 2020 Unyscape. All rights reserved.
//

import UIKit
import Alamofire
import WebKit

class Wallet3DSecureURLWebViewVC: UIViewController, WKNavigationDelegate {

    @IBOutlet weak var threeDSecureWebView: WKWebView!
    var str_threeDSecureURL = ""
    var str_orderID = ""
    var amount_Final = 0
    var timer = Timer()
    var tokenNameStr = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        threeDSecureWebView.navigationDelegate = self
        threeDSecureWebView.scrollView.bounces = false

        self.loadUrlOnWebView()
        timer = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(self.getOrderStatus), userInfo: nil, repeats: true)

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ _animated: Bool) {
           super.viewWillAppear(_animated)
           self.navigationController?.navigationBar.isHidden = true
           
       }
       
    override func viewDidDisappear(_ animated: Bool) {
        self.timer.invalidate()
    }
    
    func loadUrlOnWebView() {
           
           guard let myURL = URL(string: self.str_threeDSecureURL) else { return}
           let myRequest = URLRequest(url: myURL)
           self.threeDSecureWebView.load(myRequest)
           
       }
   
    //MARK: API Calling...
    @objc func getOrderStatus() {
            
            let parameters : Parameters = [
                
                "order_id" : self.str_orderID,
                "token_name" : tokenNameStr,
                "is_wallet" : "1",
                "devicetype" : "ios",
                "cacheignore" : "\(Int(NSDate().timeIntervalSince1970))" //To ignore cache from api...
            ]
            
            Alamofire.request(Constants.apiURL+"webservice/checkout_paymentstatus.php", method: .post, parameters: parameters)
                .responseJSON { response in
                    switch response.result {
                    case .success:
                        if let json = response.result.value{
                            guard let _ : NSDictionary = json as? NSDictionary else {
                                return
                            }
                            do {
                                // make sure this JSON is in the format we expect
                                let jsonData =  try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
                                let decoded = try JSONSerialization.jsonObject(with: jsonData, options: []) as! NSDictionary
                                // here "decoded" is of type `Any`, decoded from JSON data
                                if decoded["msg"] as? String == "success"{
                                    
                                    AppDelegate.hideHUD(inView: self.view)
                                    self.timer.invalidate()
                                    let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "thankYouVC") as! ThankYouVC
                                    vc.string_orderStatus = "success"
                                    vc.string_OrderId = "\(self.str_orderID)"
                                    vc.string_Price = "\(self.amount_Final)"
                                    self.navigationController?.pushViewController(vc, animated: false)
                                    
                                }else if(decoded["msg"] as? String == "failed"){
                                    
                                    AppDelegate.hideHUD(inView: self.view)
                                    self.timer.invalidate()
                                    let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "thankYouVC") as! ThankYouVC
                                    vc.string_orderStatus = "failed"
                                    vc.string_OrderId = "\(self.str_orderID)"
                                    vc.string_Price = "\(self.amount_Final)"
                                    self.navigationController?.pushViewController(vc, animated: false)
                                    
                                }
                                
                                }
                            catch _ as NSError {
                            }
                        }
                    case .failure( _): break
                       // Utility.showAlert(message: "حدث خطأ - يرجي اعادة المحاولة", controller: self) //Something went wrong, please try again.
                    }
            }
        }
        
  //MARK: - WKWebView Delegates
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        DispatchQueue.main.async {
            AppDelegate.hideHUD(inView: self.view)
        }
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        DispatchQueue.main.async {
            AppDelegate.hideHUD(inView: self.view)
        }
       
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        
        if navigationAction.navigationType == WKNavigationType.linkActivated {
            if let url = navigationAction.request.url {
                decisionHandler(.cancel)
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        }
        else {
            decisionHandler(.allow)
        }
    }
    
   

}
