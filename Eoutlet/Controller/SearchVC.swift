
import Alamofire
import SDWebImage
import AppsFlyerLib

class SearchHistoryTableViewCell : UITableViewCell
{
    @IBOutlet weak var label_Title: UILabel!
}

class SearchVC: UIViewController, UISearchBarDelegate, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet weak var constraints_topForSearchBar: NSLayoutConstraint!
    @IBOutlet weak var label_noRecordFound: UILabel!
    @IBOutlet weak var searchbar_Top: UISearchBar!
    @IBOutlet weak var collectionViewProductList: UICollectionView!
    @IBOutlet weak var searchHistoryTableView: UITableView!
    var searchHistoryArray : [String] = []
    var filterHistoryArray : [String] = []

    var refreshControl = UIRefreshControl()
    var array_ProductList : NSMutableArray = NSMutableArray()
    var pageNumber : Int = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let height_Statusbar =  UIApplication.shared.statusBarFrame.height
        if height_Statusbar != 0 {//If status bar is showing...
            constraints_topForSearchBar.constant = -20
        }
        var hasTopNotch: Bool {
            if #available(iOS 11.0,  *) {
                return UIApplication.shared.delegate?.window??.safeAreaInsets.top ?? 0 > 20
            }
            return false
        }
        
        if hasTopNotch {//If status bar is showing...
            constraints_topForSearchBar.constant = -40
        }
        
        refreshControl.addTarget(self, action: #selector(pullToRefreshCollectionView), for: UIControl.Event.valueChanged)
        collectionViewProductList.addSubview(refreshControl) //Pull to refresh in UICollectionViewController
        
        self.collectionViewProductList.isHidden = true
        self.label_noRecordFound.isHidden = true
     
        searchHistoryArray = UserDefaults.standard.stringArray(forKey: "searchHistory") ?? [String]()
        filterHistoryArray = searchHistoryArray
        
        self.searchHistoryTableView.isHidden = false
        self.searchHistoryTableView.keyboardDismissMode = .onDrag  

        self.searchHistoryTableView.tableFooterView = UIView()
        self.searchHistoryTableView.reloadData()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        searchbar_Top.becomeFirstResponder()
    }
    
    //MARK: API Calling...
    func searchProductAPICalled(keyword: NSString){
        let parameters : Parameters = [
            "keyword" : keyword,
            "page" : pageNumber,
            "cacheignore" : "\(Int(NSDate().timeIntervalSince1970))" //To ignore cache from api...
        ]
        print("searchProductAPICalled------------->\(pageNumber)")
        AppDelegate.showHUD(inView: self.view, message:"Loading....")
        Alamofire.request(Constants.apiURL + "webservice/search.php", method: .get, parameters: parameters)
            .responseJSON { response in
                AppDelegate.hideHUD(inView: self.view)
                switch response.result {
                case .success:
                    if let json = response.result.value{
                        guard let _ : NSDictionary = json as? NSDictionary else {
                            return
                        }
                        do {
                            // make sure this JSON is in the format we expect
                            let jsonData =  try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
                            let decoded = try JSONSerialization.jsonObject(with: jsonData, options: []) as! NSDictionary
                            // here "decoded" is of type `Any`, decoded from JSON data
               
                            //its for locally store history for search...
                            if !self.searchHistoryArray.contains(keyword as String) { // we do not need to add duplicate
                                var tempArr : [String] = []
                                tempArr.append("test")
                                for i in 0..<self.searchHistoryArray.count {
                                    tempArr.append(self.searchHistoryArray[i])
                                }
                                tempArr[0] = keyword as String
                                self.searchHistoryArray.removeAll()
                                
                                for i in 0..<tempArr.count {
                                    if i < 10 { // 10 is limit for user default
                                        self.searchHistoryArray.append(tempArr[i])
                                    }
                                    else {
                                        break
                                    }
                                }
                                self.filterHistoryArray.removeAll()
                                self.filterHistoryArray = self.searchHistoryArray
                                
                                UserDefaults.standard.set(self.searchHistoryArray, forKey: "searchHistory")
                            }
                            self.searchHistoryTableView.reloadData()
                            self.searchHistoryTableView.isHidden = true
                            
                            guard let dataArray : NSArray = decoded["data"] as? NSArray else {
                                return
                            }
                            self.pageNumber = decoded["page"] as? Int ?? 0
                            
                            if dataArray.count > 0 {
                                if(self.array_ProductList.count>0 && self.pageNumber == 1){//When page number == 1, then need to remove all old data
                                    self.array_ProductList.removeAllObjects()
                                }
                                
                                self.searchAppsFlyer()
                                self.searchFirebaseAnylatics()
                                
                                self.array_ProductList.addObjects(from: (dataArray ) as! [Any])
                                self.isProductListHidden(ishidden: false)
                                self.collectionViewProductList.reloadData()
                                if(self.searchbar_Top.becomeFirstResponder()){self.searchbar_Top.resignFirstResponder()}
                            }
                            else{
                                if self.array_ProductList.count == 0{
                                    self.isProductListHidden(ishidden: true)
                                }
                                else{
                                    self.isProductListHidden(ishidden: false)
                                }
                            }
                        }
                        catch let error as NSError {
                            self.isProductListHidden(ishidden: true)
                            print("Failed to load: \(error.localizedDescription)")
                        }
                    }
                case .failure( _):
                    //Utility.showAlert(message: error.localizedDescription, controller: self)
                    Utility.showAlert(message: "حدث خطأ - يرجي اعادة المحاولة", controller: self) //Something went wrong, please try again.
                }
        }
    }
    
    @objc func pullToRefreshCollectionView(sender:AnyObject) {
        guard let trimmedString = self.searchbar_Top.text?.trimmingCharacters(in: .whitespacesAndNewlines)
            else {return}
        
        if trimmedString.isEmpty {
            print("search text is empty .")
            return
        }
        
        if(self.searchbar_Top.becomeFirstResponder()){self.searchbar_Top.resignFirstResponder()}
        pageNumber = 1 //Need to reset page number 1, this is will fetch initial level data
        searchProductAPICalled(keyword: trimmedString as NSString)
    }
    
    func isProductListHidden(ishidden : Bool) {
        if ishidden {
            self.collectionViewProductList.isHidden = true
            self.label_noRecordFound.isHidden = false
        }
        else{
            self.collectionViewProductList.isHidden = false
            self.label_noRecordFound.isHidden = true
        }
    }
    
    //MARK: UICollection View
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return array_ProductList.count
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        var width: CGFloat = 0
        if UIDevice.current.userInterfaceIdiom == .phone{
            width = (collectionViewProductList.bounds.size.width - 30) / 2
            
        } else {
            
            width = (collectionViewProductList.bounds.size.width - 40) / 3
        }
        return CGSize(width: width, height: width + 120)
    }
    
    internal func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "productListCollectionViewCell", for: indexPath) as! ProductListCollectionViewCell
        let dict : NSDictionary = array_ProductList[indexPath.row] as! NSDictionary
        cell.label_Title.text = dict["name"] as? String
        cell.label_OldPrice.isHidden = false
        cell.label_NewFrontPrice.isHidden = true
        //  cell.base_View.setShadow(shadowSize: 10.0, radius: 10.0)
        
        if let intValue = (dict["old_price"] as? Int) {
            
            if(intValue == 0){
                cell.label_OldPrice.isHidden = true
                cell.label_NewFrontPrice.isHidden = false
            }
            cell.label_OldPrice.text = String(format: "SAR %d", intValue)
            
        }else if let strValue = (dict["old_price"] as? String){
            
            if((strValue.replacingOccurrences(of: ",", with: "")) == "0"){
                cell.label_OldPrice.isHidden = true
                cell.label_NewFrontPrice.isHidden = false
            }
            cell.label_OldPrice.text = String(format: "SAR %d", (strValue.replacingOccurrences(of: ",", with: "") as NSString).integerValue)
            
        }else{
            print("some other data types exists.")
        }
        
        
        if let intValue = (dict["price"] as? Int) {
            cell.label_NewPrice.text = String(format: "SAR %d", intValue)
            cell.label_NewFrontPrice.text = String(format: "SAR %d", intValue)
        }else if let strValue = (dict["price"] as? String){
            cell.label_NewPrice.text = String(format: "SAR %d", (strValue.replacingOccurrences(of: ",", with: "") as NSString).integerValue)
            cell.label_NewFrontPrice.text = String(format: "SAR %d", (strValue.replacingOccurrences(of: ",", with: "") as NSString).integerValue)
        }else{
            print("some other data types exists.")
        }
        
        cell.imageView_Icon.sd_imageIndicator = SDWebImageActivityIndicator.gray
        cell.imageView_Icon.sd_imageIndicator?.startAnimatingIndicator()
        let url = NSURL(string: dict["img"] as? String ?? "")
        cell.imageView_Icon.sd_setImage(with: url as URL?) { (image, error, cache, urls) in
            if (error != nil) {// Failed to load image
            } else {// Successful in loading image
                cell.imageView_Icon.sd_imageIndicator?.stopAnimatingIndicator()
            }}
        
        //set shadow on product list cell collectionview
        cell.layoutIfNeeded()
        cell.base_View.backgroundColor = UIColor.white //your background color...
        cell.base_View.cornerRadius = 5
        cell.base_View.setShadow(x: -5, y: -5, width: cell.base_View.bounds.width + 10, height: cell.base_View.bounds.height + 10, radius: 5)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "productDetailVC") as? ProductDetailVC
        vc?.dict_Product = array_ProductList[indexPath.row] as! NSDictionary
        self.navigationController?.pushViewController(vc!, animated: false)
    }
    
    func  scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if collectionViewProductList.contentOffset.y >= (collectionViewProductList.contentSize.height - collectionViewProductList.frame.size.height) {//you reached end of the UICollectionview
            
            guard let trimmedString = self.searchbar_Top.text?.trimmingCharacters(in: .whitespacesAndNewlines)
                else {return}
            
            if trimmedString.isEmpty {
                print("search text is empty .")
                return
            }
            
            if(self.searchbar_Top.becomeFirstResponder()){self.searchbar_Top.resignFirstResponder()}
            pageNumber += 1
            searchProductAPICalled(keyword: trimmedString as NSString)
        }
    }
    
    //MARK: UISearchBar Delegate
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.searchHistoryTableView.isHidden = false
        if searchText == "" {
            filterHistoryArray = searchHistoryArray
            self.searchHistoryTableView.reloadData()
        }
        else {
        filterHistoryArray = searchHistoryArray.filter({($0.localizedCaseInsensitiveContains(searchText))})
        self.searchHistoryTableView.reloadData()
        }
    }
        
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar){
        self.navigationController?.popViewController(animated: false)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        guard let trimmedString = self.searchbar_Top.text?.trimmingCharacters(in: .whitespacesAndNewlines)
            else {return}
        
        if trimmedString.isEmpty {
            print("search text is empty .")
            return
        }
        
        if(self.searchbar_Top.becomeFirstResponder()){self.searchbar_Top.resignFirstResponder()}
        
        DispatchQueue.main.async {
            
            self.array_ProductList.removeAllObjects()
            self.collectionViewProductList.reloadData()
            
        }
        pageNumber = 1 //Need to reset page number 1, this is will fetch initial level data
        searchProductAPICalled(keyword: trimmedString as NSString)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.filterHistoryArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = searchHistoryTableView.dequeueReusableCell(withIdentifier: "searchHistoryTableViewCell") as! SearchHistoryTableViewCell
        cell.label_Title.text = self.filterHistoryArray[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.searchHistoryTableView.isHidden = true
        self.searchbar_Top.text = self.filterHistoryArray[indexPath.row] as String
        searchProductAPICalled(keyword: self.filterHistoryArray[indexPath.row] as NSString)
    }
      
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
}

//for appsflyer
extension SearchVC {
    func searchAppsFlyer() {
        let productInfo : NSMutableDictionary =  NSMutableDictionary()
        productInfo.setValue(loggedInUser.shared.string_userID, forKey: "user_id")
        analyticsAppsFlyer.shared.AnalyticsEventSearch(paramas: productInfo)
    }
}

//for firebase
extension SearchVC {
    func searchFirebaseAnylatics(){
        let productInfo : NSMutableDictionary =  NSMutableDictionary()
        productInfo.setValue(searchbar_Top.text, forKey: "search_item")
        analyticsFirebase.shared.AnalyticsEventViewSearchResults(paramas: productInfo as NSDictionary)
    }
}
