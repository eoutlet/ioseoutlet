import UIKit
import Alamofire
import FirebaseAnalytics
import SDWebImage

protocol CartTableViewCellDelegate {
    
    func buttonDeleteClicked(sender: UIButton)
    func buttonDecreaseQuantityClicked(sender: UIButton)
    func buttonIncreaseQuantityClicked(sender: UIButton)
    
}

class CartTableViewCell: UITableViewCell{
    
    @IBOutlet weak var view_backgroundForAllDataHolder: UIView!
    @IBOutlet weak var view_OutOfStockIndicator: UIView!
    @IBOutlet weak var label_OutOfStockMessage: UILabel!
    @IBOutlet weak var imageview_Banner: UIImageView!
    @IBOutlet weak var label_Title: UILabel!
    @IBOutlet weak var label_OldPrice: UILabel!
    @IBOutlet weak var label_NewPrice: UILabel!
    @IBOutlet weak var label_Size: UILabel!
    @IBOutlet weak var label_Quantity: UILabel!
    @IBOutlet weak var button_Delete: UIButton!
    @IBOutlet weak var stackView_increamentOrDecreament: UIStackView!
    @IBOutlet weak var button_Decrement: UIButton!
    @IBOutlet weak var button_Incremnent: UIButton!
    
    var delegate: CartTableViewCellDelegate?
    
    @IBAction func button_deleteClicked(_ sender: UIButton) {
        delegate?.buttonDeleteClicked(sender: sender)
    }
    
    @IBAction func button_DecreaseQuantityClicked(_ sender: UIButton) {
        delegate?.buttonDecreaseQuantityClicked(sender: sender)
    }
    
    @IBAction func button_IncreaseQuantityClicked(_ sender: UIButton) {
        delegate?.buttonIncreaseQuantityClicked(sender: sender)
    }
}

class CartVC: UIViewController,UITableViewDataSource,UITableViewDelegate,CartTableViewCellDelegate,closeTransparent {
    func closeView(idStr: String) {
        
    }
       
    @IBOutlet weak var scrollSlider: SwiftlyScrollSlider!

    @IBOutlet weak var constraints_CartScrollViewHeight: NSLayoutConstraint!
    @IBOutlet weak var cartUIView: UIView!
    @IBOutlet weak var constraints_CartUIViewHeight: NSLayoutConstraint!
    @IBOutlet weak var constraints_ScrollTableLeading: NSLayoutConstraint!

    var array_CartItem : NSMutableArray = NSMutableArray()
    var dict_CompleteCartRecord : NSMutableDictionary = NSMutableDictionary()
    
    @IBOutlet weak var label_NoItemAvailable: UILabel!
    @IBOutlet weak var scrollview_basescrollView: UIScrollView!
    @IBOutlet weak var scrollview_tableView: UIScrollView!
    @IBOutlet weak var tableView_Cart: UITableView!
    
    @IBOutlet weak var view_BottomTaxTotalPriceGrandTotalHolder: UIView!
    
    @IBOutlet weak var label_TotalPriceValue: UILabel!
    @IBOutlet weak var label_GiftPriceValue: UILabel!
    @IBOutlet weak var label_GrandTotalPriceValue: UILabel!
    
    @IBOutlet weak var button_SecureCheckOut: UIButton!
    @IBOutlet weak var view_SecureCheckOut: UIView!
    @IBOutlet weak var giftCardUIView: UIView!
    @IBOutlet weak var image_gift: UIImageView!
        
    var rowHeight = 140  // table view row height
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.ishideScrollViewAndSecureCheckOut(ishide: true)
        
        //Disable swipe gesture for popviewcontroller...
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        
        //We are adding notification observer, it will called when thankyou screen will come, it will do reset checkbox.
        NotificationCenter.default.addObserver(self, selector: #selector(giftBoxReset), name: Notification.Name("giftBoxReset"), object: nil)
        
        scrollSlider.thumbImageView?.image = UIImage(named: "ScrollSliderCustom.png") // Change an image of the scroll slider
        scrollSlider.lineBackgroundView?.backgroundColor = UIColor.init(red: 232/255, green: 232/255, blue: 232/255, alpha: 1.0) // A color of the line
        scrollSlider.lineBackgroundView?.layer.borderColor = UIColor.init(red: 232/255, green: 232/255, blue: 232/255, alpha: 1.0).cgColor // A border of the line color
        scrollSlider.isUserInteractionEnabled = false
    }
    
    //It will reset checkbox for giftbox.
    @objc func giftBoxReset() {
        self.removeGiftCardValue()
    }
    
    func ishideScrollViewAndSecureCheckOut(ishide: Bool){
        if ishide == true {
            label_NoItemAvailable.isHidden = false
            scrollview_basescrollView.isHidden = true
            view_SecureCheckOut.isHidden = true
            button_SecureCheckOut.isHidden = true
            if let tabItems = self.tabBarController?.tabBar.items {
                // In this case we want to modify the badge number of the first tab:
                let tabItem = tabItems[0]
                tabItem.badgeValue = "0"
            }
        }else {
            label_NoItemAvailable.isHidden = true
            scrollview_basescrollView.isHidden = false
            view_SecureCheckOut.isHidden = false
            button_SecureCheckOut.isHidden = false
        }
    }
    
    override func viewWillAppear(_ _animated: Bool) {
        
        self.getAllCartItemAPICalled()
        
        self.navigationController?.navigationBar.isHidden = true
        
        self.updateCartItemInNavigationBar()
    }
    
    func updateCartItemInNavigationBar() {
        //set cart item count on home screen navigation & tab bar...
        if AppDelegate.cart_item_count != 0 {
            if let tabItems = self.tabBarController?.tabBar.items {
                let tabItem = tabItems[0]
                tabItem.badgeValue = "\(AppDelegate.cart_item_count)"
            }
        }
    }
    
    //MARK: Calculate Grand Total
    func calculateGrandTotal(){
        
        var int_GrandTotal = 0
        
        let str_subTotalValue : NSString = NSString(string: label_TotalPriceValue.text ?? "")
        int_GrandTotal = int_GrandTotal + str_subTotalValue.integerValue
        
        let str_GiftValue : NSString = NSString(string: label_GiftPriceValue.text ?? "")
        int_GrandTotal = int_GrandTotal + str_GiftValue.integerValue
        
        label_GrandTotalPriceValue.text = "\(int_GrandTotal)"
        
    }
    
    //MARK: API Calling...
    func getAllCartItemAPICalled(){
        let userDefaults = UserDefaults.standard
        let decoded : Data = userDefaults.data(forKey: "isguestMaskKeyAndCardIdFetched") ?? Data()
        var str_cartID = ""
        do {
            if let decodedData : Dictionary<String, Any> = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? Dictionary<String, Any> {
                if let crtId =  decodedData["cart_id"]{
                    str_cartID = String(format:"\(crtId)")
                }
            }
        }
        catch {
            print("Couldn't unarchive data")
        }
        
        var str_maskKey = ""
        do {
            if let decodedData : Dictionary<String, Any> = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? Dictionary<String, Any> {
                if let mskKey =  decodedData["mask_key"]{
                    str_maskKey = String(format:"\(mskKey)")
                }
            }
        }
        catch {
            print("Couldn't unarchive data")
        }
        
        var str_userID = ""
        if !loggedInUser.shared.string_userID.isEmpty && loggedInUser.shared.string_userID.count != 0{
            str_userID = String(format:"\(loggedInUser.shared.string_userID)")
            str_cartID = ""
            str_maskKey = ""
        }
        
        let parameters : Parameters = [
            "customer_id" : str_userID,
            "cart_id" : str_cartID,
            "mask_key" : str_maskKey,
            "cacheignore" : "\(Int(NSDate().timeIntervalSince1970))" //To ignore cache from api...
        ]
        
        AppDelegate.showHUD(inView: self.view, message:"Loading....")
        Alamofire.request(Constants.apiURL + "webservice/viewcart.php", method: .post, parameters: parameters)
            .responseJSON{ response in
                AppDelegate.hideHUD(inView: self.view)
                switch response.result {
                case .success:
                    if let json = response.result.value{
                        guard let _ : NSDictionary = json as? NSDictionary else {
                            return
                        }
                        do {
                            // make sure this JSON is in the format we expect
                            let jsonData = try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
                            let decoded = try JSONSerialization.jsonObject(with: jsonData, options: []) as! NSDictionary
                            //print("API Data Response: \(decoded)")
                            guard let dataArray : NSArray = decoded["data"] as? NSArray else {
                                return
                            }
                            self.array_CartItem.removeAllObjects()
                            self.dict_CompleteCartRecord.removeAllObjects()
                            
                            if dataArray.count > 0 {
                                
                                self.array_CartItem.addObjects(from: dataArray as! [Any])
                                self.setAllCartValueAfterFetchingRecords(cartRecord:self.array_CartItem, CompleteCartRecord:decoded)
                                self.dict_CompleteCartRecord.addEntries(from: decoded as! [AnyHashable : Any])
                                
                            }else {
                                
                                self.label_TotalPriceValue.text = "0"
                                self.label_GrandTotalPriceValue.text = "0"
                                self.ishideScrollViewAndSecureCheckOut(ishide: true)
                                if let tabItems = self.tabBarController?.tabBar.items {
                                    //In this case we want to modify the badge number of the first tab:
                                    let tabItem = tabItems[0]
                                    tabItem.badgeValue = "0"
                                    AppDelegate.cart_item_count = 0
                                    self.updateCartItemInNavigationBar()
                                }
                            }
                            
                            //Set tableview height according to items in cart...
                            if self.array_CartItem.count >= 3 {
                                self.constraints_CartScrollViewHeight.constant = CGFloat(self.rowHeight * 3)
                            }
                            else if self.array_CartItem.count >= 2 {
                                self.constraints_CartScrollViewHeight.constant = CGFloat(self.rowHeight * 2)
                            }
                            else if self.array_CartItem.count == 1 {
                                self.constraints_CartScrollViewHeight.constant = CGFloat(self.rowHeight)
                            }
                            else {
                                self.constraints_CartScrollViewHeight.constant = 0
                            }
                            
                            //set the height for View which have table inside
                            self.constraints_CartUIViewHeight.constant = CGFloat(self.rowHeight * self.array_CartItem.count)
                            
                            //set the height for scroll slider as per table height
                            if self.array_CartItem.count > 3 {
                                self.constraints_ScrollTableLeading.constant = 15
                                self.scrollSlider.thumbScrollView?.frame.size.height = CGFloat(self.rowHeight * self.array_CartItem.count)
                                self.scrollSlider.layoutSubviews()
                            }else {
                                self.constraints_ScrollTableLeading.constant = 0
                            }
                            
                            self.tableView_Cart.reloadData()
                        }
                        catch let error as NSError {
                            print("Failed to load: \(error.localizedDescription)")
                        }
                    }
                case .failure( _):
                    //Utility.showAlert(message: error.localizedDescription, controller: self)
                    Utility.showAlert(message: "حدث خطأ - يرجي اعادة المحاولة", controller: self) //Something went wrong, please try again.
                }
        }
    }
    
    func getCurrentStatusOfAllCartProductsAPICalled() {
        let userDefaults = UserDefaults.standard
        let decoded : Data = userDefaults.data(forKey: "isguestMaskKeyAndCardIdFetched") ?? Data()
        
        var str_cartID = ""
        do {
            if let decodedData : Dictionary<String, Any> = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? Dictionary<String, Any> {
                if let crtId =  decodedData["cart_id"]{
                    str_cartID = String(format:"\(crtId)")
                }
            }
        }
        catch {
            print("Couldn't unarchive data")
        }
        
        var str_maskKey = ""
        do {
            if let decodedData : Dictionary<String, Any> = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? Dictionary<String, Any> {
                if let mskKey =  decodedData["mask_key"]{
                    str_maskKey = String(format:"\(mskKey)")
                }
            }
        }
        catch {
            print("Couldn't unarchive data")
        }
        
        var str_userID = ""
        if !loggedInUser.shared.string_userID.isEmpty && loggedInUser.shared.string_userID.count != 0 {
            str_userID = String(format:"\(loggedInUser.shared.string_userID)")
            str_cartID = ""
            str_maskKey = ""
        }
        
        let parameters : Parameters = [
            "customer_id" : str_userID,
            "cart_id" : str_cartID,
            "mask_key" : str_maskKey,
            "cacheignore" : "\(Int(NSDate().timeIntervalSince1970))" //To ignore cache from api...
        ]
        
        AppDelegate.showHUD(inView: self.view, message:"Loading....")
        Alamofire.request(Constants.apiURL + "webservice/viewcart.php", method: .post, parameters: parameters)
            .responseJSON{ response in
                AppDelegate.hideHUD(inView: self.view)
                switch response.result {
                case .success:
                    if let json = response.result.value{
                        guard let _ : NSDictionary = json as? NSDictionary else {
                            return
                        }
                        do {
                            // make sure this JSON is in the format we expect
                            let jsonData = try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
                            let decoded = try JSONSerialization.jsonObject(with: jsonData, options: []) as! NSDictionary
                            //print("API Data Response: \(decoded)")
                            
                            guard let dataArray : NSArray = decoded["data"] as? NSArray else {
                                return
                            }
                            
                            self.array_CartItem.removeAllObjects()
                            self.dict_CompleteCartRecord.removeAllObjects()
                            
                            if dataArray.count > 0 {
                                self.array_CartItem.addObjects(from: dataArray as! [Any])
                                self.setAllCartValueAfterFetchingRecords(cartRecord:self.array_CartItem, CompleteCartRecord:decoded)
                                self.dict_CompleteCartRecord.addEntries(from: decoded as! [AnyHashable : Any])
                                
                            }else {
                                
                                self.label_TotalPriceValue.text = "0"
                                self.label_GrandTotalPriceValue.text = "0"
                                self.ishideScrollViewAndSecureCheckOut(ishide: true)
                                if let tabItems = self.tabBarController?.tabBar.items {
                                    
                                    //In this case we want to modify the badge number of the first tab:
                                    let tabItem = tabItems[0]
                                    tabItem.badgeValue = "0"
                                    AppDelegate.cart_item_count = 0
                                    self.updateCartItemInNavigationBar()
                                }  
                            }
                            self.tableView_Cart.reloadData()
                            self.constraints_CartUIViewHeight.constant = CGFloat(self.rowHeight * self.array_CartItem.count)//Set tableview height according to items in cart...
                            self.checkOutofStockItems()//checkOutofStockItems....
                            
                        }
                        catch let error as NSError {
                            print("Failed to load: \(error.localizedDescription)")
                        }
                    }
                case .failure( _):
                    //Utility.showAlert(message: error.localizedDescription, controller: self)
                    Utility.showAlert(message: "حدث خطأ - يرجي اعادة المحاولة", controller: self) //Something went wrong, please try again.
                }
        }
    }
    
    func checkOutofStockItems(){
        
        //Check is there any item, out of stock...
        for indx in 0 ..< self.array_CartItem.count{
            let dict_Product = self.array_CartItem[indx] as! NSDictionary
            if let int_inStock = (dict_Product["instock"] as? Int) {
                if (int_inStock == 0){
                    Utility.showAlert(message: "بعض المنتجات غير متوفرة بالمخزون", controller: self)
                    return
                }
            }
        }
        
        //Check if user already logged in...
        let userDefaults = UserDefaults.standard
        let decoded = userDefaults.data(forKey: "loggedInUserRecord")
        if (decoded == nil) { //User is not logged In...
            self.showAlert()
            return
        }
        else{
            // let decodedTeams = NSKeyedUnarchiver.unarchiveObject(with: decoded!)
            do {
                guard let _ : Dictionary<String, Any> = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded!) as? Dictionary<String, Any> else{//User is not logged In...
                    self.showAlert()
                    return
                }
            }
            catch {
                print("Couldn't unarchive data")
            }
        }
        
        if self.array_CartItem.count == 0 {
            Utility.showAlert(message: "Cart is empty.", controller: self)
        }else {
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "checkOutVC") as? CheckOutVC
            vc?.array_CartItem = array_CartItem.mutableCopy() as! NSMutableArray
            vc?.dict_CompleteCartRecord = dict_CompleteCartRecord.mutableCopy() as! NSMutableDictionary
           
            if image_gift.image == UIImage.init(named: "yellow-tick") {
                vc?.is_GiftApplied = "1"
            }
            self.navigationController?.pushViewController(vc!, animated: false)
        }
    }
    
    func setAllCartValueAfterFetchingRecords(cartRecord:NSArray, CompleteCartRecord:NSDictionary){
        
        if let intValue = (CompleteCartRecord["subtotal"] as? Int) {
            self.label_TotalPriceValue.text = "\(intValue)"
        }
        
        self.calculateGrandTotal()
        
        self.ishideScrollViewAndSecureCheckOut(ishide: false)
        
        if let tabItems = self.tabBarController?.tabBar.items {
            
            var total_ItemInCart = 0
            for indx in 0 ..< self.array_CartItem.count{
                let dict_Product = self.array_CartItem[indx] as! NSDictionary
                if let intValue = (dict_Product["qty"] as? Int) {
                    total_ItemInCart = total_ItemInCart + intValue
                }
            }
            
            let tabItem = tabItems[0]
            tabItem.badgeValue = "\(total_ItemInCart)"
            AppDelegate.cart_item_count = total_ItemInCart
            self.updateCartItemInNavigationBar()
            
        }
        
        
        
        
    }
    
    func updateItemFromCartApiCalled(parameters: Parameters){
        AppDelegate.showHUD(inView: self.view, message:"Loading....")
        Alamofire.request(Constants.apiURL + "webservice/updatecart.php", method: .post, parameters: parameters)
            .responseJSON { response in
                AppDelegate.hideHUD(inView: self.view)
                switch response.result {
                case .success:
                    if let json = response.result.value{
                        guard let _ : NSDictionary = json as? NSDictionary else {
                            return
                        }
                        do {
                            //make sure this JSON is in the format we expect
                            let jsonData =  try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
                            let decoded = try JSONSerialization.jsonObject(with: jsonData, options: []) as! NSDictionary
                            if decoded["msg"] as? String == "success" {
                                self.getAllCartItemAPICalled()
                            }
                            else {
                                Utility.showAlert(message: decoded["data"] as? String ?? "", controller: self)
                            }
                        }
                        catch let error as NSError {
                            print("Failed to load: \(error.localizedDescription)")
                        }
                    }
                case .failure( _):
                    //Utility.showAlert(message: error.localizedDescription, controller: self)
                    Utility.showAlert(message: "حدث خطأ - يرجي اعادة المحاولة", controller: self) //Something went wrong, please try again.
                }
        }
    }
    
    func deleteItemFromCartApiCalled(parameters: Parameters){
        AppDelegate.showHUD(inView: self.view, message:"Loading....")
        Alamofire.request(Constants.apiURL + "webservice/prodel.php", method: .post, parameters: parameters)
            .responseJSON { response in
                AppDelegate.hideHUD(inView: self.view)
                switch response.result {
                case .success:
                    if let json = response.result.value{
                        guard let _ : NSDictionary = json as? NSDictionary else {
                            return
                        }
                        do {
                            // make sure this JSON is in the format we expect
                            let jsonData =  try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
                            let decoded = try JSONSerialization.jsonObject(with: jsonData, options: []) as! NSDictionary
                            if decoded["msg"] as? String == "success" {
                                self.getAllCartItemAPICalled()
                            }
                            else {
                                iToast.show("حدث خطأ - يرجي اعادة المحاولة")//An error has occurred - please try again
                            }
                        }
                        catch let error as NSError {
                            print("Failed to load: \(error.localizedDescription)")
                        }
                    }
                case .failure( _):
                    //Utility.showAlert(message: error.localizedDescription, controller: self)
                    Utility.showAlert(message: "حدث خطأ - يرجي اعادة المحاولة", controller: self) //Something went wrong, please try again.
                }
        }
    }
    
    
    //MARK: - UITableView DataSource
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(rowHeight)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return array_CartItem.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cartTableViewCell", for: indexPath) as! CartTableViewCell
        cell.delegate = self
        let dict_Product = array_CartItem[indexPath.row] as! NSDictionary
        
        if let inStock : Int = dict_Product["instock"] as? Int{
            if (inStock == 1) {//this means item is available in stock...
                cell.stackView_increamentOrDecreament.isHidden = false
                cell.view_OutOfStockIndicator.isHidden = true
                cell.label_OutOfStockMessage.isHidden = true
            }
            else {
                cell.stackView_increamentOrDecreament.isHidden = true
                cell.view_OutOfStockIndicator.isHidden = false
                cell.label_OutOfStockMessage.isHidden = false
            }
        }
        
        cell.label_Title.text = dict_Product["name"] as? String
        cell.imageview_Banner.sd_imageIndicator = SDWebImageActivityIndicator.gray
        cell.imageview_Banner.sd_imageIndicator?.startAnimatingIndicator()
        let url = NSURL(string: dict_Product["img"] as? String ?? "")
        cell.imageview_Banner.sd_setImage(with: url as URL?) { (image, error, cache, urls) in
            if (error != nil) {// Failed to load image
            }
            else {// Successful in loading image
                cell.imageview_Banner.sd_imageIndicator?.stopAnimatingIndicator()
            }
        }
        
        
        if let intValue : Int = (dict_Product["old_price"] as? Int) {
                       
                       if(intValue == 0){
                           cell.label_OldPrice.isHidden = true
                       }
                       cell.label_OldPrice.text = String(format: "SAR %d", intValue)
                       
                   }else if let strValue = (dict_Product["old_price"] as? String){
                       
                       if((strValue.replacingOccurrences(of: ",", with: "")) == "0"){
                           cell.label_OldPrice.isHidden = true
                       }
                       cell.label_OldPrice.text = String(format: "SAR %d", (strValue.replacingOccurrences(of: ",", with: "") as NSString).integerValue)
                       
                   }else{
                       print("some other data types exists.")
                   }
                   
                   
        
        if let intValue : Int = (dict_Product["price"] as? Int) {
            cell.label_NewPrice.text = String(format: "SAR %d", intValue)
        }
        else if let strValue = (dict_Product["price"] as? String) {
            cell.label_NewPrice.text = String(format: "SAR %d", (strValue.replacingOccurrences(of: ",", with: "") as NSString).integerValue)
        }
        
        cell.label_Size.text = "\(self.getSizeofProduct(arraySizeAndColor: dict_Product["option"] as! NSArray))"
        
        if let intValue = (dict_Product["qty"] as? Int) {
            cell.label_Quantity.text = String(format: "%d", intValue)
        }
        else if let strValue = (dict_Product["qty"] as? String) {
            cell.label_Quantity.text = String(format: "%d", (strValue as NSString).integerValue)
        }
        
        cell.button_Delete.accessibilityLabel = String(format: "%d", indexPath.row)
        cell.button_Decrement.accessibilityLabel = String(format: "%d", indexPath.row)
        cell.button_Incremnent.accessibilityLabel = String(format: "%d", indexPath.row)
        
        cell.layoutIfNeeded()
        cell.view_backgroundForAllDataHolder.setShadow(shadowSize: 10.0, radius: 5)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "productDetailVC") as? ProductDetailVC
        let dict_Product = array_CartItem[indexPath.row] as! NSDictionary
        vc?.dict_Product = dict_Product["product_data"] as! NSDictionary
        self.navigationController?.pushViewController(vc!, animated: false)
    }
    
    func getSizeofProduct(arraySizeAndColor: NSArray) -> String {
        var str_finalSize = ""
        
        if arraySizeAndColor.count > 0 {
            let dict_Obj0 = arraySizeAndColor[0] as! NSDictionary
            
            // let dict_Obj0 = dict_Obj12[1] as! NSDictionary
            
            let dict_Size = dict_Obj0["size"] as! NSDictionary
            
            if let intValue = (dict_Size["label"] as? Int) {
                str_finalSize = String(format: "%d", intValue)
            }
            else if let strValue = (dict_Size["label"] as? String) {
                str_finalSize = String(format: "%@", strValue)
            }
            // str_finalSize = dict_Size["label"] as? String ?? ""
        }
        return str_finalSize
    }
    
    func buttonDeleteClicked(sender: UIButton) {
        self.deleteMessageWithOptions(sender: sender)
    }
    
    func deleteMessageWithOptions(sender: UIButton){
        let alertController = UIAlertController(title: Constants.msg_Alert, message: "هل أنت متأكد من إلغاء هذا العنصر من سلة التسوق؟", preferredStyle: .alert)//Do you really want to delete?"
        alertController.addAction(UIAlertAction(title: "إلغاء", style: .destructive, handler: { action in //Cancel
            switch action.style{
            case .cancel:
                print("cancel")
            case .destructive:
                print("destructive")
            case .default:
                print("default")
            @unknown default:
                print("unknown default")
            }}))
        
        alertController.addAction(UIAlertAction(title: "موافق", style: .default, handler: { action in //Agree
            switch action.style{
            case .cancel:
                print("cancel")
            case .destructive:
                print("destructive")
            case .default:
                print("default")
                let row : Int = Int((sender.accessibilityLabel)!) ?? 0
                let array_Dict : Dictionary<String, Any> = self.array_CartItem[row] as! Dictionary<String, Any>
                let userDefaults = UserDefaults.standard
                let decoded : Data = userDefaults.data(forKey: "isguestMaskKeyAndCardIdFetched") ?? Data()
                var str_cartID = ""
                do {
                    if let decodedData : Dictionary<String, Any> = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? Dictionary<String, Any> {
                        if let crtId =  decodedData["cart_id"]{
                            str_cartID = String(format:"\(crtId)")
                        }
                    }
                }
                catch {
                    print("Couldn't unarchive data")
                }
                
                var str_maskKey = ""
                do {
                    /// let decodedTeams : Dictionary<String, Any> = NSKeyedUnarchiver.unarchiveObject(with: decoded!) as! Dictionary<String, Any>
                    if let decodedData : Dictionary<String, Any> = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? Dictionary<String, Any> {
                        if let mskKey =  decodedData["mask_key"]{
                            str_maskKey = String(format:"\(mskKey)")
                        }
                    }
                }
                catch {
                    print("Couldn't unarchive data")
                }
                
                var str_userID = ""
                if !loggedInUser.shared.string_userID.isEmpty && loggedInUser.shared.string_userID.count != 0{
                    
                    str_userID = String(format:"\(loggedInUser.shared.string_userID)")
                    str_cartID = ""
                    str_maskKey = ""
                }
                
                var itmId = ""
                if let intValue = (array_Dict["id"] as? Int) {
                    itmId = String(format: "%d", intValue)
                }
                else if let strValue = (array_Dict["id"] as? String){
                    itmId = String(format: "%@", strValue)
                }
                
                let params : Parameters = [
                    "customer_id" : str_userID,
                    "cart_id" : str_cartID,
                    "mask_key" : str_maskKey,
                    "item_id" : itmId,
                    "cacheignore" : "\(Int(NSDate().timeIntervalSince1970))" //To ignore cache from api...
                ]
                
                self.deleteItemFromCartApiCalled(parameters: params)
                
            @unknown default:
                print("unknown default")
            }}))
        self.present(alertController, animated: true, completion: nil)
    }
    
    func buttonDecreaseQuantityClicked(sender: UIButton) {
        let row : Int = Int((sender.accessibilityLabel)!) ?? 0
        let dict_Product : Dictionary<String, Any> = self.array_CartItem[row] as! Dictionary<String, Any>
        var quantity = 0
        if let intValue = (dict_Product["qty"] as? Int) {
            quantity = intValue
        }
        else if let strValue = (dict_Product["qty"] as? String) {
            quantity = (strValue as NSString).integerValue
        }
        
        let userDefaults = UserDefaults.standard
        let decoded : Data = userDefaults.data(forKey: "isguestMaskKeyAndCardIdFetched") ?? Data()
        
        var str_cartID = ""
        do {
            
            if let decodedData : Dictionary<String, Any> = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? Dictionary<String, Any> {
                if let crtId =  decodedData["cart_id"]{
                    str_cartID = String(format:"\(crtId)")
                }
            }
        }
        catch {
            print("Couldn't unarchive data")
        }
        
        var str_maskKey = ""
        do {
            /// let decodedTeams : Dictionary<String, Any> = NSKeyedUnarchiver.unarchiveObject(with: decoded!) as! Dictionary<String, Any>
            if let decodedData : Dictionary<String, Any> = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? Dictionary<String, Any> {
                if let mskKey =  decodedData["mask_key"]{
                    str_maskKey = String(format:"\(mskKey)")
                }
            }
        }
        catch {
            print("Couldn't unarchive data")
        }
        
        var str_userID = ""
        if !loggedInUser.shared.string_userID.isEmpty && loggedInUser.shared.string_userID.count != 0{
            str_userID = String(format:"\(loggedInUser.shared.string_userID)")
            str_cartID = ""
            str_maskKey = ""
        }
        
        var itmId = ""
        if let intValue = (dict_Product["id"] as? Int) {
            itmId = String(format: "%d", intValue)
        }
        else if let strValue = (dict_Product["id"] as? String) {
            itmId = String(format: "%@", strValue)
        }
        
        var skuId = ""
        if let intValue = (dict_Product["sku"] as? Int) {
            skuId = String(format: "%d", intValue)
        }
        else if let strValue = (dict_Product["sku"] as? String) {
            skuId = String(format: "%@", strValue)
        }
        
        if quantity == 1{
            
            let params : Parameters = [
                "customer_id" : str_userID,
                "cart_id" : str_cartID,
                "mask_key" : str_maskKey,
                "item_id" : itmId,
            ]
            
            self.deleteItemFromCartApiCalled(parameters: params)
        }
        else{
            quantity -= 1 //Need quantity decrease by 1, because on every click we are decreasing quantity.
            let params : Parameters = [
                "customer_id" : str_userID,
                "cart_id" : str_cartID,
                "mask_key" : str_maskKey,
                "item_id" : itmId,
                "sku" : skuId,
                "qty" : quantity,
                "cacheignore" : "\(Int(NSDate().timeIntervalSince1970))" //To ignore cache from api...
            ]
            self.updateItemFromCartApiCalled(parameters: params)
        }
    }
    
    func buttonIncreaseQuantityClicked(sender: UIButton) {
        let row : Int = Int((sender.accessibilityLabel)!) ?? 0
        let dict_Product : Dictionary<String, Any> = self.array_CartItem[row] as! Dictionary<String, Any>
        var quantity = 0
        if let intValue = (dict_Product["qty"] as? Int) {
            quantity = intValue
        }
        else if let strValue = (dict_Product["qty"] as? String) {
            quantity = (strValue as NSString).integerValue
        }
        
        let userDefaults = UserDefaults.standard
        let decoded : Data = userDefaults.data(forKey: "isguestMaskKeyAndCardIdFetched") ?? Data()
        
        var str_cartID = ""
        do {
            if let decodedData : Dictionary<String, Any> = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? Dictionary<String, Any> {
                if let crtId =  decodedData["cart_id"]{
                    str_cartID = String(format:"\(crtId)")
                }
            }
        }
        catch {
            print("Couldn't unarchive data")
        }
        
        var str_maskKey = ""
        do {
            if let decodedData : Dictionary<String, Any> = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? Dictionary<String, Any> {
                if let mskKey =  decodedData["mask_key"]{
                    str_maskKey = String(format:"\(mskKey)")
                }
            }
        }
        catch {
            print("Couldn't unarchive data")
        }
        
        var str_userID = ""
        if !loggedInUser.shared.string_userID.isEmpty && loggedInUser.shared.string_userID.count != 0{
            str_userID = String(format:"\(loggedInUser.shared.string_userID)")
            str_cartID = ""
            str_maskKey = ""
        }
        
        var itmId = ""
        if let intValue = (dict_Product["id"] as? Int) {
            itmId = String(format: "%d", intValue)
        }
        else if let strValue = (dict_Product["id"] as? String){
            itmId = String(format: "%@", strValue)
        }
        
        var skuId = ""
        if let intValue = (dict_Product["sku"] as? Int) {
            skuId = String(format: "%d", intValue)
        }
        else if let strValue = (dict_Product["sku"] as? String){
            skuId = String(format: "%@", strValue)
        }
        
        quantity += 1
        let params : Parameters = [
            "customer_id" : str_userID,
            "cart_id" : str_cartID,
            "mask_key" : str_maskKey,
            "item_id" : itmId,
            "sku" : skuId,
            "qty" : quantity,
            "cacheignore" : "\(Int(NSDate().timeIntervalSince1970))" //To ignore cache from api...
        ]
        self.updateItemFromCartApiCalled(parameters: params)
    }
    
    //MARK: - UIButton Clicks
    @IBAction func buttonSearchClicked(_ sender: UIButton) {
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "searchVC") as! SearchVC
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    @IBAction func button_secureCheckOutClicked(_ sender: UIButton) {
        
        if ("\(loggedInUser.shared.string_firstName)".isEmpty || "\(loggedInUser.shared.string_Email)".isEmpty){//Need to check firtname and email is empty in local stoarge. Because this is using frequently using in checkout page...
            
            self.showAlert()
            
            let userDefaults_loggedInUser = UserDefaults.standard
            userDefaults_loggedInUser.removeObject(forKey:"loggedInUserRecord")
            userDefaults_loggedInUser.synchronize()
            
            let userDefaults_DeliveryAddress = UserDefaults.standard
            userDefaults_DeliveryAddress.removeObject(forKey:"userDefaultDeliveryAddress")
            userDefaults_DeliveryAddress.synchronize()
            
            loggedInUser.shared.deinitializeAllUserDetails()
            return
        }
        
        self.getCurrentStatusOfAllCartProductsAPICalled()
        
    }
    
    @IBAction func button_giftCheckBoxClicked(_ sender: UIButton) {
        
        self.setGiftCardValue()
        
    }
    
    @IBAction func button_viewGiftBoxClicked(_ sender: UIButton) {
        let modalViewController = GiftBoxDialogViewController()
        modalViewController.modalPresentationStyle = .overFullScreen // .overCurrentContext
//        modalViewController.modalTransitionStyle =  UIModalTransitionStyle.flipHorizontal
        modalViewController.delegate = self
        self.present(modalViewController, animated: false)
    }
   
    func showAlert(){
        
        let alertController = UIAlertController(title: Constants.msg_Alert, message: "يجب تسجيل الدخول أولا", preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "حسنا", style: .default, handler: { action in
            switch action.style{
            case .default:
                print("default")
                Constants.isComingFromCartScreen = true
                self.selectTabBar(index: 1)
            case .cancel:
                print("cancel")
            case .destructive:
                print("destructive")
            @unknown default: break
            }}))
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    func setGiftCardValue(){
        
        if image_gift.image == UIImage.init(named: "yellow-tick") {
            self.removeGiftCardValue()
        }else {
            image_gift.image = UIImage.init(named: "yellow-tick")
            if let strValue = (self.dict_CompleteCartRecord["gift_wrap_fee"] as? String){
                label_GiftPriceValue.text = strValue
            }
        }
        
        self.calculateGrandTotal()
        
    }
    
    func removeGiftCardValue(){
        
        image_gift.image = UIImage.init(named: "gray-tick")
        label_GiftPriceValue.text = "0"
        self.calculateGrandTotal()
        
    }
    
}
