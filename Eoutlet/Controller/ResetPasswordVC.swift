import Alamofire

class ResetPasswordVC: UIViewController{
    
    var dict_verifyOTPAPIResponse : NSDictionary = NSDictionary()
    
    @IBOutlet weak var textField_Password: UITextField!
    @IBOutlet weak var textField_confirmPassword: UITextField!
    
    var array_iconsName : NSMutableArray = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addNavigationControllerActivities()
    }
    
    override func viewWillAppear(_ _animated: Bool) {
        super.viewWillAppear(_animated)
    }
    
    //MARK: Navigation Settings
    func addNavigationControllerActivities(){
        
        
    }
        
    //MARK: UIButton Clicks
    @IBAction func button_resetMyPasswordClicked(_ sender: UIButton) {
                
        guard let str_Password = textField_Password.text?.trimmingCharacters(in: .whitespacesAndNewlines) else{
            return
        }
        
        if str_Password.count < 7 {
            
            Utility.showAlert(message:"كلمة السر يجب أن لا تقل عن 7 أرقام", controller: self)
            return
            
        }
        
        guard let str_ConfirmPassword = textField_confirmPassword.text?.trimmingCharacters(in: .whitespacesAndNewlines) else{return
        }
        if str_ConfirmPassword == ""{
            
            Utility.showAlert(message:"برجاء ادخال كود التأكيد", controller: self)
            return
            
        }
        
        if str_Password != str_ConfirmPassword{
            
            Utility.showAlert(message:"كلمة المرور لا تتطابق مع الكلمة الاولى", controller: self)
            return
            
        }
        
        let parameters : Parameters = [
            
            "customerId" : dict_verifyOTPAPIResponse["customerId"] ?? "",
            "url" : dict_verifyOTPAPIResponse["url"] ?? "",
            "password" : str_Password,
            "c_password" : str_ConfirmPassword,
            "cacheignore" : "\(Int(NSDate().timeIntervalSince1970))" //To ignore cache from api...
            
        ]
        
        AppDelegate.showHUD(inView: self.view, message:"Loading....")
        
        Alamofire.request(Constants.apiURL + "webservice/resetpassword.php", method: .post, parameters: parameters)
            
            .responseJSON { response in
                
                AppDelegate.hideHUD(inView: self.view)
                
                switch response.result {
                case .success:
                    
                    if let json = response.result.value{
                        
                        print("JSON Response : \(json)")
                        
                        guard let _ : NSDictionary = json as? NSDictionary else {
                            return
                        }
                        do {
                            
                            let jsonData =  try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
                            let decoded = try JSONSerialization.jsonObject(with: jsonData, options: []) as! NSDictionary
                            if decoded["msg"] as? String == "success"{
                                
                                iToast.show("تم استعادة كلمة السر بنجاح")//Password reset successfully.
                                self.navigationController?.popToRootViewController(animated: false)
                                
                            }else{
                                iToast.show(decoded["msg"] as? String)
                            }
                            
                        } catch _ as NSError {
                            
                        }
                        
                    }
                case .failure( _):
                        //Utility.showAlert(message: error.localizedDescription, controller: self)
                        Utility.showAlert(message: "حدث خطأ - يرجي اعادة المحاولة", controller: self) //Something went wrong, please try again.
                    }
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
        //Dispose of any resources that can be recreated.
        
    }
    
}

