
import UIKit

class HelpAndSupportTableViewCell : UITableViewCell {
    @IBOutlet weak var label_Title: UILabel!
}

class HelpAndSupportVC: UIViewController {
    @IBOutlet weak var helpAndSupportTableView: UITableView!
    var titleArray = ["الشحن والتوصيل", "الأسئلة الشائعة", "سياسة الاستبدال والاسترجاع","تواصل معنا" ] //index-->0(Shipping and delivery),index-->1(common questions),index-->2(replace and return policy)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        helpAndSupportTableView.tableFooterView = UIView()
        helpAndSupportTableView.separatorInset = UIEdgeInsets.zero
    }
    
     override func viewWillAppear(_ _animated: Bool) {
             super.viewWillAppear(_animated)
             self.navigationController?.navigationBar.isHidden = true
         }
         
      //MARK: - UIButton Clicks
         @IBAction func backButtonClicked(_ sender: UIButton) {
             self.navigationController?.popViewController(animated: false)
         }
}

extension HelpAndSupportVC : UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return titleArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "helpAndSupportTableViewCell", for: indexPath) as! HelpAndSupportTableViewCell
        cell.label_Title?.text = titleArray[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if(indexPath.row == 3){
            
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "contactUsVC") as! ContactUsVC
            self.navigationController?.pushViewController(vc, animated: false)
            
        }else{
            
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "webViewController") as! WebViewController
            vc.titleStr = titleArray[indexPath.row]
            vc.selectedIndex = indexPath.row
            self.navigationController?.pushViewController(vc, animated: false)
        }
          
    }
}
