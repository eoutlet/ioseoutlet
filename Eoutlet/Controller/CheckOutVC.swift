
import Alamofire
import AppsFlyerLib
import FirebaseAnalytics

class CheckOutVC: UIViewController, deliveryAddressChangeDelegate {
    @IBOutlet weak var view_ShippingAddress: UIView!
    @IBOutlet weak var view_addAddressButton: UIView!
    @IBOutlet weak var view_creditCardAndCOD: UIView!
    @IBOutlet weak var scrollview_Base: UIScrollView!
    @IBOutlet weak var constraint_heightDeliveryAddressView: NSLayoutConstraint!
    @IBOutlet weak var constraint_heightPhoneView: NSLayoutConstraint!
    @IBOutlet weak var couponView: UIView!
    @IBOutlet weak var label_Name: UILabel!
    @IBOutlet weak var label_Street: UILabel!
    @IBOutlet weak var label_City: UILabel!
    @IBOutlet weak var label_Country: UILabel!
    @IBOutlet weak var label_mobileNumber: UILabel!

    
    @IBOutlet weak var label_SubTotalPriceValue: UILabel!
    @IBOutlet weak var label_GiftCardValue: UILabel!
    
    @IBOutlet weak var label_ShippingSoudiArabiaValue: UILabel!
    @IBOutlet weak var label_ShippingSoudiArabiaText: UILabel!
    @IBOutlet weak var label_GrandTotalPriceValue: UILabel!
    @IBOutlet weak var textField_CoupanCode: UITextField!
    @IBOutlet weak var plusMinusLbl: UILabel!
    @IBOutlet weak var couponButtonUIView: UIView!
    @IBOutlet weak var label_CoupanDiscountValue: UILabel!
    @IBOutlet weak var label_CoupanDiscountText: UILabel!
    
    var is_deliveryAddressAdded = false
    
    var iscouponCodeApplied = false
    
    var is_GiftApplied = "0" // set default gift not apply
    
    var array_CartItem : NSMutableArray = NSMutableArray()
    var dict_CompleteCartRecord : NSMutableDictionary = NSMutableDictionary()
    var shippingMethod = ""
    var total_Amount = "0"  // set default value amount
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        plusMinusLbl.text = "+"
        
        //Set All bottom label price,tax, grandtotal...
        self.setAllBottomLabelsValues()
        
        self.couponView.isHidden = true
        self.plusMinusLbl.text = "+"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.isHidden = true
        
        //Check if user is logged out...
        let userDefaults = UserDefaults.standard
        let decoded = userDefaults.data(forKey: "loggedInUserRecord")
        if (decoded == nil) {
            self.navigationController?.popToRootViewController(animated: false)
        }
        
        self.setDeliveryAddress()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.view_ShippingAddress.setShadow(shadowSize: 10.0, radius: 10.0)
        self.view_addAddressButton.setShadow(shadowSize: 10.0, radius: 15.0)
        self.couponButtonUIView.setShadow(shadowSize: 10.0, radius: 15.0)
        self.label_GrandTotalPriceValue.setShadow(shadowSize: 10.0, radius: 10.0)
    }
    
    //MARK: - UIButton Clicks
    @IBAction func backButtonClicked(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: false)
    }
    
    func setAllBottomLabelsValues(){
        
        if is_GiftApplied == "1" {  
            if let strValue = (self.dict_CompleteCartRecord["gift_wrap_fee"] as? String){
                label_GiftCardValue.text = strValue
            }
        }
        
        if let intValue = (self.dict_CompleteCartRecord["subtotal"] as? Int) {
            self.label_SubTotalPriceValue.text = "\(intValue)"
        }
        
        self.calculateGrandTotal()
    }
    
    func setDeliveryAddress(){
        
        self.is_deliveryAddressAdded = false
        constraint_heightDeliveryAddressView.constant = 0
        constraint_heightPhoneView.constant = 0
        let userDefaults = UserDefaults.standard
        let decoded = userDefaults.data(forKey: "userDefaultDeliveryAddress")
        if (decoded != nil) {
            
            if let dict_Address : NSDictionary = NSKeyedUnarchiver.unarchiveObject(with: decoded!) as? NSDictionary {
                
                self.is_deliveryAddressAdded = true
                constraint_heightDeliveryAddressView.constant = 115
                constraint_heightPhoneView.constant = 30

                label_Name.text = "\(dict_Address["firstname"] as! String) \(dict_Address["lastname"] as! String)"
                label_Name.accessibilityLabel = "\(dict_Address["firstname"] as! String)"
                label_Name.accessibilityHint = "\(dict_Address["lastname"] as! String)"
                label_Street.text = dict_Address["street"] as? String
                label_City.text = dict_Address["city"] as? String
                label_Country.text = dict_Address["country"] as? String
                label_Country.accessibilityLabel = dict_Address["country_id"] as? String
                label_mobileNumber.text = dict_Address["telephone"] as? String
                
                self.getCountryShippningTaxInfo(countryID: dict_Address["country_id"] as? String ?? "")
                
            }else {
                self.is_deliveryAddressAdded = false
                constraint_heightDeliveryAddressView.constant = 0
                constraint_heightPhoneView.constant = 0
                
                label_Name.text = ""
                label_Name.accessibilityLabel = ""
                label_Name.accessibilityHint = ""
                label_Street.text = ""
                label_City.text = ""
                label_Country.text = ""
                label_Country.accessibilityLabel = ""
                label_mobileNumber.text = ""
                
                self.getCountryShippningTaxInfo(countryID: "")
            }
        }
        else {
            label_Name.text = ""
            label_Name.accessibilityLabel = ""
            label_Name.accessibilityHint = ""
            label_Street.text = ""
            label_City.text = ""
            label_Country.text = ""
            label_Country.accessibilityLabel = ""
            label_mobileNumber.text = ""
            
            self.getCountryShippningTaxInfo(countryID: "")
        }
    }
    
    //MARK: Calculate Grand Total
    func calculateGrandTotal(){
        
        var int_GrandTotal = 0
        
        if let intValue = (self.dict_CompleteCartRecord["subtotal"] as? Int) {
            int_GrandTotal = int_GrandTotal + intValue}
        let str_giftCardValue : NSString = NSString(string: label_GiftCardValue.text ?? "")
        int_GrandTotal = int_GrandTotal + str_giftCardValue.integerValue
        
        let str_coupanDiscountValue : NSString = NSString(string: label_CoupanDiscountValue.text ?? "")
        int_GrandTotal = int_GrandTotal - str_coupanDiscountValue.integerValue
        
        let str_saudiArabiaValue : NSString = NSString(string: label_ShippingSoudiArabiaValue.text ?? "")
        int_GrandTotal = int_GrandTotal + str_saudiArabiaValue.integerValue
        
        total_Amount = "\(int_GrandTotal)"
        label_GrandTotalPriceValue.text = "SAR \(int_GrandTotal) الاجمالي الكلي"
        
    }
    
    //MARK: API Calling...
    func getCountryShippningTaxInfo(countryID : String){
        var str_userID = ""
        
        if !loggedInUser.shared.string_userID.isEmpty && loggedInUser.shared.string_userID.count != 0 {
            str_userID = String(format:"\(loggedInUser.shared.string_userID)")
        }
        
        let parameters : Parameters = [
            "customer_id" : str_userID,
            "country_id" : countryID,
            "cacheignore" : "\(Int(NSDate().timeIntervalSince1970))" //To ignore cache from api...
        ]
        
        AppDelegate.showHUD(inView: self.view, message:"Loading....")
        Alamofire.request(Constants.apiURL + "webservice/getshipping-estimation.php", method: .get, parameters: parameters)
            .responseJSON { response in
                AppDelegate.hideHUD(inView: self.view)
                switch response.result {
                case .success:
                    if let json = response.result.value {
                        guard let _ : NSDictionary = json as? NSDictionary else {
                            return
                        }
                        do {
                            // make sure this JSON is in the format we expect
                            let jsonData =  try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
                            let decoded = try JSONSerialization.jsonObject(with: jsonData, options: []) as! NSDictionary
                            
                            print("API Data Response: \(decoded)")
                            
                            if decoded["msg"] as? String == "success"{
                                self.setCountryShippingCharge(shippingRecord:decoded)
                                self.shippingMethod = decoded["shipping_method"] as? String ?? ""
                            }else {
                                self.label_ShippingSoudiArabiaValue.text = "0"
                                self.label_ShippingSoudiArabiaValue.accessibilityLabel = "0" //AccessibilityLabel is shipping_tax here...
                                self.label_ShippingSoudiArabiaText.text = "الشحن"
                                self.shippingMethod = ""
                                self.calculateGrandTotal()
                            }
                        }
                        catch let error as NSError {
                            print("Failed to load: \(error.localizedDescription)")
                        }
                    }
                case .failure( _):
                    Utility.showAlert(message: "حدث خطأ - يرجي اعادة المحاولة", controller: self) //Something went wrong, please try again.
                }
        }
    }
    
    func setCountryShippingCharge(shippingRecord:NSDictionary){
        
        if let intValue = (shippingRecord["shipping"] as? Int) {
            label_ShippingSoudiArabiaValue.text = "\(intValue)"
        }
        else if let strValue = (shippingRecord["shipping"] as? String) {
            label_ShippingSoudiArabiaValue.text = "\(strValue)"
        }
        
        if let intValue = (shippingRecord["shipping_tax"] as? Int) {
            label_ShippingSoudiArabiaValue.accessibilityLabel = "\(intValue)" //AccessibilityLabel is shipping_tax here...
        }
        else if let strValue = (shippingRecord["shipping_tax"] as? String) {
            label_ShippingSoudiArabiaValue.accessibilityLabel = "\(strValue)" //AccessibilityLabel is shipping_tax here...
        }
        
        label_ShippingSoudiArabiaText.text = shippingRecord["shipping_title"] as? String
        
        self.calculateGrandTotal()
        
    }
    
    func applyCoupanCodeAPICalled(coupanCode:NSString){
        
        iscouponCodeApplied = false
        if(textField_CoupanCode.isFirstResponder){textField_CoupanCode.resignFirstResponder()}
        
        let userDefaults = UserDefaults.standard
        let decoded : Data = userDefaults.data(forKey: "isguestMaskKeyAndCardIdFetched") ?? Data()
        var str_cartID = ""
        do {
            if let decodedData : Dictionary<String, Any> = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? Dictionary<String, Any> {
                if let crtId =  decodedData["cart_id"]{
                    str_cartID = String(format:"\(crtId)")
                }
            }
        }
        catch {
            print("Couldn't unarchive data")
        }
        
        var str_maskKey = ""
        do {
            if let decodedData : Dictionary<String, Any> = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? Dictionary<String, Any> {
                if let mskKey =  decodedData["mask_key"]{
                    str_maskKey = String(format:"\(mskKey)")
                }
            }
        }
        catch {
            print("Couldn't unarchive data")
        }
        
        
        let saudiArabiaShippingCharge : NSString =  NSString(string: label_ShippingSoudiArabiaValue.text ?? "")
        
        let parameters : Parameters = [
            "customer_id" : loggedInUser.shared.string_userID,
            "coupon" : coupanCode,
            "cart_id" : str_cartID,
            "mask_key" : str_maskKey,
            "subtotal" : "\(self.dict_CompleteCartRecord["subtotal"] as? Int ?? 0)",
            "shippingcharge" : "\(saudiArabiaShippingCharge)",
            "cacheignore" : "\(Int(NSDate().timeIntervalSince1970))" //To ignore cache from api...
        ]
        
        AppDelegate.showHUD(inView: self.view, message:"Loading....")
        Alamofire.request(Constants.apiURL + "webservice/getcoupon.php", method: .get, parameters: parameters)
            .responseJSON { response in
                AppDelegate.hideHUD(inView: self.view)
                switch response.result {
                case .success:
                    if let json = response.result.value{
                        guard let _ : NSDictionary = json as? NSDictionary else {
                            return
                        }
                        do {
                            // make sure this JSON is in the format we expect
                            let jsonData =  try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
                            let decoded = try JSONSerialization.jsonObject(with: jsonData, options: []) as! NSDictionary
                            print("API Data Response: \(decoded)")
                            
                            if decoded["msg"] as? String == "success"{
                                self.setCoupanCodeValue(coupanCodeDict:decoded, coupanCode:coupanCode as String)
                            }
                            else{
                                iToast.show(decoded["message"] as? String)//An error has occurred - please try again
                            }
                        }
                        catch let error as NSError {
                            print("Failed to load: \(error.localizedDescription)")
                        }
                    }
                case .failure( _):
                    Utility.showAlert(message: "حدث خطأ - يرجي اعادة المحاولة", controller: self) //Something went wrong, please try again.
                }
        }
    }
        
    func getSizeofProduct(arraySizeAndColor: NSArray) -> String {
        var str_finalSize = ""
        
        if arraySizeAndColor.count > 0 {
            
            let dict_Obj0 = arraySizeAndColor[0] as! NSDictionary
            
            let dict_Size = dict_Obj0["size"] as! NSDictionary
            if let intValue = (dict_Size["label"] as? Int) {
                str_finalSize = String(format: "%d", intValue)
            }
            else if let strValue = (dict_Size["label"] as? String){
                str_finalSize = String(format: "%@", strValue)
            }
            // str_finalSize = dict_Size["label"] as? String ?? ""
        }
        return str_finalSize
    }
    
    //MARK: Delivery Address Change Successfully Delegate
    func deliveryAddressChangeSuccessfully(){

        let userDefaults = UserDefaults.standard
        let decoded = userDefaults.data(forKey: "userDefaultDeliveryAddress")
        if (decoded != nil) {
            
            if let dict_Address : NSDictionary = NSKeyedUnarchiver.unarchiveObject(with: decoded!) as? NSDictionary {
                self.is_deliveryAddressAdded = true
                
                constraint_heightDeliveryAddressView.constant = 115
                constraint_heightPhoneView.constant = 30

                label_Name.text = "\(dict_Address["firstname"] as! String) \(dict_Address["lastname"] as! String)"
                label_Name.accessibilityLabel = "\(dict_Address["firstname"] as! String)"
                label_Name.accessibilityHint = "\(dict_Address["lastname"] as! String)"
                label_Street.text = dict_Address["street"] as? String
                label_City.text = dict_Address["city"] as? String
                label_Country.text = dict_Address["country"] as? String
                label_Country.accessibilityLabel = dict_Address["country_id"] as? String
                label_mobileNumber.text = dict_Address["telephone"] as? String
                
                self.getCountryShippningTaxInfo(countryID: dict_Address["country_id"] as? String ?? "")
            }
        }
    }
    
    //MARK: UIButton Clicks
    @IBAction func button_changeAddressClicked(_ sender: UIButton) {
        textField_CoupanCode.resignFirstResponder()

        self.couponView.isHidden = true
        plusMinusLbl.text = "+"
        
        self.removeCoupanCodeValue()//Need to remove applied coupon code, because address will change.
        
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "allDeliveryAddressessVC") as? AllDeliveryAddressessVC
        vc?.delegateDeliveryAddressChange = self as deliveryAddressChangeDelegate
        self.navigationController?.pushViewController(vc!, animated: false)
    }
    
    func showOpenSettingAppAlert(){
        
        let alertController = UIAlertController(title: Constants.msg_Alert, message: "برجاء إضافة بطاقتك الائتمانية الى محفظة الايفون", preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "حسنا", style: .default, handler: { action in
            switch action.style{
            case .default:
                print("default")
                //let url = URL(string: "App-Prefs:root=PASSBOOK")
                //let url = URL(string: "prefs:root=MUSIC")
                //let url = URL(string: UIApplication.openSettingsURLString)
                let url = URL(string: "shoebox://url-scheme")
                if UIApplication.shared.canOpenURL(url!) {
                    UIApplication.shared.open(url!, options: [:], completionHandler: nil)
                }
            case .cancel:
                print("cancel")
            case .destructive:
                print("destructive")
            @unknown default: break
            }}))
        self.present(alertController, animated: true, completion: nil)
    }
    
    func showAddDeliveryAddressAlert() {
        let alertController = UIAlertController(title: Constants.msg_Alert, message: "إضافة عنوان الشحن", preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "حسنا", style: .default, handler: { action in
            switch action.style{
            case .default:
                print("default")
                let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "allDeliveryAddressessVC") as? AllDeliveryAddressessVC
                vc?.delegateDeliveryAddressChange = self as deliveryAddressChangeDelegate
                self.navigationController?.pushViewController(vc!, animated: false)
            case .cancel:
                print("cancel")
            case .destructive:
                print("destructive")
            @unknown default: break
            }}))
        self.present(alertController, animated: true, completion: nil)
    }
    
    func showAlert(){
        let alertController = UIAlertController(title: Constants.msg_Alert, message: "يجب تسجيل الدخول أولا", preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "حسنا", style: .default, handler: { action in
            switch action.style{
            case .default:
                print("default")
                self.selectTabBar(index: 1)
            case .cancel:
                print("cancel")
            case .destructive:
                print("destructive")
            @unknown default: break
            }}))
        self.present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func button_couponClicked(_ sender: UIButton) {
        if plusMinusLbl.text == "+" {
            UIView.animate(withDuration: 0.3, animations: {
                self.couponView.isHidden = false
                self.plusMinusLbl.text = "-"
                self.view.layoutIfNeeded()
            })
            self.textField_CoupanCode.becomeFirstResponder()
        }
        else {
            UIView.animate(withDuration: 0.3, animations: {
                self.couponView.isHidden = true
                self.plusMinusLbl.text = "+"
                self.view.layoutIfNeeded()
            })
            self.textField_CoupanCode.resignFirstResponder()
        }
    }
    
    @IBAction func button_applyCouponClicked(_ sender: UIButton) {
        guard let trimmedString = textField_CoupanCode.text?.trimmingCharacters(in: .whitespacesAndNewlines)
            else {return}
        
        if trimmedString.isEmpty {
            Utility.showAlert(message:"من فضلك ادخل كود الرصيد بشكل صحيح", controller: self) //Please enter password.
            return
        }
        
        if iscouponCodeApplied == false {
            self.applyCoupanCodeAPICalled(coupanCode: trimmedString as NSString)
        }
    }
    
    @IBAction func removeCouponCrossButtonClicked(_ sender: UIButton) {
        self.removeCoupanCodeValue()
    }
    
    func setCoupanCodeValue(coupanCodeDict:NSDictionary, coupanCode:String){
        
        iscouponCodeApplied = true
        
        let intValue_discount = coupanCodeDict["discount_amount"] as? Int ?? 0
        
        label_CoupanDiscountValue.text = "\(intValue_discount)"
        
        label_CoupanDiscountText.text = "القسيمة (\(coupanCode))"
        
        self.calculateGrandTotal()
        
        textField_CoupanCode.isUserInteractionEnabled = false
    }
    
    func removeCoupanCodeValue(){
        
        iscouponCodeApplied = false
        
        label_CoupanDiscountValue.text = "0"
        label_CoupanDiscountText.text = "القسيمة"
        
        self.calculateGrandTotal()
        
        textField_CoupanCode.text = ""
        textField_CoupanCode.isUserInteractionEnabled = true
    }
    @IBAction func button_SaveAndContinueClicked(_ sender: UIButton) {
        
        //Check if user already logged in...
        let userDefaults = UserDefaults.standard
        
        guard let decoded = userDefaults.data(forKey: "loggedInUserRecord")else{
            self.showAlert()
            return
        }
        do {
            guard let _ : Dictionary<String, Any> = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? Dictionary<String, Any> else{//User is not logged In...
                self.showAlert()
                return
            }} catch {print("Couldn't unarchive data")}
        
        if !is_deliveryAddressAdded{
            self.showAddDeliveryAddressAlert()
            return
        }
        
        if total_Amount.contains("-"){//This is only for exceptional case, when grand total may contain negative value(-45 SAR)...
            return
        }
        
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "checkOutPaymentOptionsVC") as? CheckOutPaymentOptionsVC
        vc?.dict_CompleteCartRecord = dict_CompleteCartRecord.mutableCopy() as! NSMutableDictionary
        vc?.array_CartItem = array_CartItem.mutableCopy() as! NSMutableArray
        vc?.first_name = label_Name.accessibilityLabel ?? ""
        vc?.last_name = label_Name.accessibilityHint ?? ""
        vc?.street = label_Street.text ?? ""
        vc?.city = label_City.text ?? ""
        vc?.country = label_Country.text ?? ""
        vc?.country_id = label_Country.accessibilityLabel ?? ""
        vc?.mobile = label_mobileNumber.text ?? ""
        
        if iscouponCodeApplied == false {
            vc?.str_Coupon_Code = ""
        }
        else {
            guard let trimmedString = textField_CoupanCode.text?.trimmingCharacters(in: .whitespacesAndNewlines)
                else {return}
            vc?.str_Coupon_Code = trimmedString
        }
        
        let str_grandValue : NSString = NSString(string: total_Amount)
        vc?.amount_Final =  str_grandValue.integerValue
        let str_shippingCharge : NSString = NSString(string: label_ShippingSoudiArabiaValue.text ?? "")
        vc?.shippingCharge = str_shippingCharge.integerValue
        vc?.shippingMethod = shippingMethod
        vc?.is_GiftApplied = is_GiftApplied
        self.navigationController?.pushViewController(vc!, animated: false)        
    }
}
