
import UIKit

class NotificationAllowVC: UIViewController{
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    
    
    @IBAction func button_registerNotification(_ sender: UIButton) {
        
        let userDefaults = UserDefaults.standard
        userDefaults.set("yes", forKey: "isFirstTimeRegisterNotificationScreenAppeared")
        userDefaults.synchronize()
        
        self.setupPushNotification(application: UIApplication.shared)
        
    }
    func setupPushNotification(application: UIApplication){
        
        let center = UNUserNotificationCenter.current()
        center.requestAuthorization(options: [.alert, .sound]) { (granted, error) in
            /* were we authorised or not? */
            if granted{
                DispatchQueue.main.async { // Correct
                    application.registerForRemoteNotifications()
                    print("Allow")//From UIAlertView of Allow/Don't Allow options
                }
                DispatchQueue.main.async {
                    
                    let initialViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "tabBarVCViewController")
                    UIApplication.shared.keyWindow?.rootViewController = initialViewController
                    UIApplication.shared.keyWindow?.makeKeyAndVisible()
                    
                }
            }else{
                print("Don't Allow")//From UIAlertView of Allow/Don't Allow options
                DispatchQueue.main.async {
                    
                    let initialViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "tabBarVCViewController")
                    UIApplication.shared.keyWindow?.rootViewController = initialViewController
                    UIApplication.shared.keyWindow?.makeKeyAndVisible()
                    
                }
            }
            
        }
    }
    
    @IBAction func button_skipNotification(_ sender: UIButton) {
        
        let userDefaults = UserDefaults.standard
        userDefaults.set("yes", forKey: "isFirstTimeRegisterNotificationScreenAppeared")
        userDefaults.synchronize()
        
        DispatchQueue.main.async {
            
            let initialViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "tabBarVCViewController")
            UIApplication.shared.keyWindow?.rootViewController = initialViewController
            UIApplication.shared.keyWindow?.makeKeyAndVisible()
            
        }
        
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
