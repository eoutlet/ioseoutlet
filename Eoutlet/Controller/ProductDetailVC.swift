

import Alamofire
import SDWebImage
import AppsFlyerLib
import FirebaseAnalytics
import ImageSlideshow

////  For Size Collection View Dynamic Layout (for fixing cell spacing) //////
class CustomViewFlowLayout: UICollectionViewFlowLayout {
    let cellSpacing : CGFloat = 10.0
    
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        self.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        let attributes = super.layoutAttributesForElements(in: rect)
        self.scrollDirection = .horizontal
        var leftMargin = sectionInset.left
        var maxY: CGFloat = -1.0
        attributes?.forEach { layoutAttribute in
            if layoutAttribute.frame.origin.y >= maxY {
                leftMargin = sectionInset.left
            }
            layoutAttribute.frame.origin.x = leftMargin
            leftMargin += layoutAttribute.frame.width + cellSpacing
            maxY = max(layoutAttribute.frame.maxY , maxY)
        }
        return attributes
    }
}
//////////////////////////************//////////////////////

public class CollectionViewCell: UICollectionViewCell {
    
    public private(set) var imageView: UIImageView!
    //weak var delegate: DTPhotoCollectionViewCellDelegate?
    override public init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        imageView = UIImageView(frame: CGRect.zero)
        imageView.contentMode = .scaleAspectFit
        imageView.layer.cornerRadius = 10
        imageView.layer.masksToBounds = true
        imageView.backgroundColor = UIColor.white
        contentView.addSubview(imageView)
        contentView.backgroundColor = .clear
        backgroundColor = .clear
    }
    
    override public func layoutSubviews() {
        super.layoutSubviews()
        let margin = CGFloat(5)
        imageView.frame = CGRect(x: margin, y: margin, width: bounds.size.width - 2 * margin, height: bounds.size.height - 2 * margin)
    }
}

class SizeListCollectionViewCell : UICollectionViewCell
{
    
    @IBOutlet weak var label_Title: UILabel!
    @IBOutlet weak var constraint_TitleLabelWidth: NSLayoutConstraint!
    @IBOutlet weak var imageview_OutofStock: UIImageView!
    @IBOutlet weak var constraint_OutofStockImageViewWidth: NSLayoutConstraint!
    //    @IBOutlet weak var label_TitleWidthConstraints: NSLayoutConstraint!
}

//Related Product...
class RelatedProductsCollectionViewCell : UICollectionViewCell
{
    @IBOutlet weak var base_View: UIView!
    @IBOutlet weak var imageView_Icon: UIImageView!
    @IBOutlet weak var label_Title: UILabel!
    @IBOutlet weak var label_OldPrice: UILabel!
    @IBOutlet weak var label_NewPrice: UILabel!
}

class ProductDetailVC: UIViewController,UIScrollViewDelegate,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {
    
    fileprivate var selected_BannerImageIndex: Int = 0
    var imageview_SelectedBanner = UIImageView()
    var array_BannerImages = NSArray()
    var dict_Product : NSDictionary = NSDictionary()
    var clickedSectionIndexForSize : Int = 0
    var isSizeSelected : Bool = false
    
    @IBOutlet weak var prdouctImagesSliderSlideShow: ImageSlideshow!
    @IBOutlet weak var prdouctImagesSliderViewHeightConstraints: NSLayoutConstraint!
    
    @IBOutlet weak var label_Title: UILabel!
    
    @IBOutlet weak var label_productTitle: UILabel!
    @IBOutlet weak var label_SKUNumber: UILabel!
    @IBOutlet weak var label_productDetail: UILabel!
    
    @IBOutlet weak var constraint_heightOfProductDetail: NSLayoutConstraint!
    @IBOutlet weak var constraint_heightOfViewMoreButton: NSLayoutConstraint!
    
    @IBOutlet weak var button_sizeChart: UIButton!
    
    @IBOutlet weak var label_OldPrice: UILabel!
    @IBOutlet weak var label_NewPrice: UILabel!
    
    //Color Section...
    var array_Color : NSMutableArray = NSMutableArray()
    @IBOutlet weak var imageview_Color: UIImageView!
    
    //Size Section...
    @IBOutlet weak var view_SizeContainer: UIView!
    var array_Size : NSMutableArray = NSMutableArray()
    @IBOutlet weak var label_Size: UILabel!
    @IBOutlet weak var collectionViewSizeListList: UICollectionView!
    
    //Quantity Section...
    @IBOutlet weak var label_Quantity: UILabel!
    
    //RelatedProducts Section...
    @IBOutlet weak var constraints_relatedProductsView: NSLayoutConstraint!
    @IBOutlet weak var collectionView_relatedProducts: UICollectionView!
    var array_relatedProducts : NSMutableArray = NSMutableArray()
    
    @IBOutlet weak var constraints_heightSizeView: NSLayoutConstraint!
    
    let columnLayout = CustomViewFlowLayout()
    var totalWidthPerRow = CGFloat(0)
    var rowCounts = 0
    var spaceBetweenCell = CGFloat(10)
    
    override func viewDidLoad() {
        super.viewDidLoad()
                
        label_Title.text = dict_Product["name"] as! NSString as String
        if let strValue = (dict_Product["category_name"] as? String){
            label_Title.text = strValue
        }
        
        label_productTitle.text = dict_Product["name"] as! NSString as String
        
        label_OldPrice.isHidden = false
        if let intValue = (dict_Product["old_price"] as? Int) {
            
            if(intValue == 0){
                label_OldPrice.isHidden = true
            }
            label_OldPrice.text = String(format: "SAR %d", intValue)
        }
        else if let strValue = (dict_Product["old_price"] as? String){
            
            if((strValue.replacingOccurrences(of: ",", with: "")) == "0"){
                label_OldPrice.isHidden = true
            }
            label_OldPrice.text = String(format: "SAR %d", (strValue.replacingOccurrences(of: ",", with: "") as NSString).integerValue)
        }
        else{
            print("some other data types exists.")
        }
        
        if let intValue = (dict_Product["price"] as? Int) {
            label_NewPrice.text = String(format: "SAR %d", intValue)
        }
        else if let strValue = (dict_Product["price"] as? String){
            label_NewPrice.text = String(format: "SAR %d", (strValue.replacingOccurrences(of: ",", with: "") as NSString).integerValue)
        }
        else{
            print("some other data types exists.")
        }
        
        label_SKUNumber.text = dict_Product["sku"] as? NSString as String?
        
        let textFont = UIFont.init(name: Constants.helvetica_regular_font, size: 12) ?? UIFont()
        let lbl = labelHeightAccordingToText(text:dict_Product["short_descrition"] as? NSString as String? ?? "", font: textFont, width: self.view.frame.size.width - 20)
        let height = lbl.frame.height
        
        let maxSize = CGSize(width: lbl.frame.size.width, height: CGFloat(Float.infinity))
        let charSize = textFont.lineHeight
        let text = (lbl.text ?? "") as NSString
        let textSize = text.boundingRect(with: maxSize, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: textFont], context: nil)
        let numberOfLines = Int(ceil(textSize.height/charSize))
          
        if numberOfLines > 2 {
            self.constraint_heightOfProductDetail.constant = (height / CGFloat(numberOfLines)) * 2
            self.constraint_heightOfViewMoreButton.constant = 30
        }
        else {
            constraint_heightOfProductDetail.constant = height
            constraint_heightOfViewMoreButton.constant = 0
        }
        
        
        let paragraphStyle = NSMutableParagraphStyle()
        //line height size
        paragraphStyle.lineSpacing = 6.0
        let attrString = NSMutableAttributedString(string: dict_Product["short_descrition"] as? String ?? "")
        attrString.addAttribute(NSAttributedString.Key.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attrString.length))
        label_productDetail.attributedText = attrString
        label_productDetail.textAlignment = .right
        
        //        label_productDetail.text = dict_Product["short_descrition"] as? NSString as String?
        //        label_productDetail.sizeToFit()
        
        
        label_Quantity.text = "1"
        
        //Get All product Imeages API Called...
        self.getproductBannerImagesAPICalled(skuNumber: (dict_Product["sku"] as! NSString) as String)
        
        if dict_Product["type"] as! String == "simple"{
            // set color dictionary for simple product
            let dict_Color : NSMutableDictionary =  NSMutableDictionary()
            dict_Color.setValue(dict_Product["color_name"] as! String, forKey: "name")
            dict_Color.setValue(dict_Product["color"] as! String, forKey: "image")
            self.array_Color.insert(dict_Color, at: 0)
            // call color method for set color image
            self.setColorImage(array_colorImage:self.array_Color)
            
            label_Size.text = dict_Product["size"] as? String
            
            constraints_heightSizeView.constant = 25
            
        }
        else{ //Only in configurable type product will have size and color...
            //Get Product Size And Color API Called...
            //collectionViewSizeListList.collectionViewLayout = columnLayout
            //collectionViewSizeListList.contentInsetAdjustmentBehavior = .always
            collectionViewSizeListList.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)

            self.getSizeAndColorAPICalled(skuNumber: (dict_Product["sku"] as! NSString) as String)
        }
        
        //If size chart value is 0, then size chart option will be hidden...
        if let intValue = (dict_Product["size_chart"] as? Int) {
            if intValue == 0{button_sizeChart.isHidden = true}
        }else if let strValue = (dict_Product["size_chart"] as? String){
            if strValue == "0"{button_sizeChart.isHidden = true}
        }
        
        //Related Products Block...
        constraints_relatedProductsView.constant = 0 //Set default hieght for related products as zero when data will get from API then will set again it's value...
        
        DispatchQueue.global(qos: .background).async {
            // Call your background task
            self.getrelatedProductsAPICalled(productId:self.dict_Product["id"] as! String)
        }
        self.viewItemFirebaseAnylatics(dct_Product: dict_Product)
    }
    
    func labelHeightAccordingToText(text: String, font: UIFont, width:CGFloat) -> UILabel {
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        let paragraphStyle = NSMutableParagraphStyle()
        //line height size
        paragraphStyle.lineSpacing = 6.0
        let attrString = NSMutableAttributedString(string: text)
        attrString.addAttribute(NSAttributedString.Key.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attrString.length)) 
        label.attributedText = attrString
        label.textAlignment = .right
        
        label.sizeToFit()
        return label
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.isHidden = true
        
        self.updateCartItemInNavigationBar()
    }
    
    func updateCartItemInNavigationBar() {
        //set cart item count on home screen navigation & tab bar...
        if AppDelegate.cart_item_count != 0 {
            if let tabItems = self.tabBarController?.tabBar.items {
                let tabItem = tabItems[0]
                tabItem.badgeValue = "\(AppDelegate.cart_item_count)"
            }
        }
    }
    
    //MARK: API Calling...
    func getproductBannerImagesAPICalled(skuNumber: String?){
        
        let parameters : Parameters = [
            "cacheignore" : "\(Int(NSDate().timeIntervalSince1970))" //To ignore cache from api...
        ]
        
        guard let skuNumber = skuNumber?.trimmingCharacters(in: .whitespacesAndNewlines) else{
            return
        }
        AppDelegate.showHUD(inView: self.view, message:"Loading....")
        Alamofire.request(Constants.mediaSourceURL + "rest/V1/products/" + skuNumber, method: .get, parameters: parameters)
            .responseJSON { response in
                AppDelegate.hideHUD(inView: self.view)
                switch response.result {
                case .success:
                    if let json = response.result.value{
                        // print("JSON Response : \(json)")
                        guard let _ : NSDictionary = json as? NSDictionary else {
                            return
                        }
                        do {
                            // make sure this JSON is in the format we expect
                            let jsonData =  try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
                            let decoded = try JSONSerialization.jsonObject(with: jsonData, options: []) as! NSDictionary
                            let arrayImages = decoded["media_gallery_entries"]  as? NSArray
                            if arrayImages?.count ?? 0 > 0{
                                //                                self.showImagesOnSrollView(array_Images: arrayImages ?? NSArray())
                                self.showImagesOnSrollView(array_Images: arrayImages ?? NSArray())
                            }
                        }
                        catch let error as NSError {
                            print("Failed to load: \(error.localizedDescription)")
                        }
                    }
                case .failure( _):
                    //Utility.showAlert(message: error.localizedDescription, controller: self)
                    Utility.showAlert(message: "حدث خطأ - يرجي اعادة المحاولة", controller: self) //Something went wrong, please try again.
                }
        }
    }
    
    func getSizeAndColorAPICalled(skuNumber: String){
        
        let parameters : Parameters = [
            "sku" : skuNumber,
            "cacheignore" : "\(Int(NSDate().timeIntervalSince1970))" //To ignore cache from api...
            
        ]
        AppDelegate.showHUD(inView: self.view, message:"Loading....")
        Alamofire.request(Constants.apiURL + "webservice/prodgetsizecolor.php", method: .post, parameters: parameters)
            .responseJSON { response in
                AppDelegate.hideHUD(inView: self.view)
                switch response.result {
                case .success:
                    if let json = response.result.value{
                        // print("JSON Response : \(json)")
                        guard let _ : NSDictionary = json as? NSDictionary else {
                            return
                        }
                        do {
                            // make sure this JSON is in the format we expect
                            let jsonData =  try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
                            let decoded = try JSONSerialization.jsonObject(with: jsonData, options: []) as! NSDictionary
                            guard let arraySizeAndColor : NSArray = decoded["data"] as? NSArray else {
                                return
                            }
                            
                            if arraySizeAndColor.count == 1{
                                let dict_Obj0 = arraySizeAndColor[0] as! NSDictionary
                                
                                if(dict_Obj0["label"] as! String == "Size" || dict_Obj0["label"] as! String == "المقاس") {
                                    let sizeArray = dict_Obj0["data"] as! NSArray
                                    self.array_Size.addObjects(from:sizeArray  as! [Any])
                                    
                                }
                                else if(dict_Obj0["label"] as! String == "Color"){
                                    let colorArray = dict_Obj0["data"] as! NSArray
                                    self.array_Color.addObjects(from:colorArray  as! [Any])
                                }
                            }
                            
                            if arraySizeAndColor.count == 2{
                                let dict_Obj0 = arraySizeAndColor[0] as! NSDictionary
                                
                                if(dict_Obj0["label"] as! String == "Size" || dict_Obj0["label"] as! String == "المقاس") {
                                    let sizeArray = dict_Obj0["data"] as! NSArray
                                    self.array_Size.addObjects(from:sizeArray  as! [Any])
                                    
                                }
                                else if(dict_Obj0["label"] as! String == "Color"){
                                    let colorArray = dict_Obj0["data"] as! NSArray
                                    self.array_Color.addObjects(from:colorArray  as! [Any])
                                }
                                
                                let dict_Obj1 = arraySizeAndColor[1] as! NSDictionary
                                
                                if(dict_Obj1["label"] as! String == "Size" || dict_Obj1["label"] as! String == "المقاس"){
                                    let sizeArray = dict_Obj1["data"] as! NSArray
                                    self.array_Size.addObjects(from:sizeArray  as! [Any])
                                }
                                else if(dict_Obj1["label"] as! String == "Color"){
                                    let colorArray = dict_Obj1["data"] as! NSArray
                                    self.array_Color.addObjects(from:colorArray  as! [Any])
                                }
                            }
                            
                            if (self.array_Color.count > 0){
                                self.setColorImage(array_colorImage:self.array_Color)
                            }
                            
                            self.sizeCollectionViewReload()
                            
                        }
                        catch let error as NSError {
                            print("Failed to load: \(error.localizedDescription)")
                        }
                    }
                case .failure( _):
                    //Utility.showAlert(message: error.localizedDescription, controller: self)
                    Utility.showAlert(message: "حدث خطأ - يرجي اعادة المحاولة", controller: self) //Something went wrong, please try again.
                }
        }
    }
    
    func sizeCollectionViewReload() {
        
        if self.array_Size.count % 5 == 0 {
            let linesForSizeCollectionView = self.array_Size.count / 5
            let height = CGFloat(35 * linesForSizeCollectionView) + CGFloat(10 * (linesForSizeCollectionView-1)) + 25
            self.constraints_heightSizeView.constant = height
        }else {
            let linesForSizeCollectionView = ((self.array_Size.count / 5) + 1)
            let height = CGFloat(35 * linesForSizeCollectionView) + CGFloat(10 * (linesForSizeCollectionView-1)) + 25
            self.constraints_heightSizeView.constant = height
        }
        totalWidthPerRow = CGFloat(0)
        rowCounts = 0
        spaceBetweenCell = CGFloat(10)
        
        self.collectionViewSizeListList.reloadData()
    }
    
    func setColorImage(array_colorImage:NSArray){
        let dictColor : NSDictionary = array_colorImage[0] as! NSDictionary
        let url = NSURL(string: dictColor["image"] as? String ?? "")
        //        label_Color.text = dictColor["name"] as? String ?? ""
        imageview_Color.sd_setImage(with: url as URL?)
    }
    
    func showImagesOnSrollView(array_Images : NSArray){
        if UIDevice.current.userInterfaceIdiom == .phone  {
            prdouctImagesSliderViewHeightConstraints.constant = 340
        }
        else{
            prdouctImagesSliderViewHeightConstraints.constant = 540
        }
        
        //autoscroll collectionview
        prdouctImagesSliderSlideShow.slideshowInterval = 0.0
        prdouctImagesSliderSlideShow.pageIndicatorPosition = .init(horizontal: .center, vertical: .bottom)
        prdouctImagesSliderSlideShow.contentScaleMode = .scaleAspectFit
        prdouctImagesSliderSlideShow.circular = false
        prdouctImagesSliderSlideShow.activityIndicator = DefaultActivityIndicator(style: .gray, color: nil)
        prdouctImagesSliderSlideShow.scrollView.backgroundColor = UIColor(red: 225 / 255.0,green: 225 / 255.0,blue: 225 / 255.0,alpha: CGFloat(1.0))
        prdouctImagesSliderSlideShow.activityIndicator = DefaultActivityIndicator()
        
        let pageControl = UIPageControl()
        pageControl.currentPageIndicatorTintColor = UIColor.white
        pageControl.pageIndicatorTintColor = UIColor.init(red: 1, green: 1, blue: 1, alpha: 0.6)
        pageControl.isEnabled = false
        prdouctImagesSliderSlideShow.pageIndicator = pageControl
        //prdouctImagesSliderSlideShow.pageIndicator = LabelPageIndicator()  ////it will show like 1/8,2/8,....
        
        let taprecognizer_Banner = UITapGestureRecognizer(target: self, action: #selector(didTapBanner))
        taprecognizer_Banner.numberOfTapsRequired = 1
        taprecognizer_Banner.accessibilityElements = [array_Images.count]
        prdouctImagesSliderSlideShow.addGestureRecognizer(taprecognizer_Banner)
        prdouctImagesSliderSlideShow.currentPageChanged = { page in
        }
        
        //let localSource = [BundleImageSource(imageString: "newsliderImage"), BundleImageSource(imageString: "newsliderImage1"), BundleImageSource(imageString: "newsliderImage2"), BundleImageSource(imageString: "newsliderImage3")]
        
        var arr = [AlamofireSource]()
        for indx in 0 ..< array_Images.count {
            let dict_Image = array_Images[indx] as? NSDictionary
            arr.append(AlamofireSource(urlString: Constants.mediaSourceURL + "pub/media/catalog/product\(dict_Image?["file"] ?? "")")!)
        }
        self.prdouctImagesSliderSlideShow.setImageInputs(arr)
    }
    
    @objc func didTapBanner(gesture : UITapGestureRecognizer) {
        let ges_array = gesture.accessibilityElements
        var count = 0
        if(ges_array?.count ?? 0 > 0){
            count =  ges_array?[0] as! Int
        }
        
        if(count == 0){
            return
        }
        
        /*
         let fullScreenController = prdouctImagesSliderSlideShow.presentFullScreenController(from: self)
         fullScreenController.slideshow.activityIndicator = DefaultActivityIndicator(style: .gray, color: nil)
         fullScreenController.slideshow.circular = false
         fullScreenController.slideshow.pageIndicatorPosition = .init(horizontal: .center, vertical: .under)
         fullScreenController.slideshow.pageControl.currentPageIndicatorTintColor = Constants.default_ThemeColor
         fullScreenController.slideshow.pageControl.pageIndicatorTintColor = UIColor.gray
         
         */
        let fullScreenController = prdouctImagesSliderSlideShow.presentFullScreenController(from: self)
        fullScreenController.slideshow.activityIndicator = DefaultActivityIndicator(style: .white, color: nil)
        fullScreenController.slideshow.circular = false
        
        let pageControl = UIPageControl()
        pageControl.currentPageIndicatorTintColor = Constants.default_ThemeColor
        pageControl.pageIndicatorTintColor = UIColor.gray
        pageControl.isEnabled = false
        pageControl.numberOfPages = count
        fullScreenController.slideshow.pageIndicator = pageControl
        fullScreenController.slideshow.pageIndicatorPosition = .init(horizontal: .center, vertical: .under)
    }
    
    //    func showImagesOnSrollView(array_Images : NSArray){
    //        var xPoint : CGFloat = 0
    //
    //        for indx in 0 ..< array_Images.count{
    //
    //            let imgageViewBanner = UIImageView()
    //
    //            if UIDevice.current.userInterfaceIdiom == .phone  {
    //
    //                imgageViewBanner.frame = CGRect(x: xPoint, y: 0, width: self.view.frame.size.width, height: 350)
    //            }else{
    //                imgageViewBanner.frame = CGRect(x: xPoint, y: 0, width: self.view.frame.size.width, height: 550)
    //            }
    //            //let url = NSURL(string: array_Images["img"] as? String ?? "")
    //            let dict_Image = array_Images[indx] as? NSDictionary
    //            let imageUrl : NSString = Constants.apiURL + "pub/media/catalog/product\(dict_Image?["file"] ?? "")" as NSString
    //            imgageViewBanner.sd_imageIndicator = SDWebImageActivityIndicator.gray
    //            imgageViewBanner.sd_imageIndicator?.startAnimatingIndicator()
    //            let url = NSURL(string:  imageUrl as String)
    //            imgageViewBanner.sd_setImage(with: url as URL?) { (image, error, cache, urls) in
    //                if (error != nil) {// Failed to load image
    //                } else {// Successful in loading image
    //                    imgageViewBanner.sd_imageIndicator?.stopAnimatingIndicator()
    //                }}
    //            imgageViewBanner.contentMode = .scaleAspectFit
    //            imgageViewBanner.backgroundColor = UIColor(red: 225 / 255.0,green: 225 / 255.0,blue: 225 / 255.0,alpha: CGFloat(1.0))
    //            imgageViewBanner.tag = indx
    //            imgageViewBanner.isUserInteractionEnabled = true
    //            scrollview_Banners.addSubview(imgageViewBanner)
    //            xPoint =  xPoint + self.view.frame.size.width
    //
    //
    ////            //Add Tap gesture on image view for zooming...
    ////            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.imgageViewBannerTap(_:)))
    ////            tapGesture.numberOfTapsRequired = 1
    ////            tapGesture.accessibilityElements = array_Images as? [Any]
    ////            imgageViewBanner.addGestureRecognizer(tapGesture)
    //
    //
    //        }
    //
    //        let wdth : Int = Int(self.view.frame.size.width)
    //
    //        //iPhone X, XS, XS Max, XR(i.e. notch devices)
    //        if UIDevice.current.userInterfaceIdiom == .phone{
    //            scrollview_Banners.contentSize = CGSize(width: wdth * array_Images.count, height: 350)
    //        }else{
    //            scrollview_Banners.contentSize = CGSize(width: wdth * array_Images.count, height: 550)
    //        }
    //
    //        pagecontrol_Counter.addTarget(self, action: #selector(changePage(sender:)), for: .valueChanged)
    //        pagecontrol_Counter.numberOfPages = array_Images.count
    //    }
    
    
    //    @objc func imgageViewBannerTap(_ sender: UITapGestureRecognizer? = nil) {
    //
    //        let imgview_Slected = sender?.view as! UIImageView
    //        array_BannerImages = sender?.accessibilityElements as NSArray? ?? NSArray()
    //        imageview_SelectedBanner = imgview_Slected
    //        selected_BannerImageIndex = imgview_Slected.tag
    //
    //        //           let productPhotoZoomVC = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "productPhotoZoomVC") as! ProductPhotoZoomVC
    //        //          let ary = sender?.accessibilityElements as NSArray? ?? NSArray()
    //        //          productPhotoZoomVC.array_Images = ary
    //        //          self.navigationController?.pushViewController(productPhotoZoomVC, animated: false)
    //        //
    //        let viewController = SimplePhotoViewerController(referencedView: imgview_Slected, image: imgview_Slected.image)
    //        viewController.dataSource = self
    //        viewController.delegate = self
    //        present(viewController, animated: true, completion: nil)
    //
    //    }
    
    func addToCartAPICalled(userId: String, cartId:String, maskKey:String, skuNumber: String, quantity: String, size: String, color: String){
        let parameters : Parameters = [
            "customer_id" : userId,
            "cart_id" : cartId,
            "mask_key" : maskKey,
            "sku" : skuNumber,
            "qty" : quantity,
            "size" : size,
            "color" : color,
            "type" : dict_Product["type"] as! String,
            "product_id" : dict_Product["id"] as! String,
            "cacheignore" : "\(Int(NSDate().timeIntervalSince1970))" //To ignore cache from api...
        ]
        
        AppDelegate.showHUD(inView: self.view, message:"Loading....")
        Alamofire.request(Constants.apiURL + "webservice/addcart.php", method: .post, parameters: parameters)
            .responseJSON { response in
                AppDelegate.hideHUD(inView: self.view)
                switch response.result {
                case .success:
                    
                    if let json = response.result.value{
                        
                        guard let _ : NSDictionary = json as? NSDictionary else {
                            return
                        }
                        
                        do {
                            // make sure this JSON is in the format we expect
                            
                            let jsonData =  try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
                            let decoded = try JSONSerialization.jsonObject(with: jsonData, options: []) as! NSDictionary
                            if decoded["msg"] as? String == "success"{
                                
                                //                                //This is used for empty all pushed screens...
                                //                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "refreshCartScreenForUpdatedRecord"), object: nil)
                                
                                self.addToCartAppsFlyerAnalytics(quantity: quantity)
                                self.addToCartFirebaseAnylatics(paramas:parameters)
                                self.getAllCartItemAPICalled()
                                
                            }else{
                                
                                iToast.show("حدث خطأ - يرجي اعادة المحاولة")//An error has occurred - please try again
                            }
                            
                        } catch let error as NSError {
                            
                            print("Failed to load: \(error.localizedDescription)")
                        }
                        
                    }
                case .failure( _):
                    //Utility.showAlert(message: error.localizedDescription, controller: self)
                    Utility.showAlert(message: "حدث خطأ - يرجي اعادة المحاولة", controller: self) //Something went wrong, please try again.
                }
        }
        
    }
    
    func getAllCartItemAPICalled(){
        let userDefaults = UserDefaults.standard
        let decoded : Data = userDefaults.data(forKey: "isguestMaskKeyAndCardIdFetched") ?? Data()
        
        var str_cartID = ""
        do {
            
            if let decodedData : Dictionary<String, Any> = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? Dictionary<String, Any> {
                if let crtId =  decodedData["cart_id"]{
                    str_cartID = String(format:"\(crtId)")
                }
            }
        } catch {
            print("Couldn't unarchive data")
        }
        
        var str_maskKey = ""
        do {
            
            if let decodedData : Dictionary<String, Any> = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? Dictionary<String, Any> {
                if let mskKey =  decodedData["mask_key"]{
                    str_maskKey = String(format:"\(mskKey)")
                }
            }
            
        } catch {
            print("Couldn't unarchive data")
        }
        
        
        var str_userID = ""
        
        if loggedInUser.shared.string_userID.isEmpty || loggedInUser.shared.string_userID.count == 0{
        }else{
            
            str_userID = String(format:"\(loggedInUser.shared.string_userID)")
            str_cartID = ""
            str_maskKey = ""
            
        }
        
        
        let parameters : Parameters = [
            
            "customer_id" : str_userID,
            "cart_id" : str_cartID,
            "mask_key" : str_maskKey,
            "cacheignore" : "\(Int(NSDate().timeIntervalSince1970))" //To ignore cache from api...
            
        ]
        
        AppDelegate.showHUD(inView: self.view, message:"Loading....")
        Alamofire.request(Constants.apiURL+"webservice/viewcart.php", method: .post, parameters: parameters)
            
            .responseJSON{ response in
                
                AppDelegate.hideHUD(inView: self.view)
                
                switch response.result {
                case .success:
                    
                    if let json = response.result.value{
                        
                        guard let _ : NSDictionary = json as? NSDictionary else {
                            return
                        }
                        
                        
                        do {
                            // make sure this JSON is in the format we expect
                            
                            let jsonData =  try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
                            let decoded = try JSONSerialization.jsonObject(with: jsonData, options: []) as! NSDictionary
                            
                            if decoded["msg"] as? String == "success"{
                                
                                guard let dataArray : NSArray = decoded["data"] as? NSArray else {
                                    return
                                }
                                
                                if dataArray.count > 0{
                                    if let tabItems = self.tabBarController?.tabBar.items {
                                        var total_ItemInCart = 0
                                        for indx in 0 ..< dataArray.count{
                                            let dict_Product = dataArray[indx] as! NSDictionary
                                            if let intValue = (dict_Product["qty"] as? Int) {
                                                total_ItemInCart = total_ItemInCart + intValue
                                            }
                                        }
                                        let tabItem = tabItems[0]
                                        tabItem.badgeValue = "\(total_ItemInCart)"
                                        AppDelegate.cart_item_count = total_ItemInCart
                                        self.updateCartItemInNavigationBar()
                                    }
                                }
                                
                            }else{
                                
                                iToast.show("حدث خطأ - يرجي اعادة المحاولة")//An error has occurred - please try again
                                
                            }
                            
                        } catch let error as NSError {
                            
                            print("Failed to load: \(error.localizedDescription)")
                        }
                        
                    }
                case .failure( _):
                    //Utility.showAlert(message: error.localizedDescription, controller: self)
                    Utility.showAlert(message: "حدث خطأ - يرجي اعادة المحاولة", controller: self) //Something went wrong, please try again.
                }
        }
    }
    
    func getrelatedProductsAPICalled(productId: String){
        
        
        let parameters : Parameters = [
            
            "pid" : productId,
            "cacheignore" : "\(Int(NSDate().timeIntervalSince1970))" //To ignore cache from api...
            
        ]
        
        
        //AppDelegate.showHUD(inView: self.view, message:"Loading....")
        
        Alamofire.request(Constants.apiURL+"webservice/getrelatedproduct.php", method: .get, parameters: parameters)
            
            .responseJSON { response in
                
                AppDelegate.hideHUD(inView: self.view)
                
                switch response.result {
                case .success:
                    
                    if let json = response.result.value{
                        
                        guard let _ : NSDictionary = json as? NSDictionary else {
                            return
                        }
                        
                        do {
                            // make sure this JSON is in the format we expect
                            
                            let jsonData =  try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
                            let decoded = try JSONSerialization.jsonObject(with: jsonData, options: []) as! NSDictionary
                            
                            // here "decoded" is of type `Any`, decoded from JSON data
                            let arrayofGetData = decoded["data"] as! NSArray
                            if arrayofGetData.count > 0{
                                
                                self.array_relatedProducts.addObjects(from: (arrayofGetData ) as! [Any])
                                DispatchQueue.main.async {
                                    // UI Updates here for task complete.
                                    self.collectionView_relatedProducts.reloadData()
                                }
                                
                                if UIDevice.current.userInterfaceIdiom == .pad{//Need to set height constraint, initial it was 0.
                                    self.constraints_relatedProductsView.constant = 480
                                }else{
                                    self.constraints_relatedProductsView.constant = 400
                                }
                                
                            }else{
                                self.constraints_relatedProductsView.constant = 0
                            }
                            
                        } catch let error as NSError {
                            
                            print("Failed to load: \(error.localizedDescription)")
                            
                        }
                        
                    }
                case .failure( _):
                    //Utility.showAlert(message: error.localizedDescription, controller: self)
                    Utility.showAlert(message: "حدث خطأ - يرجي اعادة المحاولة", controller: self) //Something went wrong, please try again.
                }
        }
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView.tag == 0 {
            return array_Size.count
        }
        else if(collectionView.tag == 1) {
            return array_relatedProducts.count
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if (collectionView.tag == 0) {
            let dict : NSDictionary = array_Size[indexPath.row] as! NSDictionary
            //******for dynamic cell widh and collection view height******//
            let dynamicCellWidth = self.widthForLabel(text: dict["name"] as? String ?? "", height: 40) + 20
            if indexPath.item == array_Size.count - 1 {
                totalWidthPerRow += dynamicCellWidth
            }else {
                totalWidthPerRow += dynamicCellWidth + spaceBetweenCell
            }
            
            if (totalWidthPerRow > collectionViewSizeListList.bounds.width) {
                rowCounts = rowCounts + 1
                totalWidthPerRow = dynamicCellWidth + spaceBetweenCell
            }
            //**********************************************************//
            let width =  Int((self.view.bounds.size.width-60) / 5)
            return CGSize(width: width, height: 35)
           // return CGSize(width: 50, height: 30)
         // return  CGSize(width: 40, height: 30)
            //return CGSize(width: dynamicCellWidth , height: 30)
            
        }else if(collectionView.tag == 1) {
            if UIDevice.current.userInterfaceIdiom == .pad {
                return CGSize(width: 250, height: 380)
            }
            else {
                return CGSize(width: 250, height: 360)
            }
        }
        else {
            return CGSize(width: 0, height: 0)
        }
    }
    
//    func collectionView(_ collectionView: UICollectionView,
//                          layout collectionViewLayout: UICollectionViewLayout,
//    minimumInteritemSpacingForSectionAt section: Int) -> CGFloat
//    {
//
//       return 120
//    }
    //Function to calculate width for label based on text
    func widthForLabel(text:String, height:CGFloat) -> CGFloat {
        
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: CGFloat.greatestFiniteMagnitude, height: height))
        label.numberOfLines = 1
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        if UIDevice.current.userInterfaceIdiom == .phone {
            label.font = UIFont(name: Constants.helvetica_regular_font, size: 10)!
        }else {
            label.font = UIFont(name: Constants.helvetica_regular_font, size: 12)!
        }
        label.text = text
        label.sizeToFit()
        return label.frame.width
        
    }
    
    internal func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if (collectionView.tag == 0){
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "sizeListCollectionViewCell", for: indexPath) as! SizeListCollectionViewCell
            
            let dict : NSDictionary = array_Size[indexPath.row] as! NSDictionary
            cell.label_Title.text = dict["name"] as? String
            cell.label_Title.accessibilityLabel = dict["value_index"] as? String
            //cell.label_TitleWidthConstraints.constant = self.widthForLabel(text: dict["name"] as? String ?? "", height: 40) + 20
            let width =  Int((self.view.bounds.size.width-60) / 5)
            cell.constraint_TitleLabelWidth.constant = CGFloat(width)
            cell.constraint_OutofStockImageViewWidth.constant = CGFloat(width)

           //cell.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)

            if(dict["salable"] as? String == "1"){
                
                cell.imageview_OutofStock.isHidden = true
                cell.isUserInteractionEnabled = true
                
                if(clickedSectionIndexForSize == indexPath.row){
                    
                    isSizeSelected = true
                    label_Size.text = dict["name"] as? String
                    cell.label_Title.backgroundColor = UIColor.init(red: 74/255, green: 73/255, blue: 71/255, alpha: 1.0)
                    cell.label_Title.textColor = UIColor.white
                    self.setPriceOfProduct(dict_Product: dict)
                    
                }else{
                    cell.label_Title.backgroundColor = UIColor.white
                    cell.label_Title.textColor = UIColor.black
                }
                
            }else{
                cell.imageview_OutofStock.isHidden = false
                cell.isUserInteractionEnabled = false
                cell.label_Title.backgroundColor = UIColor.init(red: 246/255, green: 247/255, blue: 251/255, alpha: 1.0)
                cell.label_Title.textColor = UIColor.black
            }
            cell.contentView.transform = CGAffineTransform(scaleX: -1, y: 1)
            return cell
        }
        else if(collectionView.tag == 1){
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "productListCollectionViewCell", for: indexPath) as! ProductListCollectionViewCell
            let dict : NSDictionary = array_relatedProducts[indexPath.row] as! NSDictionary
            cell.imageView_Icon.sd_imageIndicator = SDWebImageActivityIndicator.gray
            cell.imageView_Icon.sd_imageIndicator?.startAnimatingIndicator()
            let url = NSURL(string: dict["img"] as? String ?? "" )
            cell.imageView_Icon.sd_setImage(with: url as URL?) { (image, error, cache, urls) in
                if (error != nil) {// Failed to load image
                } else {// Successful in loading image
                    cell.imageView_Icon.sd_imageIndicator?.stopAnimatingIndicator()
                }}
            
            cell.label_Title.text = dict["name"] as? String
            
            cell.label_OldPrice.isHidden = false
            cell.label_NewFrontPrice.isHidden = true
            if let intValue = (dict["old_price"] as? Int) {
                if(intValue == 0){
                    cell.label_NewFrontPrice.isHidden = false
                    cell.label_OldPrice.isHidden = true
                }
                cell.label_OldPrice.text = String(format: "SAR %d", intValue)
            }
            else if let strValue = (dict["old_price"] as? String){
                if((strValue.replacingOccurrences(of: ",", with: "")) == "0"){
                    cell.label_OldPrice.isHidden = true
                    cell.label_NewFrontPrice.isHidden = false
                }
                cell.label_OldPrice.text = String(format: "SAR %d", (strValue.replacingOccurrences(of: ",", with: "") as NSString).integerValue)
            }
            
            if let intValue = (dict["price"] as? Int) {
                cell.label_NewPrice.text = String(format: "SAR %d", intValue)
                cell.label_NewFrontPrice.text = String(format: "SAR %d", intValue)
            }
            else if let strValue = (dict["price"] as? String){
                cell.label_NewPrice.text = String(format: "SAR %d", (strValue.replacingOccurrences(of: ",", with: "") as NSString).integerValue)
                cell.label_NewFrontPrice.text = String(format: "SAR %d", (strValue.replacingOccurrences(of: ",", with: "") as NSString).integerValue)
            }
            
            //set shadow on product list cell collectionview
            cell.layoutIfNeeded()
            cell.base_View.backgroundColor = UIColor.white //your background color...
            cell.base_View.cornerRadius = 5
            cell.base_View.setShadow(x: -5, y: -5, width: cell.base_View.bounds.width + 10, height: cell.base_View.bounds.height + 10, radius: 5)
            return cell
        }
        return UICollectionViewCell()
    }
    func setPriceOfProduct(dict_Product:NSDictionary){
        
        label_OldPrice.isHidden = false
        if let intValue = (dict_Product["old_price"] as? Int) {
            
            if(intValue == 0){
                label_OldPrice.isHidden = true
            }
            label_OldPrice.text = String(format: "SAR %d", intValue)
        }
        else if let strValue = (dict_Product["old_price"] as? String){
            
            if((strValue.replacingOccurrences(of: ",", with: "")) == "0"){
                label_OldPrice.isHidden = true
            }
            label_OldPrice.text = String(format: "SAR %d", (strValue.replacingOccurrences(of: ",", with: "") as NSString).integerValue)
        }
        
        if let intValue = (dict_Product["price"] as? Int) {
            label_NewPrice.text = String(format: "SAR %d", intValue)
        }
        else if let strValue = (dict_Product["price"] as? String){
            label_NewPrice.text = String(format: "SAR %d", (strValue.replacingOccurrences(of: ",", with: "") as NSString).integerValue)
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView.tag == 0 {
            if dict_Product["type"] as! String != "simple" {  // for configurable product only
                clickedSectionIndexForSize = indexPath.row
                self.sizeCollectionViewReload()
            }
        }
        else if(collectionView.tag == 1){
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "productDetailVC") as? ProductDetailVC
            vc?.dict_Product = array_relatedProducts[indexPath.row] as! NSDictionary
            self.navigationController?.pushViewController(vc!, animated: false)
        }
    }
    
    //MARK: UIButton Clicks
    @IBAction func backButtonClicked(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func button_ViewMoreClicked(_ sender: UIButton) {
        let textFont = UIFont.init(name: Constants.helvetica_regular_font, size: 12) ?? UIFont()
        let lbl = labelHeightAccordingToText(text:dict_Product["short_descrition"] as? NSString as String? ?? "", font: textFont, width: self.view.frame.size.width - (UIDevice.current.userInterfaceIdiom == .pad ? 300 : 20))
        let height = lbl.frame.height
        
        self.constraint_heightOfProductDetail.constant = height //proudct details height as per text
        self.constraint_heightOfViewMoreButton.constant = 0  //view more button hide
    }
    @IBAction func button_sizeChartClicked(_ sender: UIButton) {
            
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "sizeChartWebViewVC") as! SizeChartWebViewVC
        if let intValue = (dict_Product["size_chart"] as? Int) {
            vc.str_sizeChartWebViewURL = Constants.apiURL + "webservice/sizechart.php?sizechart=\(intValue)"
        }else if let strValue = (dict_Product["size_chart"] as? String){
            vc.str_sizeChartWebViewURL = Constants.apiURL + "webservice/sizechart.php?sizechart=\(strValue)"
        }
        self.navigationController?.pushViewController(vc, animated: false)
            
        }
    @IBAction func button_DecreaseQuantityClicked(_ sender: UIButton) {
        var value : Int = (label_Quantity.text! as NSString).integerValue
        if value == 1{
            return
        }else{
            value = value - 1
        }
        
        label_Quantity.text  = "\(value)"
    }
    
    @IBAction func button_IncreaseQuantityClicked(_ sender: UIButton) {
        var value : Int = (label_Quantity.text! as NSString).integerValue
        value = value + 1
        
        label_Quantity.text  = "\(value)"
    }
    
    @IBAction func button_addToCart(_ sender: UIButton) {
        let userDefaults = UserDefaults.standard
        let decoded : Data = userDefaults.data(forKey: "isguestMaskKeyAndCardIdFetched") ?? Data()
        
        var str_cartID = ""
        do {
            
            if let decodedData : Dictionary<String, Any> = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? Dictionary<String, Any> {
                if let crtId =  decodedData["cart_id"]{
                    str_cartID = String(format:"\(crtId)")
                }
            }
        } catch {
            print("Couldn't unarchive data")
        }
        
        
        var str_maskKey = ""
        do {
            
            if let decodedData : Dictionary<String, Any> = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? Dictionary<String, Any> {
                if let mskKey =  decodedData["mask_key"]{
                    str_maskKey = String(format:"\(mskKey)")
                }
            }
        } catch {
            print("Couldn't unarchive data")
        }
        
        var str_userID = ""
        if !loggedInUser.shared.string_userID.isEmpty && loggedInUser.shared.string_userID.count != 0{
            
            str_userID = String(format:"\(loggedInUser.shared.string_userID)")
            str_cartID = ""
            str_maskKey = ""
            
        }
        
        let value : Int = (label_Quantity.text! as NSString).integerValue
        
        if dict_Product["type"] as! String == "simple"{
            
            self.addToCartAPICalled(userId: str_userID, cartId:str_cartID, maskKey:str_maskKey, skuNumber: (dict_Product["sku"] as! NSString) as String, quantity: "\(value)", size: "", color: "")
            
        }else{
            
            var str_Color : String = ""
            if (array_Color.count > 0){
                
                let dictColor : NSDictionary = array_Color[0] as! NSDictionary
                str_Color = dictColor["value_index"] as? String ?? ""
                
            }
            
            var str_Size : String = ""
            if array_Size.count > clickedSectionIndexForSize {
                
                let dictSize : NSDictionary = array_Size[clickedSectionIndexForSize] as! NSDictionary
                str_Size = dictSize["value_index"] as! String
                
            }
            
            if (!isSizeSelected){
                
                iToast.show("برجاء اختيار المقاس المطلوب أولا") //Please select your desired size first
                return
            }
            
            self.addToCartAPICalled(userId: str_userID, cartId:str_cartID, maskKey:str_maskKey, skuNumber: (dict_Product["sku"] as! NSString) as String, quantity: "\(value)", size: str_Size, color: str_Color)
        }
    }
}

//for appsflyer
extension ProductDetailVC {
    func addToCartAppsFlyerAnalytics(quantity: String) {
        
        var totalPricevalue : NSString = NSString(string: label_NewPrice.text ?? "")
        totalPricevalue =  totalPricevalue.replacingOccurrences(of: "SAR", with: "") as NSString
        let productInfo : NSMutableDictionary =  NSMutableDictionary()
        productInfo.setValue("SAR", forKey: "currency")
        productInfo.setValue(totalPricevalue, forKey: "price")
        productInfo.setValue(quantity, forKey: "quantity")
        productInfo.setValue(self.dict_Product["type"] as! String, forKey: "type")
        productInfo.setValue(self.dict_Product["id"] as! String, forKey: "id")
        analyticsAppsFlyer.shared.AnalyticsEventAddToCart(paramas: productInfo)
    }
}

//for firebase
extension ProductDetailVC {
    func addToCartFirebaseAnylatics(paramas: Parameters){
        let productInfo : NSMutableDictionary =  NSMutableDictionary()
        productInfo.setValue("SAR", forKey: "currency")
        productInfo.setValue(self.dict_Product["category_name"] as? String ?? "", forKey: "category_name")
        productInfo.setValue(paramas["sku"] as? String ?? "", forKey: "sku")
        productInfo.setValue(Locale.current.regionCode ?? "", forKey: "location")
        let itemName : NSString = NSString(string: label_productTitle.text ?? "")
        productInfo.setValue(itemName, forKey: "itemname")
        var totalPricevalue : NSString = NSString(string: label_NewPrice.text ?? "")
        totalPricevalue =  totalPricevalue.replacingOccurrences(of: "SAR", with: "") as NSString
        productInfo.setValue(totalPricevalue, forKey: "price")
        productInfo.setValue(paramas["qty"] as? String ?? "", forKey: "quantity")
        productInfo.setValue(paramas["product_id"] as? String ?? "", forKey: "productid")
        analyticsFirebase.shared.AnalyticsEventAddToCart(paramas: productInfo as NSDictionary)
    }
    
    func viewItemFirebaseAnylatics(dct_Product: NSDictionary){
        
        let productInfo : NSMutableDictionary =  NSMutableDictionary()
        productInfo.setValue(dct_Product["sku"] as? String ?? "", forKey: "sku")
        productInfo.setValue(Locale.current.regionCode ?? "", forKey: "location")
        analyticsFirebase.shared.AnalyticsEventViewItem(paramas: productInfo as NSDictionary)
        
    }
}
