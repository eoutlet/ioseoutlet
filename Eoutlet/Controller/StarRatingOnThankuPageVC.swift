//
//  StarRatingOnThankuPageVC.swift
//  Eoutlet
//
//  Created by Sudhir Rohilla on 13/08/20.
//  Copyright © 2020 Unyscape. All rights reserved.
//

import UIKit

class StarRatingOnThankuPageVC: UIViewController {
    @IBOutlet weak var button_star1: UIButton!
    @IBOutlet weak var button_star2: UIButton!
    @IBOutlet weak var button_star3: UIButton!
    @IBOutlet weak var button_star4: UIButton!
    @IBOutlet weak var button_star5: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()

        let dimAlphaRedColor =  UIColor.black.withAlphaComponent(0.5)
               self.view.backgroundColor = dimAlphaRedColor
               view.isOpaque = true
               
        button_star1.setImage(UIImage(named: "ratingstar_yellow"), for: UIControl.State.normal)
        button_star2.setImage(UIImage(named: "ratingstar_yellow"), for: UIControl.State.normal)
        button_star3.setImage(UIImage(named: "ratingstar_yellow"), for: UIControl.State.normal)
        button_star4.setImage(UIImage(named: "ratingstar_yellow"), for: UIControl.State.normal)
        button_star5.setImage(UIImage(named: "ratingstar_gray"), for: UIControl.State.normal)
        
    }
    @IBAction func button_star1Clicked(_ sender: UIButton) {
     
        button_star1.setImage(UIImage(named: "ratingstar_yellow"), for: UIControl.State.normal)
        button_star2.setImage(UIImage(named: "ratingstar_gray"), for: UIControl.State.normal)
        button_star3.setImage(UIImage(named: "ratingstar_gray"), for: UIControl.State.normal)
        button_star4.setImage(UIImage(named: "ratingstar_gray"), for: UIControl.State.normal)
        button_star5.setImage(UIImage(named: "ratingstar_gray"), for: UIControl.State.normal)
    }
    
    @IBAction func button_star2Clicked(_ sender: UIButton) {
        
        button_star1.setImage(UIImage(named: "ratingstar_yellow"), for: UIControl.State.normal)
        button_star2.setImage(UIImage(named: "ratingstar_yellow"), for: UIControl.State.normal)
        button_star3.setImage(UIImage(named: "ratingstar_gray"), for: UIControl.State.normal)
        button_star4.setImage(UIImage(named: "ratingstar_gray"), for: UIControl.State.normal)
        button_star5.setImage(UIImage(named: "ratingstar_gray"), for: UIControl.State.normal)
       }
       
    @IBAction func button_star3Clicked(_ sender: UIButton) {
        
        button_star1.setImage(UIImage(named: "ratingstar_yellow"), for: UIControl.State.normal)
        button_star2.setImage(UIImage(named: "ratingstar_yellow"), for: UIControl.State.normal)
        button_star3.setImage(UIImage(named: "ratingstar_yellow"), for: UIControl.State.normal)
        button_star4.setImage(UIImage(named: "ratingstar_gray"), for: UIControl.State.normal)
        button_star5.setImage(UIImage(named: "ratingstar_gray"), for: UIControl.State.normal)
       }
       
    @IBAction func button_star4Clicked(_ sender: UIButton) {
        
        button_star1.setImage(UIImage(named: "ratingstar_yellow"), for: UIControl.State.normal)
        button_star2.setImage(UIImage(named: "ratingstar_yellow"), for: UIControl.State.normal)
        button_star3.setImage(UIImage(named: "ratingstar_yellow"), for: UIControl.State.normal)
        button_star4.setImage(UIImage(named: "ratingstar_yellow"), for: UIControl.State.normal)
        button_star5.setImage(UIImage(named: "ratingstar_gray"), for: UIControl.State.normal)
        
       }
       
    @IBAction func button_star5Clicked(_ sender: UIButton) {
        
        button_star1.setImage(UIImage(named: "ratingstar_yellow"), for: UIControl.State.normal)
        button_star2.setImage(UIImage(named: "ratingstar_yellow"), for: UIControl.State.normal)
        button_star3.setImage(UIImage(named: "ratingstar_yellow"), for: UIControl.State.normal)
        button_star4.setImage(UIImage(named: "ratingstar_yellow"), for: UIControl.State.normal)
        button_star5.setImage(UIImage(named: "ratingstar_yellow"), for: UIControl.State.normal)
       }
       

    @IBAction func button_finalRatingSubmitClicked(_ sender: UIButton) {
        
        
        if button_star3.currentImage == UIImage(named: "ratingstar_yellow.png") {
            
            let appleID = "1484601181"
            let url = "https://itunes.apple.com/app/id\(appleID)?action=write-review"
            if let path = URL(string: url) {
                    UIApplication.shared.open(path, options: [:], completionHandler: nil)
            }
            
        }
        
        self.dismiss(animated: true, completion: nil)
            
        
        
                   
        
    }
    
}
