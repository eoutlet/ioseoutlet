
import Alamofire
class MainFilterCell : UITableViewCell
{
    @IBOutlet weak var label_Name: UILabel!
}

class SecondaryFilterCell : UITableViewCell
{
    @IBOutlet weak var label_Name: UILabel!
    @IBOutlet weak var imageview_tickUntick: UIImageView!
}

protocol applyfilterDelegate {
    func fileteredAppliedSuccessfully(array_filteredToBeApplied : NSArray, array_MainFiltersFromFilterScreen:NSArray)
}

class FilterVC: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    var delegate_Filter : applyfilterDelegate?
    
    var array_MainFilters : NSMutableArray = NSMutableArray()
    var array_tempFilterHolder : NSArray = NSArray()
    
    @IBOutlet weak var tableview_Main: UITableView!
    
    //var array_SecondaryFilters : NSMutableArray = NSMutableArray()
    @IBOutlet weak var tableview_Secondary: UITableView!
    
    var str_catID : NSString = ""
    var int_SelectedMainFilterIndex = 0
    var str_SelectedMainCodeValue : NSString = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.isHidden = true
        
        // Do any additional setup after loading the view.
        
        self.tableview_Main.tableFooterView = UIView()
        self.tableview_Secondary.tableFooterView = UIView()
        
        if self.array_tempFilterHolder.count == 0 {
            
            //Default applied filters are not stored, then need to fetch all filters from API...
            self.getInitialAllFiltersAPICalled()
            
        }
        else{
            
            //If filters are already applied...
            self.array_MainFilters.addObjects(from: array_tempFilterHolder as! [Any])
            
        }
    }
    
    //MARK: - UIButton Clicks
    @IBAction func backButtonClicked(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: false)
    }
        
    //MARK: API Calling...
    func getInitialAllFiltersAPICalled(){
        let parameters : Parameters = [
            "id" : str_catID as String,
            "cacheignore" : "\(Int(NSDate().timeIntervalSince1970))" //To ignore cache from api...
        ]
        
        AppDelegate.showHUD(inView: self.view, message:"Loading....")
        Alamofire.request(Constants.apiURL + "webservice/filterapi.php", method: .post, parameters: parameters)
            .responseJSON { response in
                AppDelegate.hideHUD(inView: self.view)
                switch response.result {
                case .success:
                    if let json = response.result.value{
                        //print("JSON Response : \(json)")
                        guard let _ : NSDictionary = json as? NSDictionary else {
                            return
                        }
                        do {
                            // make sure this JSON is in the format we expect
                            let jsonData =  try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
                            let decoded = try JSONSerialization.jsonObject(with: jsonData, options: []) as! NSDictionary
                            // here "decoded" is of type `Any`, decoded from JSON data
                            guard let dataArray : NSArray = decoded["data"] as? NSArray else {
                                return
                            }
                            
                            if dataArray.count > 0 {
                                 
                                self.array_MainFilters.removeAllObjects()
                                self.array_MainFilters.addObjects(from: (dataArray ) as! [Any])
                                
                                for indx_outer in 0 ..< self.array_MainFilters.count{
                                    
                                    let dict_outer : NSMutableDictionary = NSMutableDictionary(dictionary: self.array_MainFilters[indx_outer] as! NSDictionary)
                                    let array_Data : NSMutableArray = NSMutableArray(array: dict_outer["data"] as! NSArray)
                                    for indx_inner in 0 ..< array_Data.count{
                                        
                                        let dict_inner : NSMutableDictionary = NSMutableDictionary(dictionary: array_Data[indx_inner] as? NSDictionary ?? NSDictionary())
                                        dict_inner.setValue("no", forKey: "isSelected")
                                        array_Data.replaceObject(at: indx_inner, with: dict_inner)
                                    }
                                    
                                    dict_outer.setValue(array_Data, forKey: "data")
                                    self.array_MainFilters.replaceObject(at: indx_outer, with: dict_outer)
                                    
                                }
                                
                                self.tableview_Main.reloadData()
                                self.tableview_Secondary.reloadData()
                                
                            }else{
                                iToast.show("No record found.")
                            }
                            
                        } catch let error as NSError {
                            
                            print("Failed to load: \(error.localizedDescription)")
                        }
                    }
                case .failure( _):
                        //Utility.showAlert(message: error.localizedDescription, controller: self)
                        Utility.showAlert(message: "حدث خطأ - يرجي اعادة المحاولة", controller: self) //Something went wrong, please try again.
                    }
        }
    }
    
    @objc func getFiltersFromSelection(){
        
        //            let parameters : Parameters = [
        //
        //                     "cat_id" : str_catID as String,
        //                     "manufacturer" : "698,504",
        //                     "item_type" : "654,659",
        //                     "size" : "570,605"
        //
        //                 ]
        //
        
        
        let parameters : NSMutableDictionary =  NSMutableDictionary()
        parameters.setValue(str_catID, forKey: "id")
        parameters.setValue("\(Int(NSDate().timeIntervalSince1970))", forKey: "cacheignore")//To ignore cache from api...
        
        for indx_outer in 0 ..< self.array_MainFilters.count{
            
            let dict_outer : NSDictionary = self.array_MainFilters[indx_outer] as! NSDictionary
            
            let str_SelectedFilters : NSMutableString =  NSMutableString(string: "")
            let array_Data : NSArray = dict_outer["data"] as? NSArray ?? NSArray()
            for indx_inner in 0 ..< array_Data.count{
                
                let dict_Filter = array_Data[indx_inner] as! NSDictionary
                if dict_Filter["isSelected"] as? String == "yes"{
                    
                    if str_SelectedFilters == ""{
                        
                        str_SelectedFilters.append(dict_Filter["value"] as? String ?? "")
                        
                    }else{
                        
                        str_SelectedFilters.append(",\(dict_Filter["value"] as? String ?? "")")
                        
                    }
                    
                }
            }
            parameters.setValue(str_SelectedFilters, forKey: dict_outer["code"] as? String ?? "")
            
        }
        
        print("parameters----->\(parameters)")
        
        AppDelegate.showHUD(inView: self.view, message:"Loading....")
        
        Alamofire.request(Constants.apiURL + "webservice/filterapi.php", method: .post, parameters: parameters as? Parameters)
            
            .responseJSON { response in
                
                AppDelegate.hideHUD(inView: self.view)
                
                switch response.result {
                case .success:
                    
                    if let json = response.result.value{
                        
                        //print("JSON Response : \(json)")
                        guard let _ : NSDictionary = json as? NSDictionary else {
                            return
                        }
                        
                        
                        do {
                            
                            // make sure this JSON is in the format we expect
                            let jsonData =  try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
                            let decoded = try JSONSerialization.jsonObject(with: jsonData, options: []) as! NSDictionary
                            // here "decoded" is of type `Any`, decoded from JSON data
                            guard let dataArray : NSArray = decoded["data"] as? NSArray else {
                                return
                            }
                            if dataArray.count > 0 {
                                
                                self.filterAllOldDataWithLatestdata(array_NewData: dataArray)
                                
                            }else{
                                iToast.show("No record found.")
                            }
                            
                        } catch let error as NSError {
                            
                            print("Failed to load: \(error.localizedDescription)")
                        }
                    }
               case .failure( _):
                        //Utility.showAlert(message: error.localizedDescription, controller: self)
                        Utility.showAlert(message: "حدث خطأ - يرجي اعادة المحاولة", controller: self) //Something went wrong, please try again.
                    }
        }
        
    }
    
    func filterAllOldDataWithLatestdata(array_NewData : NSArray) {
        
        for indx_NewData in 0 ..< array_NewData.count{//Set selected object index in left side list, if old data counter varies from new data...
            let dct_atIndexOfNewData : NSDictionary = array_NewData[indx_NewData] as? NSDictionary ?? NSDictionary()
            if (dct_atIndexOfNewData["code"] as? NSString == str_SelectedMainCodeValue){
                self.int_SelectedMainFilterIndex = indx_NewData
            }
        }
        
        let array_tempHoldOldData = self.array_MainFilters.copy() as! NSArray
        
        self.array_MainFilters.removeAllObjects()
        
        let array_FinalDataNew : NSMutableArray = NSMutableArray(array: array_NewData)
        
        for indx_NewData in 0 ..< array_FinalDataNew.count{
            
            let dictionary_atIndexOfNewData : NSMutableDictionary = NSMutableDictionary(dictionary: array_FinalDataNew[indx_NewData] as? NSDictionary ?? NSDictionary())
            let string_CodeNew =  dictionary_atIndexOfNewData["code"] as? String ?? "" //i.e string_Code = "item_type","manufacturer","size"
            let predicate0 = NSPredicate(format: "code == %@",string_CodeNew)
            let array_filteredCodeOld : NSArray = array_tempHoldOldData.filtered(using: predicate0) as NSArray
            if array_filteredCodeOld.count > 0{
                
                let dict_tempOld0 = array_filteredCodeOld[0] as? NSDictionary ?? NSDictionary()
                let array_DataOfCodeOld =  dict_tempOld0["data"] as? NSArray ?? NSArray()
                let array_DataOfCodeNew : NSMutableArray = NSMutableArray(array: dictionary_atIndexOfNewData["data"] as? NSArray ?? NSArray())//i.e string_Code = "item_type","manufacturer","size"
                
                for indx_DataOfCodeNew in 0 ..< array_DataOfCodeNew.count{
                    
                    let dict_tempNewSingleRecord : NSMutableDictionary = NSMutableDictionary(dictionary: array_DataOfCodeNew[indx_DataOfCodeNew] as? NSDictionary ?? NSDictionary())
                    let predicate1 = NSPredicate(format: "value == %@",dict_tempNewSingleRecord["value"] as? String ?? "")
                    let array_tempfilteredValue : NSArray = array_DataOfCodeOld.filtered(using: predicate1) as NSArray
                    if array_tempfilteredValue.count > 0 {
                        let dict_tempOld1 = array_tempfilteredValue[0] as? NSDictionary ?? NSDictionary()
                        if dict_tempOld1["isSelected"] as? String == "yes"{dict_tempNewSingleRecord.setValue("yes", forKey: "isSelected")}else{dict_tempNewSingleRecord.setValue("no", forKey: "isSelected")}
                    }else{dict_tempNewSingleRecord.setValue("no", forKey: "isSelected")}
                    array_DataOfCodeNew.replaceObject(at: indx_DataOfCodeNew, with: dict_tempNewSingleRecord)
                    
                }
                
                dictionary_atIndexOfNewData.setValue(array_DataOfCodeNew, forKey: "data")//Replace record after adding isSelected key to every record in "data" array...
                
            }
            
            array_FinalDataNew.replaceObject(at: indx_NewData, with: dictionary_atIndexOfNewData)
            
        }
        
        array_MainFilters.addObjects(from: array_FinalDataNew as! [Any])//Add New Data from changing old selections from old record...
        
        self.tableview_Main.reloadData()
        self.tableview_Secondary.reloadData()
    }
    
    
    //MARK: - UITableView DataSource
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 40
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView.tag == 0 {
            return array_MainFilters.count
        }else{
            
            
            if self.array_MainFilters.count == 0{
                return 0
            }
            
            guard let dictAddress = self.array_MainFilters[self.int_SelectedMainFilterIndex] as? NSDictionary else {
                return 0
            }
            
            let array_Data = dictAddress["data"] as? NSArray ?? NSArray()
            
            return array_Data.count
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView.tag == 0{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "mainFilterCell", for: indexPath) as! MainFilterCell
            let dict_Address = array_MainFilters[indexPath.row] as? NSDictionary
            
            if let dataArray = dict_Address?["data"] as? NSArray {
                
                let resultPredicate = NSPredicate(format: "isSelected contains[c] %@", "yes" )
                let array_filtered = dataArray.filtered(using: resultPredicate)
                if array_filtered.count == 0{
                    cell.label_Name.text = "\(dict_Address?["name"] as! String)"
                }else{
                    cell.label_Name.text = "(\(array_filtered.count)) \(dict_Address?["name"] as! String)"
                }
                
            }
            
            if indexPath.row  == int_SelectedMainFilterIndex {
                
                cell.label_Name.textColor = Constants.default_ThemeColor
                cell.contentView.backgroundColor = UIColor.white
                
            }else{
                
                cell.label_Name.textColor = UIColor(red: 42/255, green: 42/255, blue: 42/255, alpha: 1.0)
                cell.contentView.backgroundColor = UIColor(red: 245 / 255.0,green: 245 / 255.0,blue: 245 / 255.0,alpha: CGFloat(1.0))
                
            }
            cell.separatorInset = UIEdgeInsets.zero
            return cell
            
            
        }else{
            
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "secondaryFilterCell", for: indexPath) as! SecondaryFilterCell
            //let dict_Address = array_SecondaryFilters[indexPath.row] as? NSDictionary
            let dict = self.array_MainFilters[self.int_SelectedMainFilterIndex] as? NSDictionary
            let array_Data = dict?["data"] as! NSArray
            let dict_Filter = array_Data[indexPath.row] as! NSDictionary
            
            
            cell.label_Name.text = dict_Filter["display"] as? String
            if dict_Filter["isSelected"] as? String == "yes"{
                
                cell.imageview_tickUntick.image = UIImage(named: "checkfilter")
                cell.label_Name.textColor = Constants.default_ThemeColor
                
            }else{
                
                cell.imageview_tickUntick.image = UIImage(named: "uncheckfilter")
                cell.label_Name.textColor = UIColor(red: 42/255, green: 42/255, blue: 42/255, alpha: 1.0)
                
            }
            
            
            cell.contentView.backgroundColor = UIColor.white
            cell.separatorInset = UIEdgeInsets.zero
            return cell
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView.tag == 0{
            
            //guard let dictAddress = array_MainFilters[indexPath.row] as? NSDictionary else { return }
            //guard let data = dictAddress["data"] as? NSArray else { return }
            
            int_SelectedMainFilterIndex = indexPath.row
            let dict_MainRecord : NSDictionary = self.array_MainFilters[int_SelectedMainFilterIndex] as! NSDictionary
            str_SelectedMainCodeValue = dict_MainRecord["code"] as? NSString ?? ""
            //array_SecondaryFilters.removeAllObjects()
            //array_SecondaryFilters.addObjects(from: data as! [Any])
            
        }else{
            
            
            let dict_outer : NSMutableDictionary = NSMutableDictionary(dictionary: self.array_MainFilters[int_SelectedMainFilterIndex] as! NSDictionary)
            let array_Data : NSMutableArray = NSMutableArray(array: dict_outer["data"] as! NSArray)
            
            let dict_inner : NSMutableDictionary = NSMutableDictionary(dictionary: array_Data[indexPath.row] as! NSDictionary)
            if dict_inner["isSelected"] as? String == "yes"{
                
                dict_inner.setValue("no", forKey: "isSelected")
                
            }else{
                
                dict_inner.setValue("yes", forKey: "isSelected")
                
            }
            array_Data.replaceObject(at: indexPath.row, with: dict_inner)
            
            dict_outer.setValue(array_Data, forKey: "data")
            self.array_MainFilters.replaceObject(at: int_SelectedMainFilterIndex, with: dict_outer)
            
            
            self.perform(#selector(getFiltersFromSelection), with: nil, afterDelay: 0.5)
            
            
        }
        
        tableview_Main.reloadData()
        tableview_Secondary.reloadData()
        
    }
    
    
    
    
    //MARK: UIButton Clicks
    @IBAction func button_applyFilterClicked(_ sender: UIButton) {
        
        if (self.array_MainFilters.count == 0){return}
        
        let  array_filteredToBeApplied : NSMutableArray = NSMutableArray()
        
        for indx_outer in 0 ..< self.array_MainFilters.count{
            
            let dict_outer : NSMutableDictionary = NSMutableDictionary(dictionary: self.array_MainFilters[indx_outer] as! NSDictionary)
            let array_Data =  dict_outer["data"] as! NSArray
            
            let ary_ToBeAdd : NSMutableArray = NSMutableArray()
            for indx_inner in 0 ..< array_Data.count{
                
                let dict_inner : NSMutableDictionary = NSMutableDictionary(dictionary: array_Data[indx_inner] as! NSDictionary)
                if dict_inner["isSelected"] as? String == "yes"{
                    ary_ToBeAdd.add(dict_inner)
                }
                
            }
            
            dict_outer.setValue(ary_ToBeAdd, forKey: "data")
            
            array_filteredToBeApplied.add(dict_outer)
            
        }
        
        if array_filteredToBeApplied.count == 0 {
            return
        }
        
        self.navigationController?.popViewController(animated: false)
        self.delegate_Filter?.fileteredAppliedSuccessfully(array_filteredToBeApplied: array_filteredToBeApplied, array_MainFiltersFromFilterScreen:self.array_MainFilters as NSArray)
        
    }
    
    
    @IBAction func button_clearFilterClicked(_ sender: UIButton) {
        
        
        str_SelectedMainCodeValue = ""
        int_SelectedMainFilterIndex = 0
        
        self.getInitialAllFiltersAPICalled()
        
        //        if (self.array_MainFilters.count == 0){return}
        //
        //        int_SelectedMainFilterIndex = 0
        //
        //
        //        for indx_outer in 0 ..< self.array_MainFilters.count{
        //
        //            let dict_outer : NSMutableDictionary = NSMutableDictionary(dictionary: self.array_MainFilters[indx_outer] as! NSDictionary)
        //            let array_Data : NSMutableArray = NSMutableArray(array: dict_outer["data"] as! NSArray)
        //            for indx_inner in 0 ..< array_Data.count{
        //
        //                let dict_inner : NSMutableDictionary = NSMutableDictionary(dictionary: array_Data[indx_inner] as! NSDictionary)
        //                dict_inner.setValue("no", forKey: "isSelected")
        //                array_Data.replaceObject(at: indx_inner, with: dict_inner)
        //
        //            }
        //
        //            dict_outer.setValue(array_Data, forKey: "data")
        //            self.array_MainFilters.replaceObject(at: indx_outer, with: dict_outer)
        //
        //        }
        //
        //        self.tableview_Main.reloadData()
        //        self.tableview_Secondary.reloadData()
    }
    
    
}




