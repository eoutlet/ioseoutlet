import Alamofire
import IQKeyboardManagerSwift
import Reachability
import Firebase
import FirebaseMessaging
import UIKit
import AppsFlyerLib
import FirebaseCrashlytics

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate, MessagingDelegate,AppsFlyerTrackerDelegate {
    
    
    
    var window: UIWindow?
    var application_AppDelegate: UIApplication?
    static let shared = AppDelegate()
    var reachability: Reachability = try! Reachability()
    var data_AppsFlyerCompaignData : NSMutableDictionary = NSMutableDictionary()
    static var cart_item_count : Int = 0
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
     
        self.AllConfigurations()

        let decoded = UserDefaults.standard.value(forKey: "isFirstTimeRegisterNotificationScreenAppeared") as? String
        if decoded != "yes"{//If user is very first time opening app, then screen for notification will appear...
            let initialViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "notificationAllowVC")
            self.window?.rootViewController = initialViewController
        }


        self.setupReachability()
        self.application_AppDelegate = application
        self.checkAnyPushNotification(launchOptions: launchOptions)
        return true
        
        
    }
    
    //MARK: All Configurations
    func AllConfigurations(){
      
        // Use Firebase library to configure APIs
        FirebaseApp.configure()
        
        
        //For data message (sent via FCM)
        Messaging.messaging().delegate = self
        
        IQKeyboardManager.shared.enable = true
        // Crashlytics.sharedInstance().crash()
        
        AppsFlyerTracker.shared().appsFlyerDevKey = "Qy2rqKhCQPVNLdyt7Rb5ra"
        AppsFlyerTracker.shared().appleAppID = "1484601181" //Eoutlet app id...
        AppsFlyerTracker.shared().delegate = self

        /* Set isDebug to true to see AppsFlyer debug logs */
        AppsFlyerTracker.shared().isDebug = true
        //Override point for customization after application launch.
        
        let userDefaults = UserDefaults.standard
        let decoded : Data = userDefaults.data(forKey: "loggedInUserRecord") ?? Data()
        do {
            if let decodedData : Dictionary<String, Any> = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? Dictionary<String, Any> {
                loggedInUser.shared.initializeUserDetails(dicuserdetail: decodedData)
            }
        }
        catch {
            print("Couldn't unarchive data")
        }
        
        self.getAllCartItemAPICalled()
        
    }
    
    //MARK: CheckAnyPushNotification In App Killed State
    func checkAnyPushNotification(launchOptions: [UIApplication.LaunchOptionsKey: Any]?){
        
        if let option = launchOptions {
            let info = option[UIApplication.LaunchOptionsKey.remoteNotification]
            if (info != nil) {
                if let notif_Data = info as? NSDictionary{
                    /*
                     Need to check "id" and "name" keys are existing or not in notification payload. If not, then no need to reidect on product list page...
                     */
                    if let id_Prod =  (notif_Data["id"] as? String), let name_Prod = (notif_Data["name"] as? String) {
                        if !id_Prod.isEmpty || !name_Prod.isEmpty{
                            self.redirectToProductPageFromPushNotification(notificationData: info as! NSDictionary)
                        }
                    }
                }
            }
        }
    }
    
    
    //MARK: Registration Remote Notification
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
      
        //Sent APNs token from Apple to FCM Server
        Messaging.messaging().apnsToken = deviceToken
        
        let deviceToken: String = deviceToken.map { String(format: "%02.2hhx", $0) }.joined()
        print("Device token is: \(deviceToken)")

    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError: Error) {
     
    }
    func applicationDidFailToRegisterUserNotificationSettingsNotification(notification: NSNotification) {
        // set button status to 'failed'
    }
    //MARK: Recieve FCM Token
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
        
        print("Firebase registration token: \(fcmToken)")
        // let dataDict:[String: String] = ["token": fcmToken]
        //NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
        // Note: This callback is fired at each app startup and whenever a new token is generated.
        // If necessary send token to application server.
        self.sendFCMTokenToServer(deviceID: "\(UIDevice.current.identifierForVendor?.uuidString ?? "")", fcmToken: "\(fcmToken)")
        
    }
    
    //MARK: API Calling...
    func getAllCartItemAPICalled(){
        let userDefaults = UserDefaults.standard
        let decoded : Data = userDefaults.data(forKey: "isguestMaskKeyAndCardIdFetched") ?? Data()
        var str_cartID = ""
        do {
            if let decodedData : Dictionary<String, Any> = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? Dictionary<String, Any> {
                if let crtId =  decodedData["cart_id"]{
                    str_cartID = String(format:"\(crtId)")
                }
            }
        }
        catch {
            print("Couldn't unarchive data")
        }
        
        var str_maskKey = ""
        do {
            if let decodedData : Dictionary<String, Any> = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? Dictionary<String, Any> {
                if let mskKey =  decodedData["mask_key"]{
                    str_maskKey = String(format:"\(mskKey)")
                }
            }
        }
        catch {
            print("Couldn't unarchive data")
        }
        
        var str_userID = ""
        if !loggedInUser.shared.string_userID.isEmpty && loggedInUser.shared.string_userID.count != 0{
            str_userID = String(format:"\(loggedInUser.shared.string_userID)")
            str_cartID = ""
            str_maskKey = ""
        }
        
        let parameters : Parameters = [
            "customer_id" : str_userID,
            "cart_id" : str_cartID,
            "mask_key" : str_maskKey,
            "cacheignore" : "\(Int(NSDate().timeIntervalSince1970))" //To ignore cache from api...
        ]
        
        Alamofire.request(Constants.apiURL + "webservice/viewcart.php", method: .post, parameters: parameters)
            .responseJSON{ response in
                switch response.result {
                case .success:
                    if let json = response.result.value{
                        guard let _ : NSDictionary = json as? NSDictionary else {
                            return
                        }
                        do {
                            // make sure this JSON is in the format we expect
                            let jsonData = try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
                            let decoded = try JSONSerialization.jsonObject(with: jsonData, options: []) as! NSDictionary
                            guard let dataArray : NSArray = decoded["data"] as? NSArray else {
                                return
                            }
                            
                            let array_CartItem = NSMutableArray()
                            if dataArray.count > 0{
                                array_CartItem.addObjects(from: dataArray as! [Any])
                                var total_ItemInCart = 0
                                for indx in 0 ..< array_CartItem.count{
                                    let dict_Product = array_CartItem[indx] as! NSDictionary
                                    if let intValue = (dict_Product["qty"] as? Int) {
                                        total_ItemInCart = total_ItemInCart + intValue
                                    }
                                }
                                AppDelegate.cart_item_count = total_ItemInCart
                            }
                            else {
                            }
                            
                        }
                        catch let error as NSError {
                            print("Failed to load: \(error.localizedDescription)")
                        }
                    }
                case .failure( _):
                    break// Utility.showAlert(message: error.localizedDescription, controller: self)
                }
        }
    }
    
    func sendFCMTokenToServer(deviceID: String, fcmToken: String){
      
        let parameters : Parameters = [
            "device_id" : deviceID,
            "fcm_token" : fcmToken,
            "devicetype" : "ios",
            "cacheignore" : "\(Int(NSDate().timeIntervalSince1970))" //To ignore cache from api...
        ]
        
        Alamofire.request(Constants.apiURL + "webservice/deviceregister.php", method: .post, parameters: parameters)
            .responseJSON { response in
                switch response.result {
                case .success:
                    if let json = response.result.value{
                        guard let _ : NSDictionary = json as? NSDictionary else {
                            return
                        }
                        do {
                            //Make sure this JSON is in the format we expect
                            let jsonData =  try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
                            let decoded = try JSONSerialization.jsonObject(with: jsonData, options: []) as! NSDictionary
                            if decoded["msg"] as? String == "success"{
                                
                            }
                        }
                        catch let error as NSError {
                            print("Failed to load: \(error.localizedDescription)")
                        }
                    }
                case .failure(let error):
                    print("Failed to load: \(error.localizedDescription)")
                }
        }
    }
    
    //MARK: Recieve Remote Notification
   
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: (UNNotificationPresentationOptions) -> Void){
        /*
         let userInfo = notification.request.content.userInfo
         // Print full message.
         print(userInfo) */
        
        // Change this to your preferred presentation option
        completionHandler(UNNotificationPresentationOptions.alert)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        //  Handle data of notification
        
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        
        // Print message ID.
        //  if let messageID = userInfo[gcmMessageIDKey] {
        //    print("Message ID: \(messageID)")
        //}
        
        guard let notif_Data = userInfo as? NSDictionary else {
            return
        }
        /*
         Need to check "id" and "name" keys are existing or not in notification payload. If not, then no need to reidect on product list page...
         */
        guard let id_Prod =  (notif_Data["id"] as? String) else {
            return
        }
        guard let name_Prod = (notif_Data["name"] as? String) else {
            return
        }
        
        if id_Prod.isEmpty || name_Prod.isEmpty{
            return
        }
        
        self.redirectToProductPageFromPushNotification(notificationData: notif_Data)
        
        //        print(notif_Data)
        //        print(notif_Data["id"] ?? "")
        //        print(notif_Data["name"] ?? "")
    }
    
    func redirectToProductPageFromPushNotification(notificationData: NSDictionary){
        
        if let data : NSDictionary = notificationData as NSDictionary?{
            print("\(data)")
            
            let deadlineTime = DispatchTime.now() + .seconds(3)
            DispatchQueue.main.asyncAfter(deadline: deadlineTime) {
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                
                // instantiate the view controller from storyboard
                if  let tabBarController = storyboard.instantiateViewController(withIdentifier: "tabBarVCViewController") as? TabBarVCViewController {
                    // set the view controller as root
                    self.window?.rootViewController = tabBarController
                    tabBarController.selectedIndex = 4
                    
                    if let navController = tabBarController.selectedViewController as? UINavigationController {
                        
                        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "productListVC") as! ProductListVC
                        let dict_Cat : NSMutableDictionary =  NSMutableDictionary() //id, name fetching from data set in appsflyer's account for particular compaign...(i.e Attribution Link Parameters)
                        dict_Cat.setValue("\(data["id"] as? String ?? "")", forKey: "id")
                        dict_Cat.setValue("\(data["name"] as? String ?? "")", forKey: "name")
                        vc.dict_subCategory = dict_Cat
                        let string_Children =  data["children"] as? String ?? ""
                        let array_ChildrenFromAppsFlyer =  string_Children.components(separatedBy: "|")
                        if(string_Children != "" && array_ChildrenFromAppsFlyer.count > 0 ){
                            
                            let array_ChildrenFinal : NSMutableArray = NSMutableArray()
                            for indx in 0 ..< array_ChildrenFromAppsFlyer.count  {
                                
                                let dict0 : NSMutableDictionary = NSMutableDictionary()
                                let indexData = array_ChildrenFromAppsFlyer[indx] as  String
                                let array_IdAndName =  indexData.components(separatedBy: ",")
                                
                                dict0.setValue(array_IdAndName[0], forKey: "id")//Default
                                dict0.setValue(array_IdAndName[1], forKey: "name")
                                array_ChildrenFinal.add(dict0)
                            }
                            vc.array_HeaderSubCategorytList.addObjects(from: array_ChildrenFinal as! [Any])
                        }
                        navController.pushViewController(vc, animated: false)
                    }
                }
            }
        }
    }
    
    //MARK: App States
    func applicationWillResignActive(_ application: UIApplication) {
        //Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        //Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        var bgTask: UIBackgroundTaskIdentifier = UIBackgroundTaskIdentifier(rawValue: 0)
        bgTask = application.beginBackgroundTask(expirationHandler: {
            application.endBackgroundTask(bgTask)
            bgTask = UIBackgroundTaskIdentifier.invalid
        })
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        //Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        //Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        AppsFlyerTracker.shared().trackAppLaunch()
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        
    }
    
    //MARK: REACHABILITY
    func setupReachability(){
        //Declare this inside of viewWillAppear...
        NotificationCenter.default.addObserver(self, selector: #selector(reachabilityChanged(note:)), name: .reachabilityChanged, object: reachability)
        do {
            try reachability.startNotifier()
        }
        catch {
            print("could not start reachability notifier")
        }
    }
    
    @objc func reachabilityChanged(note: Notification) {
        
        let reachability = note.object as! Reachability
        
        switch reachability.connection {
        case .wifi:
            print("Reachable via WiFi")
        case .cellular:
            print("Reachable via Cellular")
        case .none:
            iToast.show("الشبكة غير متوفرة")
            print("Network not reachable")
        case .unavailable:
            iToast.show("الشبكة غير متوفرة")
            print("Network not reachable")
        }
    }
    
    
    //MARK: - AppsFlyerTrackerDelegate
    func onConversionDataSuccess(_ installData: [AnyHashable: Any]) {
        //  Utility.showAlert(message: "onConversionDataSuccess", controller: self.window?.rootViewController ?? UIViewController())
        
        /*
         guard let first_launch_flag = installData["is_first_launch"] as? Int else {
         return
         }
         
         guard let status = installData["af_status"] as? String else {
         return
         }
         
         if(first_launch_flag == 1) {
         
         if(status == "Non-organic") {
         
         if let media_source = installData["media_source"] ,
         
         let campaign = installData["campaign"]{
         print("This is a Non-Organic install. Media source: \(media_source) Campaign: \(campaign)")
         }
         
         } else {
         
         print("This is an organic install.")
         }
         
         } else {
         
         print("Not First Launch")
         
         }
         */
    }
    
    func onConversionDataFail(_ error: Error!) {
        if let err = error{
            print(err)
        }
    }
    
    func onAppOpenAttribution(_ attributionData: [AnyHashable : Any]!) {//when user clicks any appsflyer ad, then this delegate will call when app's instance is terminated/killed. Otherwise same delegate in "HomeVC.swift" screen will call when app's instance is alive...
        
        if let data : NSDictionary = attributionData as NSDictionary?{
            print("\(data)")
            self.data_AppsFlyerCompaignData  = data.mutableCopy() as! NSMutableDictionary
        }
    }
    
    func onAppOpenAttributionFailure(_ error: Error!) {
        if let err = error{
            print(err)
        }
    }
    
    // Reports app open from a Universal Link for iOS 9 or later
    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {
        
        //First, you verify that the passed-in userActivity has expected characteristics. Ultimately, you want to get the path component for the activity. Otherwise, you return false to indicate that the app can’t handle the activity
        // 1
        guard userActivity.activityType == NSUserActivityTypeBrowsingWeb,
            let url = userActivity.webpageURL,
            let _ = URLComponents(url: url, resolvingAgainstBaseURL: true) else {
                return false
        }
        ////        // 2
        ////         if let computer = ItemHandler.sharedInstance.items
        ////           .filter({ $0.path == components.path}).first {
        ////           presentDetailViewController(computer)
        ////           return true
        ////         }
        //         // 3
        //         if let webpageUrl = URL(string: "http://rw-universal-links-final.herokuapp.com") {
        //           application.open(webpageUrl)
        //           return false
        //         }
        AppsFlyerTracker.shared().continue(userActivity, restorationHandler: restorationHandler as? ([Any]?) -> Void)
        return true
    }
    
    //     func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([Any]?) -> Void) -> Bool {
    //      AppsFlyerTracker.shared().continue(userActivity, restorationHandler: restorationHandler)
    //      return true
    //    }
    //
    //
    
    // Reports app open from deep link from apps which do not support Universal Links (Twitter) and for iOS8 and below
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        AppsFlyerTracker.shared().handleOpen(url, sourceApplication: sourceApplication, withAnnotation: annotation)
        return true
    }
    
    // Reports app open from deep link for iOS 10 or later
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        // Utility.showAlert(message: "application", controller: self.window?.rootViewController ?? UIViewController())
        AppsFlyerTracker.shared().handleOpen(url, options: options)
        
        //        return application(app, open: url,
        //                                sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String,
        //                                annotation: "")
        return true
    }
}


