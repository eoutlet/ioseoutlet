//
//  TrackOrderVC.swift
//  Eoutlet
//
//  Created by Unyscape Infocom on 06/02/20.
//  Copyright © 2020 Unyscape. All rights reserved.
//

import UIKit
import WebKit
import Alamofire

//track order tableview cell
class TrackOrderTableViewCell : UITableViewCell {
    @IBOutlet weak var cellUIView: UIView!
    @IBOutlet weak var webView: WKWebView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
  
//track order view controler
class TrackOrderVC: UIViewController, UITableViewDataSource, UITableViewDelegate, WKNavigationDelegate, WKUIDelegate, UIGestureRecognizerDelegate  {
    @IBOutlet weak var trackOrderTableView: UITableView!
    var refreshControl = UIRefreshControl()
    var array_TrackOrderList : NSMutableArray = NSMutableArray()
    
    var arrayOfBool:[Bool] = NSMutableArray() as! [Bool]
    var contentHeights : [CGFloat] = NSMutableArray() as! [CGFloat]
    let cell_Height = 260
    
    override func viewDidLoad() {  
        super.viewDidLoad()
                
        let headerNib = UINib.init(nibName: "TrackOrderHeaderUIView", bundle: Bundle.main)
        trackOrderTableView.register(headerNib, forHeaderFooterViewReuseIdentifier: "TrackOrderHeaderUIView")
        
        //pull to refresh
        refreshControl.addTarget(self, action: #selector(pullToRefreshTableView), for: UIControl.Event.valueChanged)
        trackOrderTableView.addSubview(refreshControl)
        
        self.getTrackOrderAPICalled()
    }
    
      override func viewWillAppear(_ _animated: Bool) {
          super.viewWillAppear(_animated)
          self.navigationController?.navigationBar.isHidden = true
      }
      
      //MARK: - UIButton Clicks
      @IBAction func backButtonClicked(_ sender: UIButton) {
          self.navigationController?.popViewController(animated: false)
      }
    
    @IBAction func buttonSearchClicked(_ sender: UIButton) {
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "searchVC") as! SearchVC
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    //MARK: API Calling...
    func getTrackOrderAPICalled(){
        let parameters : Parameters = [
            "customer_id" : loggedInUser.shared.string_userID,
            "cacheignore" : "\(Int(NSDate().timeIntervalSince1970))" //To ignore cache from api...
        ]
        
        AppDelegate.showHUD(inView: self.view, message:"")
        Alamofire.request(Constants.apiURL+"webservice/getcustomertrackingorder.php", method: .post, parameters: parameters)
            .responseJSON { response in
                AppDelegate.hideHUD(inView: self.view)
                switch response.result {
                case .success:
                    if let json = response.result.value{
                        //print("JSON Response : \(json)")
                        guard let _ : NSDictionary = json as? NSDictionary else {
                            return
                        }
                        do {
                            // make sure this JSON is in the format we expect
                            let jsonData =  try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
                            let decoded = try JSONSerialization.jsonObject(with: jsonData, options: []) as! NSDictionary
                            // here "decoded" is of type `Any`, decoded from JSON data
                            guard let dataArray : NSArray = decoded["data"] as? NSArray else {
                                return
                            }
                            
                            if dataArray.count > 0 {
                                //print(dataArray)
                                self.array_TrackOrderList.removeAllObjects()
                                self.arrayOfBool.removeAll()
                                self.contentHeights.removeAll()
                                dataArray.forEach({ (value) in
                                    self.arrayOfBool.append(true)
                                    self.contentHeights.append(0.0)
                                })
                                self.array_TrackOrderList.addObjects(from: (dataArray ) as! [Any])
                               
                                self.trackOrderTableView.reloadData {
                                    self.refreshControl.endRefreshing()
                                }
                            }
                            else{
                                //iToast.show("لايوجد نتائج للبحث")
                            }
                        }
                        catch let error as NSError {
                            print("Failed to load: \(error.localizedDescription)")
                        }
                    }
                case .failure( _):
                    //Utility.showAlert(message: error.localizedDescription, controller: self)
                    Utility.showAlert(message: "حدث خطأ - يرجي اعادة المحاولة", controller: self) //Something went wrong, please try again.
                }
        }
    }
    
    @objc func pullToRefreshTableView(sender:AnyObject) {
        self.getTrackOrderAPICalled()
    }
    
    //set the number of section of tableview
    func numberOfSections(in tableView: UITableView) -> Int {
        return array_TrackOrderList.count
    }
    
    //set the number of row as per section
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if arrayOfBool[section] == true {
            return 0
        }
        else {
            return 1
        }
    }
    
    //configure the tableview row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "trackOrderTableViewCell", for: indexPath) as! TrackOrderTableViewCell
        //        //row shadow
        //        let shadowSize : CGFloat = 5.0
        //        let shadowPath = UIBezierPath(rect: CGRect(x: Int(-shadowSize / 2),
        //                                                   y: 2,
        //                                                   width: Int(self.view.frame.size.width),
        //                                                   height: cell_Height-10))
        //        cell.cellUIView.layer.masksToBounds = false
        //        cell.cellUIView.layer.shadowColor = UIColor.lightGray.cgColor
        //        cell.cellUIView.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        //        cell.cellUIView.layer.shadowOpacity = 0.5
        //        cell.cellUIView.layer.shadowRadius = 5.0
        //        cell.cellUIView.layer.shadowPath = shadowPath.cgPath
        cell.webView.navigationDelegate = self
        cell.webView.uiDelegate = self
        cell.webView.scrollView.isScrollEnabled = false
        cell.webView.accessibilityLabel = "\(indexPath.section)"
        
        DispatchQueue.main.async {
            let orderDict : NSDictionary = self.array_TrackOrderList[indexPath.section] as! NSDictionary
            if orderDict["tracking_url"] as? String ?? "" != "" {
                cell.webView.load(URLRequest.init(url: URL(string: (orderDict["tracking_url"] as? String)!)!))
            }
        }
        return cell
    }
    
    //set tableview row height as per device like iphone or ipad
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return contentHeights[indexPath.section]
    }
    
    //did select action when user click on tableview cell
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    
    //table header view
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView : TrackOrderHeaderUIView = trackOrderTableView.dequeueReusableHeaderFooterView(withIdentifier: "TrackOrderHeaderUIView") as! TrackOrderHeaderUIView
        headerView.clipsToBounds = true
       // headerView.backgroundColor = UIColor.white

        let orderDict : NSDictionary = array_TrackOrderList[section] as! NSDictionary
        headerView.orderNumberLbl.text = "طلب رقم : \(orderDict["increment_order_id"] as? String ?? "")" //Order NO
        headerView.amountLbl.text = "SAR \(orderDict["total_amount"] as? Int ?? 0)" //Total Amount
        
        headerView.accessibilityLabel = "\(section)"
        let tap = UITapGestureRecognizer(target: self, action:#selector(self.handleTap(_:)))
        tap.delegate = self
        headerView.addGestureRecognizer(tap)
        
        return headerView
    }
      
    //set tableview header height as per device like iphone or ipad
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 95
    }
    
      //table view header view action
      @objc func handleTap(_ sender: UITapGestureRecognizer) {
          guard let accessibilityLabel = sender.view?.accessibilityLabel else { return }
        
        let orderDict : NSDictionary = array_TrackOrderList[Int(accessibilityLabel)!] as! NSDictionary
        if orderDict["tracking_url"] as? String ?? "" != "" {
            if arrayOfBool[Int(accessibilityLabel)!] == true {
                for i in 0..<arrayOfBool.count {  // set the bool value false (only one section will expand at a time)
                    if Int(accessibilityLabel)! == i {
                        arrayOfBool[Int(accessibilityLabel)!] = false
                    }
                    else {
                        arrayOfBool[i] = true
                    }
                }
            }
            else{
                arrayOfBool[Int(accessibilityLabel)!] = true // this condition called when user click on expanded tableview section preview button
            }
            // reload tableview
            self.trackOrderTableView.reloadData {
                //reload completed
                DispatchQueue.main.async {
                    if self.arrayOfBool[Int(accessibilityLabel)!] == false {
                        //scroll to top the sected section after reloading the tableview
                        self.trackOrderTableView.scrollToRow(at: IndexPath(row: 0, section: Int(accessibilityLabel)!), at: .top, animated: true)
                    }
                }
            }
        }
        else {
            Utility.showAlert(message: "حدث خطأ - يرجي اعادة المحاولة", controller: self)//Something went wrong, please try again.
        }
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        AppDelegate.hideHUD(inView: self.view)
        if (contentHeights[Int(webView.accessibilityLabel!)!] >= 20.0) {
            // we already know height, no need to reload cell
            return
        }
        
        print(webView.scrollView.contentSize.height)
        contentHeights[Int(webView.accessibilityLabel!)!] = webView.scrollView.contentSize.height
          
//        trackOrderTableView.reloadData {
//            AppDelegate.hideHUD(inView: self.view)
//        }
        
          DispatchQueue.main.async {
            self.trackOrderTableView.reloadRows(at: [(NSIndexPath(row: 0, section: Int(webView.accessibilityLabel!)!) as IndexPath)], with: .automatic)
        }
    }
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        AppDelegate.showHUD(inView: self.view, message: "")
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        AppDelegate.hideHUD(inView: self.view)
    }
}
