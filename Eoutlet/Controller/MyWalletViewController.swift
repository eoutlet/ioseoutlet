//
//  MyWalletViewController.swift
//  Eoutlet
//
//  Created by Unyscape Infocom on 18/03/20.
//  Copyright © 2020 Unyscape. All rights reserved.
//

import UIKit
import Alamofire

class MyWalletTableViewCell : UITableViewCell {
    
    @IBOutlet weak var view_backgroundForAllDataHolder: UIView!
    @IBOutlet weak var label_OrderId: UILabel!
    @IBOutlet weak var label_Amount: UILabel!
    @IBOutlet weak var label_Action: UILabel!
    @IBOutlet weak var label_CreatedAt: UILabel!
}

class MyWalletViewController: UIViewController, UITextFieldDelegate, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet weak var myWalletAmountLbl: UILabel!
    @IBOutlet weak var walletTableView: UITableView!
    @IBOutlet weak var amountTextField: UITextField!
    var array_WalletTransactionList : NSMutableArray = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        amountTextField.delegate = self
        walletTableView.tableFooterView = UIView()
      
    }
    
    
    override func viewWillAppear(_ _animated: Bool) {
        super.viewWillAppear(_animated)
        self.navigationController?.navigationBar.isHidden = true
        self.getMyWalletAPICalled()
    }
    
    @IBAction func buttonSearchClicked(_ sender: UIButton) {
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "searchVC") as! SearchVC
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let invalidCharacters = CharacterSet(charactersIn: "0123456789").inverted
        return string.rangeOfCharacter(from: invalidCharacters) == nil
    }
    
    //MARK: - UIButton Clicks
    @IBAction func backButtonClicked(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func addMoneyButtonClicked(_ sender: Any) {
//
//        let amountStr = self.amountTextField.text
//        if amountStr != "" {
//            if Int(amountStr!)! > 0 {
//                let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "walletPaymentOptionsVC") as! WalletPaymentOptionsVC
//                vc.final_amount = amountStr!
//                self.navigationController?.pushViewController(vc, animated: false)
//            }
//        }
      
           let str_grandValue : NSString = NSString(string: self.amountTextField.text ?? "")
          if str_grandValue.integerValue > 0 {
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "walletPaymentOptionsVC") as! WalletPaymentOptionsVC
            vc.amount_Final = str_grandValue.integerValue
            self.navigationController?.pushViewController(vc, animated: false)
         }
                
           
        
    }
    
    //MARK: API Calling...
    func getMyWalletAPICalled(){
        
        let parameters : Parameters = [
            "customer_id" : loggedInUser.shared.string_userID,
            "cacheignore" : "\(Int(NSDate().timeIntervalSince1970))" //To ignore cache from api...
        ]
        
        AppDelegate.showHUD(inView: self.view, message:"")
        Alamofire.request(Constants.apiURL+"webservice/getcustomerwallet.php", method: .get, parameters: parameters)
            .responseJSON { response in
                AppDelegate.hideHUD(inView: self.view)
                switch response.result {
                case .success:
                    if let json = response.result.value{
                        //print("JSON Response : \(json)")
                        guard let _ : NSDictionary = json as? NSDictionary else {
                            return
                        }
                        do {
                            // make sure this JSON is in the format we expect
                            let jsonData =  try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
                            let decoded = try JSONSerialization.jsonObject(with: jsonData, options: []) as! NSDictionary
                            
                            guard let dataDict : NSDictionary = decoded["data"] as? NSDictionary else {
                                return
                            }
                            
                            if let intValue = (dataDict["wallet_amount"] as? Int) {
                                self.myWalletAmountLbl.text = String(format: "SAR %d", intValue)
                            }
                            else if let strValue = (dataDict["wallet_amount"] as? String){
                                self.myWalletAmountLbl.text = String(format: "SAR %d", (strValue.replacingOccurrences(of: ",", with: "") as NSString).integerValue)
                            }
                                                        
                            // here "decoded" is of type `Any`, decoded from JSON data
                            guard let dataArray : NSArray = dataDict["transactions"] as? NSArray else {
                                return
                            }
                            
                            if dataArray.count > 0 {
                                //print(dataArray)
                                self.array_WalletTransactionList.removeAllObjects()
                                self.array_WalletTransactionList.addObjects(from: (dataArray ) as! [Any])
                                self.walletTableView.reloadData {}
                            }
                            else{
                                //iToast.show("لايوجد نتائج للبحث")
                            }
                        }
                        catch let error as NSError {
                            print("Failed to load: \(error.localizedDescription)")
                        }
                    }
                case .failure( _):
                    //Utility.showAlert(message: error.localizedDescription, controller: self)
                    Utility.showAlert(message: "حدث خطأ - يرجي اعادة المحاولة", controller: self) //Something went wrong, please try again.
                }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.array_WalletTransactionList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : MyWalletTableViewCell = walletTableView.dequeueReusableCell(withIdentifier: "myWalletTableViewCell") as! MyWalletTableViewCell
        
        
       
        let walletDict : NSDictionary = array_WalletTransactionList[indexPath.row] as! NSDictionary
        
        if let intValue = (walletDict["amount"] as? Int) {
            cell.label_Amount.text = String(format: "SAR %d", intValue)
        }
        else if let strValue = (walletDict["amount"] as? String){
            cell.label_Amount.text = String(format: "SAR %d", (strValue.replacingOccurrences(of: ",", with: "") as NSString).integerValue)
        }
        
        cell.label_CreatedAt.text = walletDict["create_at"] as? String ?? ""
        
        let ordr_Id = walletDict["order_id"] as? String ?? ""
        cell.label_OrderId.text = "ORDER # " + ordr_Id
        
        cell.label_Action.text =  walletDict["action"] as? String ?? ""
        
        
        if let intValue = (walletDict["action_flag"] as? Int) {
            if intValue == 0 { //0 for credit, 1 for debit
                cell.label_Action.textColor = UIColor.init(red: 52/255, green: 196/255, blue: 124/255, alpha: 1.0)
            }
            else {
                cell.label_Action.textColor = UIColor.init(red: 161/255, green: 55/255, blue: 55/255, alpha: 1.0)
            }
        }
        else if let strValue = (walletDict["action_flag"] as? String){
            if strValue == "0" { //0 for credit, 1 for debit
                cell.label_Action.textColor = UIColor.init(red: 52/255, green: 196/255, blue: 124/255, alpha: 1.0)
            }
            else {
                cell.label_Action.textColor = UIColor.init(red: 161/255, green: 55/255, blue: 55/255, alpha: 1.0)
            }
        }  

        cell.layoutIfNeeded()
        cell.view_backgroundForAllDataHolder.setShadow(x: -5, y: -5, width: tableView.bounds.width-10, height: cell.view_backgroundForAllDataHolder.bounds.height + 10, radius: 5)
        cell.view_backgroundForAllDataHolder.cornerRadius = 5
        return cell
    }
    
    
}

