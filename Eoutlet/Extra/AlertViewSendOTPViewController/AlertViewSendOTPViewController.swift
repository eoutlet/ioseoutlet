//
//  AlertViewSendOTPViewController.swift
//  Eoutlet
//
//  Created by admin on 09/09/20.
//  Copyright © 2020 Unyscape. All rights reserved.
//

import UIKit
protocol sendOTPCloseTransparent {
    func closeView(idStr : String)
}
class AlertViewSendOTPViewController: UIViewController {
    @IBOutlet weak var alertView: UIView!
    @IBOutlet weak var sendOtpButton: UIButton!
    var delegate : sendOTPCloseTransparent?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let dimAlphaRedColor =  UIColor.black.withAlphaComponent(0.5)
        self.view.backgroundColor = dimAlphaRedColor
        view.isOpaque = true
        
        alertView.layer.cornerRadius = 10.0
        alertView.layer.borderColor = UIColor.gray.cgColor
        alertView.layer.borderWidth = 0.5
        alertView.clipsToBounds = true
        
       // sendOtpButton.setShadow(x: -5, y: -5, width: self.sendOtpButton.bounds.size.width - 290, height: sendOtpButton.bounds.size.height + 10, radius: 5)
    }
    
    @IBAction func sendOtpBtnClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
        delegate?.closeView(idStr: "")
    }
}

