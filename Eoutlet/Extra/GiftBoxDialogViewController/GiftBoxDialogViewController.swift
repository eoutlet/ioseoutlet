//
//  PopupOptionsViewController.swift
//  SwiftProject
//
//  Created by ChawtechSolutions on 22/12/18.
//  Copyright © 2018 ChawtechSolutions. All rights reserved.
//

import UIKit

protocol closeTransparent {
    func closeView(idStr : String)  
}

class GiftBoxDialogViewController: UIViewController {
    @IBOutlet weak var alertView: UIView!
    var delegate : closeTransparent?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let dimAlphaRedColor =  UIColor.black.withAlphaComponent(0.5)
        self.view.backgroundColor = dimAlphaRedColor
        view.isOpaque = true
        
        alertView.layer.cornerRadius = 10.0
        alertView.layer.borderColor = UIColor.gray.cgColor
        alertView.layer.borderWidth = 0.5
        alertView.clipsToBounds = true
    }
    
    @IBAction func cancelBtnClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
        delegate?.closeView(idStr: "")  
    }
    
    @IBAction func closeBtnClicked(_ sender: Any) {
           self.dismiss(animated: true, completion: nil)
           delegate?.closeView(idStr: "")
       }
}

