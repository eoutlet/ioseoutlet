//
//  CustomTabBar.swift
//  Eoutlet
//
//  Created by Sudhir Rohilla on 23/07/19.
//  Copyright © 2019 Unyscape. All rights reserved.
//

import UIKit

class CustomTabBar: UITabBar {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
     */
  override var traitCollection: UITraitCollection {
        return UITraitCollection(horizontalSizeClass: .compact)
    }


}
