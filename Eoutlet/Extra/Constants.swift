//
//  Constants.swift
//  esyDigi
//
//  Created by Sudhir Rohilla on 10/06/19.
//  Copyright © 2019 Unyscape. All rights reserved.

import Alamofire
import IQKeyboardManagerSwift
import MBProgressHUD
import Reachability
import UIKit
import Frames

class Constants: NSObject {
    
    
    static var isComingFromCartScreen = false
    static let msg_Alert = "تنبيه" //Attention
    static let default_ThemeColor : UIColor = UIColor(red: 216/255, green: 166/255, blue: 100/255, alpha: 1.0)//Dark yellow
    static let default_TextColor : UIColor = UIColor(red: 166/255, green: 55/255, blue: 53/255, alpha: 1.0)//Dark maroon color....
    
    //Custom Fonts
    static let helvetica_bold_font = "HelveticaNeueLTArabic-Bold"
    static let helvetica_regular_font = "HelveticaNeueLTArabic-Roman"
    static let helvetica_light_font = "HelveticaNeueLTArabic-Light"
    static let montserrat_regular_font = "Montserrat-Regular"
    static let montserrat_medium_font = "Montserrat-Medium"
    static let montserrat_semibold_font = "Montserrat-SemiBold"
    static let montserrat_bold_font = "Montserrat-Bold"

    
    //**************************Start Staging Environment*****************************//
//    static let apiURL = "https://staging.eoutlet.com/V10/"
//    static let mediaSourceURL = "https://staging.eoutlet.com/"
//    static let KCheckOutApiKey = "pk_test_33a80189-931c-4a3c-bf96-316b53cb636c"
//    static let KCheckOutEnviroment = Environment.sandbox
//    static let KApplePayMerchantIdentifier = "merchant.com.eoutlet.EoutletCheckoutStaging"
   //**************************End Staging Environment*****************************//
    
    
    //**************************Start Production Environment*************************//
    static let apiURL = "https://www.eoutlet.com/V10/"
    static let mediaSourceURL = "https://www.eoutlet.com/"
    static let KCheckOutApiKey = "pk_a2433084-728d-4eff-acff-6bdfd9988022"
    static let KCheckOutEnviroment = Environment.live
    static let KApplePayMerchantIdentifier = "merchant.com.eoutlet.EoutletCheckout"
   //**************************End Production Environment*************************//
    
}
