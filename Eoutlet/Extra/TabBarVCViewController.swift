//
//  TabBarVCViewController.swift
//  Eoutlet
//
//  Created by Sudhir Rohilla on 23/07/19.
//  Copyright © 2019 Unyscape. All rights reserved.

import UIKit

class TabBarVCViewController: UITabBarController {
 
    override func viewDidLayoutSubviews() {
       super.viewDidLayoutSubviews()
       let tabWidth = (tabBar.frame.width/CGFloat(tabBar.items!.count))
       let tabHeight = tabBar.frame.height
        self.tabBar.selectionIndicatorImage = UIImage().makeImageWithColorAndSize(color: UIColor(red: 216/255, green: 166/255, blue: 100/255, alpha: 1.0), size: CGSize(width: tabWidth, height: tabHeight)).resizableImage(withCapInsets: UIEdgeInsets.zero)  
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        // UITabBar.appearance().backgroundColor = UIColor(red: 237/255, green: 38/255, blue: 43/255, alpha: 1.0)
        //UITabBar.appearance().tintColor = UIColor(red: 237/255, green: 38/255, blue: 43/255, alpha: 1.0)
        //self.tabBar.app
        // Do any additional setup after loading the view.
        //self.tabBar.tintColor = UIColor.white
        //self.tabBar.barTintColor = UIColor(red: 237/255, green: 38/255, blue: 43/255, alpha: 1.0)
        //self.tabBar.unselectedItemTintColor = UIColor(red: 103/255, green: 7/255, blue: 48/255, alpha: 1.0)
        
        self.tabBar.tintColor = UIColor.white

        self.tabBar.barTintColor = UIColor.white
        self.tabBar.unselectedItemTintColor =  UIColor.init(red: 119/255, green: 131/255, blue: 143/255, alpha: 1.0)
        
        self.tabBar.isTranslucent = false
        
        self.selectedIndex = 4
        if let tabItems = self.tabBar.items {
            
            //In this case we want to modify the badge number of the first tab:
            let tabItem = tabItems[0]
          tabItem.badgeColor = Constants.default_ThemeColor
            let fontAttributes =  [NSAttributedString.Key.foregroundColor: UIColor.white,]
            tabItem.setBadgeTextAttributes(fontAttributes, for: .normal)
            
        }
        
    }
   
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
      
        if item.tag == 0 {
            item.badgeColor = UIColor.white
            let fontAttributes =  [NSAttributedString.Key.foregroundColor: UIColor.darkGray,]
            item.setBadgeTextAttributes(fontAttributes, for: .normal)
        }else {
            if let tabItems = self.tabBar.items {
                //In this case we want to modify the badge number of the first tab:
                let tabItem = tabItems[0]
              tabItem.badgeColor = Constants.default_ThemeColor
                let fontAttributes =  [NSAttributedString.Key.foregroundColor: UIColor.white,]
                tabItem.setBadgeTextAttributes(fontAttributes, for: .normal)
            }
        }
        
        let vc = self.viewControllers![item.tag] as! UINavigationController
        vc.popToRootViewController(animated: false)
    }
}
