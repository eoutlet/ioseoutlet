//
//  TrackOrderHeaderUIView.swift
//  Eoutlet
//
//  Created by Unyscape Infocom on 06/02/20.
//  Copyright © 2020 Unyscape. All rights reserved.
//

import UIKit

class TrackOrderHeaderUIView: UITableViewHeaderFooterView {
    @IBOutlet weak var orderNumberLbl: UILabel!
    @IBOutlet weak var amountLbl: UILabel!

    // Only override draw() if you perform custom drawing.
       // An empty implementation adversely affects performance during animation.
       override func draw(_ rect: CGRect) {
//           self.backgroundColor = UIColor.white
       }
}
