//
//  MyOrderHeaderUIView.swift
//  Eoutlet
//
//  Created by Unyscape Infocom on 28/01/20.
//  Copyright © 2020 Unyscape. All rights reserved.
//

import UIKit

class MyOrderHeaderUIView: UITableViewHeaderFooterView {
    @IBOutlet weak var orderNumberLbl: UILabel!
    @IBOutlet weak var totalAmountLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var statusLbl: UILabel!
    @IBOutlet weak var reorderBtn: UIButton!
    @IBOutlet weak var cancelBtn: UIButton!

    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
//        self.backgroundColor = UIColor.white  
    }
}
