//
//  loggedInUser.swift
//  esyDigi
//
//  Created by Sudhir Rohilla on 12/06/19.
//  Copyright © 2019 Unyscape. All rights reserved.
//

import UIKit
import FirebaseAnalytics

class analyticsFirebase {
    
    static let shared = analyticsFirebase()
        
    private init(){
        
    }
    
    func AnalyticsEventAddPaymentInfo(paramas: NSDictionary){
        Analytics.logEvent("add_payment_info", parameters: [
            AnalyticsParameterCurrency: paramas["currency"] as? String ?? "",
            AnalyticsParameterItemID: paramas["sku"] as? String ?? "",
            AnalyticsParameterItemName: paramas["itemname"] as? String ?? "",
            AnalyticsParameterPrice: paramas["price"] as? String ?? "",
            AnalyticsParameterQuantity: paramas["quantity"] as? String ?? "",
            AnalyticsParameterValue: paramas["product_id"] as? String ?? ""
        ])
    }
    
    func AnalyticsEventAddToCart(paramas: NSDictionary){
        Analytics.logEvent("add_to_cart", parameters: [
            AnalyticsParameterQuantity: paramas["quantity"] as? String ?? "",
            AnalyticsParameterItemCategory: paramas["category_name"] as? String ?? "",
            AnalyticsParameterItemName: paramas["itemname"] as? String ?? "",
            AnalyticsParameterItemID: paramas["sku"] as? String ?? "",
            AnalyticsParameterItemLocationID: paramas["location"] as? String ?? "",
            AnalyticsParameterValue: paramas["product_id"] as? String ?? "",
            AnalyticsParameterPrice: paramas["price"] as? String ?? "",
        ])
    }
    
    func AnalyticsEventAddToWishlist(paramas: NSDictionary){
        Analytics.logEvent("add_to_wishlist", parameters: [:])
    }
    
    func AnalyticsEventBeginCheckout(paramas: NSDictionary){
        Analytics.logEvent("begin_checkout", parameters: [
            AnalyticsParameterCurrency: paramas["currency"] as? String ?? "",
            AnalyticsParameterValue: paramas["value"] as? String ?? "",
            AnalyticsParameterCoupon: paramas["coupon"] as? String ?? "",
        ])
    }
    
    func AnalyticsEventEcommercePurchase(paramas: NSDictionary){
        Analytics.logEvent("ecommerce_purchase", parameters: [
             AnalyticsParameterCoupon: paramas["coupon"] as? String ?? "",
            AnalyticsParameterCurrency: paramas["currency"] as? String ?? "",
            AnalyticsParameterValue: paramas["value"] as? Int ?? "",
            AnalyticsParameterTax: paramas["tax"] as? String ?? "",
            AnalyticsParameterShipping: paramas["shipping"] as? String ?? "",
            AnalyticsParameterTransactionID: paramas["transactionid"] as? String ?? "",
        ])
    }
    
    func AnalyticsEventPurchaseRefund(paramas: NSDictionary){
        Analytics.logEvent("purchase_refund", parameters: [:])
    }
    
    func AnalyticsEventViewItem(paramas: NSDictionary){
        Analytics.logEvent("view_item", parameters: [
            AnalyticsParameterItemID: paramas["sku"] as? String ?? "",
            AnalyticsParameterItemLocationID: paramas["location"] as? String ?? "",
        ])
    }
    
    func AnalyticsEventViewItemList(paramas: NSDictionary){
        Analytics.logEvent("view_item_list", parameters: [
        AnalyticsParameterItemCategory: paramas["category_name"] as? String ?? "",
        ])
    }
    
    func AnalyticsEventViewSearchResults(paramas: NSDictionary){
        Analytics.logEvent("view_search_results", parameters: [
        AnalyticsParameterSearchTerm: paramas["search_item"] as? String ?? "",
        ])
    }
    
//    func AnalyticsEventCampFirstOpen(paramas: NSDictionary){
//          Analytics.logEvent("view_search_results", parameters: [
//              AnalyticsParameterSource: "Gmail",
//              AnalyticsParameterSearchTerm: "abc",
//          ])
//      }
}
