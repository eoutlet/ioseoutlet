//
//  analyticsAppsFlyer.swift
//  Eoutlet
//
//  Created by Sudhir Rohilla on 11/12/19.
//  Copyright © 2019 Unyscape. All rights reserved.
//

import UIKit
import AppsFlyerLib

class analyticsAppsFlyer {
    
    static let shared = analyticsAppsFlyer()
        
    private init(){
       
    }
    
    func AnalyticsEventUpdate(paramas: NSDictionary){
           AppsFlyerTracker.shared().trackEvent(AFEventUpdate, withValues: [
               AFEventParamOldVersion: paramas["old_version"] as? String ?? "",
               AFEventParamNewVersion: paramas["new_version"] as? String ?? "",
           ]);
       }
    
    func AnalyticsEventLogin(paramas: NSDictionary){
           AppsFlyerTracker.shared().trackEvent(AFEventLogin, withValues: [
               AFEventParamCustomerUserId: paramas["user_id"] as? String ?? "",
           ]);
       }
    
    func AnalyticsEventSearch(paramas: NSDictionary){
           AppsFlyerTracker.shared().trackEvent(AFEventSearch, withValues: [
               AFEventParamCustomerUserId: paramas["user_id"] as? String ?? "",
           ]);
       }
    
    func AnalyticsEventAddToCart(paramas: NSDictionary){
           AppsFlyerTracker.shared().trackEvent(AFEventAddToCart, withValues: [
               AFEventParamCurrency: paramas["currency"] as? String ?? "",
               AFEventParamPrice: paramas["price"] as? String ?? "",
               AFEventParamQuantity: paramas["quantity"] as? String ?? "",
               AFEventParamContentType: paramas["type"] as! String,
               AFEventParamContentId: paramas["id"] as! String,
               AFEventParamDescription: "",
           ]);
       }
    
    func AnalyticsEventAddPaymentInfo(paramas: NSDictionary){
        AppsFlyerTracker.shared().trackEvent(AFEventAddPaymentInfo, withValues: [
            AFEventParamCurrency : paramas["currency"] as? String ?? "",
            AFEventParamPrice : paramas["price"] as? String ?? "",
            AFEventParamPaymentInfoAvailable : "",
            AFEventParamCustomerUserId: paramas["user_id"] as? String ?? "",
        ]);
    }
        
    func AnalyticsEventInitiateCheckout(paramas: NSDictionary){
        AppsFlyerTracker.shared().trackEvent(AFEventInitiatedCheckout, withValues: [
            AFEventParamCurrency : paramas["currency"] as? String ?? "",
            AFEventParamPrice : paramas["price"] as? String ?? "",
            AFEventParamQuantity : "",
            AFEventParamPaymentInfoAvailable : "",
            AFEventParamCustomerUserId: paramas["user_id"] as? String ?? "",
            AFEventParamCouponCode: paramas["coupon"] as? String ?? "",
            AFEventParamContentId : "",
        ]);
    }
    
    func AnalyticsEventPurchase(paramas: NSDictionary){
            AppsFlyerTracker.shared().trackEvent(AFEventPurchase, withValues: [
            AFEventParamCurrency: paramas["currency"] as? String ?? "",
            AFEventParamPrice: paramas["price"] as? String ?? "",
            AFEventParamSuccess: paramas["order_status"] as? String ?? "",
            AFEventParamQuantity: "",
            AFEventParamPaymentInfoAvailable: "",
            AFEventParamCustomerUserId: paramas["user_id"] as? String ?? "",
            AFEventParamOrderId: paramas["order_id"] as? String ?? "",
            AFEventParamCouponCode: paramas["coupon"] as? String ?? "",
            AFEventParamContentId: "",
            AFEventParamRevenue: paramas["price"] as? String ?? "",
        ]);
    }
        
    func AnalyticsEventContentView(paramas: NSDictionary){
        AppsFlyerTracker.shared().trackEvent(AFEventContentView, withValues: [
            AFEventParamCustomerUserId: paramas["user_id"] as? String ?? "",
        ]);
    }
}
