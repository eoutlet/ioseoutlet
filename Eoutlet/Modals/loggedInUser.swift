//
//  loggedInUser.swift
//  esyDigi
//
//  Created by Sudhir Rohilla on 12/06/19.
//  Copyright © 2019 Unyscape. All rights reserved.
//

import UIKit

class loggedInUser {
    
    static let shared = loggedInUser()
    
    var string_userID : String
    var string_firstName : String
    var string_lastName : String
    var string_Email : String
    var string_country : String
    var string_mobileNumber : String

    private init(){
        self.string_userID = ""
        self.string_firstName = ""
        self.string_lastName = ""
        self.string_Email = ""
        self.string_country = ""
        self.string_mobileNumber = ""
    }
    
    func initializeUserDetails(dicuserdetail : Dictionary<String, Any>) {
        
        if let intValue = (dicuserdetail["id"] as? Int) {
            self.string_userID = String(format: "%d", intValue)
        }
        else if let strValue = (dicuserdetail["id"] as? String){
            self.string_userID = strValue
        }
        self.string_firstName = dicuserdetail["fname"] as! String
        self.string_lastName = dicuserdetail["lname"] as! String
        self.string_Email = dicuserdetail["email"] as! String
        if let intValue = (dicuserdetail["mob"] as? Int) {
            self.string_mobileNumber = String(format: "%d", intValue)
        }
        else if let strValue = (dicuserdetail["mob"] as? String){
            self.string_mobileNumber = strValue
        }
        
        if let intValue = (dicuserdetail["country_code"] as? Int) {
            self.string_country = String(format: "%d", intValue)
        }
        else if let strValue = (dicuserdetail["country_code"] as? String){
            self.string_country = strValue
        }
    }  
    
    func deinitializeAllUserDetails() {
        self.string_userID = ""
        self.string_firstName = ""
        self.string_lastName = ""
        self.string_Email = ""
        self.string_country = ""
        self.string_mobileNumber = ""
    }
    
    
}
